function cargar_menu_laterl(){
	console.log("CARGANDO EL MENU LATERAL");
	
	//	Backoffice 
	menu_alta("li_insert_usersBackoffice", "ajax/usuarios/insert_usersBackoffice.html");
	menu_modificar("li_update_usersBackoffice" , "ajax/usuarios/update_usersBackoffice.html");
	menu_buscar("li_search_usersBackoffice" , "ajax/usuarios/search_usersBackoffice.html");
	menu_consultar("li_query_usersBackoffice" , "ajax/usuarios/query_usersBackoffice.html");
	menu_baja("li_delete_usersBackoffice", "ajax/usuarios/delete_usersBackoffice.html");
	
	// Camaras
	menu_alta("li_insert_camara", "ajax/camara/insert_camara.html");
	menu_modificar("li_update_camara" , "ajax/camara/update_camara.html");
	menu_buscar("li_query_camara" , "ajax/camara/query_camara.html");
	menu_consultar("li_search_camara" , "ajax/camara/search_camara.html");
	menu_baja("li_delete_camara", "ajax/camara/delete_camara.html");
	
	// Empresa
	menu_alta("li_insert_cliente", "ajax/empresa/insert_cliente.html");
	menu_modificar("li_update_cliente" , "ajax/empresa/update_cliente.html");
	menu_buscar("li_query_cliente" , "ajax/empresa/query_cliente.html");
	menu_consultar("li_search_cliente" , "ajax/empresa/search_cliente.html");
	menu_baja("li_delete_cliente" , "ajax/empresa/delete_cliente.html");
	
	//cliente_cta
	menu_modificar("li_update_clienteCta" , "ajax/empresa/update_cliente_cta.html");
	menu_buscar("li_query_clienteCta" , "ajax/empresa/query_cliente_cta.html");
	
	// Producto
	menu_alta("li_insert_producto", "ajax/producto/insert_producto.html");
	menu_modificar("li_update_producto" , "ajax/producto/update_producto.html");
	menu_buscar("li_query_producto" , "ajax/producto/query_producto.html");
	menu_consultar("li_search_producto" , "ajax/producto/search_producto.html");	
	menu_otros("li_asignar_producto", "ajax/producto/asignar_producto.html", "fa-code-fork", "Asignar al cliente");
	menu_baja("li_delete_producto" , "ajax/producto/delete_producto.html");
	
	// Router
	menu_alta("li_insert_router", "ajax/router/insert_router.html");
	menu_modificar("li_update_router" , "ajax/router/update_router.html");
	menu_buscar("li_query_router" , "ajax/router/query_router.html");
	menu_consultar("li_search_router" , "ajax/router/search_router.html");
	menu_baja("li_delete_router" , "ajax/router/delete_router.html");
	
	// Sim
	menu_alta("li_insert_sim", "ajax/sim/insert_sim.html");
	menu_modificar("li_update_sim" , "ajax/sim/update_sim.html");
	menu_buscar("li_query_sim" , "ajax/sim/query_sim.html");
	menu_consultar("li_search_sim" , "ajax/sim/search_sim.html");
	menu_baja("li_delete_sim" , "ajax/sim/delete_sim.html");
	
	// dispositivo Emisor	
	menu_alta("li_insert_dispositivoEmisor", "ajax/dispositivoEmisor/insert_dispositivo_emisor.html");
	menu_modificar("li_update_dispositivoEmisor" , "ajax/dispositivoEmisor/update_dispositivo_emisor.html");
	menu_buscar("li_query_dispositivoEmisor" , "ajax/dispositivoEmisor/query_dispositivo_emisor.html");
	menu_consultar("li_search_dispositivoEmisor" , "ajax/dispositivoEmisor/search_dispositivo_emisor.html");
	menu_baja("li_delete_dispositivoEmisor" , "ajax/dispositivoEmisor/delete_dispositivo_emisor.html");
	
	// Log acciones
	menu_buscar("li_search_log_acciones" , "ajax/logAcciones/search_log_acciones.html");
	
	// Log backoffices
	menu_buscar("li_search_log_backoffices" , "ajax/logBackoffices/search_log_backoffices.html");
	
	// Tipo Eventos log
	menu_alta("li_insert_tipo_evento_log", "ajax/tipoEventoLog/insert_tipo_evento_log.html");
	menu_modificar("li_update_tipo_evento_log" , "ajax/tipoEventoLog/update_tipo_evento_log.html");
	menu_buscar("li_query_tipo_evento_log" , "ajax/tipoEventoLog/query_tipo_evento_log.html");
	menu_consultar("li_search_tipo_evento_log" , "ajax/tipoEventoLog/search_tipo_evento_log.html");
	menu_baja("li_delete_tipo_evento_log" , "ajax/tipoEventoLog/delete_tipo_evento_log.html");
	
	
	// Impuestos
	menu_alta("li_insert_impuestos", "ajax/impuestos/insert_impuestos.html");
	menu_modificar("li_update_impuestos" , "ajax/impuestos/update_impuestos.html");
	menu_buscar("li_query_impuestos" , "ajax/impuestos/query_impuestos.html");
	menu_consultar("li_search_impuestos" , "ajax/impuestos/search_impuestos.html");
	menu_baja("li_delete_impuestos" , "ajax/impuestos/delete_impuestos.html");
	
	// Paises
	menu_alta("li_insert_pais", "ajax/pais/insert_pais.html");
	menu_modificar("li_update_pais" , "ajax/pais/update_pais.html");
	menu_buscar("li_query_pais" , "ajax/pais/query_pais.html");
	menu_consultar("li_search_pais" , "ajax/pais/search_pais.html");
	menu_baja("li_delete_pais" , "ajax/pais/delete_pais.html");
	
	// Provincia
	menu_alta("li_insert_provincia", "ajax/provincia/insert_provincia.html");
	menu_modificar("li_update_provincia" , "ajax/provincia/update_provincia.html");
	menu_buscar("li_query_provincia" , "ajax/provincia/query_provincia.html");
	menu_consultar("li_search_provincia" , "ajax/provincia/search_provincia.html");
	menu_baja("li_delete_provincia" , "ajax/provincia/delete_provincia.html");
	
	// Rol usuario
	menu_alta("li_insert_rol_user", "ajax/rolUser/insert_rol_user.html");
	menu_modificar("li_update_rol_user" , "ajax/rolUser/update_rol_user.html");
	menu_buscar("li_query_rol_user" , "ajax/rolUser/query_rol_user.html");
	menu_consultar("li_search_rol_user" , "ajax/rolUser/search_rol_user.html");
	menu_baja("li_delete_rol_user" , "ajax/rolUser/delete_rol_user.html");
	
	// Tipo Aviso
	menu_alta("li_insert_tipo_aviso", "ajax/tipoAviso/insert_tipo_aviso.html");
	menu_modificar("li_update_tipo_aviso" , "ajax/tipoAviso/update_tipo_aviso.html");
	menu_buscar("li_query_tipo_aviso" , "ajax/tipoAviso/query_tipo_aviso.html");
	menu_consultar("li_search_tipo_aviso" , "ajax/tipoAviso/search_tipo_aviso.html");
	menu_baja("li_delete_tipo_aviso" , "ajax/tipoAviso/delete_tipo_aviso.html");
	
	// Tipo Producto
	menu_alta("li_insert_tipo_producto", "ajax/tipoProducto/insert_tipo_producto.html");
	menu_modificar("li_update_tipo_producto" , "ajax/tipoProducto/update_tipo_producto.html");
	menu_buscar("li_query_tipo_producto" , "ajax/tipoProducto/query_tipo_producto.html");
	menu_consultar("li_search_tipo_producto" , "ajax/tipoProducto/search_tipo_producto.html");
	menu_baja("li_delete_tipo_producto" , "ajax/tipoProducto/delete_tipo_producto.html");
	
	// Tipo Router
	menu_alta("li_insert_tipo_router", "ajax/tipoRouter/insert_tipo_router.html");
	menu_modificar("li_update_tipo_router" , "ajax/tipoRouter/update_tipo_router.html");
	menu_buscar("li_query_tipo_router" , "ajax/tipoRouter/query_tipo_router.html");
	menu_consultar("li_search_tipo_router" , "ajax/tipoRouter/search_tipo_router.html");
	menu_baja("li_delete_tipo_router" , "ajax/tipoRouter/delete_tipo_router.html");

	// Tipo Camara
	menu_alta("li_insert_tipo_camara", "ajax/tipoCamara/insert_tipo_camara.html");
	menu_modificar("li_update_tipo_camara" , "ajax/tipoCamara/update_tipo_camara.html");
	menu_buscar("li_query_tipo_camara" , "ajax/tipoCamara/query_tipo_camara.html");
	menu_consultar("li_search_tipo_camara" , "ajax/tipoCamara/search_tipo_camara.html");
	menu_baja("li_delete_tipo_camara" , "ajax/tipoCamara/delete_tipo_camara.html");
	
	
	

	// Sports
	menu_alta("li_insert_userSport", "ajax/sport/insert_userSport.html");
	menu_otros("li_insert_logosCliente_sport", "ajax/sport/insert_logosCliente.html", "fa-picture-o", "Logos")
	menu_otros("li_update_redesSociales_sport", "ajax/sport/update_redesSociales.html", "fa-asterisk", "Redes Sociales ")
	
}



function menu_alta(nameDiv, rutaHref){
	var texto = '';
	texto += '<a class="ajax-link" href="'+rutaHref+'"> ';
		texto += '<i class="fa fa-pencil-square"></i> Altas ';
	texto += '</a>';
	$("#"+nameDiv).html(texto); 				
				
}

function menu_baja(nameDiv, rutaHref){
	var texto = '';
	texto += '<a class="ajax-link" href="'+rutaHref+'">';
		texto += '<i class="fa fa-trash-o"></i> Eliminar (Próx.)';
	texto += '</a>';
	$("#"+nameDiv).html(texto); 
}

	
function menu_modificar(nameDiv, rutaHref){
	var texto = '';
	texto += '<a class="ajax-link" href="'+rutaHref+'">';
		texto += '<i class="fa fa-pencil-square-o"></i> Modificar datos ';
	texto += '</a>';
	$("#"+nameDiv).html(texto); 			
}


function menu_buscar(nameDiv, rutaHref){
	var texto = '';
	texto += '<a class="ajax-link" href="'+rutaHref+'"> ';
		texto += '<i class="fa fa-search"></i> Buscar   ';
	texto += '</a>';
	$("#"+nameDiv).html(texto); 			 
}


function menu_consultar(nameDiv, rutaHref){
	var texto = '';
	texto += '<a class="ajax-link" href="'+rutaHref+'"> ';
	texto += '<i class="fa fa-info-circle"></i>  Consultar';
	texto += '</a>';
	$("#"+nameDiv).html(texto); 				
}

				
function menu_otros(nameDiv, rutaHref, clase_i, etiqueta){
	var texto = '';
	texto += '<a class="ajax-link" href="'+rutaHref+'"> ';
	texto += '<i class="fa '+clase_i+'"></i> '+etiqueta;
	texto += '</a>';
	$("#"+nameDiv).html(texto); 				
}
