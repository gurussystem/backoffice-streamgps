/**
* @descipcion Limpiar cadena caracteres extraños , tildes y ñ . Si hay un espacio lo cambiamos por un guion bajo.
* @param String cadena, cadena de caracteres a limpiar
* @return cadena limpia
*/
function getCleanedString(cadena){
	// Definimos los caracteres que queremos eliminar
	var specialChars = "!@#$^&%*()+=-[]\/{}|:<>?,'.€?¿!¡`´¨·";

	// Eliminamos todos los caracteres extraños y las dobles comillas.
	for (var i = 0; i < specialChars.length; i++) {				
		cadena= cadena.replace(new RegExp("\\" + '"', 'gi'), '');	// las dobles comillas las pongo a parte xq en el array no lo coge.
		cadena= cadena.replace(new RegExp("\\" + specialChars[i], 'gi'), '');
	}   
				
	// Lo queremos devolver limpio en minusculas
	cadena = cadena.toLowerCase();

	// Quitamos espacios y los sustituimos por un guion bajo _ 
	cadena = cadena.replace(/ /g,"_");

	// Eliminamos las 'a' con tildes y caracteres extraños.
	var specialChars_a = "ÃÀÁÄÂÅãàáäâãå";
	for (var i = 0; i < specialChars_a.length; i++) {				
		cadena= cadena.replace(new RegExp("\\" + specialChars_a[i], 'gi'), 'a');
	}
	
	// Eliminamos las 'ae' con tildes y caracteres extraños.				
	var specialChars_ae = "Ææ";
	for (var i = 0; i < specialChars_ae.length; i++) {				
		cadena= cadena.replace(new RegExp("\\" + specialChars_ae[i], 'gi'), 'ae');
	}  
	
	// Eliminamos las 'e' con tildes y caracteres extraños.		
	var specialChars_e = "ÈÉËÊèéëê";
	for (var i = 0; i < specialChars_e.length; i++) {				
		cadena= cadena.replace(new RegExp("\\" + specialChars_e[i], 'gi'), 'e');
	}  
	
	// Eliminamos las 'i' con tildes y caracteres extraños.	
	var specialChars_i = "ÌÍÏÎìíïî";
	for (var i = 0; i < specialChars_i.length; i++) {				
		cadena= cadena.replace(new RegExp("\\" + specialChars_i[i], 'gi'), 'i');
	}  
	
	// Eliminamos las 'o' con tildes y caracteres extraños.	
	var specialChars_o = "ÒÓÖÔÕØòóöôõø";
	for (var i = 0; i < specialChars_o.length; i++) {				
		cadena= cadena.replace(new RegExp("\\" + specialChars_o[i], 'gi'), 'o');
	}  
	
	// Eliminamos las 'u' con tildes y caracteres extraños.		
	var specialChars_u = "ÙÚÜÛùúüû";
	for (var i = 0; i < specialChars_u.length; i++) {				
		cadena= cadena.replace(new RegExp("\\" + specialChars_u[i], 'gi'), 'u');
	}  
	
	// Eliminamos las 'ñ'.		
	var specialChars_n = "Ññ";
	for (var i = 0; i < specialChars_n.length; i++) {				
		cadena= cadena.replace(new RegExp("\\" + specialChars_n[i], 'gi'), 'n');
	}  	
			
	// Eliminamos las 'Y' con tildes y caracteres extraños.				
	var specialChars_y = "ÝýÿŸ";
	for (var i = 0; i < specialChars_y.length; i++) {				
		cadena= cadena.replace(new RegExp("\\" + specialChars_y[i], 'gi'), 'y');
	}  		
		
	// Eliminamos las 'c' con tildes y caracteres extraños.			
	var specialChars_c = "Çç";
	for (var i = 0; i < specialChars_c.length; i++) {				
		cadena= cadena.replace(new RegExp("\\" + specialChars_c[i], 'gi'), 'c');
	}  
				
	return cadena;
}



/**
* @descipcion Función que solo permite la entrada de numeros, un signo negativo y un punto para separar los decimales
* @param String e, caracter introducido en el input
* @return el caracter introducido si es numerico.
*/
function soloNumeros(e){
		
	// capturamos la tecla pulsada
	var teclaPulsada=window.event ? window.event.keyCode:e.which;

	// capturamos el contenido del input
	var valor=document.getElementById("telefono").value;

	if(valor.length<10) {
					
		// Si el usuario pulsa la tecla enter o el punto y no hay ningun otro punto
		if(teclaPulsada==13) {
			return true;
		}

		// devolvemos true o false dependiendo de si es numerico o no
		return /\d/.test(String.fromCharCode(teclaPulsada));

	}else{
		return false;
	}
	
} // fin funcion


/**
* @descipcion Calculamos el tiempo de desfase respecto donde estemos con el servidor que esta en UTC.
* @return tiempo de desfase.
*/
function desfaseHoraria (){
	var desfase_aux = new Date().getTimezoneOffset();
				
	// si es un numero negativo se suma, si es un numero positivo se resta
	if(desfase_aux < 0 ) {
		return desfase = Math.abs(desfase_aux);					  
	}else if(desfase_aux > 0 ){
		return desfase = -desfase_aux;		
	}else{
		return desfase = 0;		
	}
	
} // fin funcion


/**
* @descipcion Comprobamos que el formato del email es valido 
* @return si el email es incorrecto muestra un mensaje, sino devuelve el email.
*/
function validarEmail( email, nomDiv ) {
	expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if ( !expr.test(email) ){
		$("#"+nomDiv+"_error").css("display","block");
		$("#"+nomDiv+"_error").html("La dirección de correo " + email + " es incorrecta.");
	}else{
		return email;				
	}
	
} // fin funcion



