	/********************************************************************************************************
	* @nameFunction 	insert_inputText()
	* @description 	 	Insertamos un bloque de input tipo 'text'
	********************************************************************************************************/
	function insert_inputText(idForm, nombreCampo, etiqueta, txtPlaceholder, txtTitle, valor){
		// console.log("(insert_inputText) " + nombreCampo + " , " +  etiqueta + " , " +  txtPlaceholder + " , " +  txtTitle);
		var id_label = nombreCampo+"_lbl";
		var id_input = nombreCampo;
		var id_errorMsg = "errorMsg_"+nombreCampo;
		
		var html_input ='';
		html_input += '<div class="form-group">	';
		html_input += '<label class="col-sm-3 control-label" id="'+id_label+'">'+etiqueta+'</label>';
		html_input += '<div class="col-sm-5">';
		html_input += '<input type="text" id="'+id_input+'" class="form-control" >';
		html_input += '<p class="bg-danger" id="'+id_errorMsg+'"  style="display:none;">  </p>';
		html_input += '</div>';
		html_input += '</div>';
		 $('#'+idForm).append(html_input);
		
		// placeholder
		placeholder_textos(nombreCampo, txtPlaceholder, txtTitle, valor);	
	}

	/********************************************************************************************************
	* @nameFunction 	insert_inputRadio()
	* @description 	 	Insertamos un bloque de radio 
	********************************************************************************************************/
	function insert_inputRadio(idForm, nombreCampo, etiqueta, txtPlaceholder, txtTitle, arrayRadios){
	
		var id_label = nombreCampo+"_lbl";
		var id_errorMsg = "errorMsg_"+nombreCampo;

		console.log("Num objetos: " + arrayRadios.length);
		var html_input ='<div class="form-group">';
		html_input += '<div class="col-sm-12">';
		html_input += '<label class="col-sm-3 control-label" id="'+id_label+'">'+etiqueta+'</label>';
		if(arrayRadios.length != 0){
			for(var i=0; i < arrayRadios.length; i++){
				var id_input = nombreCampo+"_"+i;
				html_input += '<div class="radio-inline">';
				html_input += '<label>';
				html_input += '<input type="radio" name="radio-inline" id="'+id_input+'" class="form-control" value="'+arrayRadios[i].valor+'">';
				html_input += arrayRadios[i].texto;
				html_input += '<i class="fa fa-circle-o"></i>';
				html_input += '</label>';
				html_input += '</div>';
			}
		}	
		html_input += '</div>';
		html_input += '<p class="bg-danger" id="'+id_errorMsg+'"  style="display:none;">  </p>';
		html_input += '</div>';
		$('#'+idForm).append(html_input);
	}
	
	function push_arrayRadio(_texto, _valor, _array){
		var arrayNew = _array;
		arrayNew.push({
			texto:_texto,
			valor:_valor
	   });   
	}
	
	
	
	/********************************************************************************************************
	* @nameFunction 	insert_selectOption()
	* @description 	 	Insertamos un bloque de radio 
	********************************************************************************************************/
	function insert_selectOption_basic(idForm, nombreCampo, etiqueta, txtPlaceholder, txtTitle, array){
		
			var id_select = nombreCampo;
		var id_label = nombreCampo+"_lbl";
		var id_errorMsg = "errorMsg_"+nombreCampo;
		
		var html_input ='<div class="form-group">';
		html_input += '<label class="col-sm-3 control-label" id="'+id_label+'">'+etiqueta+'</label>';
		html_input += '<div class="col-sm-5">';
		html_input += '<select class="form-control" id="'+id_select+'">';
		html_input += '<option value="-1" selected="selected" name="defaultProvincia">'+txtPlaceholder+'</option>';
		if(array.length != 0){
			for(var i=0; i < array.length; i++){
				var id_input = nombreCampo+"_"+i;
				html_input +='<option value="'+array[i].valor+'">'+array[i].texto+'</option>';
			}
		}	
		html_input +='</select>';
		html_input += '</div>';
		html_input += '<p class="bg-danger" id="'+id_errorMsg+'" style="display:none;">   </p>	';
		html_input += '</div>';
		html_input += '</div>';
		$('#'+idForm).append(html_input);
	}
	
	
		
	function insert_inputEmail(idForm, nombreCampo, etiqueta, txtPlaceholder, txtTitle, valor){
		
		var id_label = nombreCampo+"_lbl";
		var id_input = nombreCampo;
		var id_errorMsg = "errorMsg_"+nombreCampo;
		
		var html_input ='';
		html_input += '<div class="form-group">	';
		html_input += '<label class="col-sm-3 control-label" id="'+id_label+'">'+etiqueta+'</label>';
		html_input += '<div class="col-sm-5">';
		html_input += '<input type="email" id="'+id_input+'" class="form-control">';
		html_input += '<p class="bg-danger" id="'+id_errorMsg+'"  style="display:none;">  </p>';
		html_input += '</div>';
		html_input += '</div>';
		$('#'+idForm).append(html_input);

		// placeholder
		placeholder_textos(nombreCampo, txtPlaceholder, txtTitle, valor);	
	}
	
	
	
	function insert_inputFile(idForm, nombreCampo, etiqueta, txtPlaceholder, txtTitle, valor){
		
		var id_label = nombreCampo+"_lbl";
		var id_input = nombreCampo;
		var name_input = nombreCampo+"_file";
		var id_errorMsg = "errorMsg_"+nombreCampo;
		
		var html_input ='';
		html_input += '<div class="form-group">	';
		html_input += '<label class="col-sm-3 control-label" id="'+id_label+'">'+etiqueta+'</label>';
		html_input += '<div class="col-sm-5">';
		html_input += '<input id="'+id_input+'" type="file" name="'+name_input+'" class="file" data-show-preview="false">';
		html_input += '<p class="bg-danger" id="'+id_errorMsg+'"  style="display:none;">  </p>';
		html_input += '</div>';
		html_input += '</div>';
		$('#'+idForm).append(html_input);
		
	}

	
	function insert_button(idForm, nombreCampo, etiqueta,  array ){
		
		var html_input ='';
		html_input += '<div class="form-group">';
		html_input += '<div class="col-sm-9 col-sm-offset-3">';
		html_input += '<button type="submit" id="submit" class="btn btn-primary" onclick="'+array[0].funcion+'"> '+array[0].texto+'</button>';
		html_input += '<button type="reset" id="reset" class="btn btn-primary" onclick="'+array[1].funcion+'"> '+array[1].texto+'</button>';
		html_input += '	<p id="successMsg"  style="display:none;">  </p>';
		html_input += '</div>';
		html_input += '</div>';
		
		$('#'+idForm).append(html_input);
	}
	
	function push_arrayButton(_funcion, _texto, _array){
		var arrayNew = _array;
		arrayNew.push({
				texto:_texto,
				funcion:_funcion
		}); 
	}

	
	/***********************************************************************************************************
		Textos placeholder y tooltip.
			- nombreVariable: es el nombre del identificador del input
			- txtVariable: Hace referencia al placeholder. Si 'txtVariable2' esta vacio servira para los dos casos.
			- txtVariable2: Hace referencia al data-original-title, que es el del tooltip. Es por si tiene que ser diferente
	***********************************************************************************************************/
	function placeholder_textos(nomVariable, txtVariable, txtVariable2, valor){
		
		if(txtVariable2 == ""){
			$('#'+nomVariable).attr("placeholder", txtVariable);
			$('#'+nomVariable).attr("data-original-title", txtVariable);	
			$('#'+nomVariable+"_lbl").attr("title", txtVariable);	
		}else{
			$('#'+nomVariable).attr("placeholder", txtVariable);
			$('#'+nomVariable).attr("data-original-title", txtVariable2);	
			$('#'+nomVariable+"_lbl").attr("title", txtVariable2);	
		}
		
		if(valor != ""){
			$('#'+nomVariable).attr("value", valor);
		}
		
		//Esto en lugar de tenerlo en cada input, lo ponemos directamente aqui
		$('#'+nomVariable).attr("data-toggle", 'tooltip');	 
		$('#'+nomVariable).attr("data-placement", 'bottom'); //posicion donde saldrá el mensaje
		
	}
	
	
