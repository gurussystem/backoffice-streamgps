/* scriptStreamgps 1.0.0-dev (5167 lineas)
 * �2015
 */

/** post
 * @summary     Backoffice Streamgps
 * @description Backoffice Streamgps
 * @version     1.0.0-dev
 * @file        scriptStreamgps.js
 * @author      Marisa Valero Garcia
 * @contact     
 * @copyright   Copyright 2015 Sstreamgps S.L
 *
 */
 
 
/**  (esto es ejemplo de como tiene que estar documentado)
 * TableTools provides flexible buttons and other tools for a DataTables enhanced table
 * @class TableTools
 * @constructor
 * @param {Object} oDT DataTables instance
 * @param {Object} oOpts TableTools options
 * @param {String} oOpts.sSwfPath ZeroClipboard SWF path
 * @param {String} oOpts.sRowSelect Row selection options - 'none', 'single' or 'multi'
 * @param {Function} oOpts.fnPreRowSelect Callback function just prior to row selection
 * @param {Function} oOpts.fnRowSelected Callback function just after row selection
 * @param {Function} oOpts.fnRowDeselected Callback function when row is deselected
 * @param {Array} oOpts.aButtons List of buttons to be used
 */
 
 
	// Definici�n Variables		
	var countUserBack;
	var countCamara;
	var countCta;
	var count; //empresa
	var countImpuestos;
	var countProducto;
	var countPais;
	var countProvincias;
	var countRolUser;
	var countRouter;
	var countSim;
	var countTipoAviso;
	var countTipoEventoLog;
	var countTipoProducto;
	var countTipoRouter;
	var countTipoCamara;
	var countDispositivoEmisor;
	
	var latEmpresa;
	var lngEmpresa;
	
	var url_pagina_web ="inicio.php";
	
	// Variables globales para los acentos 
	var min_a = "\u00e1";
	var min_e = "\u00e9";
	var min_i = "\u00ed";
	var min_o = "\u00f3";
	var min_u = "\u00fa";
	var min_n = "\u00f1";
	
	var may_a = "\u00c1";
	var may_e = "\u00c9";
	var may_i = "\u00cd";
	var may_o = "\u00d3";
	var may_u = "\u00da";
	var may_n = "\u00d1";

	// array que guarda los routers, camaras y cliente	
	var arrayUserBack = new Array();
	var arrayCamaras = new Array();
	var arrayCta = new Array();
	var arrayCliente = new Array();
	var arrayImpuestos = new Array();
	var arraylogAcciones = new Array();
	var arraylogBackoffices = new Array();
	var arrayProducto = new Array();
	var arrayProvincias = new Array();
	var arrayRolUser = new Array();
	var arrayRouters = new Array();
	var arraySim = new Array();	
	var arrayTipoAviso = new Array();
	var arrayTipoEventoLog = new Array();
	var arrayTipoProducto = new Array();
	var arrayTipoCamara = new Array();
	var arrayTipoRouter = new Array();
	var arrayDispositivoEmision = new Array();
	
	
	// Variables auxiliares para el cambio de idSim, idCamara, idRouter de un producto
	var idSimAux = "";
	var idSimNew = "";
	
	var idCamaraAux = "";
	var idCamaraNew = "";	
	
	var idRouterAux = "";
	var idRouterNew = "";
	
	var idDispositivoEmisorAux = "";
	var idDispositivoEmisorNew = "";	
	
	var idClienteSeleccionAux = "";
	var idClienteSeleccionNew = "";
	
	
	// Variables cliente y cta_cliente
	var titular_ctac;
	var iban_ctac;
	var entidad_ctac;
	var sucursal_ctac;
	var dc_ctac;
	var numCuenta_ctac;
	var swift_ctac;
	
	
	/**************************************************************************************
		A�adimos un input al HTML.
		En HTML a�adir un '<div id="nombre_contenedor"> </div>'
		En $(document).ready(function() {} a�adir: 	inputCode(nom, fichero, disabled) donde:
			- nom es el mismo nombre puesto en el div
			- fichero es una variable compuesta por el nombre del archivo html (ejemplo => insert_camaraHTML)
			- disabled : 0 es campo activo y  1 campo 'disabled'
	**************************************************************************************/
	function inputCode(nom, fichero, disabled){				
		var itxt = '<div class="form-group">';
			itxt += '<label id="'+nom+'_lbl" class="col-sm-3 control-label"></label>';
			itxt += '<div class="col-sm-5">';
				itxt += '<input  type="text" id="'+nom+'" class="form-control" >';
				itxt += '<p class="bg-danger" id="errorMsg_'+nom+'"  style="display:none;">     </p>';
			itxt += '</div>';
		itxt += '</div>';		
		$("#"+nom+"_contenedor").html(itxt);
		
		// Cuando hagamos click eliminaremos los mensajes de error, en caso que hayan
		$("#"+nom).click(function() {
			eliminarSMS(fichero, nom);
		});
		
		// Desactivamos el campo para que no se pueda escribir
		if(disabled == 1)
			$("#"+nom).attr("disabled", true); 
	}
	
	/**************************************************************************************
		A�adimos un select Option al HTML.
		En HTML a�adir un '<div id="nombre_contenedor"> </div>'
		En $(document).ready(function() {} a�adir: 	selectOptionCode(nom, fichero, opBtn) donde:
			- nom es el mismo nombre puesto en el div
			- fichero es una variable compuesta por el nombre del archivo html (ejemplo => insert_camaraHTML)
			- opBtn : A�adimos bot�n 'Reiniciar Formulario' donde lo borramos todo
	**************************************************************************************/
	function selectOptionCode(nom, fichero, opBtn){
		var itxt='';
		itxt += '<div class="form-group">';
		itxt += '<label id="'+nom+'_lbl" class="col-sm-3 control-label"></label>';
		itxt += '<div class="col-sm-5">';
		itxt += '<div id="cargandoDatos_'+nom+'" class="cargarDatos txtLeft">   </div> ';
		itxt += '<div id="selectOption_'+nom+'"> </div>';
		itxt += '<p class="bg-danger" id="errorMsg_'+nom+'" style="display:none;">   </p>';
		itxt += '</div>';
		if(opBtn==1)
		itxt += '<button id="reset" type="reset" class="btn btn-primary" onclick="borrar_datos_form();$(&#39;form&#39;).hide();"> </button>'; //&#39; comilla s
		itxt += '</div>';		
		$("#"+nom+"_contenedor").html(itxt);
		
		// Cuando hagamos click en el select-option borramos los mensajes que hayan 
		$("#selectOption_"+nom).click(function() {
			eliminarSMS(fichero, nom);
		});
	}		
	
	/*****************************************************************************************
		Autogeneraci�n del campo 'dyndns' del router. 
		S�lo se utiliza en insert/update del router.
	*****************************************************************************************/
	function agregaDyndns(){
		var valorSerial = $("#numSerialRouter").val();
		var serialDyndns = valorSerial.substr(-8);
		var newDyndns = "SG"+serialDyndns+".dyndns.tv";		
		$("#dyndnsRouter").val(newDyndns);
	}
	
	
	/***
	
	**/
	function validarEstado(estado, dondeViene){
		var op1, op0;
		
		switch(dondeViene){ // x si tengo que a�adir m�s casos
			case 'cliente':
				op0 = 'Desactivo';
				op1 = 'Activo';
				break;			
		} // fin switch
		
		if(estado == 1)			
			return op1;
		else
			return op0;
	}
	
	/***
	
	**/
	function validarPropietario(estado, dondeViene){
			var op1, op0;
		
		switch(dondeViene){ // x si tengo que a�adir mas casos
			case 'sim':
				op0 = 'Ocupada';
				op1 = 'Libre';
				break;	

			case 'producto':
				op0 = 'Sin asignar';
				op1 = 'Asignado';
				break;
				
		} // fin switch
		
		if(!estado)			
			return op1;
		else
			return op0;
	}
	
	/***********************************************************************************************************
		Instrucciones de uso: 
		1. HTML - Encima del contenedor de la informaci�n, pondremos: 	<div id="cargandoDatos_" class="cargarDatos cdCenter cdLeft">  </div> 	
		2. JS - Para iniciar el loader: cargarDatos(nameDiv, 'start')
		3. JS - Para finalizar el loader: cargarDatos(nameDiv, 'end')
	***********************************************************************************************************/ 	
	function cargandoDatos(nomDiv, estado, operacion){
		// console.log(" cargandoDatos(" + nomDiv + " ,  " + estado  + " ,  " + operacion + ") " );
		// Select-options
		if(operacion == 'seleccion'){ 
			var nombreDiv = "#cargandoDatos_"+nomDiv;	
			var imgLoader = '<img src="img/ajaxLoader/bar120.gif" class="ajaxLoader" alt="Cargando datos..."/> ';
			
		} else{	// guardar, modificar...
			var nombreDiv = "#guardandoDatos";	
			$(nombreDiv).show();
			
			if(operacion == 'insert') 
				var imgLoader = ' <img src="img/ajaxLoader/loaderB32.gif" class="ajaxLoader" alt="Cargando datos..."/> Guardando Datos...';
			else // update
				var imgLoader = ' <img src="img/ajaxLoader/loaderB32.gif" class="ajaxLoader" alt="Cargando datos..."/> Actualizando Datos...';
		}
		
		switch(estado){
			case 'start':
				$(nombreDiv).html(imgLoader);	
				break;
				
			case 'end':
				$(nombreDiv).hide();
				break;
		} // fin switch()	
	}
 
	
	/***********************************************************************************************************
		Cabecera de las cajas.
		opVerNotas: Cuando queramos a�adir notas lo pondremos a 1 y en el html el siguiente bloque: <div id="avisoLista"> </div>
	***********************************************************************************************************/
	function boxHeaderInfo(titulo, iconoClass, nomDiv, opVerNotas){
		var texto = '<div class="box-header">';
		texto += '<div class="box-name">';
		texto += '<i class="fa '+iconoClass+'"></i>';
		texto += '<span> '+ titulo + '</span>';
		texto += '</div>';
		texto += '<div class="box-icons pull-right">';
		
		if(opVerNotas == 1){
			texto += '<a class="btnVerAviso">';
				texto += '<i class="fa fa-eye"></i>';
			texto += '</a>';
		}		
		
		texto += '<a class="collapse-link">';
		texto += '<i class="fa fa-chevron-up"></i>';
		texto += '</a>';
		
		texto += '<a class="expand-link">';
		texto += '<i class="fa fa-expand"></i>';
		texto += '</a>';
		
		texto += '<a class="close-link">';
		texto += '<i class="fa fa-times"></i>';
		texto += '</a>';
		
		texto += '</div>';
		
		texto += '<div class="no-move"></div>';
		texto += '</div>';
		
		$("#"+nomDiv).html(texto);
	}

	
	/**********************************************************************************
		Calculamos el tiempo de desfase respecto donde estemos con el servidor que esta en UTC.
	**********************************************************************************/
	function desfaseHoraria (){
		var desfase_aux = new Date().getTimezoneOffset();
		
		// si es un numero negativo se suma, si es un numero positivo se resta
		if(desfase_aux < 0 ) {
			return desfase = Math.abs(desfase_aux);					  
		}else if(desfase_aux > 0 ){
			return desfase = -desfase_aux;		
		}else{
			return desfase = 0;		
		}
	}
	
	/************************************************************************************
	* Valida que los campos no esten vacios.
	************************************************************************************/
	function login_user_streamgps(){
		var user = $('#username').val();
		var pass = $('#password').val();
		console.log("user: " + user);
		console.log("pass: " + pass);
		
		// desactivamos los mensajes de error
		$("#errorMsg_username").hide();
		$("#errorMsg_password").hide();
		$("#errorMsg_validacion").hide();
			
		if(user ==''){
			$("#errorMsg_username").show();
			$("#errorMsg_username").html("Introduce tu usuario");
		}else if(pass == ''){ 
			$("#errorMsg_password").show();
			$("#errorMsg_password").html("Introduce tu clave");
		}else{ //Si todos los campos estan correctamente relleno, podemos entrar entramos.
			login(user,pass);
		}
	}
	
	/************************************************************************************
	* Comprueba que los datos introducidos esten en la BD.
	* En caso contrario mostramos un mensaje informativo.
	* @param {String} user, Nombre del usuario que quiere entrar en el backoffice
	* @param {String} pass, Clave del usuario que quiere entrar en el backoffice
	************************************************************************************/
	function login(user,pass){
		$.ajax({
			  type: 'GET',
			  url: './io/select_login.php',
			  data: { 
				'user': user,
				'pass': pass,
				'tiempoDesfase' : desfaseHoraria()
			  },
			  success: function(response) {
				var json = $.parseJSON(response);
								
				//Datos incorrectos, informamos con un mensje y limpiamos inputs
				if(json['success']==0){
					$("#errorMsg_validacion").show();
					$("#errorMsg_validacion").html("Lo sentimos, los datos introducidos son incorrectos"); 
					
					//Vacio los campos
					$('#username').val("");
					$('#password').val("");
				}
				//Datos correctos, redirijimos
				else{
					document.location= "inicio.php";					
				}
			}
		});
	}
	
	

	/*****************************************************************
		Funcion para esconder el mensaje general (de exito o de error)
	****************************************************************/
	function eliminarSMS(dondeViene, idInput){	
		// console.log("eliminarSMS("+ dondeViene + " , " + idInput + ")" );
		
		validar_div_visible('errorMsg_'+idInput , 'hide');
		
		// mensaje general
		switch(dondeViene){
			case 'indexHTML':
				validar_div_visible('errorMsg_validacion', 'hide');		
				break;
			
			default:
				validar_div_visible('successMsg' , 'hide');
				break;
			
			/* 
				aqui probar si funciona, estaba revisando todas las funciones y comentandolas
			case 'insert_usersBackofficeHTML': 
			case 'update_usersBackofficeHTML':  // No salen los mensajes cuando estan vacios y se validan
			case 'insert_clienteHTML': 
			case 'update_clienteHTML': 
			case 'update_cliente_ctaHTML':
			case 'insert_camaraHTML': 
			case 'update_camaraHTML': 
			case 'insert_impuestosHTML': 
			case 'update_impuestosHTML': 
			case 'insert_paisHTML': 
			case 'update_paisHTML': 		
			case 'insert_productoHTML': 			
			case 'update_productoHTML': 
			case 'insert_provinciaHTML': 			
			case 'update_provinciaHTML': 
			case 'insert_rolUserHTML': 			
			case 'update_rolUserHTML': 
			case 'insert_routerHTML': 			
			case 'update_routerHTML': 
			case 'insert_simHTML': 			
			case 'update_simHTML': 
			case 'insert_tipoAvisoHTML': 			
			case 'update_tipoAvisoHTML': 
			case 'insert_tipoCamaraHTML': 			
			case 'update_tipoCamaraHTML': 
			case 'insert_tipoEventoLogHTML': 			
			case 'update_tipoEventoLogHTML': 
			case 'insert_tipoProductoHTML': 			
			case 'update_tipoProductoHTML': 
			case 'insert_tipoRouterHTML': 			
			case 'update_tipoRouterHTML': 
			case 'insert_dispositivoEmisorHTML': 			
			case 'update_dispositivoEmisorHTML': 
			*/				
		} // fin switch
	}
	
		
	
	/**********************************************************************
		Muestra los datos del aparatado que toque, en caso que el valor sea diferente a -1
	**********************************************************************/
	function changeSelect(valor, seccion, operacion){
		console.log("changeSelect(" + valor  + " , " +  seccion   + " , " +  operacion + ")" );
		if(valor == '-1'){
			// Escondemos el formulario.
			$("form").hide();
			
			// Reiniciamos el apartado
			borrar_datos_form();
		}else{
			// Mostramos el formulario
			$("form").show();	
			
			// Mostramos los datos de la opci�n seleccionada
			switch(seccion){
				case 'userBack': mostrar_datos_users_backoffices(valor, operacion); break;				
				case 'camara': mostrar_datos_camara(valor,  operacion); break;	
				// case 'cta': m(valor, operacion); break;				
				case 'cliente': mostrar_datos_cliente(valor, operacion ); break;	 
				case 'producto': mostrar_datos_producto(valor, operacion); break;				
				case 'router': mostrar_datos_router(valor, operacion); break;							
				case 'sim': mostrar_datos_sim(valor, operacion); break;							
				case 'tipoEventoLog': mostrar_datos_tipo_evento_log(valor, operacion); break;				
				case 'impuestos': mostrar_datos_impuestos(valor, operacion); break;				
				case 'pais': mostrar_datos_pais(valor, operacion); break;			
				case 'provincias':mostrar_datos_provincias(valor, operacion);  break;				 
				case 'rolUser': mostrar_datos_rol_user(valor, operacion); break;				
				case 'tipoAviso':  mostrar_datos_tipo_aviso(valor, operacion); break;				
				case 'tipoProducto':	
					switch(valor){
						case '1': 
							console.log( "oo  cajas");
						break;
						
						case '2': 
							console.log( "oo apps");
						break;
						
						case '3':
							console.log( "oo emision personal");			
						break;
					
					}
					// mostrar_datos_tipo_producto(valor, operacion); 
				break;					
				case 'tipoRouter': mostrar_datos_tipo_router(valor, operacion); break;			
				case 'tipoCamara': mostrar_datos_tipo_camara(valor, operacion); break;									
				case 'dispositivoEmisor': mostrar_datos_dispositivo_emisor(valor, operacion); break;				
			} // Fiin swicth
		} // fin else
	}
	
	
	/************************************************************************************
	* Escondemos todos los mensajes de error de los inputs
	* S�lo lo usamos cuando es 'update'
	************************************************************************************/
	function limpiarFormulario(operacion, seccion){ 
			
		switch(seccion){
			case 'userBack':	
				if(operacion == 'update'){
					$("#nameUserBack").val("");
					$("#passUserBack").val("");
					$("#mailUserBack").val("");
					validar_div_visible('idRolUser', 'selectOptionDefault');									
				}
				break;
					
			case 'camara':
				if(operacion == 'update'){
					$("#nombreCamara").val("");
					validar_div_visible('idTipoCamara', 'selectOptionDefault');					
					$("#numSerialCamara").val("");
					$("#macCamara").val("");
					$("#userCamara").val("");
					$("#passCamara").val("");
					$("#locationIpCamara").val("");
					$("#rtspMain").val("");
					$("#resolutionMain").val("");
					$("#kbpsMain").val("");
					$("#rtspSecond").val("");
					$("#resolutionSecond").val("");
					$("#kbpsSecond").val("");
				}
				break;		
							
			case 'cliente':
				if(operacion == 'update'){
					$("#razonSocial").val("");
					$("#dni").val("");
					$("#email").val("");
					$("#direccion").val("");
					$("#poblacion").val("");
					validar_div_visible('idProvincias', 'selectOptionDefault');		
					$("#cp").val("");	
					validar_div_visible('idPais', 'selectOptionDefault');		
					$("#tlf_fijo").val("");	
					$("#tlf_movil").val("");	
					$("#ipEmpresa").val("");	
					$("#puertoEmpresa").val("");	
					$("#streamAPPCliente").val("");	
					$("#connCliente").val("");	
					validar_div_visible('videoAnalisis', 'selectOptionDefault');		
					validar_div_visible('selectEstado', 'selectOptionDefault');		
				}
				break;	

			case 'cta':	
				if(operacion == 'update'){
					$("#titularCuenta").val("");
					$("#entidad").val("");
					$("#sucursal").val("");
					$("#dc").val("");
					$("#numCuenta").val("");
					$("#iban").val("");
					$("#swift").val("");
				} 
				break;			

			case 'producto':	
				if(operacion == 'update'){
					$("#nombreProducto").val("");
					$("#descripcionProducto").val("");
					$("#usuario").val("");
					$("#clave").val("");
					$("#email").val("");
					validar_div_visible('idTipoProducto', 'selectOptionDefault');					
					validar_div_visible('idCamara', 'selectOptionDefault');					
					validar_div_visible('idRouter', 'selectOptionDefault');					
					validar_div_visible('idSim', 'selectOptionDefault');						
					validar_div_visible('idDispositivoEmisor', 'selectOptionDefault');						
					validar_div_visible('idClienteSeleccion', 'selectOptionDefault');							
				}
				break;		
					
			case 'router':  
				if(operacion == 'update'){
					$("#nombreRouter").val("");
					validar_div_visible('idTipoRouter', 'selectOptionDefault');		
					$("#numSerialRouter").val("");
					$("#macRouter").val("");
					$("#userRouter").val("");
					$("#passRouter").val("");
					$("#ipStaticRouter").val("");
					$("#dyndnsRouter").val("");
				} 
				break;										
				
			case 'sim':	
				if(operacion == 'update'){
					$("#numSim01").val("");
					$("#numSim02").val("");
					$("#pinSim").val("");
					$("#punkSim").val("");
					$("#modelSim").val("");
				}	
				break;				
				
			case 'tipoEventoLog': 	
				if(operacion == 'update'){
					$("#nombreEvento").val("");
					$("#descEvento").val("");
				}
				break;		
				
			case 'impuestos':
				if(operacion == 'update'){
					$("#nombreImpuesto").val("");
					$("#descripcionImpuesto").val("");
					$("#valorImpuesto").val("");
				}
				break;			
				
			case 'pais':	
				if(operacion == 'update'){
					$("#nombrePais").val("");
					$("#codeCalling").val("");
					validar_div_visible('continente', 'selectOptionDefault');					
					$("#capital").val("");					
					$("#latPais").val("");
					$("#lngPais").val("");
					$("#codeISO2").val("");
					$("#codeISO3").val("");
				}
				break;		
			
			case 'provincias': 
				if(operacion == 'update'){
					$("#nombreProvincias").val("");
					$("#codigoPostalProvincias").val("");
				}
				break;	
			
			case 'rolUser':		
				if(operacion == 'update'){
					$("#nombreRolUser").val("");
					$("#descripcionRolUser").val("");
				}
				break;		
			
			case 'tipoAviso':
				if(operacion == 'update'){
					$("#nombreTipoAviso").val("");
					$("#descripcionTipoAviso").val("");
				}	
				break;				
			
			case 'tipoProducto': 
				if(operacion == 'update'){
					 $("#nombreTipoProducto").val("");
					 $("#descTipoProducto").val("");
					validar_div_visible('tipoLive', 'selectOptionDefault');	
					$('input[type="checkbox"]').prop('checked', false); // limpio los checkbox
				}	
				break;		
						
			case 'tipoRouter': 
				if(operacion == 'update'){
					 $("#nombre").val("");
					 $("#descripcion").val("");
				}	
				break;		
			
			case 'tipoCamara': 
				if(operacion == 'update'){
					 $("#nombre").val("");
					 $("#descripcion").val("");
				}	
				break;					
				
			case 'dispositivoEmisor': 
				if(operacion == 'update'){
					 // $("#idDispositivoEmisor2").val("");
					$("#nombre").val("");
					$("#descripcion").val("");
					$("#numSerie").val("");
					$("#modelo").val("");
					$("#versionSO").val("");
				}	
				break;		
					
		} // Fin swicth
	}
	
	
	/************************************************************************************
	* Escondemos todos los mensajes de error con una clase que tienen en com�n
	************************************************************************************/
	function borrar_datos_form(){
		$(".bg-danger").css("display","none");
		$(".bg-success").css("display","none");
		$(".form-control").val("");
		$('input[type="checkbox"]').prop('checked', false);
		
		// Comprobamos quien esta visible y se reinicia
		validar_div_visible('idCamara', 'selectOptionDefault');
		validar_div_visible('idClienteSeleccion', 'selectOptionDefault');
		validar_div_visible('idImpuestosSelect', 'selectOptionDefault');
		validar_div_visible('idProvincias', 'selectOptionDefault');
		validar_div_visible('idRouter', 'selectOptionDefault');
		validar_div_visible('idSim', 'selectOptionDefault');
		validar_div_visible('selectEstado', 'selectOptionDefault');
		validar_div_visible('estadoCliente', 'selectOptionDefault');
		validar_div_visible('idSelect', 'selectOptionDefault');
		validar_div_visible('idUserBack', 'selectOptionDefault');
		validar_div_visible('idRolUser', 'selectOptionDefault');
		validar_div_visible('idProvincias', 'selectOptionDefault');
		validar_div_visible('idSim', 'selectOptionDefault');
		validar_div_visible('idPais', 'selectOptionDefault');
		validar_div_visible('tipoLive', 'selectOptionDefault');
		validar_div_visible('videoAnalisis', 'selectOptionDefault');
		validar_div_visible('idTipoProducto', 'selectOptionDefault');
		validar_div_visible('idTipoRouter', 'selectOptionDefault');
		validar_div_visible('idTipoCamara', 'selectOptionDefault');
		validar_div_visible('idDispositivoEmisor', 'selectOptionDefault');
		validar_div_visible('continente', 'selectOptionDefault');					
		
		// Deseleccionar rojo
		$('input').css("border", "1px solid #ccc");
	}
	
	
	
	
/* ####################################################################
	
	VALIDACIONES 	
	
#################################################################### */
	
	/************************************************************************************
	* Comprobamos si el campo esta nulo o vacio. 
	* Si esta vacio o nulo, devolvemos un guion.
	************************************************************************************/
	function validarNulo(valor){
		if((valor == "") || (valor == null)){ 
			var newValor = "-"; 
			return newValor;
		} else { 
			return valor; 
		}
	}
	
	
	/************************************************************************************
	* Comprobamos si el select option a escogido alguna opci�n.
	* Sino ha escogido alguna opci�n activamos el mensaje de error.
	************************************************************************************/	
	function validar_selectOption(nomDiv, tipoCount){
		var variable =$('#'+nomDiv).val();
		
		if(variable == '-1'){
			$("#errorMsg_"+nomDiv).show();
			$("#"+nomDiv).css("border", "1px solid red");
		}
		else{
			// Llamamos a la funcion para que contabilice que esta bien.
			 validar_contador(true, tipoCount); 
			 $("#"+nomDiv).css("border", "1px solid #ccc");
			 return variable;
		}
	}
	
	
	/************************************************************************************
	* Sumador de contadores, para controlar el numero de campos que hay correctamente rellenados.
	* Si (validacionOk = true ), entonces sumaremos en el contador correspondiente.
	************************************************************************************/		
	function validar_contador(valor, tipoCount){
		if(valor == true){			
			switch(tipoCount){
				case 'userBack': countUserBack++; break;
				case 'camara': countCamara++; break;
				case 'cliente': count++; break;				
				case 'cta': countCta++; break;
				case 'producto': countProducto++; break;				
				case 'router': countRouter++; break;				
				case 'sim': countSim++; break;
				case 'tipoEventoLog': countTipoEventoLog++; 	break;
				case 'impuestos': countImpuestos++; break;
				case 'pais': countPais++; break;
				case 'provincias': countProvincias++; break;
				case 'rolUser': countRolUser++; break;
				case 'tipoAviso': countTipoAviso++; break;
				case 'tipoProducto':	countTipoProducto++; 	break;
				case 'tipoRouter':	countTipoRouter++; break;		
				case 'tipoCamara': countTipoCamara++; break;
				case 'dispositivoEmisor': countDispositivoEmisor++; break;
				case 'userSport': countUserSport++; break;
			}//fin switch suma
		}
	}
	
	
	/************************************************************************************
	* Funcion que comprueba el estado 1-0 y lo convierte al texto que le enviamos
	************************************************************************************/
	function comprobar_booleano(valor, txtPositivo, txtNegativo){		
		if(valor==1){
			return txtPositivo;
		}else{
			return txtNegativo;
		}
	}
	

	/************************************************************************************
		Value checkbox
	************************************************************************************/
	function comprobar_checkbox(nombreDiv){
		if($("#"+nombreDiv).is(':checked')) {  
			return 1; //Activado
        } else {  
			return 0; //No activado
        } 	
	}
	
	
	/************************************************************************************
		Comprobacion de los digitos.
		Con 'maxLength' comprobamos si el tama�o es correcto.
		Con 'firstDigito' comprobamos si el primer digito es correcto.
	************************************************************************************/
	function validar_digitos(nomDiv, tipoCount, maxLength,  firstDigito ){
		var variable = $('#'+nomDiv).val();
		var validacionOK = true;
		
		// validar_primer_digito
		if(firstDigito){ 
		
			// Recogemos el primer digito de la variable
			var primerDigito = variable.charAt(0); 
			
			// Comprobamos si el digito pasado y el recogido son iguales 
			if(primerDigito != firstDigito){ 
				$("#"+nomDiv).css("border", "1px solid red");
				$("#errorMsg_"+nomDiv)
					.css("display", "block")
					.html("* Campo obligatorio, este campo debe empezar por " + firstDigito + ".") // Machacamos el mensaje de error
				validacionOK = false;
			}
		}
		
		
		// validar_num_digitos maxLength
		if(maxLength){ 	
			//Si el tama�o de la variable es != al que debe ser --> mostramos smsError
			if(variable.length != maxLength ){
				$("#"+nomDiv).css("border", "1px solid red");
				$("#errorMsg_"+nomDiv)
					.css("display", "block")					
					.html("* Campo obligatorio, compuesto de " + maxLength + "digitos"); // Machacamos el mensaje de error
				validacionOK = false;
			}
		}
		
		// Si validacionOk = true sumamos en el contado correspondiente, eso es porque ha pasado todas las validaciones
		if(validacionOK == true){			
			validar_contador(validacionOK, tipoCount);
			$("#"+nomDiv).css("border", "1px solid #ccc");
			return variable;
		}	
	}
	
	/************************************************************************************
		Comprobacion si es un email
	************************************************************************************/
	function validar_email(nomDiv, tipoCount){
		var variable = $('#'+nomDiv).val();
		var validacionOK = true;
			
		// Si la cadena que nos envian no tiene un @ o un punto --> mostramos smsError
		if(variable.indexOf('@', 0) == -1 || variable.indexOf('.', 0) == -1){ 
			$("#errorMsg_"+nomDiv).show()
			$("#"+nomDiv).css("border", "1px solid red");
				
			validacionOK = false;
		}	else{
			//Si validacionOk = true sumamos en el contado correspondiente, eso es porque ha pasado todas las validaciones
			validar_contador(validacionOK, tipoCount);
			$("#"+nomDiv).css("border", "1px solid #ccc");
			
			
			return variable;
		}
	}
	
	/************************************************************************************
		Si la variable esta nula, vacia... --> mostramos smsError
	************************************************************************************/
	function validar_input(nomDiv, tipoCount){
	console.log("validar_input: " + nomDiv + ", " + tipoCount);
		var validacionOK = true;
		var variable = $('#'+nomDiv).val();	
		
		if( (variable=="") || (variable == null ) || (variable == 'null') ){		
			$("#errorMsg_"+nomDiv).show()
			$("#"+nomDiv).css("border", "1px solid red");
			validacionOK = false;
		}else{
			// Si validacionOk = true sumamos en el contado correspondiente, eso es porque ha pasado todas las validaciones
			validar_contador(validacionOK, tipoCount);
			$("#"+nomDiv).css("border", "1px solid #ccc");
			 return variable;
		}
	}//fin funcion
	
	
	/************************************************************************************
	select option activo-desactivo (eliminar porque tiene que quedar obsoleto)
	************************************************************************************/
	function select_option_des_activo(nombreDiv){
		
		var html_selectOption ="";
		html_selectOption += "<select class='form-control' id='"+nombreDiv+"'>";
			html_selectOption += "<option value='-1' selected='selected' name='defaultEstado'> Selecciona una opci"+min_o+"n  </option>";
			html_selectOption += "<option value='0'> Desactivo </option>";
			html_selectOption += "<option value='1'> Activo </option>";	
		html_selectOption += "</select>";
		$("#selectOption_estado").html(html_selectOption);
	
	}	
	
	/************************************************************************************
	select option activo-desactivo (eliminar porque tiene que quedar obsoleto)
	************************************************************************************/
	function selectOption_continentes(nombreDiv){		
		var html_selectOption ="";
		html_selectOption += "<select class='form-control' id='"+nombreDiv+"'>";
			html_selectOption += "<option value='-1' selected='selected' name='defaultContinente'> Selecciona una opci"+min_o+"n  </option>";
			html_selectOption += "<option value='"+may_a+"frica'>"+may_a+"frica</option>";
			html_selectOption += "<option value='Norteam"+min_e+"rica'>Norteam"+min_e+"rica</option>";
			html_selectOption += "<option value='Centroam"+min_e+"rica'>Centroam"+min_e+"rica </option>";
			html_selectOption += "<option value='Sudam"+min_e+"rica'> Sudam"+min_e+"rica  </option>";
			html_selectOption += "<option value='Asia'>Asia  </option>";
			html_selectOption += "<option value='Europa'> Europa </option>";
			html_selectOption += "<option value='Ocean"+min_i+"a'>Ocean"+min_i+"a  </option>";
			html_selectOption += "<option value='Ant"+min_a+"rtida'> Ant"+min_a+"rtida </option>";
		html_selectOption += "</select>";
		cargandoDatos("continente", 'end', 'seleccion');
		$("#selectOption_continente").html(html_selectOption);
	}
	
	/************************************************************************************
		select option activo-desactivo
	************************************************************************************/
	function select_optionBinario(nomDiv, opcion0, opcion1){
		var html_selectOption = "<select class='form-control' id='"+nomDiv+"'>";
			html_selectOption += "<option value='-1' selected='selected' name='defaultEstado'> Selecciona una opci"+min_o+"n </option>";
			html_selectOption += "<option value='0'> "+opcion0+" </option>";
			html_selectOption += "<option value='1'> "+opcion1+" </option>";								
		html_selectOption += "</select>";
		cargandoDatos(nomDiv, 'end', 'seleccion');
		$("#selectOption_"+nomDiv).html(html_selectOption);
	}
	
	/************************************************************************************
		select option activo-desactivo
	************************************************************************************/
	function select_tipoNumBanco(nomDiv){
		var html_selectOption = "<select class='form-control' id='"+nomDiv+"'>";
			html_selectOption += "<option value='-1' selected='selected' name='defaultEstado'> "+ txtSelectOptionDefault+" </option>";
			html_selectOption += "<option value='ccc'> CCC </option>";
			html_selectOption += "<option value='iban'> IBAN </option>";	
			html_selectOption += "<option value='swift'> SWIFT/BIC </option>";									
		html_selectOption += "</select>";
		cargandoDatos(nomDiv, 'end', 'seleccion');
		$("#selectOption_tipoNumBanco").html(html_selectOption);
	}
	
	/************************************************************************************
	select option tipoLive
	************************************************************************************/
	function select_option_tipo_live(nombreDiv){
		//Select-option formulario estado
		var html_selectOption ="";
		html_selectOption += "<select class='form-control' id='"+nombreDiv+"'>";
			html_selectOption += "<option value='-1' selected='selected' name='defaultTipoLive'> "+txtSelectOptionDefault+" </option>";
			html_selectOption += "<option value='live'> Live </option>";
			html_selectOption += "<option value='liveflv'> Liveflv </option>";	
		html_selectOption += "</select>";
		$("#selectOption_tipoLive").html(html_selectOption);
	
	}

	

	
	/************************************************************************************
		Funcion que a partir de unos parametros recibidos muestra el valor y bloquea el campo
	************************************************************************************/
	function mostrar_datos(nombreDiv, valorDiv, operacionSQL ){
		// console.log("mostrar_datos("+nombreDiv+", "+valorDiv+", "+operacionSQL+" )");
		
		
		//el primer caso es solo para 'tipo producto' campo 'selectEstado'
			//mostrar_datos("videoAnalisis", arrayClienteSelecionado[0].videoAnalisis, operacionSQL);
		if ( ( nombreDiv == 'selectEstado') || ( nombreDiv == 'videoAnalisis') ){
			if ((valorDiv == 0)  || (valorDiv == 1)){
				$("#"+nombreDiv).css("border", "1px solid #ccc");
				$("#"+nombreDiv).val(valorDiv);		
			}
			//este caso es para el resto
		}else{
				if( (valorDiv == null) || (valorDiv == '') || (valorDiv == 'Campo vac'+min_i+'o') ){
					$("#"+nombreDiv).css("border", "1px solid red");
					// $("#"+nombreDiv).val("Campo vac"+min_i+"o");				
				}else{
					$("#"+nombreDiv).css("border", "1px solid #ccc");
					$("#"+nombreDiv).val(valorDiv);						
				}
		}

		
		//Esto es para activar/desactivar los checkbox
		if(valorDiv==1){
			$("#"+nombreDiv).prop('checked', true);
		}else{
			$("#"+nombreDiv).prop('checked', false);			
		}
		
		if(operacionSQL=="query"){ //solo cuando es una consulta, desactivaremos los campos
			$("#"+nombreDiv).attr("disabled", true);
		}
	}
	
	
	
	/************************************************************************************
	 segun en la pagina que este mostraremos una cosa u otra...
	 Yo estaba reduciendo codigo con esta funcion
	 Necesito saber:
		1. Si es un update/query/insert/search
		2. Necesito saber de donde viene .. camara/router/producto/cliente.....
	************************************************************************************/
	function menu_breadcrumb(url){
		// console.log("menu_breadcrumb: " + url);
		var partes = url.split("/");	
		
		for (var i=0; i <= partes.length-1; i++){
			//Si estoy en inicio solo tengo que mostrar un apartado
			if(partes[1] == 'dashboard.html'){
				var contenidoBreadcrumb ="<li><a href='#'>Dashboard</a></li>";
			
			//Si estoy en cualquier otro lado, tengo que mostrar toda la ruta			
			}else{
				var partes2 = partes[i].substr(partes[i].length - 4, 4);
				if(partes2 == 'html'){
					//ahora saco la parte de delante
					var partes3 = partes[i].substr(0, 6); //de aqui saco las palabras insert, update, search , query_ (con el guion bajo)
					
					//Sacar opcion
					switch(partes3){
						case 'insert': var opcion = "Alta "; break;
						case 'update': var opcion = "Modificar "; break;
						case 'query_': var opcion = "Consultar "; break;
						case 'search': var opcion = "Buscar "; break;
						case 'asigna': var opcion = "Asignar Producto "; break;
					}
					
					//Sacar producto-servicio es:
					parteMenos1 = partes[i-1];
					
					switch(parteMenos1){
						case 'usuarios': 	var servicio = "Usuario backoffices ";break;
						case 'camara': 		var servicio = "C"+min_a+"mara ";break;
						case 'empresa': 	var servicio = "Cliente ";break;
						case 'impuestos': 	var servicio = "Impuestos ";break;
						case 'logAcciones': 	var servicio = "Log acciones ";break;
						case 'logBackoffices': 	var servicio = "Log backoffices ";break;
						case 'producto': 	var servicio = "Producto ";break;
						case 'pais': 	var servicio = "Pa"+min_i+"s ";break;
						case 'provincia': 	var servicio = "Provincias ";break;
						case 'rolUser': 	var servicio = "Rol usuario ";break;
						case 'router': 		var servicio = "Router ";break;
						case 'sim': 		var servicio = "Sim ";break;
						case 'tipoAviso': 	var servicio = "Tipo de aviso ";break;
						case 'tipoEventoLog': 	var servicio = "Tipo eventos log ";break;
						case 'tipoProducto': var servicio = "Tipo de producto ";break;
						case 'tipoCamara': var servicio = "Tipo de c"+min_a+"mara ";break;
						case 'tipoRouter': var servicio = "Tipo de router ";break;
						case 'dispositivoEmisor': var servicio = "Dispositivo Emisor ";break;
						
					}
				
					//Montamos el contenido que mostraremos por pantalla
					var contenidoBreadcrumb ="<li><a href='#'>Dashboard</a></li>";
					contenidoBreadcrumb +="<li><a href='#'>"+ servicio +"</a></li>";
					contenidoBreadcrumb +="<li><a href='#'>"+ opcion + "</a></li>";		
				}
			}//fin else
		}//fin for
		
		$("#contenidoBreadcrumb").html(contenidoBreadcrumb);
	}
	
	
	/************************************************************************************
		Evaluamos la respuesta de la consultas que se hacen al insertar y modificar datos de cada tabla.
	************************************************************************************/
	function evaluar_success(response, opcion, tabla, proceso){
		switch(opcion){
			case 'insert':
				var mensaje_ok = 'Enhorabuena, se ha realizado el alta correctamente.';
				break;
			
			case 'update':
				var mensaje_ok = 'Enhorabuena, se ha realizado la modificaci'+min_o+'n correctamente.';
				break;
			
			case 'asignar':
				var mensaje_ok = 'Enhorabuena, se ha asignado correctamente el producto.';
				break;
			
			case 'desasignar':
				var mensaje_ok = 'Enhorabuena, se ha liberado correctamente el producto.';
				break;
		}
		
		
		//Desbloqueamos para mostrar el mensaje
		if(opcion == 'desasignar'){
			var nombreDiv = "successMsg_2";
		}else{
			var nombreDiv = "successMsg";
		}
		
		$("#"+nombreDiv).css("display", "block");
		
		//Borramos las clases porque sino solo funciona en el primer cambio
		$("#"+nombreDiv).removeClass("bg-success");
		$("#"+nombreDiv).removeClass("bg-danger");
		
		if(response == 1){
			borrar_datos_form(); //limpiamos el form y dejamos mensaje			
			$("#"+nombreDiv).addClass("bg-success");
			$("#"+nombreDiv).html(mensaje_ok);	
			//redirigimos
		}else{ // response = 0
			$("#"+nombreDiv).addClass("bg-danger");
			if(proceso == 'duplicado')
				$("#"+nombreDiv).html('Lo sentimos, los datos introducidos ya existen en la base de datos.');	
			else
				$("#"+nombreDiv).html('Lo sentimos, se ha producido alg'+min_u+'n tipo de error, ponte en contacto con el administrador o revisa que los datos sean correctos.');	
			
		
		}
		
		// Este apartado es para actualizar el array por si se ha cambiado el nombre.
		switch(tabla){
			case 'backoffice': 	select_users_backoffices('idUserBack', 'selectOption'); break;
			case 'camara': 	   select_camaras('idCamara', 'selectOption'); break;
			case 'cliente': 	   select_cliente('idClienteSeleccion', 'selectOption'); break;
			case 'producto': 			
				
				if ( (opcion == 'asignar') || (opcion == 'desasignar')){
					select_producto_sinAsignar('productoSinAsignar');
				}else{				
					arrayProductoSinAsignar.length=0; //vacio el array
					// Recargamos los datos porque han cambiado
					select_producto('idProducto', 'selectOption'); 
									
					select_camaras('idCamara', 'selectOption'); 
					select_routers('idRouter', 'selectOption'); 
					select_sim('idSim', 'selectOption'); 
				}
			break;
			
			case 'router':  select_routers("idRouter", "selectOption"); break;
			case 'sim':  select_sim('idSim', 'selectOption'); break;	
			case 'tipo_evento_log':  select_tipo_evento_log('idTipoEventoLogSelect', 'selectOption'); break;
			case 'impuestos':  select_impuestos('idImpuestosSelect', 'selectOption'); break;
			case 'pais':  select_pais('idPais', 'selectOption'); break;
			case 'provincias':  select_provincias('idProvincias', 'selectOption'); break;
			case 'rol_user':  select_rol_user("idRolUser", "selectOption"); break;
			case 'tipo_avisos':  select_tipo_aviso('idTipoAviso', 'selectOption'); break;			
			case 'tipo_productos':  select_tipo_producto('idTipoProducto', 'selectOption'); break;
			case 'tipo_router':  select_tipo_router('idTipoRouter', 'selectOption'); break;			
			case 'tipo_camara':  select_tipo_camara('idTipoCamara', 'selectOption'); break;
			case 'dispositivoEmisor':  select_dispositivo_emisor('idDispositivoEmisor', 'selectOption'); break;			
		} // fin switch
			
	}		
	

	
	/************************************************************************************
		datatable informacion en dashboard
	************************************************************************************/
	function datatable_dashboard_info(total, libres, asignadas, th_total, th_libres, th_asignadas, nombreDiv){
		
		//Montamos la tabla
		var html_datatable ="";
		html_datatable +='<table class="table table-bordered table-striped table-hover table-heading " id="'+nombreDiv+'">';
			html_datatable +=' <tr> <td class="td_titol"> '+ th_total +' : ' +  total+ '</td> </tr>';
			html_datatable +=' <tr> <td class="td_titol"> '+  th_libres +' : ' +libres +'</td> </tr>';  
			html_datatable +='<tr> <td class="td_titol"> '+ th_asignadas +' : ' +asignadas +'</td> </tr>'; 
		html_datatable +='</table>';
	
		return html_datatable;
	}//fin funcion

	
	
	
	
	/************************************************************************************
	
	************************************************************************************/
	function selecciona_opcion (operacion, contador){
		//Si estoy en un update/query y no hay nada lleno es porque no ha seleccionado nada..
		if((contador == 0) && ((operacion == 'query') || (operacion == 'update')) ) {	
			$(".bg-danger").css('display', 'none'); //Bloqueo los mensajes de los input
			//alert("Selecciona una opci"+min_o+"n");
			$("#errorMsg_selectOption").css('display', 'block'); //DESbloqueo el  mensajes del select-option
			$("#errorMsg_selectOption").html("Selecciona una opci"+min_o+"n"); //DESbloqueo el  mensajes del select-option
		}
	}
	
	/**
		Una vez que hemos hecho un 'insert' o 'update' ejecutamos las siguientes funciones...
	*/
	function despuesDeGuardar(arrayResult, opcion ){
		
		// Escondemos el "Guardando / Actualizando Datos"
		cargandoDatos("guardandoDatos", 'end', arrayResult[0].accion);				
		
		// Evaluamos la respuesta de la consultas que se hacen al insertar y modificar y mostramos el mensaje adecuado
		evaluar_success(arrayResult[0].response, arrayResult[0].accion, arrayResult[0].tabla);		
		
		// Insertamos un registro de la acci�n que se ha realizado
		insert_log_acciones(arrayResult);	

		// Enviamos un email  de confirmaci�n de que se ha modificado alguna cosa del backoffice
		enviar_email_confirmacion(arrayResult, opcion);					
	}
	

	
	/************************************************************************************
	activar cuando pase a produccion
	************************************************************************************/
	function enviar_email_confirmacion(arrayResult, opcionRuta){
	
		if(opcionRuta == 1){
				var rutaUrl =  'io/enviarEmail_insert_cliente.php';
		}else{
				var rutaUrl =  'io/enviarEmail.php';
		}
	
	$.ajax({
			type: 'GET',
			url: rutaUrl,
			data: { 
				'datosArray': JSON.stringify(arrayResult),
				'comprobar': 1
			},
			success: function(response) {}
		});	
	}
	
	/************************************************************************************
	
	************************************************************************************/	
	function estadoObjeto(state){
		if(state == 0){  
			return " (libre)";
		}else{
			return  " ";
		}
	}
	
	/************************************************************************************
		Bloques del select-option de los for
	************************************************************************************/
	function optionSelect_bloques(id, name, estado, posicion){
		// console.log("optionSelect_bloques("  + id + " , " +  name + " , " + estado + ") " );
		var txt = "<option name='"+posicion+"' value='"+id+"'> " ;
			txt +=  id + " - "+ name +" " + estadoObjeto(estado);
		txt += " </option>";
		return txt;
	}
	/************************************************************************************
		montamos y mostramos la tabla 
		@classesTabla:  table table-bordered table-striped table-hover table-heading	 table-datatable
	*************************************************************************************/
	function montarTabla(idTabla, thTabla, tdTabla, tfoot, nameDiv){	
		var tabla ='<table class="table table-bordered table-striped table-hover table-heading" id="'+idTabla+'">';
				tabla += '<thead>' + thTabla + '</thead>';								
				tabla += '<tbody>' + tdTabla + '</tbody>';
				tabla += '<tfoot>' + tfoot + '</tfoot>';
			tabla +='</table>';
		
		cargandoDatos(nameDiv, "end", 'seleccion'); 
		$("#"+nameDiv).html(tabla);
	}
	
	// Comprobamos que exita, 
	function validar_div_visible(nomDiv, op){
	//	console.log("(1) validar_div_visible("+nomDiv+", "+op+")")
		if ($("#"+nomDiv).is(":visible")){ 
			switch(op){
				case 'hide':
					$("#"+nomDiv).hide();
					break;
					
				case 'selectOptionDefault':
					$('#'+nomDiv +' option[value=-1]').prop("selected", true);
					break;
				
			} // fin switch
			
		} // fin if
		
	}
	
/************************************************************************************
#
# TABLA BACKOFFICES
#
************************************************************************************/	
	/************************************************************************************
		Validar datos introducidos en el formulario
	************************************************************************************/
	function validar_datos_users_backoffices(operacion){
		validar_div_visible("successMsg", "hide");		
				
		var tipoCount="userBack";
		countUserBack=0;
	
		
		// comprobamos los campos del formulario 
		var nombre = validar_input('nameUserBack',  tipoCount); 
		var pass =validar_input('passUserBack',  tipoCount);   	
		var mail = validar_email('mailUserBack',  tipoCount); 	
		var rid = validar_selectOption( 'idRolUser',  tipoCount);
		
		
		// selecciona una opcion
		selecciona_opcion(operacion, countUserBack);
		
		if(countUserBack >=4) {	
			// Guardando datos
			cargandoDatos("guardandoDatos", 'start', operacion); // ok
		
			var nameRol = $("#idRolUser option[value='"+$("#idRolUser").val()+"']").text();
			// Lo mandamos a la operacion que le corresponda
			switch(operacion){
				case 'insert':					
					insertar_datos_users_backoffices(nombre, pass, mail,rid, nameRol);
					break;
					
				case 'update':
					idSelect =  $("#idUserBack2").val(); 
					modificar_datos_users_backoffices(nombre, pass, mail,rid, idSelect, nameRol);
					break;
			}
		}
	}		
	
	
	
	/************************************************************************************
		Insertamos los datos en eude.backoffices
	************************************************************************************/
	function insertar_datos_users_backoffices(nombre, pass, mail,rid, nameRol){
		$.ajax({
			type: 'GET',
			url: 'io/usuarios/insert_usersBackoffice.php',
			data: { 
				'nombre' : nombre,
				'pass' : pass,	
				'mail' : mail,	
				'rid' : rid,	
				'nameRol' : nameRol,	
				'tiempoDesfase' : desfaseHoraria()				
			},
			success: function(response) {
				var arrayResult = $.parseJSON(response);
				despuesDeGuardar(arrayResult, 0); 		
			}
		});
	}
	
	
	/************************************************************************************
		Modificamos los datos en eude.backoffices
	************************************************************************************/
	function modificar_datos_users_backoffices(nombre, pass, mail,rid,  idSelect, nameRol){
		$.ajax({
			type: 'GET',
			url: 'io/usuarios/update_usersBackoffice.php',
			data: { 
				'idSelect' : idSelect,
				'nombre' : nombre,
				'pass' : pass,	
				'mail' : mail,	
				'rid' : rid,			
				'nameRol' : nameRol,
				'tiempoDesfase' : desfaseHoraria()				
			},
			success: function(response) {
				var arrayResult = $.parseJSON(response);
				despuesDeGuardar(arrayResult, 0); 						
			}
		});
	}
	
	
	
	/************************************************************************************
		Mostramos los datos de eude.backoffices
	************************************************************************************/
	function mostrar_datos_users_backoffices(idSelect, operacionSQL){		
		for(var i=0; i < arrayUserBack.length; i++){
			if( arrayUserBack[i].uid == idSelect){
				mostrar_datos("idUserBack2", arrayUserBack[i].uid, operacionSQL);
				mostrar_datos("nameUserBack", arrayUserBack[i].name, operacionSQL);
				mostrar_datos("passUserBack", arrayUserBack[i].pass, operacionSQL);
				mostrar_datos("mailUserBack", arrayUserBack[i].mail, operacionSQL);
				mostrar_datos("idRolUser", arrayUserBack[i].rid, operacionSQL);
				mostrar_datos("fechaAlta", arrayUserBack[i].fecha_alta, operacionSQL);
				mostrar_datos("fechaModificacion", arrayUserBack[i].fecha_mod, operacionSQL);	
				insert_log_acciones(arrayUserBack[i]);		
			} // fin if			
		} // fin for
	}
	
	
	
	/************************************************************************************
		Select de los datos de eude.backoffices
	************************************************************************************/
	function select_users_backoffices(nombreDiv, tipoObjeto){
		$.ajax({
			type: 'GET',
			url: 'io/usuarios/select_usersBackoffice.php',
			data: {	
				'tiempoDesfase' : desfaseHoraria() 
			},
			success: function(response) {
				arrayUserBack = $.parseJSON(response);
	
				switch(tipoObjeto){
					case 'selectOption':			 
						select_users_backoffices_selectOption(nombreDiv);							
						break;
					
					case 'dataTable':					
						select_users_backoffices_dataTable(nombreDiv);
						break;
				} // fin switch
			} //fin success
		});
	}

	/************************************************************************************
		Select-option formulario usuarios backoffice - todas		
	************************************************************************************/	
	function select_users_backoffices_selectOption(nombreDiv){		
		var html_selectOption ="";
		html_selectOption += "<select class='form-control' id='"+nombreDiv+"' >";
			html_selectOption += "<option value='-1' selected='selected' name='default'> "+ txtSelectOptionDefault +" </option>";
			for(var i=0; i < arrayUserBack.length; i++){
				html_selectOption += optionSelect_bloques(arrayUserBack[i].uid, arrayUserBack[i].name, 1, i);
			} // Fin for
		html_selectOption += "</select>";
		cargandoDatos("idUserBack", "end", 'seleccion');
		$("#selectOption_idUserBack").html(html_selectOption);				
	}
	
	/************************************************************************************
		tabla de la pag 'consultar' backoffices 		
	************************************************************************************/	
	function select_users_backoffices_dataTable(nombreDiv){
		// thead
		var thTabla ='<tr>';
			thTabla +='<th class="th_turquesa">Id usuario</th>';
			thTabla +='<th class="th_turquesa">Nombre</th>';
			thTabla +='<th class="th_turquesa">Mail</th>';						
			thTabla +='<th class="th_turquesa">Id rol</th>';						
			thTabla +='<th class="th_turquesa">F. Alta</th>';
			thTabla +='<th class="th_turquesa">F. Modificaci'+min_o+'n</th>';
		thTabla +='</tr>';
				
		// tbody
		var tdTabla ="";
		for(var i=0; i < arrayUserBack.length;  i++){
			tdTabla +=' <tr>';
				tdTabla +=' <td> '+ validarNulo(arrayUserBack[i].uid) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayUserBack[i].name) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayUserBack[i].mail) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayUserBack[i].rid) +' - ' + arrayUserBack[i].nomRol + '</td>';
				tdTabla +=' <td> '+ validarNulo(arrayUserBack[i].fecha_alta) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayUserBack[i].fecha_mod) +'</td>';
			tdTabla +='</tr>';
		} // for
			
		// Montamos la tabla
		montarTabla(nombreDiv, thTabla, tdTabla, thTabla, "contenido");
	}
	
/************************************************************************************
#
# TABLA C�MARAS
#
************************************************************************************/	
	
	/************************************************************************************
		Validar datos del formulario 
	************************************************************************************/
	function validar_datos_camara(operacion){
			
		validar_div_visible("successMsg", "hide");		
		countCamara=0;
		tipoCount ="camara";
		
		// Validamos los campos del formulario
		var nombreCamara = validar_input('nombreCamara',  tipoCount); 
		var idTipoCamara =validar_input('idTipoCamara',  tipoCount);  // esto sera un select-option
		var numSerialCamara = validar_input('numSerialCamara',  tipoCount); 
		var macCamara = validar_input('macCamara',  tipoCount);
		var userCamara =  validar_input('userCamara',  tipoCount); 		
		var passCamara =   validar_input('passCamara',  tipoCount); 
		var locationIpCamara =  validar_input('locationIpCamara',  tipoCount);
		var rtsp_1 = validar_input('rtspMain',  tipoCount); 
		var resolution_1 = validar_input('resolutionMain',  tipoCount); 
		var kbps_1 = validar_input('kbpsMain',  tipoCount); 
		var rtsp_2 =validar_input('rtspSecond',  tipoCount); 
		var  resolution_2 = validar_input('resolutionSecond',  tipoCount); 
		var kbps_2 = validar_input('kbpsSecond',  tipoCount); 	
	
		if(countCamara >7){	
			// Guardando datos
			cargandoDatos("guardandoDatos", 'start', operacion); //ok
			
			// Lo mandamos a la operacion que le corresponda
			switch(operacion){
				case 'insert': 
					insertar_datos_camara(nombreCamara,idTipoCamara ,numSerialCamara,macCamara,userCamara,passCamara,locationIpCamara, rtsp_1, kbps_1, resolution_1, rtsp_2, kbps_2, resolution_2);
					break;
					
				case 'update': 
					var idCamaraSelect =  $("#idCamara2").val(); 
					var fechaA = $("#fechaAlta").val(); 
					var fechaM= $("#fechaModificacion").val(); 
					modificar_datos_camara(idCamaraSelect,nombreCamara,idTipoCamara ,numSerialCamara,macCamara,userCamara,passCamara,locationIpCamara, rtsp_1, kbps_1, resolution_1, rtsp_2, kbps_2, resolution_2 ,fechaA, fechaM);
					break;
			}
		}
	}
	
	
	/************************************************************************************
		Insertamos los datos de eude.camara
	************************************************************************************/
	function insertar_datos_camara(nombreCamara,idTipoCamara ,numSerialCamara,macCamara,userCamara,passCamara,locationIpCamara,rtsp_1, kbps_1, resolution_1, rtsp_2, kbps_2, resolution_2){
			
		$.ajax({
			type: 'GET',
			url: 'io/camara/insert_camara.php',
			data: { 
				'nombreCamara' : nombreCamara,
				'idTipoCamara':idTipoCamara,
				'numSerialCamara':numSerialCamara,
				'macCamara':macCamara,
				'userCamara':userCamara,
				'passCamara':passCamara,
				'locationIpCamara':locationIpCamara,
				'rtspMain':rtsp_1,
				'resolutionMain':resolution_1,
				'kbpsMain':kbps_1,
				'rtspSecond':rtsp_2,
				'resolutionSecond':resolution_2,
				'kbpsSecond':kbps_2,
				'tiempoDesfase' : desfaseHoraria()
			},
			success: function(response) {
				var arrayResult = $.parseJSON(response);
				despuesDeGuardar(arrayResult, 0); 		
			}
		});
	
	}

	
	
	/************************************************************************************
		Insertamos  los datos de eude.camara
	************************************************************************************/
	function modificar_datos_camara(idCamaraSelect,nombreCamara,idTipoCamara ,numSerialCamara,macCamara,userCamara,passCamara,locationIpCamara, rtsp_1, kbps_1, resolution_1, rtsp_2, kbps_2, resolution_2, fechaA, fechaM){		
		$.ajax({
			type: 'GET',
			url: 'io/camara/update_camara.php',
			data: { 
				'idCamaraSelect' : idCamaraSelect,
				'nombreCamara' : nombreCamara,
				'idTipoCamara':idTipoCamara,
				'numSerialCamara':numSerialCamara,
				'macCamara':macCamara,
				'userCamara':userCamara,
				'passCamara':passCamara,
				'locationIpCamara':locationIpCamara,
				'rtsp_1':rtsp_1,
				'kbps_1':kbps_1,
				'resolution_1':resolution_1,				
				'rtsp_2':rtsp_2,
				'kbps_2':kbps_2,
				'resolution_2':resolution_2,
				'fechaA':fechaA,
				'fechaM':fechaM,
				'tiempoDesfase' : desfaseHoraria()
			},
			success: function(response) {
				var arrayResult = $.parseJSON(response);		
				despuesDeGuardar(arrayResult, 0);
			}
		});
	}
	
	
	/************************************************************************************
		Mostramos los datos que nos han solicitado en 'consultar camara'
	************************************************************************************/
	function mostrar_datos_camara(idCamara_selecionada,  operacionSQL){
		for(var i=0; i < arrayCamaras.length; i++){
			if( arrayCamaras[i].id == idCamara_selecionada){
				mostrar_datos("idCamara2", arrayCamaras[i].idCamara, operacionSQL);
				mostrar_datos("nombreCamara", arrayCamaras[i].nameCamara, operacionSQL);
				mostrar_datos("idTipoCamara", arrayCamaras[i].idTipoCamara, operacionSQL);
				mostrar_datos("numSerialCamara", arrayCamaras[i].serialnumCamara, operacionSQL);
				mostrar_datos("macCamara", arrayCamaras[i].macCamara, operacionSQL);
				mostrar_datos("userCamara", arrayCamaras[i].userCamara, operacionSQL);
				mostrar_datos("passCamara", arrayCamaras[i].passCamara, operacionSQL);
				mostrar_datos("locationIpCamara", arrayCamaras[i].localipCamara, operacionSQL);
				mostrar_datos("rtspMain", arrayCamaras[i].rtspMainCamara, operacionSQL);
				mostrar_datos("resolutionMain", arrayCamaras[i].resolutionMainCamara, operacionSQL);
				mostrar_datos("kbpsMain", arrayCamaras[i].kbpsMainCamara, operacionSQL);
				mostrar_datos("rtspSecond", arrayCamaras[i].rtspSecondCamara, operacionSQL);
				mostrar_datos("resolutionSecond", arrayCamaras[i].resolutionSecondCamara, operacionSQL);
				mostrar_datos("kbpsSecond", arrayCamaras[i].kbpsSecondCamara, operacionSQL);
				// mostrar_datos("idClienteSeleccion", arrayCamaras[i].propietarioCamara, operacionSQL);	
				mostrar_datos("propietarioCamara", arrayCamaras[i].nomPropietario, operacionSQL);	
				mostrar_datos("fechaAlta", arrayCamaras[i].fecha_alta, operacionSQL);	
				mostrar_datos("fechaModificacion", arrayCamaras[i].fecha_mod, operacionSQL);	
				// insert_log_acciones(arrayCamaras[i]); // Comentado xq da un error " Undefined offset: 0" en  insert_log_acciones.php
			} // fin if
		} // fin for		
	}
	
	
	/************************************************************************************
		Select-option, datatable... depende de la opci�n que seleccione
	************************************************************************************/
	function select_camaras(nombreDiv, tipoObjeto){
		$.ajax({
			type: 'GET',
			url: 'io/camara/select_camara.php',
			data: { 	
				'tiempoDesfase' : desfaseHoraria()
			},
			success: function(response) {				
				arrayCamaras = $.parseJSON(response);
				
				switch(tipoObjeto){
					case 'libres': 	
						// Select-option formulario camaras	 - solo las que estan libres								
						select_camaras_selectOption(nombreDiv, 'libre');						
						break;

					case 'selectOption':
						// Select-option formulario camaras	 - todas			 		
						select_camaras_selectOption(nombreDiv, 'todo');
						break;
					
					case 'dataTable':
						// tabla de la pag 'consultar' camara 
						select_camaras_datatable(nombreDiv);
						break;
						
					case 'resumen_info_Dashboard':
						// pag. dashboard - bloque 'informacion - apartado camaras'
						select_camaras_resumen_info_dashboard(nombreDiv);
					break;				
				
					case 'tabla_ultimos':
						// pag. dashboard  - bloque '10 ultimas altas'
						select_camaras_tabla_ultimas(nombreDiv);
						break;
				
				}//fin switch
			}//fin success
		});
	}	/* fin funcion */	

	/************************************************************************************
		Select option camaras 
	************************************************************************************/
	function select_camaras_selectOption(nombreDiv, opcion){
		var html_selectOption ="";
		html_selectOption += "<select class='form-control' id='idCamara' >";
			html_selectOption += "<option value='-1' selected='selected' name='defaultCamara'> "+txtSelectOptionDefault+" </option>";
			for(var i=0; i < arrayCamaras.length; i++){
				
				// solo mostramos camaras libres
				if(opcion == 'libre'){ 
					if(arrayCamaras[i].asignada==0)
					html_selectOption += optionSelect_bloques(arrayCamaras[i].idCamara, arrayCamaras[i].nameCamara, arrayCamaras[i].asignada, i);
				}
				
				// Mostramos todas las camaras
				else{
					html_selectOption += optionSelect_bloques(arrayCamaras[i].idCamara, arrayCamaras[i].nameCamara, arrayCamaras[i].asignada, i);
				}
				
			} // fin for
		html_selectOption += "</select>";
		cargandoDatos("idCamara", 'end', 'seleccion');
		$("#selectOption_idCamara").html(html_selectOption);
	}
	
	/************************************************************************************
		Tabla de la pag 'consultar' camara 
	************************************************************************************/
	function select_camaras_datatable(nombreDiv){		
		// thead
		var thTabla ='<tr>';
			thTabla +='<th class="th_turquesa">Id</th>';
			thTabla +='<th class="th_turquesa">Nombre</th>';
			thTabla +='<th class="th_turquesa">Tipo</th>';
			thTabla +='<th class="th_turquesa">Num. Serial</th>';
			thTabla +='<th class="th_turquesa">Mac</th>';
			//	thTabla +='<th class="th_turquesa">User</th>';
			//thTabla +='<th class="th_turquesa">Pass</th>';
			thTabla +='<th class="th_turquesa">Ip local</th>';
			thTabla +='<th class="th_turquesa">RTSP Main</th>';
			thTabla +='<th class="th_turquesa">Resoluci'+min_o+'n Main</th>';
			thTabla +='<th class="th_turquesa">KBPS Main</th>';
			thTabla +='<th class="th_turquesa">RTSP Second</th>';
			thTabla +='<th class="th_turquesa">Resoluci'+min_o+'n Second</th>';
			thTabla +='<th class="th_turquesa">KBPS Second</th>';
			thTabla +='<th class="th_turquesa">Propietario</th>';
		thTabla +='</tr>';
		
		//	tbody
		var tdTabla = '';
		for(var i=1; i < arrayCamaras.length; i++){
			tdTabla +=' <tr>';
				tdTabla +=' <td> '+ validarNulo(arrayCamaras[i].idCamara) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayCamaras[i].nameCamara) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayCamaras[i].idTipoCamara) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayCamaras[i].serialnumCamara) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayCamaras[i].macCamara) +'</td>';
				//tdTabla +=' <td> '+ validarNulo(arrayCamaras[i].userCamara) +'</td>';
				//tdTabla +=' <td> '+ validarNulo(arrayCamaras[i].passCamara) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayCamaras[i].localipCamara) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayCamaras[i].rtspMainCamara) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayCamaras[i].resolutionMainCamara) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayCamaras[i].kbpsMainCamara) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayCamaras[i].rtspSecondCamara) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayCamaras[i].resolutionSecondCamara) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayCamaras[i].kbpsSecondCamara) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayCamaras[i].propietarioCamara) +'</td>';							
			tdTabla +='</tr>';
		} // fin for
		
		// Montamos la tabla
		montarTabla(nombreDiv, thTabla, tdTabla, thTabla, "contenido");
	}
	
	/************************************************************************************
		Pag. dashboard - bloque 'informacion - apartado camaras'
	*************************************************************************************/
	function select_camaras_resumen_info_dashboard(nombreDiv){
		// Controlamos si no hay camaras dadas de alta
		var camLibres = 0;
		var camAsignadas = 0;
	
		// hacemos recuento de las camaras (libre = 0; ocupada =1)
		for(var i=0; i < arrayCamaras.length; i++){
			if(arrayCamaras[i].asignada == 0){
				 camLibres++;
			}else{	
				camAsignadas++;
			}
		}
				
		var tabla_dashboard_info = datatable_dashboard_info(arrayCamaras.length, camLibres, camAsignadas, 'Total', 'Libres', 'Asignadas', nombreDiv);
		cargandoDatos("resumen_Dashboard_camara","end" , 'seleccion');  // Quito el 'cargando datos.. '
		$("#resumen_Dashboard_camara").html(tabla_dashboard_info);
	}
	
	/************************************************************************************
		Pag. dashboard  - bloque '10 ultimas altas'
	*************************************************************************************/
	function select_camaras_tabla_ultimas(nombreDiv){		
		// thead
		var thTabla ='<tr>';
			thTabla +='<th class="th_turquesa">Id</th>';
			thTabla +='<th class="th_turquesa">Nombre</th>';
			thTabla +='<th class="th_turquesa">Numero Serial</th>';
			thTabla +='<th class="th_turquesa">Propietario</th>';
		thTabla +='</tr>';
		
		// tBody			
		var tdTabla = '';
		for(var i=0; i < arrayCamaras.length; i++){
			tdTabla +=' <tr>';
				tdTabla +=' <td> '+ validarNulo(arrayCamaras[i].idCamara) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayCamaras[i].nameCamara) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayCamaras[i].serialnumCamara) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayCamaras[i].nomPropietario) +'</td>';
			tdTabla +='</tr>';
		}
			
		// Montamos la tabla
		montarTabla(nombreDiv, thTabla, tdTabla, thTabla, "ultimas_Camaras");
	}
	
	

/************************************************************************************
#
# TABLA CTA_EMPRESA
#
************************************************************************************/	
	
	/************************************************************************************
		Validacion de los datos de la cuenta bancaria del cliente / empresa
		Comprobamos que no hay campos vacio.
		En caso contrario mostramos un mensaje informativo.
	************************************************************************************/
	
	var titularCuenta= "";
	var iban = "";
	var swift =  "";
	var entidad = "";
	var sucursal = ""; // Se compone de 4 digitos
	var dc = ""; // Se compone de 2 digitos				
	var numCuenta = "";
	function validar_datos_cta(operacion, tipoCount){
		console.log("validar_datos_cta(" + operacion + " , " +  tipoCount);
		validar_div_visible("successMsg", "hide");
				
		if(tipoCount=='cta'){ 	
			countCta=0;	
		}
		var countTotal=0;
		
			
		//Solo necesitamos validar estos datos cuando:
		if((tipoCount=='cliente' && operacion=='insert') || (tipoCount=='cta')){			
			
			titularCuenta =  validar_input("titularCuenta", tipoCount);	
			console.log("titularCuenta: " + titularCuenta);
			iban = validar_digitos("iban", tipoCount, 24, ""); // Se compone de 24 digitos
			console.log("iban: " + iban);
			if(iban){countTotal++;}
			swift = validar_digitos("swift", tipoCount, 11, ""); // Se compone de 11 digitos
			console.log("swift: " + swift);
			if(swift){countTotal++;}	
			
			if(!iban && !swift){
				
				// Numero entidad de la cuenta bancaria		
				entidad = validar_digitos("entidad", tipoCount, 4, ""); // Se compone de 4 digitos
				console.log("entidad: " + entidad);
				if(entidad){countTotal++;}	
				sucursal = validar_digitos("sucursal", tipoCount, 4, ""); // Se compone de 4 digitos
				console.log("sucursal: " + sucursal);
				if(sucursal){countTotal++;}	
				console.log("sucursal: " + sucursal);
				dc = validar_digitos("dc", tipoCount, 2, ""); // Se compone de 2 digitos		
				console.log("dc: " + dc);		
				if(dc){countTotal++;}	
				numCuenta = validar_digitos("numCuenta", tipoCount, 10, ""); // Se compone de 10 digitos
				console.log("numCuenta: " + numCuenta);
				if(numCuenta){countTotal++;}	
			}
			
			// console.log(entidad + " - " + sucursal + " - " +dc+ " - " + numCuenta + " - " + iban + " - " + swift);
			// console.log("countCta: " + countCta);
			
			//  Compruebo que hay algun campo lleno
			if(countTotal == 0 ) {				
				// fuerzo para esconder el mensaje de error
				$('.tipoCuenta .bg-danger').hide(); 
						
				// Muestro mensaje general de que algo tiene que rellenar
				$("#errorMsg_swift").show();
				$("#errorMsg_swift").html("Tienes que rellenar uno de los campo, IBAN, SWIFT o CCC que se compone por ENTIDAD + SUCURSAL +DC + N.CUENTA. Comprueba que has introducidos correctamente los datos ");	
			}
		}
		
		// Si estamos en la funcion porque hay que cambiar los datos: 
		if(tipoCount=='cta'){
			switch(operacion){
				case 'update': // modificar (la diferencia con el resto es que aqui hay que a�adir campos de validacion
					if((countCta==2) || (countCta==5)){ //2 validaciones cuando se trata de titular y iban o swift y 5 validaciones cuando se trata 
						var idClienteSelect =  $("#idCliente2").val(); 				
						var fechaA =  $("#cta_fechaAlta").val(); 				
						var fechaM =  $("#cta_fechaModificacion").val(); 				
						
						// A la que tenga tiempo 1. Validar el iban, swfit y n� cuenta. 2.A partir de uno de ellos sacar el resto.
						modificar_datos_cta(idClienteSelect, titularCuenta , iban , swift,  entidad, sucursal, dc , numCuenta, fechaA, fechaM);
					}
					break;		
			}
		}
		//selecciona_opcion(operacion, countCta);
	}
	
	
	/************************************************************************************
		Insertamos los datos en la base de datos eude tabla empresas
	************************************************************************************/
	function modificar_datos_cta(idClienteSelect, titularCuenta , iban , swift,  entidad, sucursal, dc , numCuenta, fechaA, fechaM){
		$.ajax({
			type: 'GET',
			url: 'io/empresa/update_cta_cliente.php',
			data: { 
				'idClienteSelect': idClienteSelect,
				'titularCuenta':titularCuenta,	
				'swift':swift,				
				'iban':iban,				
				'entidad':entidad,				
				'sucursal':sucursal,				
				'dc':dc,				
				'numCuenta':numCuenta,		
				'fechaA':fechaA,		
				'fechaM':fechaM		
			},
			success: function(response) {
				var arrayResult = $.parseJSON(response);
				
				despuesDeGuardar(arrayResult, 0);
			}
		});
	}
	
	
	
/************************************************************************************
#
# TABLA EMPRESA
#
************************************************************************************/

	/************************************************************************************
		Validar datos introducidos en el formulario
	************************************************************************************/
	function validar_datos(operacion){
		validar_div_visible("successMsg" , "hide");
		count=0;
		var tipoCount="cliente";
			
		// razon social(1)	
		var razonSocial =  validar_input('razonSocial',  tipoCount);	
	
		// NIF - DNI - CIF (2)	
		var dni = validar_input('dni',  tipoCount); 
		
		// Nombre empresa - subdominio (3)	
		var empresa = validar_input('nombreEmpresa',  tipoCount); 
			
		// ipEmpresa (4)
		var ipServer = validar_input('ipEmpresa',  tipoCount); 	
		
		// puertoEmpresa (5)
		var puertoServer =  validar_input('puertoEmpresa',  tipoCount); 		
		
		// Email del usuario (6)
		var email = validar_email('email',  tipoCount); 
		
		// clave para la plataforma (7)
		var pass = validar_input('password',  tipoCount); 
		
		// Direcci�n (8)
		var direccion = validar_input('direccion',  tipoCount); 
			
		// Poblacion (9)
		var poblacion = validar_input('poblacion',  tipoCount); 
			
		// Provincia (10)
		var provincia = validar_selectOption("idProvincias", tipoCount);
		
		// Pais (11)
		var pais = validar_selectOption("idPais", tipoCount);
			
		// Codigo postal (12)
		var cp = validar_digitos("cp", tipoCount, 5, ""); // Se compone de 5 digitos
				
		// Fecha solicitud del alta (13)
		var fechaSolicitudAlta = validar_input('fechaSolicitudAlta',  tipoCount);  
		
		//Telefono fijo (14)
		var tlf_fijo = validar_digitos("tlf_fijo", tipoCount, 9, 9); // Se compone de 9 digitos
		// smsError_firstD = "Este n"+min_u+"mero de tel"+min_e+"fono no corresponde a un n"+min_u+"mero fijo, empiezan por 9, reviselo por favor.";
		// smsError_lengthD =" El tel"+min_e+"fono fijo se compone de 9 d"+min_i+"gitos";
				
		// Telefono movil (15)		
		var tlf_movil =  validar_digitos("tlf_movil", tipoCount, 9, 6);
		//smsError_firstD = "Este n"+min_u+"mero de tel"+min_e+"fono no corresponde a un n"+min_u+"mero m"+min_o+"vil, empiezan por 6, reviselo por favor.";
		//smsError_lengthD =" El tel"+min_e+"fono m"+min_o+"vil se compone de 9 d"+min_i+"gitos";
				
		// Video Analisis (16)
		var videoAnalisis = validar_selectOption("videoAnalisis", tipoCount);
	
		// tipoNumBanco (17)
		var tipoNumBanco = validar_selectOption("tipoNumBanco", tipoCount);
				
		// llamamos a la funci�n que se encarga de la validacion de los datos bancarios
		validar_datos_cta(operacion, tipoCount);
		
		
		// Solo en caso de modificar, en el insert no se metera
		if(operacion=='update'){
			
			// Estado cliente Activo=1, desactivo=0
			var estadoCliente =  validar_selectOption("selectEstado", tipoCount);			
			//	smsError_nulo = " * Campo Obligatorio, introduce un '1' si la cuenta esta activa o '0' si esta desactiva ";
				
			var prefixCliente = validar_input('prefixCliente',  tipoCount);  		
			var serverCliente = validar_input('serverCliente',  tipoCount);  					
			var streamAPPCliente = validar_input('streamAPPCliente',  tipoCount);  
			var dominioCliente = validar_input('dominioCliente',  tipoCount);  				
			var subdominioCliente = validar_input('subdominioCliente',  tipoCount);
			var connCliente = validar_input('connCliente',  tipoCount);  
		}
		
		// Conseguimos lat y lng de la empresa
		var address = direccion + ", " + poblacion + ", " + provincia + ", " + cp;
		var geocoder = new google.maps.Geocoder();
		
		geocoder.geocode( { 'address': address}, function(results, status){
			if (status == google.maps.GeocoderStatus.OK) {	
				//addressFinal_geocoder = results[0].formatted_address;
				latEmpresa = results[0].geometry.location.lat();
				lngEmpresa = results[0].geometry.location.lng();
			}	
		});	
		
		
		switch(operacion){
			case 'insert': 
				if(count>16){  //16 validaciones cuando en datos bancarios ponen iban o swift, 19 cuando ponen ccc = (entidad+sucursal+dc+numCuenta)		
					// Guardando datos
					cargandoDatos("guardandoDatos", 'start', operacion); //ok
					/*	
					console.log("(insert) -- razonSocial: " + razonSocial);
					console.log("(insert) -- dni: " + dni);
					console.log("(insert) -- empresa: " + empresa);
					console.log("(insert) -- email: " + email);
					console.log("(insert) -- ipServer: " + ipServer);
					console.log("(insert) -- puertoServer: " + puertoServer);
					console.log("(insert) -- pass: " + pass);
					console.log("(insert) -- direccion: " + direccion);
					console.log("(insert) -- poblacion: " + poblacion);
					console.log("(insert) -- provincia: " + provincia);
					console.log("(insert) -- cp: " + cp);
					console.log("(insert) -- fechaSolicitudAlta: " + fechaSolicitudAlta);
					console.log("(insert) -- tlf_movil: " + tlf_movil);
					console.log("(insert) -- titularCuenta: " + titularCuenta);
					console.log("(insert) -- entidad: " + entidad);
					console.log("(insert) -- sucursal: " + sucursal);
					console.log("(insert) -- dc: " + dc);
					console.log("(insert) -- numCuenta: " + numCuenta);
					console.log("(insert) -- iban: " + iban);
					console.log("(insert) -- swift: " + swift);
					console.log("(insert) -- latEmpresa: " + latEmpresa);
					console.log("(insert) -- lngEmpresa: " + lngEmpresa);
					console.log("(insert) -- videoAnalisis: " + videoAnalisis);
					console.log("(insert) -- pais: " + pais);
					*/
					 insertar_datos(razonSocial,dni, empresa, email,  ipServer, puertoServer,pass,  direccion, poblacion,provincia, cp, fechaSolicitudAlta,tlf_fijo,tlf_movil, titularCuenta, entidad, sucursal, dc, numCuenta, iban, swift,  latEmpresa,lngEmpresa, videoAnalisis, pais ); 
				}
				break;
				
			case 'update': //modificar (la diferencia con el resto es que aqui hay que a�adir campos de validacion
				//if(count>18){ //validaciones tiene que hacer
					// Guardando datos
					cargandoDatos("guardandoDatos", 'start', operacion); //ok
					var idClienteSelect =  $("#idCliente2").val(); 							
					modificar_datos(idClienteSelect, dominioCliente, razonSocial, dni, email, direccion, poblacion, provincia, cp, pais, tlf_fijo, tlf_movil,  ipServer, puertoServer, connCliente, videoAnalisis);						
				//}
				break;
		}
			
	} // fin funcion
	

	
	/************************************************************************************
		Insertamos los datos en 'eude.empresas'
	#tarea --> FAlta pasar esta parte a msqli.... y 
	************************************************************************************/
	function insertar_datos(razonSocial, dni, empresa, email,  ipServer, puertoServer,pass,  direccion, poblacion,provincia, cp, fechaSolicitudAlta,tlf_fijo,tlf_movil, titularCuenta, entidad, sucursal, dc, numCuenta, iban,swift, latEmpresa,lngEmpresa, videoAnalisis, pais ){
		
	console.log("insertar_datos("+razonSocial+" , "+ dni +" , "+ empresa+" , "+ email+" , "+  ipServer+" , "+ puertoServer +" , "+ pass+" , "+  direccion+" , "+ poblacion+" , "+ provincia+" , "+  cp +" , "+ fechaSolicitudAlta +" , "+ tlf_fijo +" , "+ tlf_movil +" , "+ titularCuenta +" , "+ entidad +" , "+ sucursal +" , "+ dc +" , "+ numCuenta +" , "+ iban +" , "+ swift+" , "+ latEmpresa +" , "+ lngEmpresa +" , "+ videoAnalisis +" , "+ pais +" )");
	
		$.ajax({
			type: 'GET',
			url: 'io/empresa/insert_cliente.php',
			data: { 
				'razonSocial':razonSocial,
				'dni': dni,
				'empresa': empresa,
				'email': email,
				'ipServer': ipServer,
				'puertoServer': puertoServer,			
				'pass': pass,
				'direccion': direccion,
				'poblacion': poblacion,
				'provincia': provincia,
				'cp': cp,
				'fechaSolicitudAlta': fechaSolicitudAlta,
				'tlf_fijo': tlf_fijo,
				'tlf_movil': tlf_movil,
				'titularCuenta':titularCuenta,				
				'entidad':entidad,				
				'sucursal':sucursal,				
				'dc':dc,				
				'numCuenta':numCuenta,	
				'iban':iban,				
				'swift':swift,
				'lat':latEmpresa,
				'lng':lngEmpresa,
				'tiempoDesfase' : desfaseHoraria(),  			
				'videoAnalisis' : videoAnalisis,			
				'pais' : pais			
			},
			success: function(response) {
				var arrayResult = $.parseJSON(response);
				despuesDeGuardar(arrayResult, 1);		//El 1 es que la ruta cambia a 'enviarEmail_insert_cliente.php', el resto (0) van al mismo 'enviarEmail.php' 
			}
		});
	}
	
	
	
	/************************************************************************************
		Modificamos los datos 'eude.empresas'
	************************************************************************************/
	function  modificar_datos(idClienteSelect,dominioCliente, razonSocial, dni, email, direccion, poblacion, provincia, cp, pais, tlf_fijo, tlf_movil,  ipServer, puertoServer, connCliente, videoAnalisis){
		
		$.ajax({
			type: 'GET',
			url: 'io/empresa/update_cliente.php',
			data: { 
				'idClienteSelect':idClienteSelect,
				'razonSocial': razonSocial,
				'dominio': dominioCliente,
				'dni': dni,
				'email': email,
				'direccion': direccion,
				'poblacion': poblacion,
				'provincia': provincia,
				'idPais': pais,
				'cp': cp,
				'tlf_fijo': tlf_fijo,
				'tlf_movil': tlf_movil,
				'ipServer': ipServer,
				'puertoServer': puertoServer,
				'conn': connCliente, 
				'videoAnalisis': videoAnalisis, 
				'nombreProvincia': $("#idProvincias option[value='"+$("#idProvincias").val()+"']").text() ,
				'tiempoDesfase' : desfaseHoraria() 				
			},			
			success: function(response) {
				var arrayResult = $.parseJSON(response);
				despuesDeGuardar(arrayResult, 0);
			}
		});
		
		
	}
	
	
	
	/************************************************************************************
		Mostramos los datos que nos han solicitado de la tabla 'eude.empresas'
	************************************************************************************/
	function mostrar_datos_cliente(idCliente_selecionado, operacionSQL){
		
		for(var i=0; i < arrayCliente.length; i++){ 
			if(arrayCliente[i].idCliente == idCliente_selecionado){
				// console.log(arrayCliente[i]);
				mostrar_datos("idCliente2", arrayCliente[i].idCliente, operacionSQL);
				mostrar_datos("nombreEmpresa", arrayCliente[i].nombreCliente, operacionSQL);
				mostrar_datos("email", arrayCliente[i].emailCliente, operacionSQL);
				mostrar_datos("password", arrayCliente[i].passCliente, operacionSQL);			
				mostrar_datos("direccion", arrayCliente[i].direccionCliente, operacionSQL);	
				mostrar_datos("poblacion", arrayCliente[i].poblacionCliente, operacionSQL);
				mostrar_datos("idProvincias", arrayCliente[i].provinciaCliente, operacionSQL);
				mostrar_datos("idPais", arrayCliente[i].idPais, operacionSQL);
				mostrar_datos("cp", arrayCliente[i].cpCliente, operacionSQL);
				mostrar_datos("fechaSolicitudAlta", arrayCliente[i].fechaSolicitudAlta, operacionSQL);		
				mostrar_datos("fechaAlta", arrayCliente[i].fechaAltaCliente, operacionSQL);	
				mostrar_datos("fechaModCliente", arrayCliente[i].fechaModCliente, operacionSQL);	
				mostrar_datos("tlf_fijo", arrayCliente[i].telefonoFijo, operacionSQL);
				mostrar_datos("tlf_movil", arrayCliente[i].telefonoMovil, operacionSQL);
				mostrar_datos("razonSocial", arrayCliente[i].razonSocialCliente, operacionSQL);
				mostrar_datos("dni", arrayCliente[i].cifCliente, operacionSQL);
				mostrar_datos("titularCuenta", arrayCliente[i].titularCuenta, operacionSQL);
				mostrar_datos("iban", arrayCliente[i].iban, operacionSQL);
				mostrar_datos("swift", arrayCliente[i].swift, operacionSQL);
				mostrar_datos("entidad", arrayCliente[i].entidad, operacionSQL);
				mostrar_datos("sucursal", arrayCliente[i].sucursal, operacionSQL);
				mostrar_datos("dc", arrayCliente[i].dc, operacionSQL);
				mostrar_datos("numCuenta", arrayCliente[i].numCuenta, operacionSQL);
				mostrar_datos("selectEstado", arrayCliente[i].estadoCliente, operacionSQL);
				mostrar_datos("videoAnalisis", arrayCliente[i].videoAnalisis, operacionSQL);
				mostrar_datos("nombreBD", arrayCliente[i].bdCliente, operacionSQL);
				mostrar_datos("prefixCliente", arrayCliente[i].prefixstreamCliente, operacionSQL);
				mostrar_datos("serverCliente", arrayCliente[i].serverCliente, operacionSQL);
				mostrar_datos("streamAPPCliente", arrayCliente[i].streamappCliente, operacionSQL);
				mostrar_datos("dominioCliente", arrayCliente[i].dominioCliente, operacionSQL);	
				mostrar_datos("subdominioCliente", arrayCliente[i].subdominioCliente, operacionSQL);
				mostrar_datos("connCliente", arrayCliente[i].connCliente, operacionSQL);	
				mostrar_datos("numModulosContratados", arrayCliente[i].licencias, operacionSQL);
				mostrar_datos("puertoEmpresa", arrayCliente[i].puertoServer, operacionSQL);
				mostrar_datos("ipEmpresa", arrayCliente[i].ipServer, operacionSQL);
				mostrar_datos("cta_fechaAlta", arrayCliente[i].cta_fechaA, operacionSQL);
				mostrar_datos("cta_fechaModificacion", arrayCliente[i].cta_fechaM, operacionSQL);
				insert_log_acciones(arrayCliente[i]);	
			} // fin if
		} // fin for
	}

	
	/************************************************************************************
		Select de los clientes que tenemos.
	************************************************************************************/
	function select_cliente(nombreDiv, tipoObjeto, fichero){	
		// console.log("select_cliente: " + nombreDiv + " , " + tipoObjeto + " , " + fichero);	
		$.ajax({
			type: 'GET',
			url: 'io/empresa/select_cliente.php',
			data: { 
				'tiempoDesfase' : desfaseHoraria()
			},
			success: function(response) {			
				arrayCliente = $.parseJSON(response);				
				
				switch(tipoObjeto){
					case 'selectOption':
						// Select-option todos los clientes
						select_cliente_selectOption(nombreDiv, fichero); 						
						break;
					
					case 'dataTable':
						// tabla de la pag 'consultar' clientes 
						select_cliente_datatable(nombreDiv);
						break;
					
					case 'resumen_info_Dashboard':
						// pag. dashboard - bloque 'informacion - apartado clientes'
						select_cliente_resumen_info_dashboard(nombreDiv);
						break;
					
				
					case 'tabla_ultimos':
						// pag. dashboard  - bloque '10 ultimas altas'
						select_cliente_tabla_ultimas(nombreDiv);
						break;
				}//fin switch
			}//fin success			
		});
	}	// fin funcion 

	
	// Select-option formulario clientes	
	function select_cliente_selectOption(nombreDiv, fichero){
		var html_selectOption ="";
		html_selectOption += "<select class='form-control' id='idClienteSeleccion' >";
			html_selectOption += "<option value='-1' selected='selected' name='defaultClient'> "+txtSelectOptionDefault+"</option>";
			
			if(fichero != "cliente"){					
				html_selectOption += "<option value='0'  name='noClient'> 0 - Ning"+min_u+"n cliente </option>";
			}
							
			for(var i=0; i < arrayCliente.length; i++){
				html_selectOption += optionSelect_bloques(arrayCliente[i].idCliente, arrayCliente[i].nombreCliente, 1, i);
			} // fin for
		html_selectOption += "</select>";
		cargandoDatos("idClienteSeleccion", 'end', 'seleccion');
		$("#selectOption_idClienteSeleccion").html(html_selectOption);						
	}
	
	// tabla de la pag 'consultar' clientes 
	function select_cliente_datatable(nombreDiv){
		var  thTabla = "";
		thTabla +='<tr>';
			thTabla +='<th class="th_turquesa">Id</th>'; 
			thTabla +='<th class="th_turquesa">Subdominio</th>';
			thTabla +='<th class="th_turquesa">NIF</th>'; 
			thTabla +='<th class="th_turquesa">Email</th>'; 
			thTabla +='<th class="th_turquesa">T. Fijo</th>'; 
			thTabla +='<th class="th_turquesa">T. M'+min_o+'vil</th>'; 
			thTabla +='<th class="th_turquesa">Num. M'+min_o+'dulos</th>';
			thTabla +='<th class="th_turquesa">Estado</th>';
			thTabla +='<th class="th_turquesa">Fecha Alta</th>'; 
		thTabla +='</tr>';
				
		// tbody		
		var tdTabla =' ';
		for(var i=0; i < arrayCliente.length; i++){			
			tdTabla +=' <tr>';
				tdTabla +=' <td> '+ validarNulo(arrayCliente[i].idCliente) +'</td>'; 
				tdTabla +=' <td> '+ validarNulo(arrayCliente[i].subdominioCliente) +'</td>'; 
				tdTabla +=' <td> '+ validarNulo(arrayCliente[i].cifCliente) +'</td>'; 		
				tdTabla +=' <td> '+ validarNulo(arrayCliente[i].emailCliente) +'</td>'; 
				tdTabla +=' <td> '+ validarNulo(arrayCliente[i].telefonoFijo) +'</td>';									
				tdTabla +=' <td> '+ validarNulo(arrayCliente[i].telefonoMovil) +'</td>';								
				tdTabla +=' <td> '+ validarNulo(arrayCliente[i].licencias) +'</td>'; 		
				tdTabla +=' <td> '+ validarEstado(validarNulo(arrayCliente[i].estadoCliente), 'cliente') +'</td>'; 
				tdTabla +=' <td> '+ validarNulo(arrayCliente[i].fechaAltaCliente) +'</td>'; 													
			tdTabla +='</tr>';
		} // fin for
			
		// Monto la tabla
		montarTabla(nombreDiv, thTabla, tdTabla, thTabla, "contenido");
	}
	
	// pag. dashboard - bloque 'informacion - apartado clientes'
	function select_cliente_resumen_info_dashboard(nombreDiv){
		var altasEmpresas = 0;
		var bajasEmpresas =0;
		
		// Contamos clientes dados de alta - baja
		for(var i=0; i < arrayCliente.length; i++){
			if( arrayCliente[i].estadoCliente == 0){
				bajasEmpresas++;
			}else{
				altasEmpresas++;
			}
		}
						
		var tabla_dashboard_info = datatable_dashboard_info(arrayCliente.length, altasEmpresas, bajasEmpresas,  'Total', 'Dados de alta', 'Dados de baja', nombreDiv);
		cargandoDatos("resumen_Dashboard_cliente","end", 'seleccion');  // Quito el 'cargando datos.. '
		$("#resumen_Dashboard_cliente").html(tabla_dashboard_info);
	}
	
	// pag. dashboard  - bloque '10 ultimas altas'
	function select_cliente_tabla_ultimas(nombreDiv){
		var thTabla ='<tr>';
			thTabla +='<th class="th_turquesa">Id</th>';
			thTabla +='<th class="th_turquesa">Estado</th>';
			thTabla +='<th class="th_turquesa">Razon Social</th>';
			thTabla +='<th class="th_turquesa"> NIF </th>';
			thTabla +='<th class="th_turquesa">Subdominio</th>';
			thTabla +='<th class="th_turquesa">Fecha Alta</th>';
		thTabla +='</tr>';
					
		// Monto la tabla 
		var tdTabla ="";					
		for(var i=0; i < arrayCliente.length; i++){						
			tdTabla +=' <tr>';
				tdTabla +=' <td> '+ validarNulo(arrayCliente[i].idCliente) +'</td>';  
				tdTabla +=' <td> '+ validarNulo(arrayCliente[i].estadoCliente) +'</td>'; 
				tdTabla +=' <td> '+ validarNulo(arrayCliente[i].razonSocialCliente) +'</td>'; 
				tdTabla +=' <td> '+ validarNulo(arrayCliente[i].cifCliente) +'</td>';  
				tdTabla +=' <td> '+ validarNulo(arrayCliente[i].subdominioCliente) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayCliente[i].fechaAltaCliente) +'</td>';  
			tdTabla +='</tr>';
		}								
		
		// Monto la tabla
		
		montarTabla(nombreDiv, thTabla, tdTabla, thTabla, "ultimos_Clientes");
	}
	
	
/************************************************************************************
#
# TABLA IMPUESTOS
#
************************************************************************************/
	/************************************************************************************
		validamos los datos introducidos en el formulario
	************************************************************************************/
	function validar_datos_impuestos(operacion){
		
		validar_div_visible("successMsg" , "hide");
		var tipoCount="impuestos";
		countImpuestos=0;
		

		// Nombre del tipo de impuesto
		var nombre = validar_input('nombreImpuesto',  tipoCount);   	
			
		// Descripci�n del impuesto
		var descripcion = validar_input('descripcionImpuesto',  tipoCount);   	
			
		// Valor del impuesto
		var valor =  validar_input('valorImpuesto',  tipoCount);   	
	
		//selecciona_opcion(operacion, countCamara);		
		if(countImpuestos==3){
			// Guardando datos
			cargandoDatos("guardandoDatos", 'start', operacion); //ok
			
			//Lo mandamos a la operacion que le corresponda
			switch(operacion){
				case 'insert': //alta
					insertar_datos_impuestos(nombre, descripcion, valor);
					break;
					
				case 'update': //modificar
					idSelect =  $("#idImpuesto2").val(); 
					modificar_datos_impuestos(nombre, descripcion, valor, idSelect);
					break;
			}
		}
	}		
	
	
	/************************************************************************************
		Insertamos los datos en la base de datos eude tabla tipoProducto
	************************************************************************************/
	function insertar_datos_impuestos(nombre, descripcion, valor){
		$.ajax({
			type: 'GET',
			url: 'io/impuestos/insert_impuestos.php',
			data: { 
				'nombre' : nombre,
				'descripcion' : descripcion,
				'valor' : valor,
				'tiempoDesfase' : desfaseHoraria() 
			},
			success: function(response) {
				var arrayResult = $.parseJSON(response);
				despuesDeGuardar(arrayResult, 0);		
			}
		});
	}
	
	
	/************************************************************************************
		Modificamos los datos en la base de datos eude tabla sim
	************************************************************************************/
	function modificar_datos_impuestos(nombre, descripcion, valor, idSelect){
		$.ajax({
			type: 'GET',
			url: 'io/impuestos/update_impuestos.php',
			data: { 
				'idSelect' : idSelect,
				'nombre' : nombre,
				'descripcion' : descripcion,
				'valor' : valor,
				'tiempoDesfase' : desfaseHoraria() 
			},
			success: function(response) {
				var arrayResult = $.parseJSON(response);
				despuesDeGuardar(arrayResult, 0);
			}
		});
	}
	
	
	/************************************************************************************
		Mostramos los datos que nos han solicitado en 'consultar sim'
	************************************************************************************/
	function mostrar_datos_impuestos(idSelecionado, operacionSQL){
		for(var i = 0; i < arrayImpuestos.length; i++){
			if(arrayImpuestos[i].id == idSelecionado){
				mostrar_datos("idImpuesto2", arrayImpuestos[i].id, operacionSQL);
				mostrar_datos("nombreImpuesto", arrayImpuestos[i].nombre, operacionSQL);
				mostrar_datos("descripcionImpuesto", arrayImpuestos[i].descripcion, operacionSQL);
				mostrar_datos("valorImpuesto", arrayImpuestos[i].valor, operacionSQL);				
				mostrar_datos("fechaAlta", arrayImpuestos[i].fecha_alta, operacionSQL);
				mostrar_datos("fechaModificacion", arrayImpuestos[i].fecha_mod, operacionSQL);
				insert_log_acciones(arrayImpuestos[i]);		
			}
		}
	}
	
	
	/************************************************************************************
		Select-option y datatable de la sim
	************************************************************************************/
	function select_impuestos(nombreDiv, tipoObjeto){
		$.ajax({
			type: 'GET',
			url: 'io/impuestos/select_impuestos.php',
			data: {	
				'tiempoDesfase' : desfaseHoraria()  
			},
			success: function(response) {
				arrayImpuestos = $.parseJSON(response);
				
				switch(tipoObjeto){
					case 'selectOption':
						// Select-option formulario 	
						select_impuestos_selectOption(nombreDiv);
						break;
					
					case 'dataTable':
						// tabla de la pag 'consultar' impuestos 
						select_impuestos_dataTable(nombreDiv);
						break;

				}//fin switch
			}//fin success
		});
	}
	
	// Select-option formulario 	
	function select_impuestos_selectOption(nombreDiv){	
		var html_selectOption ="";
		html_selectOption += "<select class='form-control' id='"+nombreDiv+"' >";
			html_selectOption += "<option value='-1' selected='selected'> "+ txtSelectOptionDefault +" </option>";
			for(var i=0; i <  arrayImpuestos.length; i++){
				html_selectOption += optionSelect_bloques(arrayImpuestos[i].id, arrayImpuestos[i].nombre, 1 , i);
			} // fin for
		html_selectOption += "</select>";
		cargandoDatos("idImpuestosSelect", 'end', 'seleccion');
		$("#selectOption_idImpuestosSelect").html(html_selectOption);
	}
	
	// tabla de la pag 'consultar' impuestos 
	function select_impuestos_dataTable(nombreDiv){
		// thead
		var  thTabla = '<tr>';
			thTabla +='<th class="th_turquesa">Identificador</th>';
			thTabla +='<th class="th_turquesa">Nombre</th>';
			thTabla +='<th class="th_turquesa">Valor</th>';
			thTabla +='<th class="th_turquesa">Descripci'+min_o+'n</th>';						
			thTabla +='<th class="th_turquesa">F. Alta</th>';
			thTabla +='<th class="th_turquesa">F. Modificaci'+min_o+'n</th>';
		thTabla +='</tr>';
				
		// tbody
		var tdTabla ="";
		for(var i=0; i <  arrayImpuestos.length; i++){
			tdTabla +=' <tr>';
				tdTabla +=' <td> '+ validarNulo(arrayImpuestos[i].id) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayImpuestos[i].nombre) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayImpuestos[i].valor)+'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayImpuestos[i].descripcion) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayImpuestos[i].fecha_alta) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayImpuestos[i].fecha_mod) +'</td>';
			tdTabla +='</tr>';
		}
							
		// Monto la tabla
		montarTabla(nombreDiv, thTabla, tdTabla, thTabla, "contenido");
	}


	
/************************************************************************************
#
# TABLA LOG_ACCIONES
#
************************************************************************************/

	/************************************************************************************
		Select-option y datatable de la sim
	************************************************************************************/
	function select_log_acciones(nombreDiv, tipoObjeto){
		$.ajax({
			type: 'GET',
			url: 'io/logAcciones/select_log_acciones.php',
			data: { 	
				'tiempoDesfase' : desfaseHoraria() 
			},
			success: function(response) {
				arraylogAcciones = $.parseJSON(response);
				var total = arraylogAcciones.length;
				
				switch(tipoObjeto){
					case 'selectOption':
						break;
					
					case 'dataTable':
						var  th_datatable = "";
						th_datatable +='<tr>';
							th_datatable +='<th class="th_turquesa">Id. Usuario</th>';
							th_datatable +='<th class="th_turquesa">Usuario</th>';
							th_datatable +='<th class="th_turquesa">Fichero Consultado</th>';
							th_datatable +='<th class="th_turquesa">Id. Registro consultado </th>';
							th_datatable +='<th class="th_turquesa">Acci'+min_o+'n</th>';					
							th_datatable +='<th class="th_turquesa">IP</th>';
							th_datatable +='<th class="th_turquesa">Fecha</th>';
							
						th_datatable +='</tr>';
				
						//datatable formulario router
						var html_datatable ="";
						html_datatable +='<table class="table table-bordered table-striped table-hover table-heading table-datatable" id="'+nombreDiv+'">';
							html_datatable +='<thead>';
								html_datatable += th_datatable;							
								html_datatable +='</thead>';
							html_datatable +='<tbody>';
							//bucle
							for(var i=0; i < total; i++){
							
					
								html_datatable +=' <tr>';
									html_datatable +=' <td> '+ validarNulo(arraylogAcciones[i].idUser) +'</td>';
									html_datatable +=' <td> '+ validarNulo(arraylogAcciones[i].nameUser) +'</td>';
									html_datatable +=' <td> '+ validarNulo(arraylogAcciones[i].fichero)+'</td>';
									html_datatable +=' <td> '+ validarNulo(arraylogAcciones[i].idAfectado) +'</td>';
									html_datatable +=' <td> '+ validarNulo(arraylogAcciones[i].accion) +'</td>';
									html_datatable +=' <td> '+ validarNulo(arraylogAcciones[i].ip) +'</td>';
									html_datatable +=' <td> '+ validarNulo(arraylogAcciones[i].fecha) +'</td>';
								html_datatable +='</tr>';
							}//fin bucle
							html_datatable +='</tbody>';
							html_datatable +='<tfoot>';
								html_datatable += th_datatable;
							html_datatable +='</tfoot>';
						html_datatable +='</table>';
						$("#contenido").html(html_datatable);
						
						break;

				}//fin switch
			}//fin success
		});
	}

	
	/************************************************************************************
		Insertamos en el log acciones backoffices
	************************************************************************************/
	function insert_log_acciones(arrayResult){
	
		$.ajax({
			type: 'GET',
			url: 'io/logAcciones/insert_log_acciones.php',
			data: { 
				'datosArray': JSON.stringify(arrayResult),
				'comprobar': 1,
				'tiempoDesfase' : desfaseHoraria() 
			},
			success: function(response) {}
		});
	}
	
/************************************************************************************
#
# TABLA LOG_BACKOFFICES
#
************************************************************************************/	

	/************************************************************************************
		Select-option y datatable de la sim
	************************************************************************************/
	function select_log_backoffices(nombreDiv, tipoObjeto){
		$.ajax({
			type: 'GET',
			url: 'io/logBackoffices/select_log_backoffices.php',
			data: { 	
				'tiempoDesfase' : desfaseHoraria() 
			},
			success: function(response) {
				arraylogBackoffices = $.parseJSON(response);
				
				
				switch(tipoObjeto){
					case 'selectOption':
						break;
					
					case 'dataTable':
						// thead
						var thTabla ='<tr>';
							thTabla +='<th class="th_turquesa">Id. Usuario</th>';
							thTabla +='<th class="th_turquesa">Usuario</th>';				
							thTabla +='<th class="th_turquesa">IP</th>';
							thTabla +='<th class="th_turquesa">Fecha</th>';
						thTabla +='</tr>';
				
						// tBody
						var tdTabla  = "";
						for(var i=0; i < arraylogBackoffices.length; i++){
							tdTabla +=' <tr>';
								tdTabla +=' <td> '+ validarNulo(arraylogBackoffices[i].idUser) +'</td>';
								tdTabla +=' <td> '+ validarNulo(arraylogBackoffices[i].nameUser) +'</td>';
								tdTabla +=' <td> '+ validarNulo(arraylogBackoffices[i].ip) +'</td>';
								tdTabla +=' <td> '+ validarNulo(arraylogBackoffices[i].fecha) +'</td>';
							tdTabla +='</tr>';
						} // fin for
						
						// Montamos la tabla
						montarTabla(nombreDiv, thTabla, tdTabla, thTabla, "contenido");
						break;

				}//fin switch
			}//fin success
		});
	}


/************************************************************************************
#
# TABLA PRODUCTOS
#
************************************************************************************/	

	/************************************************************************************
		Validar datos introducidos en el formulario
	************************************************************************************/
	function validar_datos_producto(operacion){
		console.log("validar_datos_producto: " + $('#idTipoProducto').val() );
		validar_div_visible("successMsg", "hide");
		var tipoCount="producto";
		countProducto=0;
		
		// Identificador del tipo de producto
		var idTipoProducto =  validar_selectOption( 'idTipoProducto',  tipoCount);
		
		// Nombre del producto
		var nombre = validar_input('nombreProducto',  tipoCount);   	
				
		// Descripci�n del producto
		var descripcion = validar_input('descripcionProducto',  tipoCount);  
	
		// idClienteSeleccion
		var idClienteSeleccion = validar_selectOption( 'idClienteSeleccion',  tipoCount);
		
		switch(idTipoProducto){
			case '1': 
				// Identificador de la camara, en caso que sea una caja
				var idCamara =  validar_selectOption( 'idCamara',  tipoCount);
			
				// Identificador del router, en caso que sea una caja
				var idRouter = validar_selectOption( 'idRouter',  tipoCount); 
						
				// Identificador de la tarjeta sim
				var idSim = validar_selectOption( 'idSim',  tipoCount); 	
				
				var  idDispositivoEmisor = "";
				var  usuario = "";
				var  clave = "";
				var email = "";
				if(countProducto >=5) var pasamos = 1;
				
				break;
			
			case '2': 
							
				// Usuario
				var usuario = validar_input( 'usuario',  tipoCount);
				
				// Clave
				var clave = validar_input( 'clave',  tipoCount);
				
				// Email
				var email = validar_input( 'email',  tipoCount);

				var  idCamara = "";
				var  idRouter = "";
				var  idSim = "";
				var  idDispositivoEmisor = "";		
				
				if(countProducto >=5) var pasamos = 1;				
				break;
			
			case '3':
				// Identificador de la tarjeta sim
				var idSim = validar_selectOption( 'idSim',  tipoCount); 	
				
				// Identificador de la tarjeta sim
				var idDispositivoEmisor = validar_selectOption( 'idDispositivoEmisor',  tipoCount); 	
				
				var idCamara = "";
				var idRouter = "";
				var usuario = "";
				var clave = "";
				var email = "";
				
				if(countProducto >=5) var pasamos = 1;
				
				break;
		}
		

		
		//selecciona_opcion(operacion, countProducto);
		if(pasamos == 1) {		console.log(" (entre) - CONT PRODUCTOS: " + countProducto);
			// Guardando datos
			cargandoDatos("guardandoDatos", 'start', operacion); //ok
			
			switch(operacion){
				case 'insert': //alta
					insertar_datos_producto(nombre, descripcion, idTipoProducto,idCamara, idRouter, idSim, idDispositivoEmisor, usuario, clave, idClienteSeleccion, email );
					break;
					
				case 'update': //modificar
				var  idProducto2 =  $("#idProducto2").val(); 
				console.log("modificar_datos_producto:  " + nombre + " , "+ descripcion + " , "+ idTipoProducto + " , "+ idCamara + " , "+ idRouter + " , "+  idSim + " , "+  idDispositivoEmisor + " , "+  usuario + " , "+  clave + " , "+  idClienteSeleccion  +  " , " + idProducto2 ); 
					
					 modificar_datos_producto(nombre,  descripcion, idTipoProducto,idCamara, idRouter, idSim, idProducto2, idDispositivoEmisor, usuario, clave, idClienteSeleccion, email);
					break;
			}
		}
	}		
	
	
	/************************************************************************************
		Insertamos los datos en la base de datos eude tabla tipo_aviso
	************************************************************************************/
	function insertar_datos_producto(nombre, descripcion, idTipoProducto,idCamara, idRouter, idSim, idDispositivoEmisor, usuario, clave, idClienteSeleccion, email){
		$.ajax({
			type: 'GET',
			url: 'io/producto/insert_producto.php',
			data: { 
				'nombre' : nombre,
				'descripcion' : descripcion,
				'idTipoProducto' : idTipoProducto,
				'idCamara' : idCamara,
				'idRouter' : idRouter,
				'idSim' : idSim,			
				'idDispositivoEmisor' : idDispositivoEmisor,			
				'idClienteSeleccion' : idClienteSeleccion,			
				'usuario' : usuario,			
				'clave' : clave,			
				'email' : email,			
				'nameTipoProducto' :$("#idTipoProducto option[value='"+$("#idTipoProducto").val()+"']").text() ,
				'nameCamara' : $("#idCamara option[value='"+$("#idCamara").val()+"']").text(),
				'nameRouter' : $("#idRouter option[value='"+$("#idRouter").val()+"']").text(),
				'nameSim' : $("#idSim option[value='"+$("#idSim").val()+"']").text(),
				'nameDispositivoEmisor' : $("#idSim option[value='"+$("#idDispositivoEmisor").val()+"']").text(),
				'nameClienteSeleccion' : $("#idClienteSeleccion option[value='"+$("#idClienteSeleccion").val()+"']").text(),
				'tiempoDesfase' : desfaseHoraria()
			},
			success: function(response) {
				var arrayResult = $.parseJSON(response);
				despuesDeGuardar(arrayResult, 0);
			}
		});
	}
	
	
	/************************************************************************************
		Modificamos los datos en la base de datos eude tabla tipo_aviso
	************************************************************************************/
	function modificar_datos_producto(nombre,  descripcion, idTipoProducto,idCamara, idRouter, idSim,   idSelect,  idDispositivoEmisor, usuario, clave, idClienteSeleccion, email){	
		$.ajax({
			type: 'GET',
			url: 'io/producto/update_producto.php',
			data: { 
				'idProducto' : idSelect,
				'nombre' : nombre,
				'descripcion' : descripcion,	
				'idTipoProducto' : idTipoProducto,	
				'idCamara' : idCamara,	
				'idRouter' : idRouter,	
				'idSim' : idSim,	
				'idDispositivoEmisor' : idDispositivoEmisor,	
				'idCamaraAux' : idCamaraAux,	
				'idRouterAux' : idRouterAux,	
				'idSimAux' : idSimAux,	
				'idDispositivoEmisorAux' : idDispositivoEmisorAux,						
				'usuario' : usuario,					
				'clave' : clave,					
				'email' : email,					
				'idClienteSeleccion' : idClienteSeleccion,					
				'idClienteSeleccionAux' : idClienteSeleccionAux,					
				'nameClienteSeleccion' :$("#idClienteSeleccion option[value='"+$("#idClienteSeleccion").val()+"']").text() ,
				'nameTipoProducto' :$("#idTipoProducto option[value='"+$("#idTipoProducto").val()+"']").text() ,
				'nameCamara' : $("#idCamara option[value='"+$("#idCamara").val()+"']").text(),
				'nameRouter' : $("#idRouter option[value='"+$("#idRouter").val()+"']").text(),
				'nameSim' : $("#idSim option[value='"+$("#idSim").val()+"']").text(),
				'nameDispositivoEmisor' : $("#idDispositivoEmisor option[value='"+$("#idDispositivoEmisor").val()+"']").text(),
				'tiempoDesfase' : desfaseHoraria()
			},
			success: function(response) {
				var arrayResult = $.parseJSON(response);
				despuesDeGuardar(arrayResult, 0);
			}
		});
	}
	
	
	/************************************************************************************
		Mostramos los datos que nos han solicitado en 'consultar producto'
	************************************************************************************/
	function mostrar_datos_producto(idSelect, operacionSQL){
		for(var i=0; i<arrayProducto.length; i++){
			if(arrayProducto[i].id == idSelect){
				console.log(arrayProducto[i]);
				
				mostrar_datos("idProducto2", arrayProducto[i].id, operacionSQL);
				mostrar_datos("nombreProducto", arrayProducto[i].nombre, operacionSQL);
				mostrar_datos("descripcionProducto", arrayProducto[i].descripcion, operacionSQL);
				mostrar_datos("idTipoProducto", arrayProducto[i].idTipoProducto, operacionSQL);
				mostrar_datos("selectEstado", arrayProducto[i].estado, operacionSQL);
				mostrar_datos("idClienteSeleccion", arrayProducto[i].idEmpresa, operacionSQL);
				mostrar_datos("idfechaAlta", arrayProducto[i].fecha_alta, operacionSQL);
				mostrar_datos("idfechaMod", arrayProducto[i].fecha_mod, operacionSQL);
				
				// Guardamos los id actual de empresa asociada al producto
				idClienteSeleccionAux = arrayProducto[i].idEmpresa;
				
				// tipo caja
				if( arrayProducto[i].idTipoProducto == '1' ){
					mostrar_datos("idRouter", arrayProducto[i].idRouter, operacionSQL);
					mostrar_datos("idCamara", arrayProducto[i].idCamara, operacionSQL);
					mostrar_datos("idSim", arrayProducto[i].idSim, operacionSQL);
					
					// Guardamos los id actuales de la sim, camara y router en una variable auxiliares
					idSimAux = arrayProducto[i].idSim;
					idCamaraAux = arrayProducto[i].idCamara;
					idRouterAux = arrayProducto[i].idRouter;				
				}	
				
				// tipo app 
				if( arrayProducto[i].idTipoProducto == '2' ){
					mostrar_datos("usuario", arrayProducto[i].usuario, operacionSQL);
					// mostrar_datos("clave", arrayProducto[i].clave, operacionSQL);
					mostrar_datos("email", arrayProducto[i].email, operacionSQL);
				}			

				// tipo Emision personal
				if( arrayProducto[i].idTipoProducto == '3' ){
					mostrar_datos("idDispositivoEmisor", arrayProducto[i].idDispositivoEmisor, operacionSQL);
					mostrar_datos("idSim", arrayProducto[i].idSim, operacionSQL);
					
					// Guardamos los id actuales de la sim, camara y router en una variable auxiliares
					idSimAux = arrayProducto[i].idSim;		
					idDispositivoEmisorAux = arrayProducto[i].idDispositivoEmisor;	
				}
										
			 	insert_log_acciones(arrayProducto[i]);					
			}
		}
	}
	
	
	/************************************************************************************
		Select de los routers que tenemos.
		Sacamos el array de los routers que existen en la BD.
		Hacemos un select-option que mostramos en el formulario de alta de las c�maras
	************************************************************************************/
	function select_producto(nombreDiv, tipoObjeto){
		$.ajax({
			type: 'GET',
			url: 'io/producto/select_producto.php',
			data: { 
				'opcion' : 1, 	
				'tiempoDesfase' : desfaseHoraria() 
			},
			success: function(response) {
				arrayProducto = $.parseJSON(response);
								
				switch(tipoObjeto){
					case 'selectOption':
						// Select-option producto
						select_producto_selectOption(nombreDiv);
						break;
					
					case 'dataTable':
						// tabla de la pag 'consultar' producto
						select_producto_dataTable(nombreDiv);						
						break;

				}// fin switch
			}// fin success
		});
	}

	
	// tabla de la pag 'consultar' producto
	function select_producto_dataTable(nombreDiv){
		// thead
		var  thTabla = '<tr>';
			thTabla +='<th class="th_turquesa">Id</th>';
			thTabla +='<th class="th_turquesa">Nombre</th>';
			thTabla +='<th class="th_turquesa">Descripci'+min_o+'n</th>';						
			thTabla +='<th class="th_turquesa">Tipo Producto</th>';						
			thTabla +='<th class="th_turquesa">C'+min_a+'mara</th>';						
			thTabla +='<th class="th_turquesa">Router</th>';						
			thTabla +='<th class="th_turquesa">Sim</th>';						
			thTabla +='<th class="th_turquesa">F. Alta</th>';
			thTabla +='<th class="th_turquesa">F. Modificaci'+min_o+'n</th>';
		thTabla +='</tr>';
				
		// tbody
		var tdTabla ='';
		for(var i=0; i < arrayProducto.length;  i++){
			tdTabla +=' <tr>';
				tdTabla +=' <td> '+ validarNulo(arrayProducto[i].id) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayProducto[i].nombre) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayProducto[i].descripcion)+'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayProducto[i].nombreTipoProducto) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayProducto[i].nombreCamara) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayProducto[i].nombreRouter) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayProducto[i].nombreSim) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayProducto[i].fecha_alta) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayProducto[i].fecha_mod) +'</td>';
			tdTabla +='</tr>';
		} // fin for	
		
		// Monto la tabla
		montarTabla(nombreDiv, thTabla, tdTabla, thTabla, "contenido");
	}
	
	// Select-option producto
	function select_producto_selectOption(nombreDiv){
		var html_selectOption ="";
		html_selectOption += "<select class='form-control' id='"+nombreDiv+"' >";
			html_selectOption += "<option value='-1' selected='selected' name='default'> "+ txtSelectOptionDefault +" </option>";
			
			for(var i=0; i < arrayProducto.length; i++){		
				html_selectOption += optionSelect_bloques(arrayProducto[i].id, arrayProducto[i].nombre, 1, i);				
			}//fin form
			
		html_selectOption += "</select>";	
		cargandoDatos("idProducto", 'end', 'seleccion');		
		$("#selectOption_idProducto").html(html_selectOption);
	}			

	
	
/************************************************************************************
#
# TABLA PRODUCTOS - ASIGNAR
#
************************************************************************************/	
	var productosCE = new Array();
	// tabla de los productos que tiene contratado una empesa. Se muestra en 'asignar al cliente' cuando seleccionamos un cliente. 
	function mostrar_productos_contratados_empresa(idEmpresa){
		$.ajax({
			type: 'GET',
			url: 'io/producto/select_producto.php',
			data: {  
				'idSelect' : idEmpresa,
				'opcion' : 3,
				'tiempoDesfase' : desfaseHoraria()
			},
			success: function(response) {			
				productosCE = $.parseJSON(response); // productosContratadosEmpresa		 		
				
				if(productosCE[0].result == 1){
									
					// thead
					var thTabla = "<tr>";
						thTabla +="<th>Id </th>";
						thTabla +="<th>Nombre</th>";
						// thTabla +="<th>Descripcion</th>";
						thTabla +="<th>Tipo</th>";
						thTabla +="<th>Camara</th>";
						thTabla +="<th>Router</th>";
						thTabla +="<th>Sim</th>";
						thTabla +="<th>Dispositivo Emisor</th>";
						thTabla +="<th>Email</th>";
						thTabla +="<th>F. Alta</th>";
						thTabla +="<th>F. Modificaci"+min_o+"n</th>";
						thTabla +="<th>Eliminar</th>";
					thTabla +="</tr>";
					
					// tBody							
					var tdTabla ="";
					for(var i=0; i<productosCE.length; i++){
						tdTabla +="<tr>";
							tdTabla +="<td>"+productosCE[i].id+"</td>";
							tdTabla +="<td>"+productosCE[i].nombre+"</td>";
							//tdTabla +="<td>"+productosCE[i].descripcion+"</td>";
							tdTabla +="<td> ("+productosCE[i].idTipoProducto+") - "+productosCE[i].nombreTipoProducto+" </td>";
							tdTabla +="<td> ("+productosCE[i].idCamara +") - "+productosCE[i].nombreCamara+" </td>";
							tdTabla +="<td> ("+productosCE[i].idRouter  +") - "+productosCE[i].nombreRouter+"  </td>";
							tdTabla +="<td> ("+productosCE[i].idSim      +") - "+productosCE[i].nombreSim+"  </td>";
							tdTabla +="<td> ("+productosCE[i].idDispositivoEmisor   +") - "+productosCE[i].nombreDispositivoEmisor+"  </td>";
							tdTabla +="<td>"+productosCE[i].email+"</td>";
							tdTabla +="<td>"+productosCE[i].fecha_alta+"</td>";
							tdTabla +="<td>"+productosCE[i].fecha_mod+"</td>";
						
							arrayPruebas = JSON.stringify(productosCE[i]);
							// console.log(arrayPruebas);							
							tdTabla +="<td id='btn_"+productosCE[i].id+"' > <center> <img src='img/papelera.png'  width=32 title='Dar de baja al producto en esta empresa' onclick='bajaProductoContratado("+arrayPruebas+")'>  </center> </td>";
						//	tdTabla +="<td> <center> <img src='img/papelera.png'     width=32 title='Dar de baja al producto en esta empresa' onclick='bajaProductoContratado("+productosCE[i].id+", "+productosCE[i].idEmpresa+")'>  </center> </td>";
							tdTabla +="</tr>";
						
						}
						
						// Monto la tabla
						montarTabla("tableListPC", thTabla, tdTabla, thTabla, "listaProductosContratados");
						
						$("#listaProductosContratados").append("<p id='successMsg_2'  style='display:none;'>  </p>");
				}
				else{ // caso que no hay resultados
					var tablaListado ="<div class='bg-warning warning'> La empresa seleccionada no tiene ningun producto contratado!! </div>";
					$("#listaProductosContratados").html(tablaListado);
				}
				
				//	$("#formulario_asignar").css("display","block");
				
			}//fin success
		});
	}

	// Baja producto Contratado
	function bajaProductoContratado(arrayP){
			console.log("click: " + arrayP.id);
		var imgLoader = '<center> <img src="img/ajaxLoader/ajaxLoader.gif" class="ajaxLoader" alt="Cargando datos..."/> </center>  ';		
		$("#btn_"+arrayP.id).html(imgLoader);


		$.ajax({
			type: 'GET',
			url: 'io/producto/desasignar_producto.php',
			data: {  
				'idProducto' : arrayP.id,
				'idCamara' : arrayP.idCamara,
				'idRouter' : arrayP.idRouter,
				'idSim' : arrayP.idSim,
				'idEmpresa' : arrayP.idEmpresa,
				'idDispositivoEmisor' : arrayP.idDispositivoEmisor,
				'idTipoProducto' : arrayP.idTipoProducto,
				'tiempoDesfase' : desfaseHoraria()
			},
			success: function(response) {
				var arrayResult = $.parseJSON(response);
				mostrar_productos_contratados_empresa(arrayResult[0].idEmpresa);	
				despuesDeGuardar(arrayResult, 0);
				$("#idClienteSeleccion").val(arrayResult[0].idEmpresa);				
			}
			
		});
		
	}
	
	// No entiendo muys bien esta opcion - mirarla con calma cuando termine de actualizar
	var arrayProductoSinAsignar = new Array();
	function select_producto_sinAsignar(nomDiv){
		$.ajax({
			type: 'GET',
			url: 'io/producto/select_producto.php',
			data: { 
				'opcion' : 4 , 
				'tiempoDesfase' : desfaseHoraria()
			},
			success: function(response) {
				arrayProductoSinAsignar= $.parseJSON(response);
				totalProducto = productoSinAsignar.length;
				
				
							
				// Select-option formulario sim
				var html_selectOption = "";
					html_selectOption += "<option value='-1' selected='selected' name='default' > "+ txtSelectOptionDefault +" </option>";
					//if(arrayProductoSinAsignar.length<0){
						for(var i=0; i < arrayProductoSinAsignar.length; i++){
							html_selectOption += optionSelect_bloques(arrayProductoSinAsignar[i].id, arrayProductoSinAsignar[i].nombre, 1, i);
						} // fin for
					//}else{
						//html_selectOption += "<option value='-1'  name='default' > No hay productos libres </option>";
					//}
				$("#"+nomDiv).html(html_selectOption);
			}
		});
	}
		

	function mostrar_ficha_productos(idProducto_select, i){
		$("#result_i").html(i);
		$("#result_i").css("display","none");		
		$("#result_idProducto").html(arrayProductoSinAsignar[i].id);
		$("#result_idProducto").css("display","none");
		$("#result_nombre").html(arrayProductoSinAsignar[i].nombre);
		$("#result_descripcion").html( arrayProductoSinAsignar[i].descripcion);
		$("#result_idTipoProducto").html( arrayProductoSinAsignar[i].idTipoProducto);
		$("#result_idTipoProducto").css("display","none");
		$("#result_tipo").html( arrayProductoSinAsignar[i].nombreTipoProducto);
		$("#result_idCamara").html(arrayProductoSinAsignar[i].idCamara);
		$("#result_idCamara").css("display","none");
		$("#result_camara").html(arrayProductoSinAsignar[i].nombreCamara);
		$("#result_idRouter").html(arrayProductoSinAsignar[i].idRouter);
		$("#result_idRouter").css("display","none");
		$("#result_router").html(arrayProductoSinAsignar[i].nombreRouter);
		$("#result_idSim").html( arrayProductoSinAsignar[i].idSim);
		$("#result_idSim").css("display","none");
		$("#result_sim").html(arrayProductoSinAsignar[i].nombreSim);
		//if(arrayProductoSinAsignar[i].estado == 0) { var estado = 'Sin asignar'; }else{ var estado = 'Asignado';}
		$("#result_estado").html( validarPropietario(arrayProductoSinAsignar[i].estado, "producto"));
		// $("#result_estado").html( estado);
		$("#result_fechaAlta").html( arrayProductoSinAsignar[i].fecha_alta);
		$("#result_fechaMod").html( arrayProductoSinAsignar[i].fecha_mod);
	
	}
	
	
	function asignarProducto(){
		var idEmpresa = $("#idClienteSeleccion").val();
		var iSelect =  $("#result_i").text(); 
				
		// Para hacer el nuevo nombre
		var input = $("#result_nombre").text();
		var partes = input.split('_');
	
		//	var nameProducto = partes[0]+"_"+idEmpresa+"_"+partes[2];
		var nameProducto = partes[0]+"_"+idEmpresa;
		
		$.ajax({
			type: 'GET',
			url: 'io/producto/asignar_producto.php',
			data: { 
				'idEmpresa' :idEmpresa,
				'nameEmpresa' :$("#idClienteSeleccion option[value='"+$("#idClienteSeleccion").val()+"']").text(), 
				'idProducto' : $("#result_idProducto").text(),
				'nameProducto' : nameProducto,
				'idTipoProducto' : $("#result_idTipoProducto").text(),		
				'nameTipoProducto' : $("#result_tipo").text(),
				'idCamara' : $("#result_idCamara").text(),
				'nameCamara' : $("#result_camara").text(),
				'idRouter' : $("#result_idRouter").text(),
				'nameRouter' : $("#result_router").text(),
				'idSim' : $("#result_idSim").text(),
				'nameSim' : $("#result_sim").text(),
				'nombre' : $("#result_nombre").text(),
				'descripcion' : $("#result_descripcion").text(),
				'tiempoDesfase' : desfaseHoraria()
			},
			success: function(response) {	
				var arrayResult = $.parseJSON(response);			
				despuesDeGuardar(arrayResult, 0);
				
				//Limpiamos formulario
				if(arrayResult[0].response == 1){
					borrar_ficha();
					mostrar_productos_contratados_empresa(idEmpresa); 
					$("#idClienteSeleccion").val(idEmpresa);	
				}
			}
		});
	}

	function borrar_ficha(){
		//$('#idClienteSeleccion option[value=-1]').attr('selected','selected');
		$('#productoSinAsignar option[value=-1]').attr('selected','selected');
		$(".resut_label").text("");
	}

	
	
/************************************************************************************
#
# TABLA PAIS
#
************************************************************************************/	

	/************************************************************************************
		Validar datos introducidos en el formulario
	************************************************************************************/
	function validar_datos_pais(operacion){
		
		validar_div_visible("successMsg", "hide");
		var tipoCount="pais";
		countPais=0;
		
		
		// Nombre del pais
		var nombre = validar_input('nombrePais',  tipoCount);   	
		
		// CodeCalling
		var codeCalling = validar_input('codeCalling',  tipoCount);   	 
		
		// capital
		var capital =  validar_input('capital',  tipoCount);   	 
		
		// continente
		var continente = validar_selectOption('continente',  tipoCount);   	 
		// console.log("CONTINENTE: " + continente);
		
		// latPais
		var latPais = validar_input('latPais',  tipoCount);   	  
		
		// lngPais
		var lngPais = validar_input('lngPais',  tipoCount);   	   
				
		// codeISO2
		var codeISO2 = validar_input('codeISO2',  tipoCount);   	 
		
		// codeISO3
		var codeISO3 = validar_input('codeISO3',  tipoCount);   	 
		
		
		// console.log("countPais: "  + countPais);			
		if(countPais==8) {	
			// Guardando datos
			cargandoDatos("guardandoDatos", 'start', operacion); //ok
			
			//Lo mandamos a la operacion que le corresponda
			switch(operacion){
				case 'insert': // alta
					insertar_datos_pais(nombre, codeCalling, capital, continente, latPais, lngPais, codeISO2, codeISO3);
					break;
					
				case 'update': // modificar
					idSelect =  $("#idPais2").val(); 
					var fechaA =  $("#fechaAlta").val(); 
					var fechaM = $("#fechaMod").val(); 
					 modificar_datos_pais(idSelect, nombre, codeCalling,  capital, continente, latPais, lngPais, codeISO2, codeISO3, fechaA, fechaM);
					break;
			}
		 }
		
	}		
	
	
	/************************************************************************************
		Insertamos los datos en la base de datos eude tabla provincias
	************************************************************************************/
	function insertar_datos_pais(nombre, codeCalling, capital, continente, latPais, lngPais, codeISO2, codeISO3){
		/* */
		$.ajax({
			type: 'GET',
			url: 'io/pais/insert_pais.php',
			data: { 
				'nombre' : nombre,
				'codeCalling' : codeCalling,
				'capital' : capital,
				'continente' : continente,
				'latPais' : latPais,
				'lngPais' : lngPais,
				'codeISO2' : codeISO2,
				'codeISO3' : codeISO3,
				'tiempoDesfase' : desfaseHoraria()
			},
			success: function(response) {
				var arrayResult = $.parseJSON(response);
				despuesDeGuardar(arrayResult, 0);
			}
		});
	}
	
	
	/************************************************************************************
		Modificamos los datos en la base de datos eude tabla pais
	************************************************************************************/
	function modificar_datos_pais(idSelect, nombre, codeCalling, capital, continente, latPais, lngPais, codeISO2, codeISO3, fechaA, fechaM){
	
		$.ajax({
			type: 'GET',
			url: 'io/pais/update_pais.php',
			data: { 
				'idSelect' : idSelect,
				'nombre' : nombre,
				'codeCalling' : codeCalling,
				'capital' : capital,
				'continente' : continente,
				'latPais' : latPais,
				'lngPais' : lngPais,
				'codeISO2' : codeISO2,
				'codeISO3' : codeISO3,
				'tiempoDesfase' : desfaseHoraria(),
				'fechaA':fechaA,
				'fechaM':fechaM
			},
			success: function(response) {
				var arrayResult = $.parseJSON(response);
				despuesDeGuardar(arrayResult, 0);					
			}
		});
		
	}
	

	/************************************************************************************
		Mostramos los datos que nos han solicitado en 'consultar provincias'
	************************************************************************************/
	function mostrar_datos_pais(idSelecionado, operacionSQL){
		
			for(var i=0; i <  arrayPais.length; i++){
			if(arrayPais[i].id == idSelecionado){
				mostrar_datos("idPais2", arrayPais[i].id, operacionSQL);
				mostrar_datos("nombrePais", arrayPais[i].nombre, operacionSQL);
				mostrar_datos("codeCalling", arrayPais[i].codeCalling, operacionSQL);
				mostrar_datos("capital", arrayPais[i].capital, operacionSQL);
				mostrar_datos("continente", arrayPais[i].continente, operacionSQL);
				mostrar_datos("latPais", arrayPais[i].lat, operacionSQL);
				mostrar_datos("lngPais", arrayPais[i].lng, operacionSQL);
				mostrar_datos("codeISO2", arrayPais[i].codeISO2, operacionSQL);
				mostrar_datos("codeISO3", arrayPais[i].codeISO3, operacionSQL);
				mostrar_datos("fechaAlta", arrayPais[i].fecha_alta, operacionSQL);
				mostrar_datos("fechaMod", arrayPais[i].fecha_mod, operacionSQL);
				insert_log_acciones(arrayPais[i]);			
			}
		}
		
	}
	

	/************************************************************************************
		Select option pais  tipoObjeto
	************************************************************************************/
	function select_pais(nombreDiv, tipoObjeto){
		$.ajax({
			type: 'GET',
			url: 'io/pais/select_pais.php',
			data: { 	
						'tiempoDesfase' : desfaseHoraria()		
					},
			success: function(response) {
				
				arrayPais = $.parseJSON(response);				
				
				switch(tipoObjeto){
					case 'selectOption':
						// Select-option todos los paises
						select_pais_selectOption(nombreDiv); 						
						break;
					
					case 'dataTable':
						// tabla de la pag 'consultar' pais 
						select_pais_dataTable(nombreDiv);
						break;
					
				}//fin switch

			}//fin success
		});
	}
	
	// Select-option todos los paises
	function select_pais_selectOption(nombreDiv){
		var html_selectOption = "<select class='form-control' id='"+nombreDiv+"' >";
		html_selectOption += "<option value='-1' selected='selected' >"+ txtSelectOptionDefault +"</option>";
		for(var i=0; i <  arrayPais.length; i++){
			html_selectOption += optionSelect_bloques(arrayPais[i].id, arrayPais[i].nombre, 1, i);
		} // fin for
		html_selectOption += "</select>";
		cargandoDatos("idPais", 'end', 'seleccion');
		$("#selectOption_idPais").html(html_selectOption);
		
	}
	
	// tabla de la pag 'consultar' pais 
	function select_pais_dataTable(nombreDiv){
		// thead 
		var  thTabla = '<tr>';
			thTabla +='<th class="th_turquesa">Id</th>';
			thTabla +='<th class="th_turquesa">Nombre</th>';
			thTabla +='<th class="th_turquesa">CodeCalling</th>';						
			thTabla +='<th class="th_turquesa">Capital</th>';						
			thTabla +='<th class="th_turquesa">Continente</th>';						
			thTabla +='<th class="th_turquesa">Lat</th>';						
			thTabla +='<th class="th_turquesa">Lng</th>';						
			thTabla +='<th class="th_turquesa">CodeISO2</th>';						
			thTabla +='<th class="th_turquesa">CodeISO3</th>';						
			thTabla +='<th class="th_turquesa">F. Alta</th>';
			thTabla +='<th class="th_turquesa">F. Modificaci'+min_o+'n</th>';
		thTabla +='</tr>';
		
		// tbody
		var tdTabla ='';
		for(var i=0; i <  arrayPais.length;  i++){
			tdTabla +=' <tr>';
				tdTabla +=' <td> '+ validarNulo(arrayPais[i].id) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayPais[i].nombre) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayPais[i].codeCalling) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayPais[i].capital) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayPais[i].continente) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayPais[i].lat) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayPais[i].lng) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayPais[i].codeISO2) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayPais[i].codeISO3) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayPais[i].fecha_alta) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayPais[i].fecha_mod) +'</td>';
			tdTabla +='</tr>';
		} // fin for			
		
		// Montar la tabla
		montarTabla(nombreDiv, thTabla, tdTabla, thTabla, "contenido");		
	}
	

/************************************************************************************
#
# TABLA PROVINCIAS
#
************************************************************************************/	

	/************************************************************************************
		Validar datos introducidos en el formulario
	************************************************************************************/
	function validar_datos_provincias(operacion){
		
		validar_div_visible("successMsg", "hide");
		var tipoCount="provincias";
		countProvincias=0;
		
		
		// Nombre del rol de usuario
		var nombre = validar_input('nombreProvincias',  tipoCount);   		
	
		// Descripci�n del rol de usuario
		var codigoPostal = validar_input('codigoPostalProvincias',  tipoCount); 	
	
		//selecciona_opcion(operacion, countProvincias);
			
		if(countProvincias==2) {	
			// Guardando datos
			cargandoDatos("guardandoDatos", 'start', operacion); //ok
		
			//Lo mandamos a la operacion que le corresponda
			switch(operacion){
				case 'insert': //alta
					insertar_datos_provincias(nombre, codigoPostal);
					break;
					
				case 'update': //modificar
					idSelect =  $("#idProvincias2").val(); 
					var fechaA =  $("#fechaAlta").val(); 
					var fechaM = $("#fechaMod").val(); 
					modificar_datos_provincias(nombre,  codigoPostal, idSelect, fechaA, fechaM);
					break;
			}
		}
	}		
	
	
	/************************************************************************************
		Insertamos los datos en la base de datos eude tabla provincias
	************************************************************************************/
	function insertar_datos_provincias(nombre, codigoPostal){
		$.ajax({
			type: 'GET',
			url: 'io/provincias/insert_provincias.php',
			data: { 
				'nombre' : nombre,
				'codigoPostal' : codigoPostal,
				'tiempoDesfase' : desfaseHoraria()
			},
			success: function(response) {
				var arrayResult = $.parseJSON(response);
				despuesDeGuardar(arrayResult, 0);
			}
		});
	}
	
	
	/************************************************************************************
		Modificamos los datos en la base de datos eude tabla provincias
	************************************************************************************/
	function modificar_datos_provincias(nombre, codigoPostal, idSelect, fechaA, fechaM){
		$.ajax({
			type: 'GET',
			url: 'io/provincias/update_provincias.php',
			data: { 
				'idSelect' : idSelect,
				'nombre' : nombre,
				'codigoPostal' : codigoPostal,
				'tiempoDesfase' : desfaseHoraria(),
				'fechaA':fechaA,
				'fechaM':fechaM
			},
			success: function(response) {
				var arrayResult = $.parseJSON(response);
				despuesDeGuardar(arrayResult, 0);					
			}
		});
	}
	
	
	/************************************************************************************
		Mostramos los datos que nos han solicitado en 'consultar provincias'
	************************************************************************************/
	function mostrar_datos_provincias(idSelecionado, operacionSQL){
		
			for(var i=0; i <  arrayProvincias.length; i++){
			if(arrayProvincias[i].id == idSelecionado){
				mostrar_datos("idProvincias2", arrayProvincias[i].id, operacionSQL);
				mostrar_datos("nombreProvincias", arrayProvincias[i].nombre, operacionSQL);
				mostrar_datos("codigoPostalProvincias", arrayProvincias[i].codigoPostal, operacionSQL);
				mostrar_datos("fechaAlta", arrayProvincias[i].fecha_alta, operacionSQL);
				mostrar_datos("fechaMod", arrayProvincias[i].fecha_mod, operacionSQL);
				insert_log_acciones(arrayProvincias[i]);			
			}
		}
		
	}
	
	
	
	/************************************************************************************
		Select de los routers que tenemos.
		Sacamos el array de los routers que existen en la BD.
		Hacemos un select-option que mostramos en el formulario de alta de las c�maras
	************************************************************************************/
	function select_provincias(nombreDiv, tipoObjeto){
		
		$.ajax({
			type: 'GET',
			url: 'io/provincias/select_provincias.php',
			data: {	
				'tiempoDesfase' : desfaseHoraria() 
			},
			success: function(response) {
				arrayProvincias = $.parseJSON(response);
				
							
				switch(tipoObjeto){
					case 'selectOption':
						// Select-option provincias
						select_provincias_selectOption(nombreDiv); 
						break;
					
					case 'dataTable':
						// tabla de la pag 'consultar' provincias 
						select_provincias_datatable(nombreDiv);
						break;

				}//fin switch
			}//fin success
		});
	}
	
	
	/************************************************************************************
		Select option provincias 
	************************************************************************************/	
	//Select-option provincias
	function select_provincias_selectOption(nombreDiv){
		var html_selectOption ="";
		html_selectOption += "<select class='form-control' id='"+nombreDiv+"' >";
		html_selectOption += "<option value='-1' selected='selected'> "+ txtSelectOptionDefault +" </option>";
		for(var i=0; i <  arrayProvincias.length; i++){
			html_selectOption += optionSelect_bloques(arrayProvincias[i].id,arrayProvincias[i].nombre,1, i);
		} // fin for
		html_selectOption += "</select>";
		cargandoDatos("idProvincias", 'end', 'seleccion');
		$("#selectOption_idProvincias").html(html_selectOption);
	}
	
	// tabla de la pag 'consultar' provincias 
	function select_provincias_datatable(nombreDiv){
		// thead 
		var  thTabla = '<tr>';
			thTabla +='<th class="th_turquesa">Id</th>';
			thTabla +='<th class="th_turquesa">Nombre</th>';
			thTabla +='<th class="th_turquesa">C'+min_o+'digo Postal</th>';						
			thTabla +='<th class="th_turquesa">F. Alta</th>';
			thTabla +='<th class="th_turquesa">F. Modificaci'+min_o+'n</th>';
		thTabla +='</tr>';
		
		// tbody
		var tdTabla ='';
		for(var i=0; i <  arrayProvincias.length;  i++){
			tdTabla +=' <tr>';
				tdTabla +=' <td> '+ validarNulo(arrayProvincias[i].id) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayProvincias[i].nombre) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayProvincias[i].codigoPostal) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayProvincias[i].fecha_alta) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayProvincias[i].fecha_mod) +'</td>';
			tdTabla +='</tr>';
		} // fin for			
		
		// Montar la tabla
		montarTabla(nombreDiv, thTabla, tdTabla, thTabla, "contenido");				
	}
	
	
	
/************************************************************************************
#
# TABLA ROL_USERS
#
************************************************************************************/	

	/************************************************************************************
		Validar datos introducidos en el formulario
	************************************************************************************/
	function validar_datos_rol_user(operacion){
		validar_div_visible("successMsg", "hide");
		var tipoCount="rolUser";
		countRolUser=0;
		
	
	
		//Nombre del rol de usuario
		var nombre = validar_input('nombreRolUser',  tipoCount);   
	
		//Descripci�n del rol de usuario
		var descripcion = validar_input('descripcionRolUser',  tipoCount);
	
		//selecciona_opcion(operacion, countCamara);
		if(countRolUser==2) {	
			// Guardando datos
			cargandoDatos("guardandoDatos", 'start', operacion); // ok
			
			//Lo mandamos a la operacion que le corresponda
			switch(operacion){
				case 'insert': //alta
					insertar_datos_rol_user(nombre, descripcion);
					break;
					
				case 'update': //modificar
					var idRolUser2 =  $("#idRolUser2").val(); 
					var fechaA =  $("#fechaAlta").val(); 
					var fechaM =  $("#fechaModificacion").val();					
					modificar_datos_rol_user(nombre,  descripcion, idRolUser2, fechaA, fechaM);
					break;
			}
		}
	}		
	
	
	/************************************************************************************
		Insertamos los datos en la base de datos eude tabla rol_user
	************************************************************************************/
	function insertar_datos_rol_user(nombre, descripcion){
		$.ajax({
			type: 'GET',
			url: 'io/rolUser/insert_rol_user.php',
			data: { 
				'nombre' : nombre,
				'descripcion' : descripcion,
				'tiempoDesfase' : desfaseHoraria()
			},
			success: function(response) {
				var arrayResult = $.parseJSON(response);
				despuesDeGuardar(arrayResult, 0); 				
			}
		});
	}
	
	
	/************************************************************************************
		Modificamos los datos en la base de datos eude tabla rol_user
	************************************************************************************/
	function modificar_datos_rol_user(nombre, descripcion, idSelect, fechaA, fechaM){
		$.ajax({
			type: 'GET',
			url: 'io/rolUser/update_rol_user.php',
			data: { 
				'idSelect' : idSelect,
				'nombre' : nombre,
				'descripcion' : descripcion,
				'tiempoDesfase' : desfaseHoraria(),					
				'fechaA' : fechaA,						
				'fechaM' : fechaM,						
			},
			success: function(response) {
				var arrayResult = $.parseJSON(response);
				despuesDeGuardar(arrayResult, 0); 							
			}
		});
	}
	
	
	/************************************************************************************
		Mostramos los datos que nos han solicitado en 'consultar rol_user'
	************************************************************************************/
	function mostrar_datos_rol_user(idSelecionado, operacionSQL){
		$.ajax({
			type: 'GET',
			url: 'io/rolUser/select_rol_user.php',
			data: { 
				'idSelecionado' : idSelecionado,
				'tiempoDesfase' : desfaseHoraria()
			},
			success: function(response) {
				var arrayResultado = $.parseJSON(response);
				mostrar_datos("idRolUser2", arrayResultado[0].id, operacionSQL);
				mostrar_datos("nombreRolUser", arrayResultado[0].nom, operacionSQL);
				mostrar_datos("descripcionRolUser", arrayResultado[0].descrip, operacionSQL);
				mostrar_datos("fechaAlta", arrayResultado[0].fecha_alta, operacionSQL);
				mostrar_datos("fechaModificacion", arrayResultado[0].fecha_mod, operacionSQL);
				insert_log_acciones(arrayResultado);			
			}
		});
	}
	
	
	/************************************************************************************
		Select de los routers que tenemos.
		Sacamos el array de los routers que existen en la BD.
		Hacemos un select-option que mostramos en el formulario de alta de las c�maras
	************************************************************************************/
	function select_rol_user(nombreDiv, tipoObjeto){
		
		// inicio loader de cargarDatos 		
		cargandoDatos(nombreDiv, 'start' , 'seleccion');

		
		$.ajax({
			type: 'GET',
			url: 'io/rolUser/select_rol_user.php',
			data: { 	
				'tiempoDesfase' : desfaseHoraria() 
			},
			success: function(response) {
				arrayRolUser = $.parseJSON(response);
				
				switch(tipoObjeto){
					case 'selectOption':
						// Select-option de rolUser
						select_rol_user_selectOption(nombreDiv);
						break;
					
					case 'dataTable':
						// tabla de la pag 'consultar' rolUser 
						select_rol_user_datatable(nombreDiv);
						break;

				}//fin switch
				cargandoDatos(nombreDiv, 'end', 'seleccion');
			}//fin success	
			
		});
	
	}
	
	// Select-option de rolUser
	function select_rol_user_selectOption(nombreDiv){
		var html_selectOption = "<select class='form-control' id='"+nombreDiv+"' >";
			html_selectOption += "<option value='-1' selected='selected' name='defaultRolUser'> "+ txtSelectOptionDefault +" </option>";
			for(var i=0; i < arrayRolUser.length; i++){					
				html_selectOption += optionSelect_bloques(arrayRolUser[i].id , arrayRolUser[i].nom, 1, i);
			} // fin for
		html_selectOption += "</select>";			
		cargandoDatos("idRolUser", 'end', 'seleccion');			
		$("#selectOption_idRolUser").html(html_selectOption);
	}
	
	// tabla de la pag 'consultar' rolUser 
	function select_rol_user_datatable(nombreDiv){
				
		// thead
		var  thTabla = '';
		thTabla +='<tr>';
			thTabla +='<th class="th_turquesa">Id</th>';
			thTabla +='<th class="th_turquesa">Nombre</th>';
			thTabla +='<th class="th_turquesa">Descripci'+min_o+'n</th>';						
			thTabla +='<th class="th_turquesa">F. Alta</th>';
			thTabla +='<th class="th_turquesa">F. Modificaci'+min_o+'n</th>';
		thTabla +='</tr>';
			
		// tBody
		var tdTabla ="";
		for(var i=0; i < arrayRolUser.length;  i++){
			tdTabla +=' <tr>';
				tdTabla +=' <td> '+ validarNulo(arrayRolUser[i].id) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayRolUser[i].nom) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayRolUser[i].descrip) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayRolUser[i].fecha_alta) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayRolUser[i].fecha_mod) +'</td>';
			tdTabla +='</tr>';
		} // fin for

		// Montar tabla
		montarTabla(nombreDiv, thTabla, tdTabla, thTabla, "contenido");
				
	}
	
	
/************************************************************************************
#
# TABLA ROUTERS
#
************************************************************************************/	
	
	/************************************************************************************
		Router
	************************************************************************************/
	function validar_datos_router(operacion){
		validar_div_visible("successMsg", "hide");
		countRouter=0;
		tipoCount ="router";
	
		// Recogemos los campos antes evaluados
		var nombreRouter = validar_input('nombreRouter',  tipoCount);  
		var idTipoRouter =  validar_input('idTipoRouter',  tipoCount); 	
		var numSerialRouter =  validar_input('numSerialRouter',  tipoCount);	
		var macRouter =  validar_input('macRouter',  tipoCount);
		var userRouter =  validar_input('userRouter',  tipoCount); 	
		var passRouter = validar_input('passRouter',  tipoCount); 		
		var ipStaticRouter =  validar_input('ipStaticRouter',  tipoCount);
		var dyndnsRouter =  validar_input('dyndnsRouter',  tipoCount);		
	
		//selecciona_opcion(operacion, countRouter);
		if(countRouter==8){	
			// Guardando datos
			cargandoDatos("guardandoDatos", 'start', operacion); //ok
		
			//Lo mandamos a la operacion que le corresponda
			switch(operacion){
				case 'insert': //alta
					insertar_datos_router(nombreRouter,idTipoRouter,numSerialRouter,macRouter,userRouter,passRouter,ipStaticRouter,dyndnsRouter);
					break;
					
				case 'update': //modificar
					var idRouterSelect =  $("#idRouter2").val(); 
					var fechaA =   $("#fechaAlta").val();
					var fechaM =   $("#fechaModificacion").val();
					modificar_datos_router(idRouterSelect, nombreRouter,idTipoRouter,numSerialRouter,macRouter,userRouter,passRouter,ipStaticRouter,dyndnsRouter, fechaA, fechaM);
					break;
			}
		}
	}	
	
	
	/************************************************************************************
		Insertamos los datos en la base de datos eude tabla routers
	************************************************************************************/
	function insertar_datos_router(nombreRouter,idTipoRouter,numSerialRouter,macRouter,userRouter,passRouter,ipStaticRouter,dyndnsRouter){
		$.ajax({
			type: 'GET',
			url: 'io/router/insert_router.php',
			data: { 
				'nombreRouter' : nombreRouter,
				'idTipoRouter':idTipoRouter,
				'numSerialRouter':numSerialRouter,
				'macRouter':macRouter,
				'userRouter':userRouter,
				'passRouter':passRouter,
				'ipStaticRouter':ipStaticRouter,
				'dyndnsRouter':dyndnsRouter,
				'tiempoDesfase' : desfaseHoraria()
			},
			success: function(response) {
				var arrayResult = $.parseJSON(response);
				despuesDeGuardar(arrayResult, 0); 					
			}
		});
	}
	
	
	/************************************************************************************
		Modificamos los datos en la base de datos eude tabla routers
	************************************************************************************/
	function modificar_datos_router(idRouterSelect, nombreRouter,idTipoRouter,numSerialRouter,macRouter,userRouter,passRouter,ipStaticRouter,dyndnsRouter, fechaA, fechaM){
		$.ajax({
			type: 'GET',
			url: 'io/router/update_router.php',
			data: { 
				'idRouterSelect' : idRouterSelect,
				'nombreRouter' : nombreRouter,
				'idTipoRouter':idTipoRouter,
				'numSerialRouter':numSerialRouter,
				'macRouter':macRouter,
				'userRouter':userRouter,
				'passRouter':passRouter,
				'ipStaticRouter':ipStaticRouter,
				'dyndnsRouter':dyndnsRouter,
				'fechaA':fechaA,
				'fechaM':fechaM,
				'tiempoDesfase' : desfaseHoraria()
			},
			success: function(response) {				
				var arrayResult = $.parseJSON(response);
				despuesDeGuardar(arrayResult, 0); 				
			}
		});
	}
	
	
	/************************************************************************************
		Mostramos los datos de eude.router
	************************************************************************************/
	function mostrar_datos_router(idRouter, operacionSQL){
		for(var i=0; i< arrayRouters.length; i++){
			if(arrayRouters[i].idRouters ==idRouter ){
				mostrar_datos("idRouter2", arrayRouters[i].idRouters, operacionSQL);
				mostrar_datos("nombreRouter", arrayRouters[i].nameRouters, operacionSQL);
				mostrar_datos("idTipoRouter" , arrayRouters[i].idTipoRouter, operacionSQL);
				mostrar_datos("numSerialRouter", arrayRouters[i].serialnumRouters, operacionSQL);
				mostrar_datos("macRouter" , arrayRouters[i].macRouters, operacionSQL);
				mostrar_datos("userRouter", arrayRouters[i].userRouters, operacionSQL);
				mostrar_datos("passRouter", arrayRouters[i].passRouters, operacionSQL);
				mostrar_datos("ipStaticRouter" ,arrayRouters[i].ipstaticRouters, operacionSQL);
				mostrar_datos("dyndnsRouter", arrayRouters[i].dyndnsRouters, operacionSQL);
				mostrar_datos("propietarioRouter", arrayRouters[i].nomPropietario, operacionSQL);				
				mostrar_datos("fechaAlta", arrayRouters[i].fecha_alta, operacionSQL);
				mostrar_datos("fechaModificacion", arrayRouters[i].fecha_mod, operacionSQL);					
				// insert_log_acciones(arrayRouters[i]);	
			}
		}
	}
	
	
	/************************************************************************************
		Select-option, datatable... depende de la opci�n que seleccione
	************************************************************************************/
	function select_routers(nombreDiv, tipoObjeto){
	
		$.ajax({
			type: 'GET',
			url: 'io/router/select_routers.php',
			data: { 	
				'tiempoDesfase' : desfaseHoraria() 
			},
			success: function(response) {
				arrayRouters = $.parseJSON(response);
							
				switch(tipoObjeto){
					case 'libres':						
						// Select-option formulario routers	 - solo las que estan libres								
						select_routers_selectOption(nombreDiv, 'libre');	
						break;
					
					case 'selectOption':					
						// Select-option formulario routers	 - todas					
						select_routers_selectOption(nombreDiv, 'todo');
						break;
					
					case 'dataTable':
						// tabla de la pag 'consultar' routers 
						select_routers_dataTable(nombreDiv);
						break;

						
					case 'resumen_info_Dashboard':
						// pag. dashboard - bloque 'informacion - apartado routers'
						select_routers_resumen_info_Dashboard(nombreDiv);
						break;
						
					case 'tabla_ultimos': 
						// pag. dashboard  - bloque '10 ultimas altas'
						select_routers_tabla_ultimas(nombreDiv);
						break;
				}//fin switch
			}//fin success
		});
	}	
	
	function select_routers_selectOption(nombreDiv, opcion){
		var html_selectOption ="";
		html_selectOption += "<select class='form-control' id='"+nombreDiv+"' >";
			html_selectOption += "<option value='-1' selected='selected' name='defaultRouter'> "+ txtSelectOptionDefault +" </option>";
			
			for(var i=0; i < arrayRouters.length; i++){			
				// solo mostramos camaras libres
				if(opcion == 'libre'){ 
					if(arrayRouters[i].asignado==0)
					html_selectOption += optionSelect_bloques(arrayRouters[i].idRouters, arrayRouters[i].nameRouters, arrayRouters[i].asignado, i);
				}
				
				// Mostramos todas las camaras
				else{
					html_selectOption += optionSelect_bloques(arrayRouters[i].idRouters, arrayRouters[i].nameRouters, arrayRouters[i].asignado, i);
				}
				
			} //fin for
		html_selectOption += "</select>";
		cargandoDatos("idRouter", 'end', 'seleccion');
		$("#selectOption_idRouter").html(html_selectOption);
	}
	
	// tabla de la pag 'consultar' routers 
	function select_routers_dataTable(nombreDiv){
		// thead
		var  thTabla = '<tr>';
			thTabla +='<th class="th_turquesa">Id</th>';
			thTabla +='<th class="th_turquesa">Nombre</th>';
			thTabla +='<th class="th_turquesa">Tipo</th>';
			thTabla +='<th class="th_turquesa">Num. Serial</th>';
			thTabla +='<th class="th_turquesa">Mac</th>';
			thTabla +='<th class="th_turquesa">Ip statica</th>';
			thTabla +='<th class="th_turquesa">Dyndns</th>';
			thTabla +='<th class="th_turquesa">Propietario</th>';
		thTabla +='</tr>';
						
		// tbody
		var tdTabla ='';
		for(var i=1; i < arrayRouters.length; i++){
			tdTabla +=' <tr>';
				tdTabla +=' <td> '+ validarNulo(arrayRouters[i].idRouters) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayRouters[i].nameRouters) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayRouters[i].tipoRouters)+'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayRouters[i].serialnumRouters) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayRouters[i].macRouters) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayRouters[i].ipstaticRouters) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayRouters[i].dyndnsRouters) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayRouters[i].nomPropietario) +'</td>';
			tdTabla +='</tr>';
		} // fin for
			
		// montamos la table
		montarTabla(nombreDiv, thTabla, tdTabla, thTabla, "contenido");
	}
	
	// pag. dashboard - bloque 'informacion - apartado routers'
	function select_routers_resumen_info_Dashboard(nombreDiv){
		var routLibres = 0;
		var routAsignadas =0;
		
		for(var i=0; i < arrayRouters.length; i++){
			if(arrayRouters[i].asignado== 0){
				routLibres++;
			}else{	
				routAsignadas++;
			}								
		}					
		
		var tabla_dashboard_info = datatable_dashboard_info(arrayRouters.length, routLibres, routAsignadas, 'Total', 'Libres', 'Asignados', nombreDiv);
		cargandoDatos("resumen_Dashboard_router","end" , 'seleccion');  // Quito el 'cargando datos.. '
		$("#resumen_Dashboard_router").html(tabla_dashboard_info);
	}
	
	// pag. dashboard  - bloque '10 ultimas altas'
	function select_routers_tabla_ultimas(nombreDiv){

		// thead
		var  thTabla = '<tr>';		
			thTabla +='<th class="th_turquesa">Id </th>';
			thTabla +='<th class="th_turquesa">Nombre</th>';
			thTabla +='<th class="th_turquesa">Numero Serial</th>';
			thTabla +='<th class="th_turquesa">Propietario</th>';
		thTabla +='</tr>';				
		
		// tbody
		var tdTabla = '';
		for(var i=0; i < arrayRouters.length; i++){
			tdTabla +=' <tr>';
				tdTabla +=' <td> '+ validarNulo(arrayRouters[i].idRouters) +'</td>';  // clientes activos
				tdTabla +=' <td> '+ validarNulo(arrayRouters[i].nameRouters) +'</td>';  // clientes activos
				tdTabla +=' <td> '+ validarNulo(arrayRouters[i].serialnumRouters) +'</td>';  //clientes desactivos
				tdTabla +=' <td> '+ validarNulo(arrayRouters[i].nomPropietario) +'</td>';  //clientes desactivos
			tdTabla +='</tr>';
		}
				
		// Montamos la tablas 
		montarTabla(nombreDiv, thTabla, tdTabla, thTabla, "ultimos_Routers");
	}
	
	
	
	
	
/************************************************************************************
#
# TABLA SIM
#
************************************************************************************/	
	/************************************************************************************
		validamos los datos introducidos en el formulario
	************************************************************************************/
	function validar_datos_sim(operacion){
		validar_div_visible("successMsg", "hide");
		var tipoCount="sim";
		countSim=0;
		
		
		//N�mero de sim 1
		var numSim01 = validar_input('numSim01',  tipoCount);  		
		
		//N�mero de sim 2
		var numSim02 = validar_input('numSim02',  tipoCount);   		
		
		//C�digo pin de la sim
		var pinSim = validar_input('pinSim',  tipoCount);   		

		//C�digo punk de la sim
		var punkSim = validar_input('punkSim',  tipoCount);   	
		
		//Modelo de la sim
		var modelSim = validar_input('modelSim',  tipoCount);   			
				
		//selecciona_opcion(operacion, countSim);
		if((countSim==5) || (countSim==4)){	
			// Guardando datos
			cargandoDatos("guardandoDatos", 'start', operacion); //ok
		
			//Lo mandamos a la operacion que le corresponda
			switch(operacion){
				case 'insert': //alta
					//if(idClienteSeleccion=='-1') {idClienteSeleccion=0;}
					insertar_datos_sim(numSim01, numSim02, pinSim, punkSim, modelSim);
					break;
					
				case 'update': //modificar
					var idSim2 =  $("#idSim2").val(); 
					var fechaA =  $("#fechaAlta").val(); 
					var fechaM =  $("#fechaModificacion").val(); 
					modificar_datos_sim(numSim01, numSim02, pinSim, punkSim, modelSim, idSim2, fechaA, fechaM);
					break;
			}
		}
	}		
	
	
	/************************************************************************************
		Insertamos los datos en la base de datos eude tabla tipoProducto
	************************************************************************************/
	function insertar_datos_sim(numSim01, numSim02, pinSim, punkSim, modelSim){
		$.ajax({
			type: 'GET',
			url: 'io/sim/insert_sim.php',
			data: { 
				'numSim01' : numSim01,
				'numSim02' : numSim02,
				'pinSim' : pinSim,
				'punkSim' : punkSim,
				'modelSim' : modelSim,
				'tiempoDesfase' : desfaseHoraria()
			},
			success: function(response) {
				var arrayResult = $.parseJSON(response);
				despuesDeGuardar(arrayResult, 0); 						
			}
		});
	}
	
	
	/************************************************************************************
		Modificamos los datos en la base de datos eude tabla sim
	************************************************************************************/
	function modificar_datos_sim(numSim01, numSim02, pinSim, punkSim, modelSim, idSelect,fechaA, fechaM){
		$.ajax({
			type: 'GET',
			url: 'io/sim/update_sim.php',
			data: { 
				'idSelect' : idSelect,
				'numSim01' : numSim01,
				'numSim02' : numSim02,
				'pinSim' : pinSim,
				'punkSim' : punkSim,
				'modelSim' : modelSim,
				'fechaA' : fechaA,
				'fechaM' : fechaM,
				'tiempoDesfase' : desfaseHoraria()
			},
			success: function(response) {	
				var arrayResult = $.parseJSON(response);
				despuesDeGuardar(arrayResult, 0); 							
			}
		});
	}
	
	
	/************************************************************************************
		Mostramos los datos que nos han solicitado en 'consultar sim'
	************************************************************************************/
	function mostrar_datos_sim(idSelecionado, operacionSQL){
		for(var i=0; i < arraySim.length; i++){
			if(arraySim[i].idSim == idSelecionado){
				mostrar_datos("idSim2", arraySim[i].idSim, operacionSQL);
				mostrar_datos("numSim01", arraySim[i].numSim01, operacionSQL);
				mostrar_datos("numSim02", arraySim[i].numSim02, operacionSQL);
				mostrar_datos("pinSim", arraySim[i].pin, operacionSQL);
				mostrar_datos("punkSim", arraySim[i].punk, operacionSQL);
				mostrar_datos("modelSim", arraySim[i].model, operacionSQL);
				mostrar_datos("idEmpresaSim", arraySim[i].nomPropietario, operacionSQL);
				mostrar_datos("fechaAlta", arraySim[i].fecha_alta, operacionSQL);
				mostrar_datos("fechaModificacion", arraySim[i].fecha_mod, operacionSQL);
				mostrar_datos("asignada", arraySim[i].asignada, operacionSQL); 
				insert_log_acciones(arraySim[i]);
			}
		}
	}
	
	
	/************************************************************************************
		Select-option y datatable de la sim
	************************************************************************************/
	function select_sim(nombreDiv, tipoObjeto){
		$.ajax({
			type: 'GET',
			url: 'io/sim/select_sim.php',
			data: { 	
				'tiempoDesfase' : desfaseHoraria() 
			},
			success: function(response) {
				arraySim = $.parseJSON(response);
				totalSim = arraySim.length;
				
				switch(tipoObjeto){
					case 'selectOption':
						// Select-option formulario sim	 - todas					
						select_sim_selectOption(nombreDiv, 'todo');
						break;
						
					case 'libres':
						// Select-option formulario sim - solo las libres
						select_sim_selectOption(nombreDiv, 'libre');
						break;
					
					case 'dataTable':						
						// tabla de la pag 'consultar' sim 
						select_sim_dataTable(nombreDiv);
						break;

				}//fin switch
			}//fin success
		});
	}
	
	
	function select_sim_selectOption(nombreDiv, opcion){
		var html_selectOption ="";
		html_selectOption += "<select class='form-control' id='"+nombreDiv+"' >";
			html_selectOption += "<option value='-1' selected='selected' name='defaultSim'> "+ txtSelectOptionDefault +" </option>";
			for(var i=1; i < arraySim.length; i++){				
				if(opcion == 'libre'){
					if(arraySim[i].asignada==0)
					html_selectOption += optionSelect_bloques(arraySim[i].idSim, arraySim[i].numSim01, arraySim[i].asignada, i);
				}else{	
					html_selectOption += optionSelect_bloques(arraySim[i].idSim, arraySim[i].numSim01, arraySim[i].asignada, i);			
				} 
			} // fin for
		html_selectOption += "</select>";
		cargandoDatos("idSim", 'end', 'seleccion');
		$("#selectOption_idSim").html(html_selectOption);
	}
	
	// tabla de la pag 'consultar' sim 
	function select_sim_dataTable(nombreDiv){
		// thead
		var  thTabla = '<tr>';
			thTabla +='<th class="th_turquesa">Id</th>';
			thTabla +='<th class="th_turquesa">Num 1</th>';
			thTabla +='<th class="th_turquesa">Num 2</th>';
			thTabla +='<th class="th_turquesa">Pin</th>';
			thTabla +='<th class="th_turquesa">Punk</th>';
			thTabla +='<th class="th_turquesa">Modelo</th>';
			thTabla +='<th class="th_turquesa">Empresa</th>';
			thTabla +='<th class="th_turquesa">F. Alta</th>';
			thTabla +='<th class="th_turquesa">F. Modificaci'+min_o+'n</th>';
			thTabla +='<th class="th_turquesa">Estado</th>';
		thTabla +='</tr>';
			
		// tbody
		var tdTabla =' ';
		for(var i=1; i < arraySim.length; i++){
			tdTabla +=' <tr>';
				tdTabla +=' <td> '+ validarNulo(arraySim[i].idSim) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arraySim[i].numSim01) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arraySim[i].numSim02) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arraySim[i].pin) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arraySim[i].punk) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arraySim[i].model) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arraySim[i].nomPropietario) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arraySim[i].fecha_alta) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arraySim[i].fecha_mod) +'</td>';
				tdTabla +=' <td> '+ validarPropietario(arraySim[i].propietario, "sim") +'</td>';
			tdTabla +='</tr>';
		}
		
		// Montamos la tabla
		montarTabla(nombreDiv, thTabla, tdTabla, thTabla, "contenido");
	}

/************************************************************************************
#
# TABLA TIPO_AVISO
#
************************************************************************************/	
	
	/************************************************************************************
		Validacion de los datos introducidos en el formulario
	************************************************************************************/
	function validar_datos_tipo_aviso(operacion){
		validar_div_visible("successMsg", "hide");
		var tipoCount="tipoAviso";
		countTipoAviso=0;
		
		
		//Nombre del tipo de aviso
		var nombreTipoAviso = validar_input("nombreTipoAviso", tipoCount);
		
		//Descripci�n del tipo de aviso
		var descripcionTipoAviso = validar_input("descripcionTipoAviso" , tipoCount );
				
		//selecciona_opcion(operacion, countTipoAviso);
		if(countTipoAviso==2) {	
			// Guardando datos
			cargandoDatos("guardandoDatos", 'start', operacion); //ok
		
			//Lo mandamos a la operacion que le corresponda
			switch(operacion){
				case 'insert': 
					insertar_datos_tipo_aviso(nombreTipoAviso, descripcionTipoAviso);
					break;
					
				case 'update': 
					var idTipoAviso2 =  $("#idTipoAviso2").val(); 
					var fechaA =  $("#fechaAlta").val(); 
					var fechaM =  $("#fechaModificacion").val();	
					modificar_datos_tipo_aviso(nombreTipoAviso, descripcionTipoAviso, idTipoAviso2, fechaA, fechaM);
					break;
			}
		}
	}		
	
	
	/************************************************************************************
		Insertamos los datos en la base de datos eude tabla tipo_aviso
	************************************************************************************/
	function insertar_datos_tipo_aviso(nombre, descripcion){
		$.ajax({
			type: 'GET',
			url: 'io/tipoAviso/insert_tipo_aviso.php',
			data: { 
				'nombre' : nombre,
				'descripcion' : descripcion,
				'tiempoDesfase' : desfaseHoraria()
			},
			success: function(response) {
				var arrayResult = $.parseJSON(response);
				despuesDeGuardar(arrayResult, 0); 		
			}
		});
	}
	

	
	
	/************************************************************************************
		Modificamos los datos en la base de datos eude tabla tipo_aviso
	************************************************************************************/
	function modificar_datos_tipo_aviso(nombre, descripcion, idSelect, fechaA, fechaM){
		$.ajax({
			type: 'GET',
			url: 'io/tipoAviso/update_tipo_aviso.php',
			data: { 
				'idSelect' : idSelect,
				'nombre' : nombre,
				'descripcion' : descripcion,
				'tiempoDesfase' : desfaseHoraria(),				
				'fechaA' : fechaA,				
				'fechaM' : fechaM				
			},
			success: function(response) {
				var arrayResult = $.parseJSON(response);
				despuesDeGuardar(arrayResult, 0); 						
			}
		});
	}
	

	/************************************************************************************
		Mostramos los datos que nos han solicitado en 'consultar sim'
	************************************************************************************/
	function mostrar_datos_tipo_aviso(idSelecionado, operacionSQL){
			for(var i=0; i < arrayTipoAviso.length; i++){
				if(arrayTipoAviso[i].id == idSelecionado){
				mostrar_datos("idTipoAviso2", arrayTipoAviso[i].id, operacionSQL);
				mostrar_datos("nombreTipoAviso", arrayTipoAviso[i].nom, operacionSQL);
				mostrar_datos("descripcionTipoAviso", arrayTipoAviso[i].descrip, operacionSQL);
				mostrar_datos("fechaAlta", arrayTipoAviso[i].fecha_alta, operacionSQL);
				mostrar_datos("fechaModificacion", arrayTipoAviso[i].fecha_mod, operacionSQL);
				mostrar_datos("fechaModificacion", arrayTipoAviso[i].fecha_mod, operacionSQL);
				insert_log_acciones(arrayTipoAviso[i]);
			}
		}
	}
	
	
	/************************************************************************************
		Select de los routers que tenemos.
		Sacamos el array de los routers que existen en la BD.
		Hacemos un select-option que mostramos en el formulario de alta de las c�maras
	************************************************************************************/
	function select_tipo_aviso(nombreDiv, tipoObjeto){
		$.ajax({
			type: 'GET',
			url: 'io/tipoAviso/select_tipo_aviso.php',
			data: { 	
				'tiempoDesfase' : desfaseHoraria() 
			},
			success: function(response) {
				arrayTipoAviso = $.parseJSON(response);
					
				switch(tipoObjeto){
					case 'selectOption':
						// Select-option formulario tipoAviso	 - todas			
						select_tipo_aviso_selectOption(nombreDiv);
						break;
					
					case 'dataTable':
						// tabla de la pag 'consultar' tipo aviso 
						select_tipo_aviso_dataTable(nombreDiv);										
						break;
				}//fin switch
			}//fin success
		});
	}
	
	//Select-option formulario sim
	function 	select_tipo_aviso_selectOption(nombreDiv){
		var html_selectOption ="";
		html_selectOption += "<select class='form-control' id='"+nombreDiv+"' >";
			html_selectOption += "<option value='-1' selected='selected' name='defaultTipoAviso'> "+ txtSelectOptionDefault +" </option>";
			for(var i=0; i < arrayTipoAviso.length; i++){
				html_selectOption += optionSelect_bloques(arrayTipoAviso[i].id , arrayTipoAviso[i].nom, 1, i);
			} // fin for
		html_selectOption += "</select>";
		cargandoDatos("idTipoAviso", 'end' , 'seleccion');
		$("#selectOption_idTipoAviso").html(html_selectOption);
	}
	
	// tabla de la pag 'consultar' tipo aviso 
	function select_tipo_aviso_dataTable(nombreDiv){
		// thead
		var  thTabla = "";
		thTabla +='<tr>';
			thTabla +='<th class="th_turquesa">Id</th>';
			thTabla +='<th class="th_turquesa">Nombre</th>';
			thTabla +='<th class="th_turquesa">Descripci'+min_o+'n</th>';						
			thTabla +='<th class="th_turquesa">F. Alta</th>';
			thTabla +='<th class="th_turquesa">F. Modificaci'+min_o+'n</th>';
		thTabla +='</tr>';
		
		// tbody
		var tdTabla = "";
		for(var i=0; i < arrayTipoAviso.length;  i++){
			tdTabla +=' <tr>';
				tdTabla +=' <td> '+ validarNulo(arrayTipoAviso[i].id) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayTipoAviso[i].nom) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayTipoAviso[i].descrip) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayTipoAviso[i].fecha_alta) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayTipoAviso[i].fecha_mod) +'</td>';
			tdTabla +='</tr>';
		} 
				
		// Monto la tabla		
		montarTabla(nombreDiv, thTabla, tdTabla, thTabla, "contenido");
	}


/************************************************************************************
#
# TABLA TIPO_EVENTOS_LOGS tipo_evento_log
#
************************************************************************************/		
	/************************************************************************************	
		Validar datos del formulario
	************************************************************************************/
	function validar_datos_tipo_evento_log(operacion){
		validar_div_visible("successMsg", "hide");
		
		var tipoCount="tipoEventoLog";
		countTipoEventoLog=0;
		
		
		//Nombre del tipo de producto
		var nombre = validar_input('nombreEvento',  tipoCount);   	
		
		// Descripci�n del tipo de producto
		var descripcion = validar_input('descEvento',  tipoCount);   	
		
		
		
		//selecciona_opcion(operacion, countCamara);
		//Si hemos pasado la validacion lo enviaremos a la opci�n que le corresponda
		if(countTipoEventoLog==2){	
			// Guardando datos
			cargandoDatos("guardandoDatos", 'start', operacion); // ok
		
			switch(operacion){
				case 'insert': //alta
					insertar_datos_tipo_evento_log(nombre, descripcion);
					break;
					
				case 'update': //modificar
					idSelect =  $("#idEvento2").val(); 
					modificar_datos_tipo_evento_log(nombre, descripcion, idSelect);
					break;
			}
		}
	}		
	
	
	/************************************************************************************
		Insertamos los datos en la base de datos eude tabla tipoProducto
	************************************************************************************/
	function insertar_datos_tipo_evento_log(nombre, descripcion){
		$.ajax({
			type: 'GET',
			url: 'io/tipoEventoLog/insert_tipo_evento_log.php',
			data: { 
				'nombre' : nombre,
				'descripcion' : descripcion,
				'tiempoDesfase' : desfaseHoraria()
			},
			success: function(response) {
				var arrayResult = $.parseJSON(response);
				despuesDeGuardar(arrayResult, 0); 		
			}//fin succes
		});
	}
	
	
	/************************************************************************************
		Modificamos los datos en la base de datos eude tabla tipoProducto
	************************************************************************************/
	function modificar_datos_tipo_evento_log(nombre, descripcion, idSelect){
		$.ajax({
			type: 'GET',
			url: 'io/tipoEventoLog/update_tipo_evento_log.php',
			data: { 
				'idSelect' : idSelect,
				'nombre' : nombre,
				'descripcion' : descripcion,
				'tiempoDesfase' : desfaseHoraria()
			},
			success: function(response) {
				var arrayResult = $.parseJSON(response);
				despuesDeGuardar(arrayResult, 0); 							
			}
		});
	}
	

	/************************************************************************************
		Mostramos los datos que nos han solicitado en 'consultar tipos evento_log'
	************************************************************************************/
	function mostrar_datos_tipo_evento_log(idSelecionado, operacionSQL){
		for(var i=0; i < arrayTipoEventoLog.length; i++){
			if(arrayTipoEventoLog[i].id == idSelecionado){
				mostrar_datos("idEvento2", arrayTipoEventoLog[i].id, operacionSQL);
				mostrar_datos("nombreEvento", arrayTipoEventoLog[i].nombre, operacionSQL);
				mostrar_datos("descEvento", arrayTipoEventoLog[i].descripcion, operacionSQL);
				mostrar_datos("fechaAlta",arrayTipoEventoLog[i].fecha_alta, operacionSQL);
				mostrar_datos("fechaModificacion", arrayTipoEventoLog[i].fecha_mod, operacionSQL);
				insert_log_acciones(arrayTipoEventoLog[i]);	
			}
		}
	}
	
	
	/************************************************************************************
		Select de los tipos de productos que tenemos que tenemos.
	************************************************************************************/
	function select_tipo_evento_log(nombreDiv, tipoObjeto){
		$.ajax({
			type: 'GET',
			url: 'io/tipoEventoLog/select_tipo_evento_log.php',
			data: { 	
				'tiempoDesfase' : desfaseHoraria()  
			},
			success: function(response) {
				arrayTipoEventoLog = $.parseJSON(response);
						
				switch(tipoObjeto){
					case 'selectOption':
						// Select-option todos
						select_tipo_evento_log_selectOption(nombreDiv);
						break;
					
					case 'dataTable':
						// tabla de la pag 'consultar' tipo evento log 
						select_tipo_evento_log_dataTable(nombreDiv);
						break;
				}//fin switch
			}//fin success
		});
	}
	
	// Select-option todos
	function select_tipo_evento_log_selectOption(nombreDiv){
		var html_selectOption ="";
		html_selectOption += "<select class='form-control' id='"+nombreDiv+"' >";
			html_selectOption += "<option value='-1' selected='selected' name='defaultRouter'> "+ txtSelectOptionDefault +" </option>";
			for(var i=0; i < arrayTipoEventoLog.length; i++){
				html_selectOption += optionSelect_bloques(arrayTipoEventoLog[i].id, arrayTipoEventoLog[i].nombre, 1, i);
			} // fin for
		html_selectOption += "</select>";
		cargandoDatos("idTipoEventoLogSelect", 'end', 'seleccion');
		$("#selectOption_idTipoEventoLogSelect").html(html_selectOption);
	}
	
	// tabla de la pag 'consultar' tipo evento log 
	function select_tipo_evento_log_dataTable(nombreDiv){
		var  thTabla = "";
		thTabla +='<tr>';
			thTabla +='<th class="th_turquesa">Id</th>';
			thTabla +='<th class="th_turquesa">Evento</th>';
			thTabla +='<th class="th_turquesa">Descripci'+min_o+'n</th>';
			thTabla +='<th class="th_turquesa">Fecha Alta</th>';
			thTabla +='<th class="th_turquesa">Fecha Modificaci'+min_o+'n</th>';
		thTabla +='</tr>';
								
		// Montamos la tabla
		var tdTabla ='';
		for(var i=0; i < arrayTipoEventoLog.length; i++){
			tdTabla +=' <tr>';
				tdTabla +=' <td> '+ validarNulo(arrayTipoEventoLog[i].id) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayTipoEventoLog[i].nombre) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayTipoEventoLog[i].descripcion) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayTipoEventoLog[i].fecha_alta) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayTipoEventoLog[i].fecha_mod) +'</td>';
			tdTabla +='</tr>';
		} 
		
		// Montamos la tabla
		montarTabla(nombreDiv, thTabla, tdTabla, thTabla, "contenido");
	}
	
/************************************************************************************
#
# TABLA TIPO_PRODUCTOS
#
************************************************************************************/	
	
	/************************************************************************************
		Validar datos del formulario
	************************************************************************************/
	function validar_datos_tipo_producto(operacion){
		validar_div_visible("successMsg", "hide");
		
		var tipoCount="tipoProducto";
		countTipoProducto=0;
		
		
		//Nombre del tipo de producto
		var nombreTipoProducto = validar_input('nombreTipoProducto',  tipoCount);   	
		
		// Descripci�n del tipo de producto
		var descTipoProducto = validar_input('descTipoProducto',  tipoCount);   	 
		
		//Tipo de la emisi�n 'live' o 'liveflv'		
		var tipoLiveTipoProducto = validar_selectOption('tipoLive',  tipoCount);   	
		
		//Miramos si estan o no seleccionados
		var tp_gps = comprobar_checkbox("tp_gps");
		var tp_wifi = comprobar_checkbox("tp_wifi");
		var tp_3g = comprobar_checkbox("tp_3g");
		var tp_4g = comprobar_checkbox("tp_4g");
				
		
		// Si hemos pasado la validacion lo enviaremos a la opci�n que le corresponda
		if(countTipoProducto<4){			
			// Guardando datos
			cargandoDatos("guardandoDatos", 'start', operacion); //ok
		
			switch(operacion){
				case 'insert': //alta
					insertar_datos_tipo_producto(nombreTipoProducto, descTipoProducto,tipoLiveTipoProducto, tp_gps, tp_wifi, tp_3g, tp_4g);
					break;
					
				case 'update': //modificar
					var idTipoProducto2 =  $("#idTipoProducto2").val(); 
					modificar_datos_tipo_producto(idTipoProducto2, nombreTipoProducto, descTipoProducto,tipoLiveTipoProducto, tp_gps, tp_wifi, tp_3g, tp_4g);
					break;
			}
		}
	}		
	
	
	/************************************************************************************
		Insertamos los datos en la base de datos eude tabla tipoProducto
	************************************************************************************/
	function insertar_datos_tipo_producto(nombreTipoProducto, descTipoProducto,tipoLiveTipoProducto, tp_gps, tp_wifi, tp_3g, tp_4g){
		$.ajax({
			type: 'GET',
			url: 'io/tipoProducto/insert_tipo_producto.php',
			data: { 
				'nombreTipoProducto' : nombreTipoProducto,
				'descTipoProducto' : descTipoProducto,
				'tipoLiveTipoProducto' : tipoLiveTipoProducto,
				'tp_gps' : tp_gps,
				'tp_wifi' : tp_wifi,
				'tp_3g' : tp_3g,
				'tp_4g' : tp_4g,
				'tiempoDesfase' : desfaseHoraria()				
			},
			success: function(response) {
				var arrayResult = $.parseJSON(response);
				despuesDeGuardar(arrayResult, 0); 						
			}
		});
	}
	
	
	/************************************************************************************
		Modificamos los datos en la base de datos eude tabla tipoProducto
	************************************************************************************/
	function modificar_datos_tipo_producto(idSelect, nombre, desc,tipoLive,tp_gps, tp_wifi, tp_3g, tp_4g){
		$.ajax({
			type: 'GET',
			url: 'io/tipoProducto/update_tipo_producto.php',
			data: { 
				'idSelect' : idSelect,
				'nombre' : nombre,
				'desc':desc,
				'tipoLive':tipoLive,
				'tp_gps':tp_gps,
				'tp_wifi':tp_wifi,
				'tp_3g':tp_3g,
				'tp_4g':tp_4g,
				'tiempoDesfase' : desfaseHoraria()
			},
			success: function(response) {		
				var arrayResult = $.parseJSON(response);
				despuesDeGuardar(arrayResult, 0); 		
			}
		});
	}
	

	/************************************************************************************
		Mostramos los datos que nos han solicitado en 'consultar tipos productos'
	************************************************************************************/
	function mostrar_datos_tipo_producto(idSelecionado, operacionSQL){
		for(var i=0; i < arrayTipoProducto.length; i++){
			if(arrayTipoProducto[i].idTipoProducto == idSelecionado){
				mostrar_datos("idTipoProducto2", arrayTipoProducto[i].idTipoProducto, operacionSQL);
				mostrar_datos("nombreTipoProducto", arrayTipoProducto[i].nombre, operacionSQL);
				mostrar_datos("descTipoProducto", arrayTipoProducto[i].descr, operacionSQL);
				mostrar_datos("tipoLive", arrayTipoProducto[i].tipoLive, operacionSQL);
				mostrar_datos("selectEstado", arrayTipoProducto[i].estado, operacionSQL);
				mostrar_datos("tp_gps", arrayTipoProducto[i].tp_gps, operacionSQL);
				mostrar_datos("tp_wifi", arrayTipoProducto[i].tp_wifi, operacionSQL);
				mostrar_datos("tp_3g", arrayTipoProducto[i].tp_3G, operacionSQL);
				mostrar_datos("tp_4g", arrayTipoProducto[i].tp_4G, operacionSQL);
				mostrar_datos("fechaAlta", arrayTipoProducto[i].fecha_alta, operacionSQL);
				mostrar_datos("fechaModificacion", arrayTipoProducto[i].fecha_mod, operacionSQL);
				insert_log_acciones(arrayTipoProducto[i]);		
			}
		}
	}
	
	
	/************************************************************************************
		Select de los tipos de productos que tenemos que tenemos.
	************************************************************************************/
	function select_tipo_producto(nombreDiv, tipoObjeto){
		$.ajax({
			type: 'GET',
			url: 'io/tipoProducto/select_tipo_producto.php',
			data: { 	
				'tiempoDesfase' : desfaseHoraria() 
			},
			success: function(response) {
				arrayTipoProducto = $.parseJSON(response);
								
				switch(tipoObjeto){
					case 'selectOption':
						// Select-option 
						select_tipo_producto_selectOption(nombreDiv);
						break;
					
					case 'dataTable':
						// tabla de la pag 'consultar' tipoProducto 
						select_tipo_producto_datatable(nombreDiv);
						break;
				}//fin switch
			}//fin success
		});
	}
	
	function select_tipo_producto_selectOption(nombreDiv){
		var html_selectOption ="";
		html_selectOption += "<select class='form-control' id='"+nombreDiv+"' >";
			html_selectOption += "<option value='-1' selected='selected' name='defaultRouter'> "+ txtSelectOptionDefault +" </option>";
						
			for(var i=0; i < arrayTipoProducto.length; i++){
				html_selectOption += optionSelect_bloques(arrayTipoProducto[i].idTipoProducto, arrayTipoProducto[i].nombre, 1, i);
			} // fin for
		html_selectOption += "</select>";
		cargandoDatos("idTipoProducto", 'end', 'seleccion');
		$("#selectOption_idTipoProducto").html(html_selectOption);
	}
	
	// tabla de la pag 'consultar' tipoProducto 
	function select_tipo_producto_datatable(nombreDiv){
		// thead
		var  thTabla = '<tr>';
			thTabla +='<th class="th_turquesa">Id</th>';
			thTabla +='<th class="th_turquesa">Nombre</th>';
			thTabla +='<th class="th_turquesa">Descripci'+min_o+'n</th>';
			thTabla +='<th class="th_turquesa">Tipo Live</th>';
			thTabla +='<th class="th_turquesa">Estado</th>';
			thTabla +='<th class="th_turquesa">GPS</th>';							
			thTabla +='<th class="th_turquesa">WIFI</th>';
			thTabla +='<th class="th_turquesa">3G</th>';
			thTabla +='<th class="th_turquesa">4G</th>';
		thTabla +='</tr>';
				
		// tbody	
		var tdTabla ='';
		for(var i=0; i < arrayTipoProducto.length; i++){
			tdTabla +=' <tr>';
				tdTabla +=' <td> '+ validarNulo(arrayTipoProducto[i].idTipoProducto) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayTipoProducto[i].nombre) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayTipoProducto[i].descr) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayTipoProducto[i].tipoLive) +'</td>';
				tdTabla +=' <td> '+ comprobar_booleano(arrayTipoProducto[i].estado, 'Activo', 'Desactivo') +'</td>';
				tdTabla +=' <td> '+ comprobar_booleano(arrayTipoProducto[i].tp_wifi, "SI", "No") +'</td>';
				tdTabla +=' <td> '+comprobar_booleano(arrayTipoProducto[i].tp_gps, "SI", "No")+'</td>';
				tdTabla +=' <td> '+ comprobar_booleano(arrayTipoProducto[i].tp_3G, "SI", "No") +'</td>';
				tdTabla +=' <td> '+ comprobar_booleano(arrayTipoProducto[i].tp_4G, "SI", "No") +'</td>';
			tdTabla +='</tr>';
		} // fin for	
		
		// Montamos la tabla
		montarTabla(nombreDiv, thTabla, tdTabla, thTabla, "contenido");
	}
		

/************************************************************************************
#
# TABLA TIPO_ROUTER
#
************************************************************************************/	
	
	/************************************************************************************
		Validar datos del formulario
	************************************************************************************/
	function validar_datos_tipo_router(operacion){
		validar_div_visible("successMsg", "hide");
		
		var tipoCount="tipoRouter";
		countTipoRouter=0;
		
		
		// Validar campos 
		var nombre = validar_input('nombre',  tipoCount);   
		var descripcion = validar_input('descripcion',  tipoCount);   	 
		
		// Si hemos pasado la validacion lo enviaremos a la opci�n que le corresponda
		if( countTipoRouter == 2){		
			cargandoDatos("guardandoDatos", 'start', operacion); // inicio loader cargando datos
		
			switch(operacion){
				case 'insert':
					insertar_datos_tipo_router(nombre, descripcion); 
					break;
					
				case 'update': 
					var idTipoRouter2 =  $("#idTipoRouter2").val(); 
					modificar_datos_tipo_router(idTipoRouter2, nombre, descripcion);
					break;
			}
		}
	}		
	
	
	/************************************************************************************
		Insertamos los datos en la base de datos configstream tabla tipoRouter
	************************************************************************************/
	function insertar_datos_tipo_router(nombre, descripcion){
		$.ajax({
			type: 'GET',
			url: 'io/tipoRouter/insert_tipo_router.php',
			data: { 
				'nombre' : nombre,
				'descripcion' : descripcion,
				'tiempoDesfase' : desfaseHoraria()				
			},
			success: function(response) {
				var arrayResult = $.parseJSON(response);
				despuesDeGuardar(arrayResult, 0); 			
			}
		});
	}
	
	
	/************************************************************************************
		Modificamos los datos en la base de datos eude tabla tipoProducto
	************************************************************************************/
	function modificar_datos_tipo_router(idSelect, nombre, descripcion){
		$.ajax({
			type: 'GET',
			url: 'io/tipoRouter/update_tipo_router.php',
			data: { 
				'idSelect' : idSelect,
				'nombre' : nombre,
				'descripcion':descripcion,
				'tiempoDesfase' : desfaseHoraria()
			},
			success: function(response) {		
				var arrayResult = $.parseJSON(response);
				despuesDeGuardar(arrayResult, 0); 							
			}
		});
	}
	

	/************************************************************************************
		Mostramos los datos que nos han solicitado en 'consultar tipos productos'
	************************************************************************************/
	function mostrar_datos_tipo_router(idSelecionado, operacionSQL){
		for(var i=0; i < arrayTipoRouter.length; i++){
			if(arrayTipoRouter[i].id == idSelecionado){
				mostrar_datos("idTipoRouter2", arrayTipoRouter[i].id, operacionSQL);
				mostrar_datos("nombre", arrayTipoRouter[i].nombre, operacionSQL);
				mostrar_datos("descripcion", arrayTipoRouter[i].descripcion, operacionSQL);
				mostrar_datos("fechaAlta", arrayTipoRouter[i].fecha_alta, operacionSQL);
				mostrar_datos("fechaModificacion", arrayTipoRouter[i].fecha_modificacion, operacionSQL);
				insert_log_acciones(arrayTipoRouter[i]);		
			}
		}
	}
	
	
	/************************************************************************************
		Select de los tipos de routers que tenemos que tenemos.
	************************************************************************************/
	function select_tipo_router(nombreDiv, tipoObjeto){
		$.ajax({
			type: 'GET',
			url: 'io/tipoRouter/select_tipo_router.php',
			data: { 	
				'tiempoDesfase' : desfaseHoraria() 
			},
			success: function(response) {
				arrayTipoRouter = $.parseJSON(response);
				console.log(arrayTipoRouter);
								
				switch(tipoObjeto){
					case 'selectOption':
						// Select-option 
						select_tipo_router_selectOption(nombreDiv);
						break;
					
					case 'dataTable':
						// tabla de la pag 'consultar' tipoRouter
						select_tipo_router_datatable(nombreDiv);
						break;
				}//fin switch
			}//fin success
		});
	}
	
	function select_tipo_router_selectOption(nombreDiv){
		var html_selectOption ="";
		html_selectOption += "<select class='form-control' id='"+nombreDiv+"' >";
			html_selectOption += "<option value='-1' selected='selected' name='defaultRouter'> "+ txtSelectOptionDefault +" </option>";
						
			for(var i=0; i < arrayTipoRouter.length; i++){
				html_selectOption += optionSelect_bloques(arrayTipoRouter[i].id, arrayTipoRouter[i].nombre, 1, i);
			} // fin for
		html_selectOption += "</select>";
		cargandoDatos("idTipoRouter", 'end', 'seleccion');
		$("#selectOption_idTipoRouter").html(html_selectOption);
	}
	
	// tabla de la pag 'consultar' tipoProducto 
	function select_tipo_router_datatable(nombreDiv){
		// thead
		var  thTabla = '<tr>';
			thTabla +='<th class="th_turquesa">Id</th>';
			thTabla +='<th class="th_turquesa">Nombre</th>';
			thTabla +='<th class="th_turquesa">Descripci'+min_o+'n</th>';
			thTabla +='<th class="th_turquesa">fechaAlta</th>';
			thTabla +='<th class="th_turquesa">fechaModificacion</th>';
		thTabla +='</tr>';
				
		// tbody	
		var tdTabla ='';
		for(var i=0; i < arrayTipoRouter.length; i++){
			tdTabla +=' <tr>';
				tdTabla +=' <td> '+ validarNulo(arrayTipoRouter[i].id) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayTipoRouter[i].nombre) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayTipoRouter[i].descripcion) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayTipoRouter[i].fechaAlta) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayTipoRouter[i].fechaModificacion) +'</td>';
			tdTabla +='</tr>';
		} // fin for	
		
		// Montamos la tabla
		montarTabla(nombreDiv, thTabla, tdTabla, thTabla, "contenido");
	}
	

/************************************************************************************
#
# TABLA TIPO_CAMARA
#
************************************************************************************/	
	
	/************************************************************************************
		Validar datos del formulario
	************************************************************************************/
	function validar_datos_tipo_camara(operacion){
		validar_div_visible("successMsg", "hide");
		
		var tipoCount="tipoCamara";
		countTipoCamara=0;
				
		// Validar campos 
		var nombre = validar_input('nombre',  tipoCount);   
		var descripcion = validar_input('descripcion',  tipoCount);   	 
		
		// Si hemos pasado la validacion lo enviaremos a la opci�n que le corresponda
		if( countTipoCamara == 2){		
			cargandoDatos("guardandoDatos", 'start', operacion); // inicio loader cargando datos
		
			switch(operacion){
				case 'insert':
					insertar_datos_tipo_camara(nombre, descripcion); 
					break;
					
				case 'update': 
					var idTipoCamara2 =  $("#idTipoCamara").val(); 
					modificar_datos_tipo_camara(idTipoCamara2, nombre, descripcion);
					break;
			}
		}
	}		
	
	
	/************************************************************************************
		Insertamos los datos en la base de datos configstream tabla tipoRouter
	************************************************************************************/
	function insertar_datos_tipo_camara(nombre, descripcion){
		$.ajax({
			type: 'GET',
			url: 'io/tipoCamara/insert_tipo_camara.php',
			data: { 
				'nombre' : nombre,
				'descripcion' : descripcion,
				'tiempoDesfase' : desfaseHoraria()				
			},
			success: function(response) {
				var arrayResult = $.parseJSON(response);
				despuesDeGuardar(arrayResult, 0); 				
			}
		});
	}
	
	
	/************************************************************************************
		Modificamos los datos en la base de datos eude tabla tipoProducto
	************************************************************************************/
	function modificar_datos_tipo_camara(idSelect, nombre, descripcion){
		$.ajax({
			type: 'GET',
			url: 'io/tipoCamara/update_tipo_camara.php',
			data: { 
				'idSelect' : idSelect,
				'nombre' : nombre,
				'descripcion':descripcion,
				'tiempoDesfase' : desfaseHoraria()
			},
			success: function(response) {		
				var arrayResult = $.parseJSON(response);
				despuesDeGuardar(arrayResult, 0); 		
			}
		});
	}
	

	/************************************************************************************
		Mostramos los datos que nos han solicitado en 'consultar tipos productos'
	************************************************************************************/
	function mostrar_datos_tipo_camara(idSelecionado, operacionSQL){
		for(var i=0; i < arrayTipoCamara.length; i++){
			if(arrayTipoCamara[i].id == idSelecionado){
				mostrar_datos("idTipoCamara2", arrayTipoCamara[i].id, operacionSQL);
				mostrar_datos("nombre", arrayTipoCamara[i].nombre, operacionSQL);
				mostrar_datos("descripcion", arrayTipoCamara[i].descripcion, operacionSQL);
				mostrar_datos("fechaAlta", arrayTipoCamara[i].fecha_alta, operacionSQL);
				mostrar_datos("fechaModificacion", arrayTipoCamara[i].fecha_modificacion, operacionSQL);
				insert_log_acciones(arrayTipoCamara[i]);		
			}
		}
	}
	
	
	/************************************************************************************
		Select de los tipos de routers que tenemos que tenemos.
	************************************************************************************/
	function select_tipo_camara(nombreDiv, tipoObjeto){
		$.ajax({
			type: 'GET',
			url: 'io/tipoCamara/select_tipo_camara.php',
			data: { 	
				'tiempoDesfase' : desfaseHoraria() 
			},
			success: function(response) {
				arrayTipoCamara = $.parseJSON(response);
								
				switch(tipoObjeto){
					case 'selectOption':
						// Select-option 
						select_tipo_camara_selectOption(nombreDiv);
						break;
					
					case 'dataTable':
						// tabla de la pag 'consultar' tipoCamara
						select_tipo_camara_datatable(nombreDiv);
						break;
				}//fin switch
			}//fin success
		});
	}
	
	function select_tipo_camara_selectOption(nombreDiv){
		var html_selectOption ="";
		html_selectOption += "<select class='form-control' id='"+nombreDiv+"' >";
			html_selectOption += "<option value='-1' selected='selected' name='defaultSelectO'> "+ txtSelectOptionDefault +" </option>";
						
			for(var i=0; i < arrayTipoCamara.length; i++){
				html_selectOption += optionSelect_bloques(arrayTipoCamara[i].id, arrayTipoCamara[i].nombre, 1, i);
			} // fin for
		html_selectOption += "</select>";
		cargandoDatos("idTipoCamara", 'end', 'seleccion');
		$("#selectOption_idTipoCamara").html(html_selectOption);
	}
	
	// tabla de la pag 'consultar' tipoProducto 
	function select_tipo_camara_datatable(nombreDiv){
		// thead
		var  thTabla = '<tr>';
			thTabla +='<th class="th_turquesa">Id</th>';
			thTabla +='<th class="th_turquesa">Nombre</th>';
			thTabla +='<th class="th_turquesa">Descripci'+min_o+'n</th>';
			thTabla +='<th class="th_turquesa">fechaAlta</th>';
			thTabla +='<th class="th_turquesa">fechaModificacion</th>';
		thTabla +='</tr>';
				
		// tbody	
		var tdTabla ='';
		for(var i=0; i < arrayTipoCamara.length; i++){
			tdTabla +=' <tr>';
				tdTabla +=' <td> '+ validarNulo(arrayTipoCamara[i].id) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayTipoCamara[i].nombre) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayTipoCamara[i].descripcion) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayTipoCamara[i].fechaAlta) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayTipoCamara[i].fechaModificacion) +'</td>';
			tdTabla +='</tr>';
		} // fin for	
		
		// Montamos la tabla
		montarTabla(nombreDiv, thTabla, tdTabla, thTabla, "contenido");
	}	
		


/************************************************************************************
#
# TABLA DISPOSITIVOS_EMISORES
#
************************************************************************************/	
/************************************************************************************
		Validar datos del formulario
	************************************************************************************/
function validar_datos_dispositivoEmisor(operacion){
	// console.log("Validar datos dispositivo emisor.... "  +  operacion);
		validar_div_visible("successMsg", "hide");
		
		var tipoCount="dispositivoEmisor";
		countDispositivoEmisor=0;
				
		// Validar campos 
		var nombre = validar_input('nombre',  tipoCount);   
		var descripcion = validar_input('descripcion',  tipoCount);   	 
		var numSerie = validar_input('numSerie',  tipoCount);   	 
		var modelo = validar_input('modelo',  tipoCount);   	 
		var versionSO = validar_input('versionSO',  tipoCount);   
		
		// Si hemos pasado la validacion lo enviaremos a la opci�n que le corresponda
		if( countDispositivoEmisor == 5){		
			cargandoDatos("guardandoDatos", 'start', operacion); // inicio loader cargando datos
		
			switch(operacion){
				case 'insert':
					insertar_datos_dispositivo_emisor(nombre, descripcion, numSerie, modelo, versionSO); 
					break;
					
				case 'update': 
					var idDispositivoEmisor =  $("#idDispositivoEmisor").val(); 
					modificar_datos_dispositivo_emisor(idDispositivoEmisor, nombre, descripcion, numSerie, modelo, versionSO);
					break;
			}
		}
		
	}		
	
	
	/************************************************************************************
		Insertamos los datos en la base de datos eude  tabla 'dispositivo_emisor'
	************************************************************************************/
	function insertar_datos_dispositivo_emisor(nombre, descripcion,numSerie, modelo, versionSO  ){
		console.log("insertar_datos_dispositivo_emisor");
		console.log(nombre + ", " + descripcion +" ," +  numSerie +" ," + modelo+" ," + versionSO);
		$.ajax({
			type: 'GET',
			url: 'io/dispositivoEmisor/insert_dispositivo_emisor.php',
			data: { 
				'nombre' : nombre,
				'descripcion' : descripcion,
				'numSerie' : numSerie,
				'modelo' : modelo,
				'versionSO' : versionSO,
				'tiempoDesfase' : desfaseHoraria()				
			},
			success: function(response) {
				var arrayResult = $.parseJSON(response); 
				// console.log(arrayResult);
				despuesDeGuardar(arrayResult, 0);				
			}
		});
	}
	
	
	/************************************************************************************
		Modificamos los datos en la base de datos eude tabla tipoProducto
	************************************************************************************/
	function modificar_datos_dispositivo_emisor(idSelect, nombre, descripcion, numSerie, modelo, versionSO ){
		// console.log("insertar_datos_dispositivo_emisor");
		// console.log(idSelect + " , " + nombre + ", " + descripcion +" ," +  numSerie +" ," + modelo+" ," + versionSO);
		$.ajax({
			type: 'GET',
			url: 'io/dispositivoEmisor/update_dispositivo_emisor.php',
			data: { 
				'idSelect' : idSelect,
				'nombre' : nombre,
				'descripcion' : descripcion,
				'numSerie' : numSerie,
				'modelo' : modelo,
				'versionSO' : versionSO,
				'tiempoDesfase' : desfaseHoraria()
			},
			success: function(response) {		
				var arrayResult = $.parseJSON(response);
				despuesDeGuardar(arrayResult, 0);				
			}
		});
	}
	

	/************************************************************************************
		Mostramos los datos que nos han solicitado en 'consultar tipos productos'
	************************************************************************************/
	function mostrar_datos_dispositivo_emisor(idSelecionado, operacionSQL){
		for(var i=0; i < arrayDispositivoEmision.length; i++){
			if(arrayDispositivoEmision[i].id == idSelecionado){
				mostrar_datos("id", arrayDispositivoEmision[i].id, operacionSQL);
				mostrar_datos("nombre", arrayDispositivoEmision[i].nombre, operacionSQL);
				mostrar_datos("descripcion", arrayDispositivoEmision[i].descripcion, operacionSQL);
				mostrar_datos("numSerie", arrayDispositivoEmision[i].numSerie, operacionSQL);
				mostrar_datos("modelo", arrayDispositivoEmision[i].modelo, operacionSQL);
				mostrar_datos("versionSO", arrayDispositivoEmision[i].versionSO, operacionSQL);
				console.log("ASIGNADO: " + arrayDispositivoEmision[i].asignado);
				if(arrayDispositivoEmision[i].asignado == 0 ) var asignado = "Libre"; else  var asignado = "Asignado a una empresa."; 
				mostrar_datos("asignado", asignado, operacionSQL);
				mostrar_datos("fechaAlta", arrayDispositivoEmision[i].fechaAlta, operacionSQL);
				mostrar_datos("fechaModificacion", arrayDispositivoEmision[i].fechaModificacion, operacionSQL);
				insert_log_acciones(arrayDispositivoEmision[i]);		
			}
		}
	}
	
	
	/************************************************************************************
		Select de los tipos de routers que tenemos que tenemos.
	************************************************************************************/
	function select_dispositivo_emisor(nombreDiv, tipoObjeto){
		console.log("select_dispositivo_emisor(" + nombreDiv + " , " + tipoObjeto + ")" );
		$.ajax({
			type: 'GET',
			url: 'io/dispositivoEmisor/select_dispositivo_emisor.php',
			data: { 	
				'tiempoDesfase' : desfaseHoraria() 
			},
			success: function(response) {
				arrayDispositivoEmision = $.parseJSON(response);
								
				switch(tipoObjeto){
					case 'libres': 	
						// Select-option formulario camaras	 - solo las que estan libres								
						select_dispositivo_emisor_selectOption(nombreDiv, 'libre');						
						break;
				
					case 'selectOption':
						// Select-option 
						select_dispositivo_emisor_selectOption(nombreDiv, 'todo');
						break;
					
					case 'dataTable':
						// tabla de la pag 'consultar' dispositivoEmisor
						select_dispositivo_emisor_datatable(nombreDiv);
						break;
				}//fin switch
			}//fin success
		});
	}

	
	function select_dispositivo_emisor_selectOption(nombreDiv, opcion){
		var html_selectOption ="";
		html_selectOption += "<select class='form-control' id='"+nombreDiv+"' >";
			html_selectOption += "<option value='-1' selected='selected' name='defaultRouter'> "+ txtSelectOptionDefault +" </option>";
						
			for(var i=0; i < arrayDispositivoEmision.length; i++){
				
				// solo mostramos camaras libres
				if(opcion == 'libre'){ 
					if(arrayDispositivoEmision[i].asignado==0)
						html_selectOption += optionSelect_bloques(arrayDispositivoEmision[i].id, arrayDispositivoEmision[i].nombre, arrayDispositivoEmision[i].asignado, i);
				}else{
					// Mostramos todos los dispositivos de emisi�
					html_selectOption += optionSelect_bloques(arrayDispositivoEmision[i].id, arrayDispositivoEmision[i].nombre, arrayDispositivoEmision[i].asignado, i);
				}	
				
				
			} // fin for
		html_selectOption += "</select>";
		cargandoDatos("idDispositivoEmisor", 'end', 'seleccion');
		$("#selectOption_idDispositivoEmisor").html(html_selectOption);
	}
	
	// tabla de la pag 'consultar' tipoProducto 
	function select_dispositivo_emisor_datatable(nombreDiv){
		// thead
		var  thTabla = '<tr>';
			thTabla +='<th class="th_turquesa">Id</th>';
			thTabla +='<th class="th_turquesa">Nombre</th>';
			thTabla +='<th class="th_turquesa">Descripci'+min_o+'n</th>';
			thTabla +='<th class="th_turquesa">Num Serie</th>';
			thTabla +='<th class="th_turquesa">Modelo</th>';
			thTabla +='<th class="th_turquesa">Version</th>';
			thTabla +='<th class="th_turquesa">Asignado</th>';
			thTabla +='<th class="th_turquesa">fechaAlta</th>';
			thTabla +='<th class="th_turquesa">fechaModificacion</th>';
		thTabla +='</tr>';
				
			
		// tbody	
		var tdTabla ='';
		for(var i=0; i < arrayDispositivoEmision.length; i++){
			tdTabla +=' <tr>';
				tdTabla +=' <td> '+ validarNulo(arrayDispositivoEmision[i].id) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayDispositivoEmision[i].nombre) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayDispositivoEmision[i].descripcion) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayDispositivoEmision[i].numSerie) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayDispositivoEmision[i].modelo) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayDispositivoEmision[i].versionSO) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayDispositivoEmision[i].asignado) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayDispositivoEmision[i].fechaAlta) +'</td>';
				tdTabla +=' <td> '+ validarNulo(arrayDispositivoEmision[i].fechaModificacion) +'</td>';
			tdTabla +='</tr>';
		} // fin for	
		
		// Montamos la tabla
		montarTabla(nombreDiv, thTabla, tdTabla, thTabla, "contenido");
	}

		
	
/************************************************************************************
#
# TABLA userSport
#
************************************************************************************/	
	
	/************************************************************************************
		Validar datos del formulario
	************************************************************************************/
	function validar_datos_userSport(operacion){
		// Guardando datos
		cargandoDatos("guardandoDatos", 'start', operacion);

		validar_div_visible("successMsg", "hide");
		
		var tipoCount="userSport";
		countUserSport=0;
		
		
		//Identificador  tipo rol
		var idRol = $("#idRol").val();
		var userName = $("#userName").val();
		var clave = $("#clave").val();
		var email = $("#email").val();
		var telefono = $("#telefono").val();
		var session = $("#session").val();
		var estado = $("#disabled").val();
		var total_minutes = $("#total_minutes").val();
		var minutes_left = $("#minutes_left").val();
		var address = $("#address").val();
		var autostar = $("#autostar").val();
		var titulo = $("#titulo").val();
		var live = $("#live").val();
		var minutes_left = $("#minutes_left").val();
		var server = $("#server").val();
		var port = $("#port").val();
		var showJugadas = $("#showJugadas").val();
		var geoBloqueo = $("#geoBloqueo").val();
		var logo_mosca = $("#logo_mosca").val();
		var logo_mosca_on = $("#logo_mosca_on").val();
		var logoPlayer = $("#logoPlayer").val();
		var logoPosition = $("#logoPosition").val();
		var poster = $("#poster").val();
		var social_name = $("#social_name").val();
		var social_name = $("#social_name").val();
		var token = $("#token").val();
		var tokenExpire = $("#tokenExpire").val();
		var fbAppId = $("#fbAppId").val();
		var fbAppUrl = $("#fbAppUrl").val();
		

		
	
		
		//selecciona_opcion(operacion, countTipoProductos);
		
		//Si hemos pasado la validacion lo enviaremos a la opci�n que le corresponda
		/*
		if((countTipoProducto==3) || (countTipoProducto==4)){			
			switch(operacion){
				case 'insert': //alta
					insertar_datos_userSport();
					break;
					
				case 'update': //modificar
					idTipoProducto2 =  $("#idTipoProducto2").val(); 
					modificar_datos_userSport();
					break;
			}
			
		}*/
	}		
	
	
	/************************************************************************************
		Insertamos los datos en la base de datos eude tabla tipoProducto
	************************************************************************************/
	function insertar_datos_userSport(){
		
		var idRol = $("#idRol").val();
		var userName = $("#userName").val();
		var clave = $("#clave").val();
		var email = $("#email").val();
		var telefono = $("#telefono").val();
		var session = $("#session").val();
		var estado = $("#disabled").val();
		var total_minutes = $("#total_minutes").val();
		var minutes_left = $("#minutes_left").val();
		var address = $("#address").val();
		var autostar = $("#autostar").val();
		var titulo = $("#titulo").val();
		var live = $("#live").val();
		var minutes_left = $("#minutes_left").val();
		var server = $("#server").val();
		var port = $("#port").val();
		var showJugadas = $("#showJugadas").val();
		var geoBloqueo = $("#geoBloqueo").val();
		
	
		var social_name = $("#social_name").val();
		var token = $("#token").val();
		var tokenExpire = $("#tokenExpire").val();
		var fbAppId = $("#fbAppId").val();
		var fbAppUrl = $("#fbAppUrl").val();
	       
        //hacemos la petici�n ajax  
        $.ajax({
            url: 'io/sport/insert_user.php',  
            type: 'GET',
          	data: { 
				'idRol' : idRol,
				'userName' : userName,
				'clave' : clave,
				'email' : email,
				'telefono' : telefono,
				'session' : session,
				'estado' : estado,
				'total_minutes' : total_minutes,
				'minutes_left' : minutes_left,
				'address' : address,
				'autostar' : autostar,
				'titulo' : titulo,
				'minutes_left' : minutes_left,
				'server' : server,
				'port' : port,
				'showJugadas' : showJugadas,
				'geoBloqueo' : geoBloqueo,
				'social_name' : social_name,
				'token' : token,
				'tokenExpire' : tokenExpire,
				'fbAppId' : fbAppId,
				'fbAppUrl' : fbAppUrl,	
				'tiempoDesfase' : desfaseHoraria()				
			},
			success: function(response) {						
				var arrayResult = $.parseJSON(response);
				cargandoDatos("guardandoDatos", 'end', arrayResult[0].accion);				
				if( (arrayResult[0].response == 1) && (arrayResult[0].proceso == 'ok') ){
					evaluar_success(arrayResult[0].response, arrayResult[0].accion, arrayResult[0].tabla);		
					insert_log_acciones(arrayResult);					
				}else{
					$("#successMsg").html("Error, ha sucedido algun error, ponte en contacto con el administrador si persiste.");
				}				
			},       
		});		
	}
	
		
	/************************************************************************************
	
	************************************************************************************/
	 var arrayEmpresasSport =[];
	function loadEmpresas_sport(){
		$.ajax({
			  type: 'GET',
			  url: './io/sport/loadEmpresas.php',
			  data: {
				  'tiempoDesfase' : desfaseHoraria()
			  },
			  success: function(response) {
				arrayEmpresasSport = $.parseJSON(response);
				cargandoDatos("guardandoDatos", 'end', arrayResult[0].accion);				
				
				//Datos incorrectos, informamos con un mensje y limpiamos inputs				
				if(arrayEmpresasSport[0].proceso =='ok') {
					//Select-option formulario router
					var html_selectOption ="";
					html_selectOption += "<select class='form-control' id='selectEmpresaSport'  name='idUser'>";
					html_selectOption += "<option value='-1' selected='selected' name='defaultProvincia'> Selecciona el club donde hacer los cambios... </option>";
					for(var i=0; i < arrayEmpresasSport.length; i++){
						html_selectOption += "<option value='"+arrayEmpresasSport[i].id+"'>"+  arrayEmpresasSport[i].userName + "</option>";
					}//fin form
					html_selectOption += "</select>";
					$("#selectOption_userSport").html(html_selectOption);
				
				}
			
			} //fin succes
		});
 }
 