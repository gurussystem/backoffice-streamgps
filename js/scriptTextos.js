	// Textos default select-option
	var txtSelectOptionDefault = "Selecciona una opci"+min_o+"n ...";
	
	// Texto de los botones
	function txtBotones(opcion){
		
		switch(opcion){
				case 'insert':
				textos_btn('submit', ' Guardar datos ', 'Pulsa para guardar los datos del formulario.');		
				textos_btn('reset', ' Borrar datos ', 'Borrar todo los campos del formulario.');		
			break;
			
			case 'update':				
				textos_btn('submit', ' Modificar datos ', 'Pulsa para actualizar los datos del formulario.');		
				textos_btn('reset2', ' Limpiar datos ', 'Borrar s'+min_o+'lo los campos que se pueden modificar.');		
				textos_btn('reset', ' Reiniciar Formulario ', 'Borrar todo los campos del formulario.');				
				break;			
			
			
			case  'query':	
				textos_btn('reset', ' Reiniciar Formulario ', 'Borrar todo los campos del formulario.');		
				break;
		
			// case 'search':	break;
		}//fin switch
		
	}
	
	/********************************************************************************************************
	* @description 	 	A�ade los diferentes textos en las input del formulario
	* @param 				nombreVariable, es el nombre del identificador del input
	* @param 				txtVariable,  	hace referencia al placeholder. Si 'txtVariable2' esta vacio servira para los dos casos.
	* @param 				txtVariable2,  	hace referencia al data-original-title, que es el del tooltip. Es por si tiene que ser diferente
	********************************************************************************************************/
	function textos(nomVariable, txtVariable, txtVariable2, nomLabel, campObligatorio){
		// console.log("placeholder_textos(" + nomVariable + " , " + txtVariable + " , " +  txtVariable2 + " , " +  nomLabel + ")");
		
		if(txtVariable2 == "")
			var txtTitle = txtVariable;		
		else
			var txtTitle = txtVariable2;	
		
		
		// Input
		$('#'+nomVariable).attr({
			placeholder: txtVariable,
			'data-original-title': txtTitle,
			'data-toggle': 'tooltip',
			'data-placement' : 'bottom'  //posicion donde saldr? el mensaje
		});
			
		// Etiqueta 	
		$('#'+nomVariable+"_lbl")
			.attr("title", txtTitle)
			.html(nomLabel)
			
		// sms error
		if(campObligatorio == "SI"){
			$('#errorMsg_'+nomVariable).html(" * Campo obligatorio, " + txtVariable);
		//	$("#"+nomVariable).css("border", "1px solid red");
		}else{
			$('#errorMsg_'+nomVariable).html("");
		//	$("#"+nomVariable).css("border", "1px solid #ccc");
		}
	
		
			
	}
	
	function textos_btn(nomVariable, txtValue, txtTitle){
		// Etiqueta 	
		$('#'+nomVariable)
			.attr("title", txtTitle)
			.html(txtValue)
	}
	
	
	
	/**	@tabla backoffices
		@funcion que muestra los textos de los ficheros de la carpeta 'ajax/usuarios':
			- insert_usersBackoffice.html
			- update_usersBackoffice.html
			- search_usersBackoffice.html
			- query_usersBackoffice.html
	****************************************************************************************************************/
	function placeholder_textos_users_backoffices(opcion){ 
		
		txtBotones(opcion); // Texto de los botones
		
		if(opcion == 'insert'){
			//cuando el tercer campo este vacio, es porque con el primero tenemos bastante y para no repetir. Solo lo usaremos cuando el texto sea diferente
			textos("nameUserBack", "Introduce el nombre del nuevo usuario", "","Nombre: ",  "SI"); 
			textos("passUserBack", "Introduce una contrase"+min_n+"a", "", "Contrase"+min_n+"a: ",  "SI"); 
			textos("mailUserBack", "Introduce un email" , "", "Email: ",  "SI"); 
			textos("idRolUser", "Introduce un id de rol de usuario" , "", "Rol usuario: ",  "SI");  
		}else{
			textos("idUserBack2", "Identificador del rol de usuario", "", "Id usuario: ",  "SI"); 
			textos("nameUserBack", "Nombre del usuario", "","Nombre: ",  "SI"); 
			textos("passUserBack", "Contrase"+min_n+"a del usuario.", "", "Contrase"+min_n+"a: ",  "SI");
			textos("mailUserBack", "Email del usuario" , "", "Email: ",  "SI"); 
			textos("idRolUser", "Id de rol de usuario" , "", "Rol usuario: ",  "SI"); 
			textos("fechaAlta", "Fecha cuando se dio de alta.", "", "Fecha alta: ", "NO"); 
			textos("fechaModificacion", "Fecha en la que se modificaron los datos.", "", "Fecha modificaci"+min_o+"n: ", "NO"); 
		
		}
			
		switch(opcion){
			case 'insert':
				$('#titulo_pagina').html('Alta usuario backoffices');
				break;
			
			case 'update':
				$('#titulo_pagina').html('Modificar usuario backoffices');
				break;			
			
			
			case  'query':	
				$('#titulo_pagina').html('Consultar usuarios backoffices');			
				break;
		
			case 'search':
				$('#titulo_pagina').html('Informaci'+min_o+'n de usuario backoffices');
				break;
		}//fin switch
		
		// Esto es en todos igual
		$('#legend_form').html('Datos de los usuarios backoffices ');		
		$('#idUserBack_lbl').html('Selecciona un usuario:');
		$('#idRolUser_lbl').html('Rol usuario:');	
	}
	
	
	
	/**	@tabla camara
	@funcion que muestra los textos de los ficheros de la carpeta 'ajax/camara':
			- insert_camara.html
			- update_camara.html
			- search_camara.html
			- query_camara.html
	****************************************************************************************************************/
	function placeholder_textos_camara(opcion){
			
		txtBotones(opcion); // Texto de los botones
		
		if(opcion == 'insert'){
			//cuando el tercer campo este vacio, es porque con el primero tenemos bastante y para no repetir. Solo lo usaremos cuando el texto sea diferente
			textos("nombreCamara", "Introduce el nombre de la c"+min_a+"mara", "", "Nombre: " , "SI"); 
			textos("idTipoCamara", "Introduce el tipo de c"+min_a+"mara", "", "Tipo:", "SI"); 
			textos("numSerialCamara", "Introduce el n"+min_u+"mero de serial de la c"+min_a+"mara", "", "N"+min_u+"mero Serial: ", "SI"); 
			textos("macCamara", "Introduce la direcci"+min_o+"n mac de la c"+min_a+"mara", "", "Direcci"+min_o+"n Mac: ", "SI"); 
			textos("userCamara", "Introduce el usuario de la c"+min_a+"mara", "", "Usuario: ", "SI"); 
			textos("passCamara", "Introduce la clave de la c"+min_a+"mara", "", "Clave: ", "SI"); 
			textos("locationIpCamara", "Introduce la direcci"+min_o+"n ip location de la c"+min_a+"mara", "", "Location ip: ", "SI"); 
			textos("rtspMain", "Introduce el rtsp principal", "",  "RTSP: ", "SI"); 
			textos("resolutionMain", "Introduce la resoluci"+min_o+"n principal", "", "Resoluci"+min_o+"n: ", "SI"); 
			textos("kbpsMain", "Introduce los kbps principales", "", "KBPS: ", "SI"); 
			textos("rtspSecond", "Introduce el rtsp secundario", "",  "RTSP: ", "SI"); 
			textos("resolutionSecond", "Introduce la resoluci"+min_o+"n secundaria", "", "Resoluci"+min_o+"n: ", "SI"); 
			textos("kbpsSecond", "Introduce los kbps secundarios", "", "KBPS: ", "SI"); 
		}
		else {
			textos("idCamara2", "Identificador de la c"+min_a+"mara", "", "Identificador: ", "NO"); 
			textos("nombreCamara", "Nombre de la c"+min_a+"mara", "", "Nombre: ", "SI"); 
			textos("idTipoCamara", "Tipo de c"+min_a+"mara", "","Tipo: ", "SI"); 
			textos("numSerialCamara", "N"+min_u+"mero de serial de la c"+min_a+"mara", "", "N"+min_u+"mero Serial: ", "SI"); 
			textos("macCamara", "Direcci"+min_o+"n mac de la c"+min_a+"mara", "", "Direcci"+min_o+"n Mac: " , "SI"); 
			textos("userCamara", "Usuario de la c"+min_a+"mara", "", "Usuario: ", "SI"); 
			textos("passCamara", "Clave de la c"+min_a+"mara", "", "Clave: ", "SI"); 
			textos("locationIpCamara", "Direcci"+min_o+"n ip location de la c"+min_a+"mara", "", "Location ip: ", "SI"); 
			textos("rtspMain", "Rtsp principal", "", "RTSP: ", "SI"); 
			textos("resolutionMain", "Resoluci"+min_o+"n principal", "", "Resoluci"+min_o+"n:", "SI"); 
			textos("kbpsMain", "Kbps principales", "", "KBPS: ", "SI"); 
			textos("rtspSecond", "Rtsp secundario", "", "RTSP: ", "NO"); 
			textos("resolutionSecond", "Resoluci"+min_o+"n secundaria", "", "Resoluci"+min_o+"n: ", "SI"); 
			textos("kbpsSecond", "Kbps secundarios", "", "KBPS: ", "SI"); 
			textos("propietarioCamara", "Nombre de la empresa propietaria.", "", "Propietario: ", "SI"); 
			textos("fechaAlta", "Fecha alta de la c"+min_a+"mara", "", "Fecha alta: ", "SI"); 
			textos("fechaModificacion", "Fecha alta de modificaci"+min_o+"n la c"+min_a+"mara", "", "Fecha modificaci"+min_o+"n: ", "NO"); 
			
		}
		
		switch(opcion){
			case 'insert':
				$('#titulo_pagina').html('Alta c'+min_a+'mara');	
				break;
			
			case 'update':				
				$('#titulo_pagina').html('Modificar c'+min_a+'mara');		
				break;			
			
			
			case  'query':	
				$('#titulo_pagina').html('Consulta c'+min_a+'mara');
				break;
		
			case 'search':
				$('#titulo_pagina').html('Informaci'+min_o+'n C'+min_a+'maras');
				break;
		}//fin switch
		
		// Esto es en todos igual
		$('#legend_form').html('  Datos de la C'+min_a+'mara');
		$('#legend_form_main').html('Datos main');
		$('#legend_form_second').html('Datos second');
		$('#idCamara_lbl').html('Selecciona una c'+min_a+'mara:');
		
	}
	
	
	/**	@tabla  cta_empresa y empresa
	@funcion que muestra los textos de los ficheros de la carpeta 'ajax/empresa':
			- insert_cliente.html
			- update_cliente.html
			- update_cliente_cta.html
			- search_cliente.html
			- query_cliente.html			
			- query_cliente_cta.html			
	****************************************************************************************************************/
	function placeholder_textos_cliente(opcion){
			
		txtBotones(opcion); // Texto de los botones
		
		if(opcion == 'insert'){
			//cuando el tercer campo este vacio, es porque con el primero tenemos bastante y para no repetir. Solo lo usaremos cuando el texto sea diferente
			textos("nombreEmpresa", "Introduce el nombre de la empresa", "", "Nombre:", "SI"); 
			textos("email", "Introduce un email de la persona de contacto", "" , "Email: ", "SI"); 
			textos("password", "Introduce una clave", "", "Clave: ", "SI"); 
			textos("direccion", "Introduce la direcci"+min_o+"n de la empresa", "", "Direcci"+min_o+"n: ", "SI"); 
			textos("poblacion", "Introduce la poblaci"+min_o+"n donde esta la empresa", "", "Poblaci"+min_o+"n: ", "SI"); 
			textos("idProvincias", "Introduce la provincia donde esta la empresa", "", "Provincia: ", "SI"); 
			textos("cp", "Introduce el c"+min_o+"digo postal", "", "C"+min_o+"digo postal:", "SI"); 
			textos("fechaSolicitudAlta", "Introduce la fecha en que el cliente solicito el alta", "", "Fecha solicitud del alta", "SI"); 
			textos("tlf_fijo", "Introduce un tel"+min_e+"fono fijo", "", "Tel"+min_e+"fono Fijo:", "SI"); 
			textos("tlf_movil", "Introduce el tel"+min_e+"fono m"+min_o+"vil de contacto", "", "Tel"+min_e+"fono M"+min_o+"vil:", "SI"); 
			textos("razonSocial", "Introduce la raz"+min_o+"n social de la empresa", "", "Raz"+min_o+"n social", "SI"); 
			textos("dni", "Introduce tu CIF/DNI", "", "CIF/NIF/DNI:", "SI"); 
			textos("titularCuenta", "Introduce el nombre completo del titular", "", "Titular de la cuenta:", "SI"); 
			textos("tipoNumBanco", "Selecciona un tipo de cuenta", "", "Tipo cuenta bancaria:", "SI"); 
			textos("iban",  "Introduce el c"+min_o+"digo iban correspondiente a tu cuenta bancaria", "", "IBAN:", "NO"); 
			textos("swift",  "Introduce el c"+min_o+"digo swift correspondiente a tu cuenta bancaria", "", "SWIFT/BIC:", "NO"); 
			textos("entidad", "Entidad", "Introduce el n"+min_u+"mero de entidad de la cuenta bancaria", "Entidad: ", "NO"); 
			textos("sucursal", "Sucursal", "Introduce el n"+min_u+"mero de sucursal de la cuenta bancaria", "Sucursal: ", "NO"); 
			textos("dc", "DC", "Introduce el n"+min_u+"mero de d"+min_i+"gito del control de la cuenta bancaria",  "DC: ", "NO"); 
			textos("numCuenta", "N"+min_u+"mero de cuenta", "Introduce el n"+min_u+"mero de cuenta", "N"+min_u+"mero Cuenta: ", "NO"); 
			textos("idPais", "pais", "Pa"+min_i+"s", "Pa"+min_i+"s: ", "SI"); 
			textos("videoAnalisis", "Selecciona una opci"+min_o+"n", "", "Video analisis:", "SI"); 
			textos("ipEmpresa", "Introduce la direcci"+min_o+"n de ip.", "", "IP Server:", "SI"); 
			textos("puertoEmpresa", "Introduce el n"+min_u+"mero de puerto", "", "Puerto: ", "SI"); 

		}
		else{
			textos("idCliente2", "Identificador del cliente", "", "Identificador: ", "SI"); 
			textos("nombreEmpresa", "Nombre de la empresa", "", "Nombre:", "SI"); 
			textos("email", "Email de contacto de la empresa", "", "Email: ", "SI"); 
			textos("password", "Clave para que la empresa pueda entrar en su plataforma", "", "Clave: ", "SI"); 
			textos("direccion", "Direcci"+min_o+"n donde se ubica la empresa", "", "Direcci"+min_o+"n:", "SI"); 
			textos("poblacion", "Poblaci"+min_o+"n donde se ubica la empresa", "", "Poblaci"+min_o+"n:", "SI"); 
			textos("idProvincias", "Provincia donde se ubica la empresa", "", "Provincia:", "SI"); 
			textos("cp", "C"+min_o+"digo postal de la empresa", "", "C"+min_o+"digo postal:", "SI"); 
			textos("fechaAlta", "Fecha cuando se dio de alta a la empresa", "", "Fecha Alta: ", "SI"); 
			textos("fechaModificacion", "Fecha cuando modificaron los datos de la empresa", "", "Fecha Modificaci"+min_o+"n", "NO"); 
			textos("fechaSolicitudAlta", "Fecha cuando el cliente solicito el alta", "", "Fecha solicitud del alta: ", "NO"); 
			textos("cta_fechaAlta", "Fecha cuando se dio de alta a la empresa", "", "Fecha Alta cta:","NO"); 
			textos("cta_fechaModificacion", "Fecha cuando modificaron los datos ", "", "Fecha Modificaci"+min_o+"n cta", "NO"); 
			textos("tlf_fijo", "Tel"+min_e+"fono fijo de la empresa", "", "Tel"+min_e+"fono Fijo:", "SI"); 
			textos("tlf_movil", "Tel"+min_e+"fono m"+min_o+"vil de la persona de contacto", "", "Tel"+min_e+"fono M"+min_o+"vil:", "SI"); 
			textos("razonSocial", "Denominaci"+min_o+"n por la cual se conoce colectivamente a una empresa", "", "Raz"+min_o+"n social", "SI"); 
			textos("dni", "Nif/cif de la empresa", "", "CIF/NIF/DNI:", "SI"); 
			textos("titularCuenta", "Titular responsable de la cuenta bancaria", "", "Titular de la cuenta:", "SI"); 
			textos("iban", "N"+min_u+"mero iban de la cuenta bancaria", "", "IBAN:", "NO"); 
			textos("swift",  "C"+min_o+"digo swift correspondiente a tu cuenta bancaria", "","SWIFT/BIC:", "NO "); 
			textos("entidad", "Entidad", "Introduce el n"+min_u+"mero de entidad de la cuenta bancaria", "Entidad: ", "NO"); 
			textos("sucursal", "Sucursal", "Introduce el n"+min_u+"mero de sucursal de la cuenta bancaria" , "Sucursal: " , "NO"); 
			textos("dc", "DC", "Introduce el n"+min_u+"mero de d"+min_i+"gito del control de la cuenta bancaria", "DC: ", "NO"); 
			textos("numCuenta", "N"+min_u+"mero de cuenta", "Introduce el n"+min_u+"mero de cuenta", "N"+min_u+"mero Cuenta: ", "NO"); 		
			textos("estadoCliente", "Estado del cliente (Activo/Desactivo)", "", "Estado: ", "NO"); 
			textos("nombreBD", "Nombre de la base de datos de la empresa", "", "Base de datos: ", "SI"); 
			textos("prefixCliente", "Prefijo asignado a la empresa", "", "Prefijo: ", "SI"); 
			textos("serverCliente", "Servidor de la empresa", "", "Servidor: ", "SI"); 
			textos("streamAPPCliente", "Que pongo??", "", "App Stream: ", "SI"); 
			textos("dominioCliente", "Dominio de la empresa", "", "Dominio: ", "SI"); 
			textos("subdominioCliente", "Subdominio de la empresa", "", "Subdominio:", "SI"); 
			textos("connCliente", "conn???", "", "Conn: ", "SI"); 
			textos("numModulosContratados", "N"+min_u+"mero de m"+min_o+"dulos que ha contratado la empresa ", "", "N� licencias: ", "SI"); 	
			textos("idPais", "pais", "Pa"+min_i+"s", "Pa"+min_i+"s: ", "SI"); 
			textos("ipEmpresa", "Introduce la direcci"+min_o+"n de ip.", "", "IP Server:", "SI"); 
			textos("puertoEmpresa", "Introduce el n"+min_u+"mero de puerto", "", "Puerto: ", "SI"); 
			textos("idClienteSeleccion", "Identificador del cliente", "", "Selecciona un cliente: ", "SI"); 
			textos("selectEstado", "Estado del cliente ", "", "Estado cliente: ", "SI"); 
		}
			
	
		switch(opcion){
				case 'insert':
				$('#titulo_pagina').html('Alta cliente - empresa');				
				$('#texto_enunciado').html('Rellena todos los datos del nuevo cliente y pulsa el bo'+min_o+'n "Guardar datos".');
			break;
			
			case 'update':
				$('#titulo_pagina').html('Modificar datos empresa');
				$('#legend_form_otros').html(' Otros Datos');	
				// cta
				$('#titulo_pagina_cta').html('Modificar cta bancaria cliente');
				break;			
						
			case  'query':	
				$('#titulo_pagina').html('Consulta empresa');				
				//cta
				$('#titulo_pagina_cta').html('Consultar cta bancaria cliente');
				$('#legend_form_cta').html(' Datos bancarios de la Empresa');				
				$('#legend_form_otros').html(' Otros Datos');		
				break;
		
			case 'search':
				$('#titulo_pagina').html('Informaci�n Clientes');
				break;
				
		}//fin switch
		//Esto es en todos igual
		$('#legend_form').html('Datos de la Empresa');	
		$('#legend_form_direccion').html('Datos de configuraci'+min_o+'n');		
		$('#legend_form_cta').html('Datos Bancarios');
		$('#texto_label_select_id_cta').html('Selecciona un cliente:');
	}
	
	
	/**	@tabla impuestos.
		@funcion que muestra los textos de los ficheros de la carpeta 'ajax/iva':
			- insert_impuestos.html
			- update_impuestos.html
			- search_impuestos.html
			- query_impuestos.html
	****************************************************************************************************************/
	function placeholder_textos_impuestos(opcion){ 
		
		txtBotones(opcion); // Texto de los botones
		
	if(opcion == 'insert'){
			textos("nombreImpuesto", "Introduce el nombre del impuesto", "", "Nombre:" , "SI"); 
			textos("descripcionImpuesto", "Introduce el significado del impuesto", "", "Descripci"+min_o+"n: ", "SI"); 
			textos("valorImpuesto", "Introduce el valor del impuesto", "",  " Valor: ", "SI"); 
			
		}else{
			textos("idImpuestosSelect", "Identificador del impuesto", "", "Selecciona una opci"+min_o+"n: ", "SI"); 
			textos("idImpuesto2", "Identificador del impuesto", "", "Identificador: ", "NO"); 
			textos("nombreImpuesto", "Nombre del impuesto", "", "Nombre:",  "SI"); 
			textos("descripcionImpuesto", "Significado del impuesto", "", "Descripci"+min_o+"n:", "SI"); 
			textos("valorImpuesto", "Valor del impuesto", "", "Valor:", "SI"); 
			textos("fechaAlta", "Fecha Alta", "", "Fecha Alta:", "NO"); 
			textos("fechaModificacion", "Fecha modificaci"+min_o+"n", "", "Fecha Mofdificaci"+min_o+"n:", "NO"); 
		}
	
		switch(opcion){
				case 'insert':
				$('#titulo_pagina').html('Alta impuesto');
			break;
			
			case 'update':
				$('#titulo_pagina').html('Modificar datos impuesto');
								
				break;			
			
			
			case  'query':	
				$('#titulo_pagina').html('Consultar impuestos');
				break;
		
			case 'search':
				$('#titulo_pagina').html('Informaci'+min_o+'n Impuestos');
				break;
				
		}//fin switch
		//Esto es en todos igual
		$('#texto_label_select_id').html('Selecciona un impuesto: ');	
		$('#legend_form').html('Datos de los impuestos');	
			
		
		
	
	}
	
	
	/**	@tabla log_acciones
		@funcion que muestra los textos de los ficheros de la carpeta 'ajax/logAcciones':
			- search_log_acciones.html
			- query_log_acciones.html
	****************************************************************************************************************/
	function placeholder_textos_log_acciones(opcion){ /*hay que implementar*/}
	
	
	/**	@tabla log_backoffices
		@funcion que muestra los textos de los ficheros de la carpeta 'ajax/logBackoffices':
			- search_log_backoffices.html
			- query_log_backoffices.html
	****************************************************************************************************************/
	function placeholder_textos_log_backoffices(opcion){ /*hay que implementar*/}
	
	
	/**	@tabla productos
		@funcion que muestra los textos de los ficheros de la carpeta 'ajax/producto':
			- insert_producto.html
			- update_producto.html
			- search_producto.html
			- query_producto.html
	****************************************************************************************************************/
	function placeholder_textos_producto(opcion){ 
		
		txtBotones(opcion); // Texto de los botones
		
		if(opcion == 'insert'){
			//cuando el tercer campo este vacio, es porque con el primero tenemos bastante y para no repetir. Solo lo usaremos cuando el texto sea diferente
			textos("nombreProducto", "Introduce el nombre del producto", "", "Nombre: ",  "SI"); 
			textos("descripcionProducto", "Introduce una descripci"+min_o+"n para el producto", "", "Descripci"+min_o+"n:", "SI"); 
			
		}else{
			textos("idProducto", "Selecciona un opci"+min_o+"n", "", "Selecciona un opci"+min_o+"n: " , "SI"); 
			textos("idProducto2", "Identificador del producto", "", "Identificador: ", "SI"); 
			textos("idfechaAlta", "Fecha alta del producto", "", "Fecha alta:", "NO"); 
			textos("idfechaMod", "Fecha en la que se modifico el producto", "", "Fecha modificaci"+min_o+"n:", "NO"); 
		}
		textos("nombreProducto", "Introduce el nombre del producto", "", "Nombre: ",  "SI"); 
			textos("descripcionProducto", "Introduce una descripci"+min_o+"n para el producto", "", "Descripci"+min_o+"n:", "SI"); 
			textos("idClienteSeleccion", "Selecciona la empresa", "", "Selecciona la empresa:" , "SI");  // idEmpresa
			textos("idRouter", "Selecciona un router.", "", "Router: " , "SI"); 
			textos("idCamara", "Selecciona una c"+min_a+"mara", "", "C"+min_a+"mara: " , "SI");			
			textos("idDispositivoEmisor", "Selecciona una opci"+min_o+"n", "", "Dispositivo Emisor:" , "SI"); 			
			textos("idTipoProducto", "Selecciona un tipo producto", "", "Tipo Producto: ", "NO" ); 
			textos("idSim", "Selecciona una sim", "", "Sim: " , "SI"); 	
			textos("usuario", "Introduce el nombre de usuario", "", "Usuario: ",  "SI"); 
			textos("clave", "Introduce una clave", "", "Clave: ",  "SI"); 			
			textos("email", "Introduce una email", "", "Email: ",  "SI"); 			
			
		//Este hay que modificarlo (lo he copiado de producto)
			switch(opcion){
				case 'insert':
				$('#titulo_pagina').html('Alta producto');
			break;
			
			case 'update':
				$('#titulo_pagina').html('Modificar');			
				break;			
			
			
			case  'query':	
				$('#titulo_pagina').html('Consulta producto');
				break;
		
			case 'search':
				$('#titulo_pagina').html('Informaci'+min_o+'n de productos');
				break;
				
		}//fin switch
		//Esto es en todos igual
	

		if(opcion == 'asignar'){
			$('#texto_label_select_id').html('Selecciona una empresa:');
			$('#texto_label_select_id_2').html('Selecciona un producto:');
			$('#legend_form').html('Asignar un producto a un cliente');
			$('#titulo_pagina	').html('Asignar un producto a un cliente');
		
		}else{
			$('#texto_label_select_id').html('Selecciona un producto:');
			$('#legend_form').html('Datos del producto');
		}
	}
	
	
	/**	@tabla provincias
		@funcion que muestra los textos de los ficheros de la carpeta 'ajax/provincias':
			- search_provincias.html
			- query_provincias.html
			
			
			
					// query 
					<!-- idPais2 -->
					<!-- nombrePais -->
					<!-- codeCalling -->
					<!-- capital -->
					<!-- continente -->
					<!-- latPais -->
					<!-- lngPais -->
					<!-- codeISO2 -->
					<!-- codeISO3 -->
					<!-- fechaAlta -->
					<!-- fechaMod -->
	****************************************************************************************************************/
	function placeholder_textos_pais(opcion){ 	
		
		txtBotones(opcion); // Texto de los botones
		
		if(opcion == 'insert'){
			//cuando el tercer campo este vacio, es porque con el primero tenemos bastante y para no repetir. Solo lo usaremos cuando el texto sea diferente
			textos("nombrePais", "Introduce el nombre del pa"+min_i+"s", "", "Nombre: ", "SI"); 
			textos("codeCalling", "Introduce el c"+min_o+"digo para llamar", "", "C"+min_o+"digo para llamar: ", "SI");
			textos("capital", "Introduce la capital del pa"+min_i+"s", "", "Capital: ", "SI");
			textos("continente", "Introduce el continente del pa"+min_i+"s", "", "Continente: ", "SI");
			textos("latPais", "Introduce la latitud del pa"+min_i+"s", "", "Latitud: ", "SI");
			textos("lngPais", "Introduce la longitud del pa"+min_i+"s", "", "Longitud: ", "SI");
			textos("codeISO2", "Introduce el c"+min_o+"digo ISO 2 del pa"+min_i+"s", "", "C"+min_o+"digo ISO 2: ", "SI");
			textos("codeISO3", "Introduce el c"+min_o+"digo ISO 3 del pa"+min_i+"s", "", "C"+min_o+"digo ISO 3: ", "SI");
		}else{
			
			textos("idPais", "Identificador del pa"+min_i+"s", "", "Selecciona una opci"+min_o+"n: ", "SI"); 
			textos("idPais2", "Identificador del pa"+min_i+"s", "", "Identificador: ", "NO"); 
			textos("nombrePais", "Nombre del pa"+min_i+"s", "", "Nombre: ", "SI"); 
			textos("codeCalling", "C"+min_o+"digo para llamar", "", "C"+min_o+"digo para llamar: ", "SI");
			textos("capital", "Capital del pa"+min_i+"s", "", "Capital: ", "SI");
			textos("continente", "Continente del pa"+min_i+"s", "", "Continente: ", "SI");
			textos("latPais", "Latitud del pa"+min_i+"s", "" , "Latitud: ", "SI");
			textos("lngPais", "Longitud del pa"+min_i+"s", "", "Longitud: ", "SI");
			textos("codeISO2", "C"+min_o+"digo ISO 2 del pa"+min_i+"s", "", "C"+min_o+"digo ISO 2: ", "SI");
			textos("codeISO3", "C"+min_o+"digo ISO 3 del pa"+min_i+"s", "", "C"+min_o+"digo ISO 3: ", "SI");			
			textos("fechaAlta", "Fecha cuando se dio de alta.", "", "Fecha alta: ", "NO"); 
			textos("fechaMod", "Fecha en la que se modificaron los datos.", "", "Fecha modificaci"+min_o+"n: ", "NO"); 
		}
		
	
		switch(opcion){
				case 'insert':
				$('#titulo_pagina').html('Alta pais');
			break;
			
			case 'update':
				$('#titulo_pagina').html('Modificar pais');			
				break;			
			
			
			case  'query':	
				$('#titulo_pagina').html('Consultar pais');
				break;
		
			case 'search':
				$('#titulo_pagina').html('Informaci'+min_o+'n de la pais');
				break;
		}//fin switch
		
		//Esto es en todos igual
		$('#legend_form').html('Datos de la pais');
		$('#texto_label_select_id').html('Selecciona una pais:');
	}
	
	

	/**	@tabla provincias
		@funcion que muestra los textos de los ficheros de la carpeta 'ajax/provincias':
			- search_provincias.html
			- query_provincias.html
	****************************************************************************************************************/
	function placeholder_textos_provincias(opcion){ 
		
		txtBotones(opcion); // Texto de los botones
		
		if(opcion == 'insert'){
			//cuando el tercer campo este vacio, es porque con el primero tenemos bastante y para no repetir. Solo lo usaremos cuando el texto sea diferente
			textos("nombreProvincias", "Introduce el nombre de la provincia", "", "Nombre: ", "SI"); 
			textos("codigoPostalProvincias", "Introduce el c"+min_o+"digo de la provincia", "", "C"+min_o+"digo de la provincia: ", "SI"); 
		}else{
			textos("idProvincias", "Identificador del provincia", "", "Selecciona una opci"+min_o+"n: ", "SI"); 
			textos("idProvincias2", "Identificador de la provincia", "" , "Identificador: ", "NO"); 
			textos("nombreProvincias", "Nombre de la provincia", "", "Nombre: ", "SI"); 
			textos("codigoPostalProvincias", "C"+min_o+"digo de la provincia.", "", "C"+min_o+"digo de la provincia: ", "SI");			
			textos("fechaAlta", "Fecha cuando se dio de alta.", "", "Fecha alta: ", "NO"); 
			textos("fechaMod", "Fecha en la que se modificaron los datos.", "","Fecha modificaci"+min_o+"n: ", "NO"); 
		}
		
		
		switch(opcion){
				case 'insert':
				$('#titulo_pagina').html('Alta provincia');
			break;
			
			case 'update':
				$('#titulo_pagina').html('Modificar provincia');		
				break;			
			
			
			case  'query':	
				$('#titulo_pagina').html('Consultar provincia');
				break;
		
			case 'search':
				$('#titulo_pagina').html('Informaci'+min_o+'n de la provincia');
				break;
		}//fin switch
		//Esto es en todos igual
		$('#legend_form').html('Datos de la provincia');	
	}
	
	
	
	/**	@tabla rol_user
		@funcion que muestra los textos de los ficheros de la carpeta 'ajax/rolUser':
			- insert_rol_user.html
			- update_rol_user.html
			- search_rol_user.html
			- query_rol_user.html
	****************************************************************************************************************/
	function placeholder_textos_rol_user(opcion){ 
		
		txtBotones(opcion); // Texto de los botones
		
		if(opcion == 'insert'){
			//cuando el tercer campo este vacio, es porque con el primero tenemos bastante y para no repetir. Solo lo usaremos cuando el texto sea diferente
			textos("nombreRolUser", "Introduce el nombre del nuevo rol de usuario", "", "Nombre: ", "SI"); 
			textos("descripcionRolUser", "Introduce una descripci"+min_o+"n para el nuevo rol de usuario", "", "Descripci"+min_o+"n: ", "SI"); 
		}else{
			textos("idRolUser", "Selecciona un rol de usuario", "", "Selecciona una opci"+min_o+"n: ", "SI"); 
			textos("idRolUser2", "Identificador del rol de usuario", "", "Identificador: ", "NO"); 
			textos("nombreRolUser", "Nombre del rol de usuario", "", "Nombre: ", "SI"); 
			textos("descripcionRolUser", "Descripci"+min_o+"n  del rol de usuario.", "", "Descripci"+min_o+"n: ", "SI");
			
			textos("fechaAlta", "Fecha cuando se dio de alta.", "", "Fecha alta: ", "NO"); 
			textos("fechaModificacion", "Fecha en la que se modificaron los datos.", "", "Fecha modificaci"+min_o+"n: ", "NO"); 
		
		}
		
	
		switch(opcion){
				case 'insert':
				$('#titulo_pagina').html('Alta rol de usuarios');
			break;
			
			case 'update':
				$('#titulo_pagina').html('Modificar rol de usuarios');		
				break;			
			
			
			case  'query':	
				$('#titulo_pagina').html('Consultar rol de usuarios');
				break;
		
			case 'search':
				$('#titulo_pagina').html('Informaci'+min_o+'n rol de usuario');
				break;
		}//fin switch
		//Esto es en todos igual
		$('#legend_form').html('Datos del rol de usuario ');
		$('#texto_label_select_id').html('Selecciona un rol de usuario:');
	
	}
		
	
	/**	@tabla router
		@funcion que muestra los textos de los ficheros de la carpeta 'ajax/router':
			- insert_router.html
			- update_router.html
			- search_router.html
			- query_router.html
	****************************************************************************************************************/
	function placeholder_textos_router(opcion){
			
		txtBotones(opcion); // Texto de los botones
		
		if(opcion == 'insert'){
			//cuando el tercer campo este vacio, es porque con el primero tenemos bastante y para no repetir. Solo lo usaremos cuando el texto sea diferente
			textos("nombreRouter", "Introduce el nombre del router.", "", "Nombre: ", "SI"); 
			textos("idTipoRouter", "Introduce el tipo del router.", "", "Tipo: ", "SI"); 
			textos("numSerialRouter", "Introduce el n"+min_u+"mero de serial.", "" , "N"+min_u+"mero Serial: ", "SI"); 
			textos("macRouter", "Introduce la direcci"+min_o+"n MAC del router.", "", "Direcci"+min_o+"n MAC: ", "SI"); 
			textos("userRouter", "Introduce el user", "", "Usuario: ", "SI"); 
			textos("passRouter", "Introduce el password", "", "Clave: ", "SI"); 
			textos("ipStaticRouter", "Introduce la IP est"+min_a+"tica", "", "Ip static: ", "SI"); 
			textos("dyndnsRouter", "Introduce el dyndns", "", "DynDns: ", "SI"); 
		}else{
			textos("idRouter", "Selecciona una opci"+min_o+"n", "", "Selecciona una opci"+min_o+"n: " , "NO"); 
			textos("idRouter2", "Identificador del router.", "", "Identificador: " , "NO"); 
			textos("nombreRouter", "Nombre del router.", "", "Nombre: ", "SI"); 
			textos("idTipoRouter", "Tipo de router.", "", "Tipo: ", "SI"); 
			textos("numSerialRouter", "N"+min_u+"mero de serie del router.", "", "N"+min_u+"mero Serial: ", "SI"); 
			textos("macRouter", "Direcci"+min_o+"n MAC del router.", "", "Direcci"+min_o+"n MAC: ", "SI"); 
			textos("userRouter", "Nombre del usuario del router", "", "Usuario: ", "SI"); 
			textos("passRouter", "Clave para entrar en el router", "", "Clave: ", "SI"); 
			textos("ipStaticRouter", "Direcci"+min_o+"n IP est"+min_a+"tica del router", "", "Ip static: ", "SI"); 
			textos("dyndnsRouter", "Direcci"+min_o+"n de acceso remoto al router", "", "DynDns: ", "SI"); 
			textos("propietarioRouter", "Nombre de la empresa propietaria.", "", "Propietario: ", "NO"); 			
			textos("fechaAlta", "Fecha alta del router ", "", "Fecha Alta: ", "NO"); 
			textos("fechaModificacion", "Fecha alta de modificaci"+min_o+"n del router", "", "Fecha Modificaci"+min_o+"n: ", "NO"); 
		}	
	
		
		//Resto de mensajes-textos y el submenu que hay debajo de la cabecera, para ello utilizaremos un switch-case
		switch(opcion){
			case 'insert':
				$('#titulo_pagina').html('Alta Router');
				break;
			
			case 'update':
				$('#titulo_pagina').html('Modificar Router ');		
				break;
				
			case  'query':	
				$('#titulo_pagina').html('Consultar Router ');
							
				break;
				
			case 'search':
				$('#titulo_pagina').html('Informaci'+min_o+'n Router');
				break;
		}//fin switch
	
		//Es igual en todos los casos
	
		$('#legend_form').html(' Datos de la Router');
		$('#texto_label_select_id').html('Selecciona un router:');
	}
	
	
	/**	@tabla sim
		@funcion que muestra los textos de los ficheros de la carpeta 'ajax/sim':
			- insert_sim.html
			- update_sim.html
			- search_sim.html
			- query_sim.html
	****************************************************************************************************************/
	function placeholder_textos_sim(opcion){ 
		
		txtBotones(opcion); // Texto de los botones
		
		if(opcion == 'insert'){
			//cuando el tercer campo este vacio, es porque con el primero tenemos bastante y para no repetir. Solo lo usaremos cuando el texto sea diferente
			textos("numSim01", "Introduce el n"+min_u+"mero de sim (1)", "", "N"+min_u+"mero de Sim 1: ", "SI"); 
			textos("numSim02", "Introduce el n"+min_u+"mero de sim (2)", "", "N"+min_u+"mero de Sim 2: ", "SI"); 
			textos("pinSim", "Introduce el n"+min_u+"mero pin de la sim", "", "N"+min_u+"mero  pin: ", "SI"); 
			textos("punkSim", "Introduce el n"+min_u+"mero punk de la sim", "", "N"+min_u+"mero  punk: ", "SI"); 
			textos("modelSim", "Introduce el modelo de la sim", "", "Modelo:", "SI"); 
	
		}else{
			textos("idSim", "Identificador del sim.", "", "Selecciona una opci"+min_o+"n: " , "SI"); 
			textos("idSim2", "Identificador de la sim", "", "Identificador: ", "NO"); 
			textos("numSim01", "N"+min_u+"mero de sim (1)", "", "N"+min_u+"mero de Sim 1: ", "SI"); 
			textos("numSim02", "N"+min_u+"mero de sim (2)", "", "N"+min_u+"mero de Sim 2: ", "SI"); 
			textos("pinSim", "N"+min_u+"mero pin de la sim", "", "N"+min_u+"mero  pin: ", "SI"); 
			textos("punkSim", "N"+min_u+"mero punk de la sim", "", "N"+min_u+"mero  punk: ", "SI"); 
			textos("modelSim", "Modelo de la sim", "", "Modelo:", "SI"); 
			textos("idEmpresaSim", "Empresa asociada a la sim", "", "Propietario: ", "NO");
			textos("idClienteSeleccion", "Empresa  asociada a la sim", "", "NO"); 			
			textos("fechaAlta", "Fecha cuando se dio de alta la sim", "", "Fecha alta: ", "NO"); 
			textos("fechaModificacion", "Fecha en la que se modificaron los datos de la sim", "", "Fecha modificaci"+min_o+"n: ", "NO"); 
		
		}
		
		switch(opcion){
				case 'insert':
				$('#titulo_pagina').html('Alta sim');
			break;
			
			case 'update':
				$('#titulo_pagina').html('Modificar sim');
				break;			
			
			
			case  'query':	
				$('#titulo_pagina').html('Consultar sim');
				break;
		
			case 'search':
				$('#titulo_pagina').html('Informaci'+min_o+'n sim');
				break;
		}//fin switch
		
		//Esto es en todos igual
		$('#legend_form').html('Datos de la sim ');	
	
	}
	
	
	/**	@tabla 	tipo_aviso
		@funcion que muestra los textos de los ficheros de la carpeta 'ajax/tipoAviso':
			- insert_tipo_aviso.html
			- update_tipo_aviso.html
			- search_tipo_aviso.html
			- query_tipo_aviso.html 
	****************************************************************************************************************/
	function placeholder_textos_tipo_aviso(opcion){
	
		txtBotones(opcion); // Texto de los botones
				
		if(opcion == 'insert'){
			//cuando el tercer campo este vacio, es porque con el primero tenemos bastante y para no repetir. Solo lo usaremos cuando el texto sea diferente
			textos("nombreTipoAviso", "Introduce el nombre del tipo de aviso", "", "Nombre: ", "SI"); 
			textos("descripcionTipoAviso", "Introduce una descripci"+min_o+"n para el tipo de aviso.", "", "Descripci"+min_o+"n: ", "SI"); 
		}else{
			textos("idTipoAviso", "Selecciona un rol de usuario", "", "Selecciona una opci"+min_o+"n: ", "SI"); 
			textos("idTipoAviso2", "Identificador del tipo de aviso", "", "Identificador: ", "NO"); 
			textos("nombreTipoAviso", "Nombre del tipo de aviso", "", "Nombre: ", "SI"); 
			textos("descripcionTipoAviso", "Descripci"+min_o+"n  del tipo de aviso.", "", "Descripci"+min_o+"n: ", "SI");			
			textos("fechaAlta", "Fecha cuando se dio de alta.", "", "Fecha alta: ", "NO"); 
			textos("fechaModificacion", "Fecha en la que se modificaron los datos.", "", "Fecha modificaci"+min_o+"n: ", "NO"); 
		
		}
				
		switch(opcion){
				case 'insert':
				$('#titulo_pagina').html('Alta tipo aviso');
			break;
			
			case 'update':
				$('#titulo_pagina').html('Modificar tipo aviso');
				break;			
			
			
			case  'query':	
				$('#titulo_pagina').html('Consultar tipo aviso');
				break;
		
			case 'search':
				$('#titulo_pagina').html('Informaci'+min_o+'n tipo aviso');
				break;
		}//fin switch
		//Esto es en todos igual
		$('#legend_form').html('Datos del tipo aviso ');
		$('#texto_label_select_id').html('Selecciona un tipo de aviso:');
	}
	
	
	
	/**	@tabla tipo_evento_log
		@funcion que muestra los textos de los ficheros de la carpeta 'ajax/tipoEventosLog':
			- search_log_backoffices.html
			- query_log_backoffices.html
	****************************************************************************************************************/
	function placeholder_textos_tipo_evento_log(opcion){ 	
		
		txtBotones(opcion); // Texto de los botones
		
		if(opcion == 'insert'){
			//cuando el tercer campo este vacio, es porque con el primero tenemos bastante y para no repetir. Solo lo usaremos cuando el texto sea diferente
			textos("nombreEvento", "Introduce el nombre del tipo de evento del log", "", "Nombre:", "SI"); 
			textos("descEvento", "Introduce una descripci"+min_o+"n para el tipo de evento del log", "", "Descripci"+min_o+"n:", "SI"); 
		}else{
			textos("idTipoEventoLogSelect", "Identificador del tipo de evento log.", "", "Selecciona una opci"+min_o+"n: " , "SI"); 
			textos("idEvento2", "Identificador del tipo de evento log", "", "Identificador: ", "NO"); 
			textos("nombreEvento", "Nombre del tipo de evento log", "", "Nombre:", "SI"); 
			textos("descEvento", "Descripci"+min_o+"n del tipo de evento log", "", "Descripci"+min_o+"n:", "SI"); 
			textos("fechaAlta", "Fecha en la que se creo el evento", "", "Fecha alta:", "NO"); 
			textos("fechaModificacion", "Fecha cuando se modifico el evento", "", "Fecha modificaci"+min_o+"n:", "NO"); 
		}
		
		switch(opcion){
				case 'insert':
				$('#titulo_pagina').html('Alta tipo de evento del log');
			break;
			
			case 'update':
				$('#titulo_pagina').html('Modificar Tipo Evento del log');	
				break;			
			
			
			case  'query':	
				$('#titulo_pagina').html('Consulta tipo eventos del log');
				break;
		
			case 'search':
				$('#titulo_pagina').html('Informaci'+min_o+'n tipos de eventos del log');
				break;
		}//fin switch
		//Esto es en todos igual
		$('#legend_form').html('Datos del tipo de eventos de un log');
		$('#texto_label_select_id').html('Selecciona un tipo de evento del log:');
	
	
	}
	
	
	
	
	/**	@tabla tipo_producto
		@funcion que muestra los textos de los ficheros de la carpeta 'ajax/tipoProducto':
			- insert_tipo_producto.html
			- update_tipo_producto.html
			- search_tipo_producto.html
			- query_tipo_producto.html
	****************************************************************************************************************/
	function placeholder_textos_tipo_producto(opcion){ 
		
		txtBotones(opcion); // Texto de los botones
		
		if(opcion == 'insert'){
			//cuando el tercer campo este vacio, es porque con el primero tenemos bastante y para no repetir. Solo lo usaremos cuando el texto sea diferente
			textos("nombreTipoProducto", "Introduce el nombre del tipo de producto", "", "Nombre:", "SI"); 
			textos("descTipoProducto", "Introduce una descripci"+min_o+"n para el tipo de producto", "", "Descripci"+min_o+"n:", "SI"); 
			textos("tipoLive", "Selecciona una opci"+min_o+"n", "", "Tipo Live:", "SI"); 
		}else{
			textos("idTipoProducto", "Selecciona una opci"+min_o+"n", "", "Selecciona una opci"+min_o+"n:" , "SI"); 
			textos("idTipoProducto2", "Identificador del tipo de producto", "", "Identificador:" , "NO"); 
			textos("nombreTipoProducto", "Nombre del tipo de producto", "", "Nombre:", "SI"); 
			textos("descTipoProducto", "Descripci"+min_o+"n del tipo de producto", "", "Descripci"+min_o+"n:", "SI"); 
			textos("tipoLive", "Selecciona una opci"+min_o+"n", "", "Tipo emisi"+min_o+"n:", "SI"); 
			textos("fechaAlta", "Fecha en la que se creo el producto", "", "Fecha Alta: ", "NO"); 
			textos("fechaModificacion", "Fecha cuando se modifico el producto", "", "Fecha Modificaci"+min_o+"n", "NO"); 
		}
		//Nombres de las etiquetas

		//$("#tipoLiveTipoProducto_lbl").html("Tipo Live::");
		 //$("#caracteristicas_lbl").html("Caracter"+min_i+"sticas:");
		//update
		// $("#estadoCliente_lbl").html("Estado:");
		
		
		switch(opcion){
				case 'insert':
				$('#titulo_pagina').html('Alta tipo de producto');
			break;
			
			case 'update':
				$('#titulo_pagina').html('Modificar Tipo Producto');
				$("#fechaAlta_lbl").html("Fecha alta: ");
				$("#fechaModificacion_lbl").html("Fecha modificaci"+min_o+"n: ");
				break;			
			
			
			case  'query':	
				$('#titulo_pagina').html('Consulta tipo producto');
				break;
		
			case 'search':
				$('#titulo_pagina').html('Informaci'+min_o+'n tipos de productos');
				break;
		}//fin switch
		//Esto es en todos igual
		$('#legend_form').html('Datos del tipo de producto');
		$('#texto_label_select_id').html('Selecciona un tipo de producto:');
	}
	
	/**	@tabla tipo_camara
		@funcion que muestra los textos de los ficheros de la carpeta 'ajax/tipoProducto':
			- insert_tipo_camara.html
			- update_tipo_camara.html
			- search_tipo_camara.html
			- query_tipo_camara.html
	****************************************************************************************************************/
	function placeholder_textos_tipo_camara(opcion){ 
		
		txtBotones(opcion); // Texto de los botones
		
		if(opcion == 'insert'){
			//cuando el tercer campo este vacio, es porque con el primero tenemos bastante y para no repetir. Solo lo usaremos cuando el texto sea diferente
			textos("nombre", "Introduce el nombre del tipo de camara", "", "Nombre:", "SI"); 
			textos("descripcion", "Introduce una descripci"+min_o+"n para el tipo de camara", "", "Descripci"+min_o+"n:", "SI"); 
			
		}else{
			textos("idTipoCamara", "Selecciona una opci"+min_o+"n", "", "Selecciona una opci"+min_o+"n:" , "SI"); 
			textos("idTipoCamara2", "Identificador del tipo de c"+min_a+"mara", "", "Identificador:" , "NO"); 
			textos("nombre", "Nombre del tipo de c"+min_a+"mara", "", "Nombre:", "SI"); 
			textos("descripcion", "Descripci"+min_o+"n del tipo de c"+min_a+"mara", "", "Descripci"+min_o+"n:", "SI"); 
			textos("fechaAlta", "Fecha en la que se creo el tipo de c"+min_a+"mara", "", "Fecha Alta: ", "NO"); 
			textos("fechaModificacion", "Fecha cuando se modifico el tipo de c"+min_a+"mara", "", "Fecha Modificaci"+min_o+"n", "NO"); 
		
		}
		
		switch(opcion){
				case 'insert':
				$('#titulo_pagina').html('Alta tipo de camaras');
			break;
			
			case 'update':
				$('#titulo_pagina').html('Modificar Tipo Camara');
				break;			
			
			
			case  'query':	
				$('#titulo_pagina').html('Consulta tipo camara');
				break;
		
			case 'search':
				$('#titulo_pagina').html('Informaci'+min_o+'n tipos de camara');
				break;
		}//fin switch
		//Esto es en todos igual
		$('#legend_form').html('Datos del tipo de camaras');
}

/**	@tabla tipo_router
		@funcion que muestra los textos de los ficheros de la carpeta 'ajax/tipoProducto':
			- insert_tipo_router.html
			- update_tipo_router.html
			- search_tipo_router.html
			- query_tipo_router.html
	****************************************************************************************************************/
	function placeholder_textos_tipo_router(opcion){ 
		
		txtBotones(opcion); // Texto de los botones
		
		if(opcion == 'insert'){
			//cuando el tercer campo este vacio, es porque con el primero tenemos bastante y para no repetir. Solo lo usaremos cuando el texto sea diferente
			textos("nombre", "Introduce el nombre del tipo de router", "", "Nombre:", "SI"); 
			textos("descripcion", "Introduce una descripci"+min_o+"n para el tipo de router", "", "Descripci"+min_o+"n:", "SI"); 
			
		}else{
			textos("idTipoRouter", "Selecciona una opci"+min_o+"n", "", "Selecciona una opci"+min_o+"n:" , "SI"); 
			textos("idTipoRouter2", "Identificador del tipo de router", "", "Identificador:" , "NO"); 
			textos("nombre", "Nombre del tipo de router", "", "Nombre:", "SI"); 
			textos("descripcion", "Descripci"+min_o+"n del tipo de router", "", "Descripci"+min_o+"n:", "SI"); 
			textos("fechaAlta", "Fecha en la que se creo el tipo de  router", "", "Fecha Alta: ", "NO"); 
			textos("fechaModificacion", "Fecha cuando se modifico el tipo de router", "", "Fecha Modificaci"+min_o+"n: ", "NO"); 
		
		}
		
		switch(opcion){
				case 'insert':
				$('#titulo_pagina').html('Alta tipo de router');
			break;
			
			case 'update':
				$('#titulo_pagina').html('Modificar tipo de router');
				break;			
			
			case  'query':	
				$('#titulo_pagina').html('Consulta tipo router');
				break;
		
			case 'search':
				$('#titulo_pagina').html('Informaci'+min_o+'n tipos de router');
				break;
		}//fin switch
		//Esto es en todos igual
		$('#legend_form').html('Datos del tipo de router');
}



function placeholder_textos_dispositivo_emisor(opcion){
	// console.log("placeholder_textos_dispositivo_emisor");
	txtBotones(opcion); // Texto de los botones
		
		if(opcion == 'insert'){
			//cuando el tercer campo este vacio, es porque con el primero tenemos bastante y para no repetir. Solo lo usaremos cuando el texto sea diferente
			textos("nombre", "Introduce el nombre del dispositivo", "", "Nombre:", "SI"); 
			textos("descripcion", "Introduce una descripci"+min_o+"n para el dispositivo", "", "Descripci"+min_o+"n:", "SI"); 
			textos("numSerie", "Introduce el n"+min_u+"mero de serie del dispositivo ", "", "N"+min_u+"mero de serie: ", "SI"); 
			textos("modelo", "Introduce el modelo del dispositivo ", "", "Modelo:", "SI"); 
			textos("versionSO", "Introduce la versi"+min_o+"n SO del dispositivo ", "", "Versi"+min_o+"n SO:", "SI"); 
			
		}else{

			textos("idDispositivoEmisor", "Selecciona una opci"+min_o+"n", "", "Selecciona una opci"+min_o+"n:" , "SI"); 
			textos("idDispositivoEmisor2", "Identificador del dispositivo emisor", "", "Identificador:" , "NO"); 
			textos("nombre", "Introduce el nombre del dispositivo", "", "Nombre:", "SI"); 
			textos("descripcion", "Introduce una descripci"+min_o+"n para el dispositivo", "", "Descripci"+min_o+"n:", "SI"); 
			textos("numSerie", "Introduce el n"+min_u+"mero de serie del dispositivo ", "", "N"+min_u+"mero de serie: ", "SI"); 
			textos("modelo", "Introduce el modelo del dispositivo ", "", "Modelo:", "SI"); 
			textos("versionSO", "Introduce la versi"+min_o+"n SO del dispositivo ", "", "Versi"+min_o+"n SO:", "SI"); 
			textos("asignado", "Indica si el dispositivo esta asignado a una empresa.", "", "Asignado:", "SI"); 
			textos("fechaAlta", "Fecha en la que se creo el tipo de  router", "", "Fecha Alta: ", "NO"); 
			textos("fechaModificacion", "Fecha cuando se modifico el tipo de router", "", "Fecha Modificaci"+min_o+"n: ", "NO"); 
		}
		
		switch(opcion){
				case 'insert':
				$('#titulo_pagina').html('Alta dispositivo emisor');
			break;
			
			case 'update':
				$('#titulo_pagina').html('Modificar dispositivo emisor');
				break;		
			
			case  'query':	
				$('#titulo_pagina').html('Consulta dispositivo emisor');
				break; 
		
			case 'search':
				$('#titulo_pagina').html('Informaci'+min_o+'n dispositivo emisor');
				break; 
		}//fin switch
		
		//Esto es en todos igual
		$('#legend_form').html('Datos del dispositivo emisor');
}
	