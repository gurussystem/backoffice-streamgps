<ul class="nav main-menu">
	
	<!-- Inicio - Dashboard -->
	<li>
		<a href="ajax/dashboard.html" class="ajax-link">
			<i class="fa fa-dashboard"></i>
			<span class="hidden-xs">Dashboard</span>
		</a>
	</li>
				
	<!-- Backoffice  -->
	<li class="dropdown">
		<a href="#" class="dropdown-toggle">
			<i class="fa fa-users"></i>
			<span class="hidden-xs">Usuarios Backoffices </span>
		</a>
		<ul class="dropdown-menu">						
			<li id="li_insert_usersBackoffice"></li>	
			<li id="li_update_usersBackoffice"></li>	
			<li id="li_query_usersBackoffice"></li>	
			<li id="li_search_usersBackoffice"></li>	
			<li id="li_delete_usersBackoffice"></li>	
		</ul>
	</li>
		
		
	<!-- Cámara -->
	<li class="dropdown">
		<a href="#" class="dropdown-toggle">
			<i class="fa fa-video-camera"></i>
			<span class="hidden-xs">Cámara</span>
		</a>
	
		<ul class="dropdown-menu">
			<li id="li_insert_camara"></li>	
			<li id="li_update_camara"></li>	
			<li id="li_query_camara"></li>	
			<li id="li_search_camara"></li>	
			<li id="li_delete_camara"></li>	
		</ul>
	</li>
	

	<!-- Empresa -->
	<li class="dropdown">
		<a href="#" class="dropdown-toggle">
			<i class="fa fa-users"></i>
			<span class="hidden-xs">Clientes</span>
		</a>
		<ul class="dropdown-menu">
			<li id="li_insert_cliente"></li>	
			<li id="li_update_cliente"></li>	
			<li id="li_query_cliente"></li>	
			<li id="li_search_cliente"></li>	
			<li id="li_delete_cliente"></li>	
			
			<!-- cliente cta -->
			<li class="dropdown">
				<a href="#" class="dropdown-toggle">
					<i class="fa fa-plus-square"></i>
					<span class="hidden-xs">Cta Cliente</span>
				</a>
				<ul class="dropdown-menu">
					<li id="li_update_clienteCta" class="nivel3-menu"></li>
					<li id="li_query_clienteCta" class="nivel3-menu"></li>	
				</ul>
			</li>
			
				
			
		</ul>
	</li>	
	
	
	<!-- Producto  -->
	<li class="dropdown">
		<a href="#" class="dropdown-toggle">
			<i class="fa fa-briefcase"></i>
			<span class="hidden-xs"> Producto</span>
		</a>
		<ul class="dropdown-menu">	
			<li id="li_insert_producto"></li>	
			<li id="li_update_producto"></li>	
			<li id="li_query_producto"></li>	
			<li id="li_search_producto"></li>	
			<li id="li_delete_producto"></li>	
			<li id="li_asignar_producto"></li>	
		</ul>
	</li>

	
	<!-- Router -->
	<li class="dropdown">
		<a href="#" class="dropdown-toggle">
			<i class="fa fa-hdd-o"></i>
			<span class="hidden-xs">Router</span>
		</a>
		<ul class="dropdown-menu">
			<li id="li_insert_router"></li>	
			<li id="li_update_router"></li>	
			<li id="li_query_router"></li>	
			<li id="li_search_router"></li>
			<li id="li_delete_router"></li>
		</ul>
	</li>

		
	<!-- Sim  -->
	<li class="dropdown">
		<a href="#" class="dropdown-toggle">
			<i class="fa fa-credit-card"></i>
			<span class="hidden-xs"> Sim </span>
		</a>
		<ul class="dropdown-menu">
			<li id="li_insert_sim"></li>	
			<li id="li_update_sim"></li>	
			<li id="li_query_sim"></li>	
			<li id="li_search_sim"></li>
			<li id="li_delete_sim"></li>
		</ul>
	</li>	
	
	<!-- dispositivo Emisor -->
	<li class="dropdown">
		<a href="#" class="dropdown-toggle">
			<i class="fa fa-credit-card"></i>
			<span class="hidden-xs"> Dispositivo Emisor </span>
		</a>
		<ul class="dropdown-menu">
			<li id="li_insert_dispositivoEmisor"></li>	
			<li id="li_update_dispositivoEmisor"></li>	
			<li id="li_query_dispositivoEmisor"></li>	
			<li id="li_search_dispositivoEmisor"></li>
			<li id="li_delete_dispositivoEmisor"></li>
		</ul>
	</li>
	
	
	<!-- inicio Logs -->
	<li class="dropdown">
		<a href="#" class="dropdown-toggle">
			<i class="fa fa-bar-chart-o"></i>
			 <span class="hidden-xs" title="Tablas que usan en comun todos los clientes">Logs </span>
		</a>
					
		<ul class="dropdown-menu">
	
			<!-- Log acciones  -->
			<li class="dropdown">
				<a href="#" class="dropdown-toggle">
					<i class="fa fa-bar-chart-o"></i>
					<span class="hidden-xs"> Log Acciones </span>
				</a>
				<ul class="dropdown-menu">
					<li id="li_search_log_acciones" class="nivel3-menu"></li>
				</ul>
			</li>		
		
			<!-- Log backoffices  -->
			<li class="dropdown">
				<a href="#" class="dropdown-toggle">
					<i class="fa fa-bar-chart-o"></i>
					<span class="hidden-xs"> Log backoffices </span>
				</a>
				<ul class="dropdown-menu">
					<li id="li_search_log_backoffices" class="nivel3-menu"></li>
				</ul>
			</li>
			
			<!-- Tipo Eventos log  -->
			<li class="dropdown">
				<a href="#" class="dropdown-toggle">
					<i class="fa fa-book"></i>
					<span class="hidden-xs"> Tipo Eventos Log </span>
				</a>
				<ul class="dropdown-menu">
					<li id="li_insert_tipo_evento_log" class="nivel3-menu"></li>	
					<li id="li_update_tipo_evento_log" class="nivel3-menu"></li>	
					<li id="li_query_tipo_evento_log" class="nivel3-menu"></li>	
					<li id="li_search_tipo_evento_log" class="nivel3-menu"></li> 
					<li id="li_delete_tipo_evento_log" class="nivel3-menu"></li> 
				</ul>
			</li>
		</ul> <!-- Fin apartado log -->
	</li><!-- Fin apartado log -->

				
	<!-- Configstream -->
	<li class="dropdown">
		<a href="#" class="dropdown-toggle">
			<i class="fa fa-cogs"></i>
			 <span class="hidden-xs" title="Tablas comun para los clientes">Config Stream</span>
		</a>
		<ul class="dropdown-menu">
												
			<!-- Impuestos -->
			<li class="dropdown">
				<a href="#" class="dropdown-toggle">
					<i class="fa fa-plus-square"></i>
					<span class="">Impuestos</span>
				</a>
				<ul class="dropdown-menu">
					<li id="li_insert_impuestos" class="nivel3-menu"></li>
					<li id="li_update_impuestos" class="nivel3-menu"></li>
					<li id="li_query_impuestos" class="nivel3-menu"></li>
					<li id="li_search_impuestos" class="nivel3-menu"></li>
					<li id="li_delete_impuestos" class="nivel3-menu"></li>
				</ul>
			</li>
						
			<!-- Pais -->
			<li class="dropdown">
				<a href="#" class="dropdown-toggle">
					<i class="fa fa-plus-square"></i>
					<span class="">Paises </span>
				</a>
					
				<ul class="dropdown-menu">	
					<li id="li_insert_pais" class="nivel3-menu"></li>
					<li id="li_update_pais" class="nivel3-menu"></li>
					<li id="li_query_pais" class="nivel3-menu"></li>
					<li id="li_search_pais" class="nivel3-menu"></li>
					<li id="li_delete_pais" class="nivel3-menu"></li>
				</ul>
			</li>

			<!-- Provincia  -->
			<li class="dropdown">
				<a href="#" class="dropdown-toggle">
					<i class="fa fa-plus-square"></i>
					<span class=""> Provincia</span>
				</a>
				<ul class="dropdown-menu" >
					<li id="li_insert_provincia" class="nivel3-menu"></li>
					<li id="li_update_provincia" class="nivel3-menu"></li>
					<li id="li_query_provincia" class="nivel3-menu"></li>
					<li id="li_search_provincia" class="nivel3-menu"></li>
					<li id="li_delete_provincia" class="nivel3-menu"></li>
				</ul>
			</li>
	
			<!-- Rol usuario   -->
			<li class="dropdown">
				<a href="#" class="dropdown-toggle">
					<i class="fa fa-plus-square"></i>
					<span class=""> Rol usuario</span>
				</a>
				<ul class="dropdown-menu">
					<li id="li_insert_rol_user" class="nivel3-menu"></li>
					<li id="li_update_rol_user" class="nivel3-menu"></li>
					<li id="li_query_rol_user" class="nivel3-menu"></li>
					<li id="li_search_rol_user" class="nivel3-menu"></li>
					<li id="li_delete_rol_user" class="nivel3-menu"></li>
				</ul>
			</li>
			
			<!-- Tipo Aviso  -->
			<li class="dropdown">
				<a href="#" class="dropdown-toggle">
					<i class="fa fa-plus-square"></i>
					<span class=""> Tipo Aviso </span>
				</a>
				<ul class="dropdown-menu">
					<li id="li_insert_tipo_aviso" class="nivel3-menu"></li>
					<li id="li_update_tipo_aviso" class="nivel3-menu"></li>
					<li id="li_query_tipo_aviso" class="nivel3-menu"></li>
					<li id="li_search_tipo_aviso" class="nivel3-menu"></li>
					<li id="li_delete_tipo_aviso" class="nivel3-menu"></li>
				</ul>
			</li>
		
			<!-- Tipo Producto  -->
			<li class="dropdown">
				<a href="#" class="dropdown-toggle">
					<i class="fa fa-plus-square"></i>
					<span class=""> Tipo Producto </span>
				</a>
				<ul class="dropdown-menu">
					<li id="li_insert_tipo_producto"   class="nivel3-menu"></li>
					<li id="li_update_tipo_producto" class="nivel3-menu"></li>
					<li id="li_query_tipo_producto"  class="nivel3-menu"></li>
					<li id="li_search_tipo_producto" class="nivel3-menu"></li>				
					<li id="li_delete_tipo_producto" class="nivel3-menu"></li>				
				</ul>
			</li>
			
			<!-- Tipo Router  -->
			<li class="dropdown">
				<a href="#" class="dropdown-toggle">
					<i class="fa fa-plus-square"></i>
					<span class=""> Tipo Router </span>
				</a>
				<ul class="dropdown-menu">
					<li id="li_insert_tipo_router"   class="nivel3-menu"></li>
					<li id="li_update_tipo_router" class="nivel3-menu"></li>
					<li id="li_query_tipo_router"  class="nivel3-menu"></li>
					<li id="li_search_tipo_router" class="nivel3-menu"></li>				
					<li id="li_delete_tipo_router" class="nivel3-menu"></li>				
				</ul>
			</li>
			
			
			<!-- Tipo Camara  -->
			<li class="dropdown">
				<a href="#" class="dropdown-toggle">
					<i class="fa fa-plus-square"></i>
					<span class=""> Tipo Cámara </span>
				</a>
				<ul class="dropdown-menu">
					<li id="li_insert_tipo_camara"   class="nivel3-menu"></li>
					<li id="li_update_tipo_camara" class="nivel3-menu"></li>
					<li id="li_query_tipo_camara"  class="nivel3-menu"></li>
					<li id="li_search_tipo_camara" class="nivel3-menu"></li>				
					<li id="li_delete_tipo_camara" class="nivel3-menu"></li>				
				</ul>
			</li>

			

			
		</ul> <!-- fin menu multi -->
	</li> <!-- fin menu multi -->
<!-- fin configstream -->
<br>
<br>
		
	
	<!-- StreamSports  -->
	<li class="dropdown">
		<a href="#" class="dropdown-toggle">
			<i class="fa fa-users"></i>
			<span class="hidden-xs">StreamSports </span>
		</a>
		<ul class="dropdown-menu">						
			<li id="li_insert_userSport"></li>
			<li id="li_insert_logosCliente_sport"></li>
			<li id="li_update_redesSociales_sport"></li>	
		</ul>
	</li>
</ul>			