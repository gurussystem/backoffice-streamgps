<!DOCTYPE HTML>
<html>
	<head>
		<!--?php include('include/includeMetas.php');?>
		<!--?php include('include/includeFavicon.php'); -->
		<link rel="stylesheet" type="text/css" href="css/index.css"> 
		<script type="text/javascript" src="js/scriptLogin.js" ></script>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>	
		<style>
			table tr td{border:1px solid !important;}
		.estadoInicial{
			display:block;
			/*display:none;*/
		}
			
		.txtStyle{
			height: 20px;
			font-size: 15px;
			float: left;
			padding-right: 10px;
			margin-bottom: 5px;
			margin-top: 5px;
		}
			
		.selectStyleMod{
			margin-right: 10px;
			margin-top: 5px;
		}
		.tdStyleMod{
			height: 90px !important;
		}
		
		.inputPassMod, .inputSimMod{
			margin-right: 10px;
		}
		
		.medidasInputMod{
			width: 150px !important;
		}			
		</style>
		<script type="text/javascript">
			/******************************************************************************************************
				ACtivamos los selects, (numero indicado en los checkbox)
			******************************************************************************************************/
			$(document).ready(function(){
				//Devuelve el valor del checkbox
				$("input[name=numModulos]").change(function () {	 
					numModulosContratados = $(this).val();
					//alert(numModulosContratados);
					//Escondo todos los select-options
					for (var m = 1; m <=6; m++) { 
						$('#tr_box_modulo0'+m).css('display', 'none');
					}
					//Mostramos el numero de box Modulos contratados					
					for (var m = 1; m <=numModulosContratados; m++) { 
						//activamos modulos a contratar
						$('#tr_box_modulo0'+m).css('display', 'block');
					}
					vaciarInput('numModulosContratar');
				});
				listadoTipoCamaras();
			});//fin jQuery
		</script>
	</head>
	
	
	<body>
		<div id="contenedor">
			<div id="header">
				<!--img src="img/headerImakestream.jpg" title="Imakestream.com"/--> 
			</div>
		
			<div id="contenido">
				<table id="tablaContenedora" class="tabli " align='center'> <!--tabla contenedora -->
					<tr>
						<td colspan=3 id="cabecera" >
							Alta en ControlStream.tv
						</td>
					</tr>
				
				
					<tr>
										
						<td colspan=3> 
							<table align="center" class="contacto" id="signin" >
								<tr>
									<td class="marginBottom">
										<label class="texto" title="empresa">Empresa: </label>
									</td>
						
									<td>
										<input type="text" id="empresa" title="Introduce tu empresa" onclick="vaciarInput('empresa')" placeholder="Introduce el nombre de la empresa"/>
										<div id="empresa_div" class="campoObligatorio" style="display:none;"> * Campo Obligatorio, introduce el nombre de la empresa. </div>
									</td>
								</tr>
								
								<tr>
									<td class="marginBottom">
										<label class="texto" title="Usuario">Usuario: </label>
									</td>
						
									<td>
										<input type="text" id="user" title="Introduce tu usuario" onclick="vaciarInput('user')" placeholder="Introduce el nommbre del usuario" />
										<div id="user_div" class="campoObligatorio" style="display:none;"> * Campo Obligatorio, introduce un nombre de usuario </div>
									</td>
								</tr>

								<tr>
									<td class="marginBottom">
										<label class="texto" title="Password">Password: </label>
									</td>
									
									<td>
										<input type="password" id="password" title="Introduce tu password" onKeypress="teclaIntro(this, event);" onclick="vaciarInput('password')" placeholder="Introduce una contrase&ntilde;a" /> 
										<div id="password_div" class="campoObligatorio" style="display:none;"> * Campo Obligatorio, introduce una contrase&ntilde;a </div>
									</td>
								</tr>

								<tr>
									<td class="marginBottom">
										<label class="texto" title="Email">Email: </label>
									</td>
						
									<td>
										<input type="text" id="email" title="Introduce un email donde se env&iacute;aran todos la informaci&oacute;n" onclick="vaciarInput('email')" placeholder="Introduce un email" />
										<div id="email_div" class="campoObligatorio" style="display:none;"> * Campo Obligatorio, introduce un email donde enviarte la informaci&oacute;n</div>
									</td>
								</tr>
								
								<tr>
									<td class="marginBottom">
										<label class="texto" title="DNI">CIF/DNI: </label>
									</td>
						
									<td>
										<input type="text" id="dni" maxlength=9 title="Introduce tu CIF/DNI" onclick="vaciarInput('dni')" placeholder="Introduce el CIF/DNI de la empresa"/>
										<div id="dni_div" class="campoObligatorio" style="display:none;"> * Campo Obligatorio, introduce el CIF/DNI de la empresa </div>
									</td>
								</tr>
								
								<tr>
									<td class="marginBottom">
										<label class="texto" title="Direcci&oacute;n">Direcci&oacute;n: </label>
									</td>
					
									<td>
										<input type="text" id="direccion" title="Introduce la direcci&oacute;n de la empresa" onclick="vaciarInput('direccion')" placeholder="Introduce la direcci&oacute;n donde esta la empresa"/>
										<div id="direccion_div" class="campoObligatorio" style="display:none;"> * Campo Obligatorio, introduce la direcci&oacute;n de la empresa </div>
									</td>
								</tr>

								<tr>
									<td class="marginBottom">
										<label class="texto" title="Ciudad">Poblaci&oacute;n: </label>
									</td>
						
									<td>
										<input type="text" id="ciudad" title="Introduce la poblaci&oacute;n donde esta la empresa" onclick="vaciarInput('ciudad')" placeholder="Introduce la poblaci&oacute;n donde esta la empresa" />
										<div id="ciudad_div" class="campoObligatorio" style="display:none;"> * Campo Obligatorio, introduce la ciudad donde se encuentra la empresa </div>
									</td>
								</tr>

								<tr>
									<td class="marginBottom">
										<label class="texto" title="Codigo postal">Codigo postal: </label>
									</td>
					
									<td>
										<input type="text" id="cp" title="Introduce tu c&oacute;digo postal" maxlength=5 onclick="vaciarInput('cp')" placeholder="Introduce el c&oacute;digo postal" />
										<div id="cp_div" class="campoObligatorio" style="display:none;"> * Campo Obligatorio, c&oacute;digo postal de la empresa </div>
									</td>
								</tr>

								<tr>
									<td class="marginBottom">
										<label class="texto" title="Provincia">Provincia: </label>
									</td>
					
									<td>
										<input type="text" id="provincia" title="Introduce la provincia donde esta la empresa" onclick="vaciarInput('provincia')" placeholder="Introduce la provincia donde esta la empresa" />
										<!-- Añadirlo cuando puedas
										<select >
											<option value='selecciona'>Selecciona</option>             <option value='A Coruña' >A Coruña</option>             <option value='álava'>álava</option>             <option value='Albacete' >Albacete</option>             <option value='Alicante'>Alicante</option>             <option value='Almería' >Almería</option>             <option value='Asturias' >Asturias</option>             <option value='ávila' >Ávila</option>             <option value='Badajoz' >Badajoz</option>             <option value='Barcelona'>Barcelona</option>             <option value='Burgos' >Burgos</option>             <option value='Cáceres' >Cáceres</option>             <option value='Cádiz' >Cádiz</option>             <option value='Cantabria' >Cantabria</option>             <option value='Castellón' >Castellón</option>             <option value='Ceuta' >Ceuta</option>             <option value='Ciudad Real' >Ciudad Real</option>             <option value='Córdoba' >Córdoba</option>             <option value='Cuenca' >Cuenca</option>             <option value='Gerona' >Gerona</option>             <option value='Girona' >Girona</option>             <option value='Las Palmas' >Las Palmas</option>             <option value='Granada' >Granada</option>             <option value='Guadalajara' >Guadalajara</option>             <option value='Guipúzcoa' >Guipúzcoa</option>             <option value='Huelva' >Huelva</option>             <option value='Huesca' >Huesca</option>             <option value='Jaén' >Jaén</option>             <option value='La Rioja' >La Rioja</option>             <option value='León' >León</option>             <option value='Lleida' >Lleida</option>             <option value='Lugo' >Lugo</option>             <option value='Madrid' >Madrid</option>             <option value='Malaga' >Málaga</option>             <option value='Mallorca' >Mallorca</option>             <option value='Melilla' >Melilla</option>             <option value='Murcia' >Murcia</option>             <option value='Navarra' >Navarra</option>             <option value='Orense' >Orense</option>             <option value='Palencia' >Palencia</option>             <option value='Pontevedra'>Pontevedra</option>             <option value='Salamanca'>Salamanca</option>             <option value='Segovia' >Segovia</option>             <option value='Sevilla' >Sevilla</option>             <option value='Soria' >Soria</option>             <option value='Tarragona' >Tarragona</option>             <option value='Tenerife' >Tenerife</option>             <option value='Teruel' >Teruel</option>             <option value='Toledo' >Toledo</option>             <option value='Valencia' >Valencia</option>             <option value='Valladolid' >Valladolid</option>             <option value='Vizcaya' >Vizcaya</option>             <option value='Zamora' >Zamora</option>             <option value='Zaragoza'>Zaragoza</option>
										</select>
										-->
										<div id="provincia_div" class="campoObligatorio" style="display:none;"> * Campo Obligatorio, introduce la provincia donde esta la empresa </div>
									</td>
								</tr>

								<tr>
									<td class="marginBottom">
										<label class="texto" title="telefono 1">Tel&eacute;fono Fijo: </label>
									</td>
					
									<td>
										<input type="text" id="tlf" title="Introduce tu tel&eacute;fono" maxlength=9 onclick="vaciarInput('tlf')" placeholder="Introduce el tel&eacute;fono fijo de la empresa" />
										<div id="tlf_div" class="campoObligatorio" style="display:none;"> * Campo Obligatorio, introduce el tel&eacute;fono fijo de la empresa</div>
									</td>
								</tr>

								<tr>
									<td class="marginBottom">
										<label class="texto" title="telefono 2">Tel&eacute;fono Contacto (Movil): </label>
									</td>
					
									<td>
										<input type="text" id="tlf2" title="Introduce un segundo número de tel&eacute;fono" maxlength=9 onclick="vaciarInput('tlf2')" placeholder="Introduce un segundo n&uacute;mero de tel&eacute;fono"/>
										<div id="tlf2_div" class="campoObligatorio" style="display:none;"> * Campo Obligatorio, un segundo n&uacute;mero de tel&eacute;fono </div>
									</td>
								</tr>
								
								<tr>
									<td class="marginBottom">
										<label class="texto" title="Fecha Solicitud Alta">Fecha Solicitud Alta Cliente: </label>
									</td>
					
									<td>
										<input type="date" id="fechaSolicitudAlta" name="bday" title="Introduce la fecha en que el cliente solicito el alta" onclick="vaciarInput('fechaSolicitudAlta')" > 
										<div id="fechaSolicitudAlta_div" class="campoObligatorio" style="display:none;"> * Campo Obligatorio, introduce la fecha en que se solicito el alta </div>
									</td>
								</tr>

								<!--Apartado banco -->
								<tr>
									<td class="marginBottom">
										<label class="texto" title="Numero de cuenta donde el cliente hara el pago mensual">Domiciliacion Bancaria</label>
									</td>
					
									<td>
										<input type="text" id="iban"  	  class="cta" placeholder="IBAN" 			maxlength=4  title="Introduce el iban" > 
										<input type="text" id="entidad"   class="cta" placeholder="ENTIDAD" 		maxlength=4  title="Introduce la entidad" > 
										<input type="text" id="sucursal"  class="cta" placeholder="SUCURSAL"		maxlength=4  title="Introduce la sucursal" > 
										<input type="text" id="dc"  	  class="cta" placeholder="DC" 				maxlength=2  title="Introduce el DC" > 
										<input type="text" id="numCuenta" class="ctaNumCuenta" placeholder="NUMERO CUENTA"  	maxlength=10 title="Introduce el numero de cuenta" > 
										<div id="datosBancarios_div" class="campoObligatorio" style="display:none;">  </div>
									</td>
								</tr>
								
								<!-- Apartado modulos contratados <tr> <td> <tabla new> -->
								<tr>
									<td id="" >	
											<label> Modulos contratados </label>
									</td>
									
									<td>
										<label> Indica el numero de modulos que quiere el cliente </label> 
										<input type="radio" name="numModulos" value="2">2<br>
										<input type="radio" name="numModulos" value="4">4<br>
										<input type="radio" name="numModulos" value="6">6<br> 
										<div id="numModulosContratar_div" class="campoObligatorio" style="display:none;"> * Campo Obligatorio, selecciona el numero de modulos a contratar</div>
									</td>
							
							
								</tr>
							
								<!-- 
									A la que se pueda y haya tiempo hacer una funcio que te pinte el numero de 
									modulos que le indiquemos, ahora hay 6 por defecto pero si nos piden uno mas ya estamos perdidos...  
								-->
								<tr>
								<td COLSPAN=2>
								<table style="border:1px solid red;">
								<!-- Modulo 01-->
								<tr  id="tr_box_modulo01" class="estadoInicial"> 
									<td  BGCOLOR="#99CCFF" class="tdStyleMod" >
										<div id="tr_box_modulo010" class="estadoInicial">
											<label class="txtStyle height20">  Modulo 01:  </label>
											<select id="modulo01" onclick="seleccionModulo(1);" class="selectStyleMod" ></select> 
											<div style="clear:both;" id="modulo01_boxInfo">
												PASS: <input type="text" id="modulo01_pass" placeholder="Password modulo 01" title="Introduce una contrasenya para la camara"   class="inputPassMod medidasInputMod"> 
												SIM:  <input type="text" id="modulo01_sim"  placeholder="Num SIM modulo 01" title="Introduce el num de tarjeta sim de la camara"    class="inputSimMod medidasInputMod"> 
												IMEI:  <input type="text" id="modulo01_imei"  placeholder="Num IMEI modulo 01" title="Introduce el num imei del dispositivo"   class="medidasInputMod"> 
											</div>
											<div id="errorMod01" class="campoObligatorio" style="display:none;">* Campos Obligatorios, asegurate que has rellenado todo.</div>
										</div>
									</td>
								</tr>
									
								<!-- Modulo 02-->
								<tr >
									<td  BGCOLOR="#99CCFF" class="tdStyleMod" > 
										<div id="tr_box_modulo02" class="estadoInicial">
											<label class="txtStyle height20"> Modulo 02: </label> 
											<select id="modulo02" onclick="seleccionModulo(2);" class="selectStyleMod" ></select> 
											<div style="clear:both;" id="modulo02_boxInfo">
												PASS:<input type="text" id="modulo02_pass" placeholder="Password modulo 02" title="Introduce una contrasenya para la camara"   class="inputPassMod medidasInputMod"> 
												SIM:  <input type="text" id="modulo02_sim"  placeholder="Num SIM modulo 02" title="Introduce el num de tarjeta sim de la camara"    class="inputSimMod medidasInputMod">  
												IMEI:  <input type="text" id="modulo02_imei"  placeholder="Num IMEI modulo 02" title="Introduce el num imei del dispositivo"   class="medidasInputMod"> 
											</div>
											<div id="errorMod02" class="campoObligatorio"  style="display:none;">* Campos Obligatorios, asegurate que has rellenado todo.</div>
										</div>
									</td>
								</tr>
								
								<!-- Modulo 03-->
								<tr >
									<td  BGCOLOR="#99CCFF" class="tdStyleMod" > 
										<div id="tr_box_modulo03" class="estadoInicial">
											<label class="txtStyle height20"> Modulo 03: </label>
											<select id="modulo03" onclick="seleccionModulo(3); " class="selectStyleMod" ></select> 
											<div style="clear:both;" id="modulo03_boxInfo">
												PASS: <input type="text" id="modulo03_pass" placeholder="Password modulo 03" title="Introduce una contrasenya para la camara"   class="inputPassMod medidasInputMod">  
												SIM:  <input type="text" id="modulo03_sim"  placeholder="Num SIM modulo 03" title="Introduce el num de tarjeta sim de la camara"    class="inputSimMod medidasInputMod"> 
												IMEI:  <input type="text" id="modulo03_imei"  placeholder="Num IMEI modulo 03" title="Introduce el num imei del dispositivo"   class="medidasInputMod"> 
											</div>
											<div id="errorMod03" class="campoObligatorio" style="display:none;">* Campos Obligatorios, asegurate que has rellenado todo.</div>
										</div>
									</td>
								</tr>
								
								<!-- Modulo 04-->
								<tr >
									<td  BGCOLOR="#99CCFF" class="tdStyleMod"  > 
										<div id="tr_box_modulo04" class="estadoInicial">
											<label class="txtStyle height20"> Modulo 04: </label>								
											<select id="modulo04" onclick="seleccionModulo(4);" class="selectStyleMod" ></select>  
											<div style="clear:both;" id="modulo04_boxInfo">										
												PASS: <input type="text" id="modulo04_pass" placeholder="Password modulo 04" title="Introduce una contrasenya para la camara"   class="inputPassMod medidasInputMod"> 
												SIM:  <input type="text" id="modulo04_sim"  placeholder="Num SIM modulo 04" title="Introduce el num de tarjeta sim de la camara"    class="inputSimMod medidasInputMod"> 
												IMEI:  <input type="text" id="modulo04_imei"  placeholder="Num IMEI modulo 04" title="Introduce el num imei del dispositivo"   class="medidasInputMod"> 
											</div>
											<div id="errorMod04" class="campoObligatorio" style="display:none;">* Campos Obligatorios, asegurate que has rellenado todo.</div>
										</div>
									</td>
								</tr>
								
								
								<!-- Modulo 05-->
								<tr >
									<td  BGCOLOR="#99CCFF" class="tdStyleMod"> 
										<div id="tr_box_modulo05" class="estadoInicial">
											<label class="txtStyle height20">	Modulo 05: </label>
											<select id="modulo05" onclick="seleccionModulo(5);" class="selectStyleMod" ></select> 
											<div style="clear:both;" id="modulo05_boxInfo">										
												PASS: <input type="text" id="modulo05_pass" placeholder="Password modulo 05" title="Introduce una contrasenya para la camara"   class="inputPassMod medidasInputMod"> 
												SIM:   <input type="text" id="modulo05_sim"  placeholder="Num SIM modulo 05" title="Introduce el num de tarjeta sim de la camara"    class="inputSimMod medidasInputMod">  
												IMEI:  <input type="text" id="modulo05_imei"  placeholder="Num IMEI modulo 05" title="Introduce el num imei del dispositivo"   class="medidasInputMod"> 
											</div>
											<div id="errorMod05" class="campoObligatorio" style="display:none;">* Campos Obligatorios, asegurate que has rellenado todo.</div>
										</div>
									</td>
								</tr>
								
								<!-- Modulo 06-->
								<tr  >
									<td  BGCOLOR="#99CCFF" class="tdStyleMod" > 
										<div id="tr_box_modulo06" class="estadoInicial">
											<label class="txtStyle height20"> Modulo 06: </label>
											<select id="modulo06" onclick="seleccionModulo(6);" class="selectStyleMod" ></select> 
											<div style="clear:both;" id="modulo06_boxInfo">										
												PASS: <input type="text" id="modulo06_pass" placeholder="Password modulo 06" title="Introduce una contrasenya para la camara"  class="inputPassMod medidasInputMod"> 
												SIM:  <input type="text" id="modulo06_sim"  placeholder="Num SIM modulo 06" title="Introduce el num de tarjeta sim de la camara"  class="inputSimMod medidasInputMod"> 
												IMEI:  <input type="text" id="modulo06_imei"  placeholder="Num IMEI modulo 06" title="Introduce el num imei del dispositivo"   class="medidasInputMod"> 
											</div>
											<div id="errorMod06" class="campoObligatorio" style="display:none;">* Campos Obligatorios, asegurate que has rellenado todo.</div>
										</div>
									</td>
								</tr>
								</table>
								</td>
								</tr>
							
								<!-- Boton para guardar los datos -->
								<tr>
									<td colspan=3 id="">	
										<input type="button" value="Guardar Informaci&oacute;n" title="Guardar Informaci&oacute;n" onclick="validar_datos()" style=" float: right; " />		
									</td>
								</tr>
							</table> <!-- fin tabla formulario datos -->
						</td>
					</tr>
				</table> <!-- tabla principal (la que lo contiene todo) -->
				
				<p>
					<center>
						Aplicaci&oacuten optimizada para los navegadores Firefox y Chrome.
					</center>
				</p>
				
				
			</div> <!-- Fin id=contenido-->
			
			<div id="pie">
				<br/>
			</div> <!-- Fin id=pie -->
		</div> <!-- Fin id=contenido -->
	</body>
</html>