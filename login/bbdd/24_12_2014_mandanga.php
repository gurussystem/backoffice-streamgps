<?php
	//Definimos variables
	require_once("db_config.php");
	require 'Mandrill.php';
	$conexion = mysql_connect(DB_SERVER, DB_USER, DB_PASSWORD) or die(mysql_error());
	$bbddok=1; //La bbdd se ha construido correctamente
	$mailtecnico="";
	$txtmail_datosEmpresa ="";
	$txtMail_createTabla  ="";
	$txtMail_createTablaError ="";
	$txt_createBDError = "";
	$txt_createBD = "";
	
	$response = array();

	if(isset($_POST['empresa'])){
		$empresa = $_POST['empresa'];//'Pavos St Pulpis';
		$usuario = $_POST['user'];//'tica';
		$passEmpresa = $_POST['pass'];//'tica01';
		$email = $_POST['email'];//'tica01';
		$dni = $_POST['dni'];//'tica01';
		$direccion = $_POST['direccion'];//'tica01';
		$ciudad = $_POST['ciudad'];//'tica01';
		$cp = $_POST['cp'];
		$provincia = $_POST['provincia'];
		$tlf = $_POST['tlf'];
		$tlf2 = $_POST['tlf2'];
		$fechaSolicitudAlta = $_POST['fechaSolicitudAlta'];
		$datosBanco = $_POST['datosBanco'];
		$numModulosContratados = $_POST['numModulosContratados'];
		
		//Array con los datos de los modulos
		$mod_array_type = $_POST['mod_array_type'];		
		$mod_array_pass = $_POST['mod_array_pass'];	
		$mod_array_sim = $_POST['mod_array_sim'];
		$mod_array_imei = $_POST['mod_array_imei'];
		$mod_array_nom = $_POST['mod_array_nom'];

		/*
		if(isset($_POST['mod_01nom'])){ $mod_01nom = $_POST['mod_01nom'];}else{$mod_01nom = "";}
		if(isset($_POST['mod_02nom'])){ $mod_02nom = $_POST['mod_02nom'];}else{$mod_02nom = "";}
		if(isset($_POST['mod_03nom'])){ $mod_03nom = $_POST['mod_03nom'];}else{$mod_03nom = "";}
		if(isset($_POST['mod_04nom'])){ $mod_04nom = $_POST['mod_04nom'];}else{$mod_04nom = "";}
		if(isset($_POST['mod_05nom'])){ $mod_05nom = $_POST['mod_05nom'];}else{$mod_05nom = "";}
		if(isset($_POST['mod_06nom'])){ $mod_06nom = $_POST['mod_06nom'];}else{$mod_06nom = "";}
		*/
		

	}else{
		$empresa = $_GET['empresa'];//'Pavos St Pulpis';
		$usuario = $_GET['user'];//'tica';
		$passEmpresa = $_GET['pass'];//'tica01';
		$email = $_GET['email'];//'tica01';
		$dni = $_GET['dni'];//'tica01';
		$direccion = $_GET['direccion'];//'tica01';
		$ciudad = $_GET['ciudad'];//'tica01';
		$cp = $_GET['cp'];
		$provincia = $_GET['provincia'];
		$tlf = $_GET['tlf'];
		$tlf2 = $_GET['tlf2'];
		$fechaSolicitudAlta = $_GET['fechaSolicitudAlta'];
		$datosBanco = $_GET['datosBanco'];
		$numModulosContratados = $_GET['numModulosContratados'];
		
		//Array con los datos de los modulos
		$mod_array_type = $_GET['mod_array_type'];
		$mod_array_pass = $_GET['mod_array_pass'];
		$mod_array_sim = $_GET['mod_array_sim'];
		$mod_array_imei = $_GET['mod_array_imei'];
		$mod_array_nom = $_GET['mod_array_nom'];
		
		/*
		if(isset($_GET['mod_01nom'])){ $mod_01nom = $_GET['mod_01nom'];}else{$mod_01nom = "";}
		if(isset($_GET['mod_02nom'])){ $mod_02nom = $_GET['mod_02nom'];}else{$mod_02nom = "";}
		if(isset($_GET['mod_03nom'])){ $mod_03nom = $_GET['mod_03nom'];}else{$mod_03nom = "";}
		if(isset($_GET['mod_04nom'])){ $mod_04nom = $_GET['mod_04nom'];}else{$mod_04nom = "";}
		if(isset($_GET['mod_05nom'])){ $mod_05nom = $_GET['mod_05nom'];}else{$mod_05nom = "";}
		if(isset($_GET['mod_06nom'])){ $mod_06nom = $_GET['mod_06nom'];}else{$mod_06nom = "";}
		*/
	}
		
	$usuario=str_replace(array('(','\'','´',',','.','{','}','+','´','*','¨','[',']','%','&','/','%','\$','#','"','!','?','¡',')',',',';',':','^','`'),'',$usuario);
	$usuario=str_replace(' ', '_', $usuario);

	/*************************************************************************
		Montamos la parte del email donde se muestran los datos de la empresa
	**************************************************************************/
	$txtmail_datosEmpresa.='<h4> Los datos intoducidos en el formulario son los siguientes:  </h4> <br />';
	$txtmail_datosEmpresa.='<b> Empresa:   </b> '.$empresa.		'<br />';
	$txtmail_datosEmpresa.='<b> Usuario:   </b> '.$usuario.		'<br />';
	$txtmail_datosEmpresa.='<b> Password:  </b> '.$passEmpresa .	'<br />';
	$txtmail_datosEmpresa.='<b> Dominio :  </b> '.$usuario.'.controlstream.tv (en caso de no verse como link, copie y pegue en el navegador) <br />';
	$txtmail_datosEmpresa.='<b> Email:     </b> '.$email.		'<br />';
	$txtmail_datosEmpresa.='<b> CIF/DNI:   </b> '.$dni.			'<br />';
	$txtmail_datosEmpresa.='<b> Direccion: </b> '.$direccion.	'<br />';
	$txtmail_datosEmpresa.='<b> Ciudad:    </b> '.$ciudad.		'<br />';
	$txtmail_datosEmpresa.='<b> C.postal:  </b> '.$cp.			'<br />';
	$txtmail_datosEmpresa.='<b> Provincia: </b> '.$provincia.	'<br />';
	$txtmail_datosEmpresa.='<b> Tlf fijo:  </b> '.$tlf.			'<br />';
	$txtmail_datosEmpresa.='<b> Tlf movil: </b> '.$tlf2.		'<br />';
	$txtmail_datosEmpresa.='<b> Fecha alta solicitud:     </b> '.$fechaSolicitudAlta.'<br />';
	$txtmail_datosEmpresa.='<b> Cuenta Bancaria:          </b> '.$datosBanco.'		<br />';
	$txtmail_datosEmpresa.='<b> Num Modulos Contratados:  </b> '.$numModulosContratados.'<br />';
//	$countNum=0;
	for($countNum=0; $countNum<=$numModulosContratados; $countNum++){		
		//$countNum++;	
		if($mod_array_type[$countNum]!=0){
			$txtmail_datosEmpresa.='</br> <b> **********  Modulo 0'.$countNum.':  **********  </br> ';
			$txtmail_datosEmpresa.='<b> countNum:  </b> '.$countNum.'<br />';
			$txtmail_datosEmpresa.='<b> ID:     </b> '.$mod_array_type[$countNum].' </br>';
			$txtmail_datosEmpresa.='<b> PASS:   </b> '.$mod_array_pass[$countNum].' </br> ';
			$txtmail_datosEmpresa.='<b> SIM:    </b> '.$mod_array_sim[$countNum].' </br>';
			$txtmail_datosEmpresa.='<b> IMEI:   </b> '.$mod_array_imei[$countNum].'<br />';
			$txtmail_datosEmpresa.='<b> Nombre: </b> '.$mod_array_nom[$countNum].'<br />'; 
		}
		
	}
	$txtmail_datosEmpresa.='<br/> <br/>';
	
	
	//Función para limpiar caracteres
	function limpiarCaracteres($string){
		return strtr($string,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
	}


	//Conectamos con la BD eude
	define('DB_USER2', "adminbd"); // db user
	define('DB_PASSWORD2', "evaristo2002++"); // db password (mention your db password here)
	define('DB_DATABASE_EUDE2',"eude");
	define('DB_SERVER2', "vigilantiabd1.cddh4gk18qev.eu-west-1.rds.amazonaws.com"); // db server
	$conexion=mysql_close(); //cerramos conexion para poder abrir con eude
	$db = mysql_connect(DB_SERVER2, DB_USER2, DB_PASSWORD2) or die(mysql_error());
	$db = mysql_select_db(DB_DATABASE_EUDE2) or die(mysql_error()) or die(mysql_error());
	//Si nos ponen una acento el md5 retornará error
	//$nombre_bd = substr($usuario,0,4)."_".substr(md5(limpiarCaracteres($empresa)),0,4);
	$query_selectIdUltimo = "SELECT id FROM eude.empresas order by id desc limit 1";
	$id_Ultimo = mysql_query($query_selectIdUltimo); //sacamos el ultimo id y le sumamos 1 que sera el nuevo
	$res = mysql_fetch_assoc($id_Ultimo);
	$newId = $res['id'] + 1;
	$nombreBD = $usuario.'_'.$newId;
	$db=mysql_close();
	$conexion = mysql_connect(DB_SERVER, DB_USER, DB_PASSWORD) or die(mysql_error());
	$prefixstream = substr($usuario,0,3);
	
	$sql="CREATE database $nombreBD";
	$createBd=mysql_query($sql,$conexion);
	
	//Comprobamos si la creacion de la new BD ha ido bien o no.
	if(!$createBd){
		$txt_createBDError.='Error al crear la base de datos <br />';
		$bbddok=0;
	}else{
        /*****************************************************************************************************
			Como se ha creado bien la BD ya podemos empezar a crear la estructura (tablas/vistas...)
		******************************************************************************************************/
	    
		$txt_createBD.= 'Base de datos '. $nombreBD . 'se ha creado correctamente <br/>';
        mysql_select_db($nombreBD,$conexion); //Abrimos conexión a la BD creada
		
		/********************************************************************************
			Creamos la tabla avisos
		********************************************************************************/
			$tablaAvisos="CREATE TABLE IF NOT EXISTS `avisos` (
							`pkId` int(11) NOT NULL AUTO_INCREMENT,
							`userId` int(11) NOT NULL,
							`oldZoneId` int(11) NOT NULL,
							`newZoneId` int(11) NOT NULL,
							`dateOfChange` datetime NOT NULL,
							`tipoAviso` int(10) DEFAULT NULL,
							`estado` int(11) NOT NULL,
							`time_ini` datetime NOT NULL,
							`time_end` datetime NOT NULL,
							`aviso` tinyint(4) NOT NULL,
							`cerrado` tinyint(4) NOT NULL,
							PRIMARY KEY (`pkId`)
						) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
			
			$crear_tablaAvisos = mysql_query($tablaAvisos,$conexion);
			
			//Comprobamos el proceso de creacion de la tabla
			if(!$crear_tablaAvisos){
				$bbddok=0;
				$txtMail_createTablaError.=' Error al crear la tabla Avisos <br/>';
			}else{
				$txtMail_createTabla.= 'La tabla Avisos se ha creado correctamente <br/>';
			}
		
		/********************************************************************************
			Creamos la tabla emails
		********************************************************************************/
			$tablaEmails="CREATE TABLE IF NOT EXISTS `emails` (
							`id` int(11) NOT NULL AUTO_INCREMENT,
							`email` varchar(50) NOT NULL,
							`fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
							PRIMARY KEY (`id`)
						) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
			
			$crear_tablaEmails = mysql_query($tablaEmails,$conexion);
			
			//Comprobamos el proceso de creacion de la tabla
			if(!$crear_tablaEmails){
				$bbddok=0;
				$txtMail_createTablaError.= 'Error al crear la tabla Emails <br/>';
			}else{
				$txtMail_createTabla.= 'La tabla Emails se ha creado correctamente. <br/>';
			}
		
		/********************************************************************************
			Creamos la tabla grabacion
		********************************************************************************/
			$tablaGrabacions="CREATE TABLE IF NOT EXISTS `grabacions` (
								`id` int(11) NOT NULL AUTO_INCREMENT,
								`userlogin` varchar(250) NOT NULL,
								`horaini` varchar(200) CHARACTER SET utf8 NOT NULL,
								`horafin` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
								`duracion` int(11) DEFAULT NULL,
								`archivo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
								`gpsini` decimal(10,0) DEFAULT NULL,
								`gpsfin` decimal(10,0) DEFAULT NULL,
								`play` int(11) DEFAULT NULL,
								`delet` int(11) DEFAULT NULL,
								`eliminado` int(11) NOT NULL DEFAULT '0',
								`datedelete` date DEFAULT NULL,
								PRIMARY KEY (`id`)
							) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
			
			$crear_tablaGrabacions = mysql_query($tablaGrabacions,$conexion);
			
			//Comprobamos el proceso de creacion de la tabla
			if(!$crear_tablaGrabacions){
				$bbddok=0;
				$txtMail_createTablaError.= 'Error al crear la tabla Grabacions  <br/>';
			}else{
				$txtMail_createTabla.= 'La tabla Grabacions se ha creado correctamente <br/>';
			}
		
		/********************************************************************************
			Creamos la tabla LlamadasEntrantes
		********************************************************************************/
			$tablaLlamadasEntrantes="CREATE TABLE IF NOT EXISTS `llamadasentrantes` (
										`id` int(11),
										`name` text,
										`surname` text,
										`userlogin` varchar(50),
										`fecha` datetime
									);";
			
			$crear_tablaLlamadasEntrantes = mysql_query($tablaLlamadasEntrantes,$conexion);
			
			//Comprobamos el proceso de creacion de la tabla
			if(!$crear_tablaLlamadasEntrantes){
				$bbddok=0;
				$txtMail_createTablaError.= ' Error al crear la tabla LlamadasEntrantes <br/>';
			}else{
				$txtMail_createTabla.= 'La tabla LlamadasEntrantes se ha creado correctamente <br/>';
			}
		
		
		/********************************************************************************
			Creamos la tabla LlamadasPerdidas
		********************************************************************************/
			$tablaLlamadasPerdidas="CREATE TABLE IF NOT EXISTS `llamadasperdidas` (
										`id` int(11),
										`name` text,
										`surname` text,
										`userlogin` varchar(50),
										`fecha` datetime
									);";
			
			$crear_tablaLlamadasPerdidas = mysql_query($tablaLlamadasPerdidas,$conexion);
			
			//Comprobamos el proceso de creacion de la tabla
			if(!$crear_tablaLlamadasPerdidas){
				$bbddok=0;
				$txtMail_createTablaError.= 'Error al crear la tabla LlamadasPerdidas <br/>';
			}else{
				$txtMail_createTabla.='La tabla LlamadasPerdidas se creo correctamente<br/>';
			}
		
		
		/********************************************************************************
			Creamos la tabla Logins
		********************************************************************************/
			$tablaLogins="CREATE TABLE IF NOT EXISTS `logins` (
										`id` int(11) NOT NULL AUTO_INCREMENT,
										`user` varchar(20) NOT NULL,
										`pass` varchar(20) NOT NULL,
										`rol` int(11) NOT NULL,
										`sesion` int(11) NOT NULL DEFAULT '0',
										PRIMARY KEY (`id`)
									) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
			
			$crear_tablaLogins = mysql_query($tablaLogins,$conexion);
			
			//Comprobamos el proceso de creacion de la tabla
			if(!$crear_tablaLogins){
				$bbddok=0;
				$txtMail_createTablaError.= 'Error al crear la tabla Logins <br/>';
			}else{
				$txtMail_createTabla.= 'La tabla Logins se creo correctamente <br/>';                
			}
		
		
		/********************************************************************************
			Creamos la tabla Pointsxzones
		********************************************************************************/			
			$tablaPointsxzones="CREATE TABLE IF NOT EXISTS `pointsxzones` (
									`Y` float NOT NULL,
									`X` float NOT NULL,
									`zoneId` int(11) NOT NULL
								) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
			
			$crear_tablaPointsxzones = mysql_query($tablaPointsxzones,$conexion);
			
			//Comprobamos el proceso de creacion de la tabla
			if(!$crear_tablaPointsxzones){
				$bbddok=0;
				$txtMail_createTablaError.= ' Error al crear la tabla Pointsxzones <br/>';
			}else{
				$txtMail_createTabla.= 'La tabla Pointsxzones se creo correctamente<br/>';
			}
			
			
		/********************************************************************************
			Creamos la tabla Posiciones
		********************************************************************************/
			$tablaPosiciones="CREATE TABLE IF NOT EXISTS `posiciones` (
									`id` int(11) NOT NULL AUTO_INCREMENT,
									`userId` int(11) NOT NULL,
									`latitude` decimal(10,6) NOT NULL,
									`longitude` decimal(10,6) NOT NULL,
									`altitude` float NOT NULL,
									`uploadTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
									`compass` decimal(10,2) NOT NULL DEFAULT '0.00',
									`accuracy` int(11) NOT NULL,
									`dateInitStream` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
									`speed` decimal(10,6) DEFAULT NULL,
									`accelX` decimal(10,2) DEFAULT NULL,
									`accelY` decimal(10,2) DEFAULT NULL,
									`accelZ` decimal(10,2) DEFAULT NULL,
									`travelModel` varchar(45) DEFAULT NULL COMMENT 'DRIVING --> Carretera\nWALKING --> Peaton (Default) \n\n',
									PRIMARY KEY (`id`,`userId`,`latitude`,`longitude`,`altitude`,`uploadTime`,`compass`,`accuracy`,`dateInitStream`),
									KEY `index_userid` (`userId`),
									KEY `uploadTime_index` (`uploadTime`)
								) ENGINE=InnoDB  DEFAULT CHARSET=armscii8 AUTO_INCREMENT=1 ;";
							
			$crear_tablaPosiciones = mysql_query($tablaPosiciones,$conexion);
			
			//Creamos el trigger actualizarUltimaPosicion		
			$triggerPosiciones="CREATE TRIGGER `actualizarUltimaPosicion`  AFTER INSERT ON	`posiciones` FOR EACH ROW 
								INSERT INTO 
									ultimaposicion 
								SET 
									userId=new.userId,
									latitude=new.latitude,
									longitude=new.longitude,
									altitude=new.altitude,
									uploadTime=new.uploadTime,
									compass=new.compass,
									accuracy=new.accuracy,
									acceIX=new.accelX,
									acceIY=new.accelY,
									acceIZ=new.accelZ 
								ON DUPLICATE KEY 
									update latitude = new.latitude,
									longitude = new.longitude,
									altitude = new.altitude,
									uploadTime = new.uploadTime,
									compass= new.compass,
									accuracy = new.accuracy,
									acceIX=new.accelX,
									acceIY=new.accelY,
									acceIZ=new.accelZ";
									
			//Comprobamos el proceso de creacion de la tabla
			if(!$crear_tablaPosiciones){
				$bbddok=0;
				$txtMail_createTablaError.= 'Error al crear la tabla Posiciones <br/>';
			}else{
				$txtMail_createTabla.= 'La tabla Posiciones se creo correctamente <br/>';
				$crear_triggerPosiciones = mysql_query($triggerPosiciones,$conexion);
				
				//Comprobamos el proceso de creacion del trigger, ya que si se ha creado la tabla
				if(!$crear_triggerPosiciones){
					$bbddok=0;
					$txtMail_createTablaError.= 'Error al crear el trigger actualizarUltimaPosicion <br/>';
				}else{
					$txtMail_createTabla.= 'El trigger actualizarUltimaPosicion se creo correctamente<br/>';
				}	
			}

		
		/********************************************************************************
			Creamos la tabla Registrations
		*******************************************************************************/
			$tablaRegistrations="CREATE TABLE IF NOT EXISTS `registrations` (
									`m_username` varchar(64) NOT NULL,
									`m_identity` varchar(64) DEFAULT NULL,
									`m_updatetime` datetime DEFAULT NULL,
									PRIMARY KEY (`m_username`)
								) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
			
			$crear_tablaRegistrations = mysql_query($tablaRegistrations,$conexion);
			
			//Comprobamos el proceso de creacion de la tabla
			if(!$crear_tablaRegistrations){
				$bbddok=0;
				$txtMail_createTablaError.= 'Error al crear la tabla Registrations <br/>';
			}else{  
				$txtMail_createTabla.= 'La tabla Registrations se creo correctamente<br/>';       
			}
		
		
		/********************************************************************************
			Creamos la tabla Sesiones
		********************************************************************************/
			$tablaSesiones="CREATE TABLE IF NOT EXISTS `sesiones` (
								`id` int(11) NOT NULL AUTO_INCREMENT,
								`idUser` int(11) NOT NULL,
								`ip` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
								`active` int(1) NOT NULL,
								`fechaEntrada` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
								`fechaUltimoAcceso` timestamp NULL DEFAULT NULL,
								PRIMARY KEY (`id`),
								KEY `idUser` (`idUser`)
							) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
			
			$crear_tablaSesiones = mysql_query($tablaSesiones,$conexion);
			
			//Comprobamos el proceso de creacion de la tabla
			if(!$crear_tablaSesiones){
				$bbddok=0;
				$txtMail_createTablaError.= ' Error al crear la tabla Sesiones <br/>';
			}else{
				$txtMail_createTabla.= 'La tabla Sesiones se creo correctamente<br/>';  
			}
		
		/********************************************************************************
			Creamos la tabla Streamservers
		********************************************************************************/
			$tablaStreamservers="CREATE TABLE IF NOT EXISTS `streamservers` (
								`id` int(11) NOT NULL AUTO_INCREMENT,
								`server` text NOT NULL,
								`company` text NOT NULL,
								`prefixStream` text,
								`idempresa` int(11) NOT NULL,
								`ipserver` text NOT NULL,
								`streamapp` text NOT NULL,
								`recordapp` text NOT NULL,
								PRIMARY KEY (`id`)
							) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
			
			$crear_tablaStreamservers = mysql_query($tablaStreamservers,$conexion);
			
			//Comprobamos el proceso de creacion de la tabla
			if(!$crear_tablaStreamservers){
				$bbddok=0;
				$txtMail_createTablaError.= 'Error al crear la tabla Streamservers<br/>';
			}else{
				$txtMail_createTabla.= 'La tabla Streamservers se creo correctamente<br/>';
			
				$datosstream="INSERT INTO `streamservers` (`id`, `server`, `company`, `prefixStream`, `idempresa`, `ipserver`, `streamapp`, `recordapp`) 
								VALUES (1, 'rtmp://ec2-79-125-75-85.eu-west-1.compute.amazonaws.com/liverecord', '".$usuario."', '".$prefixstream."', ".$newId.", '54.246.96.123', 'live', 'vod');
							";
				$consultastream=mysql_query($datosstream);
				
				//Comprobamos el proceso del insertar datos en la tabla
				if(!$consultastream){
					$bbddok=0;
					$txtMail_createTablaError.= 'Error al insertar los datos en la tabla Streamservers, actualmente la tabla estará vacía <br/>';
				}else{
					$txtMail_createTabla.= 'Se han insertado correctamente los datos en la tabla streamserver <br/>';
				}
			}
		
		/********************************************************************************
			Creamos la tabla tipoAvisos
		********************************************************************************/
			$tablaTipoAvisos="CREATE TABLE IF NOT EXISTS `tipoAvisos` (
							`id` int(10) NOT NULL AUTO_INCREMENT,
							`nom` varchar(60) NOT NULL,
							`descrip` varchar(255) DEFAULT NULL,
							PRIMARY KEY (`id`)
						) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
			
			$crear_tablaTipoAvisos = mysql_query($tablaTipoAvisos,$conexion);
			
			//Comprobamos el proceso de creacion de la tabla
			if(!$crear_tablaTipoAvisos){
				$bbddok=0;
				$txtMail_createTablaError.= 'Error al crear la tabla tipoAvisos  <br/>';
			}else{
				$txtMail_createTabla.= 'La tabla tipoAvisos se creo correctamente<br/>';
					
				// Insertamos datos en la tabla
				$datos="INSERT INTO `tipoAvisos` (`id`, `nom`, `descrip`) VALUES (1, 'Salida zona', 'El usuario ha salido de la zona'),
																				 (2, 'Llamar', 'Petición de chat desde el movil'),
																				 (3, 'P&aacute;nico', 'Petición de assistencia desde el movil');";
				$consulta=mysql_query($datos,$conexion);
				
				//Comprobamos el proceso del insertar datos en la tabla
				if(!$consulta){
					$bbddok=0;
					$txtMail_createTablaError.= 'Error al insertar los datos en la tabla tipoAvisos, actualmente la tabla estará vacía  <br/>';
				}else{
					$txtMail_createTabla.= 'Se han insertado correctamente los datos en la tabla tipoAvisos<br/>';
				}
			}
			
		
		/********************************************************************************
			Creamos la tabla users
		********************************************************************************/
			$tablaUsers="CREATE TABLE IF NOT EXISTS `users` (
								  `id` int(11) NOT NULL AUTO_INCREMENT,
								  `userlogin` varchar(50) NOT NULL,
								  `name` text NOT NULL,
								  `surname` text NOT NULL,
								  `company` text NOT NULL,
								  `zone` int(11) NOT NULL,
								  `charge` text NOT NULL,
								  `online` int(11) DEFAULT NULL,
								  `onlinetime` date DEFAULT NULL,
								  `email` varchar(50) NOT NULL,
								  `password` text NOT NULL,
								  `admin` int(11) NOT NULL,
								  `vigilantia` int(11) NOT NULL,
								  `activo` int(1) NOT NULL DEFAULT '1' COMMENT 'Si se cierra una pagina cambiar a 0 para que no pueda emitir.',
								  `idTipoCamara` int(11) NOT NULL DEFAULT '0',
								  `sim` varchar(45) NOT NULL,
								  `imei` varchar(45) DEFAULT NULL,
								  PRIMARY KEY (`id`),
								  UNIQUE KEY `userlogin` (`userlogin`)
							) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
			
			$crear_tablaUsers = mysql_query($tablaUsers,$conexion);
			
			//Comprobamos el proceso de creacion de la tabla
			if(!$crear_tablaUsers){
				$bbddok=0;
				$txtMail_createTablaError.= 'Error al crear la tabla users  <br/>';
			}else{
				$txtMail_createTabla.= 'La tabla users se creo correctamente<br/>';
			  
				//La tabla se ha creado, vamos a insertar los datos
				//Primeo comprobamos el numero de modulos que tenemos que dar de alta, y aprovechamos la comprobacion para crear tambien los registros de la tabla ultimaposicion
				
				for($i=1; $i<=$numModulosContratados; $i++){
							
					
					
					//	$datosUltipaPosicion01 = "(1,0,0,0,CURRENT_TIMESTAMP,0.00,0,'0000-00-00 00:00:00',0,NULL,NULL,NULL,NULL,NULL,'WALKING')";
					
					
					//Definimos las variables que insertamos en la tabla
					$userlogin_tableUser = $usuario.'0'.$i;
					$name_tableUser = 'user0'.$i;
					$surname_tableUser = 'surname0'.$i;
					$company_tableUser = $usuario.'0'.$i;
					$charge_tableUser = 'cargo'.$i;
					$email_tableUser = 'email'.$i;
					
					//Monto la consulta SQL
					$datos="INSERT INTO `users` (`id`,`userlogin`,`name`,`surname`,`company`,`zone`,`charge`,`online`,`onlinetime`,`email`,`password`,`admin`,`vigilantia`,`activo`,`idTipoCamara`,`sim`,`imei`) 
							VALUES ( 
									".$i.",
									'".$userlogin_tableUser."',
									'".$userlogin_tableUser."',
									'".$surname_tableUser."',									
									'".$company_tableUser."',
									0,
									'".$charge_tableUser."',
									0, 
									NULL ,
									'".$email_tableUser."',
									'".$mod_array_pass[$i]."',
									0,
									0,
									1,
									".$mod_array_type[$i].",
									'".$mod_array_sim[$i]."',
									'".$mod_array_imei[$i]."')
								";
								
								
					$consulta=mysql_query($datos,$conexion);
					
					//Comprobamos el proceso de insertar los datos en la tabla
					if(!$consulta){
						$bbddok=0;
						$txtMail_createTablaError.= 'Error al insertar los datos en la tabla users, actualmente la tabla estará vacía  ('.$i.') <br/>';
					}else{
						$txtMail_createTabla.= 'Los datos se insertaron correctamente en la tabla users ('.$i.') <br/>';
					}
				
				}//fin for
				
				
				
			}//fin else
		
		
		/********************************************************************************
			Creamos la tabla ultimaposicion
		********************************************************************************/
			$tablaUltimaposicion="CREATE TABLE IF NOT EXISTS `ultimaposicion` (
									  `userId` int(11) NOT NULL DEFAULT '0',
									  `latitude` decimal(10,6) DEFAULT NULL,
									  `longitude` decimal(10,6) DEFAULT NULL,
									  `altitude` float DEFAULT NULL,
									  `uploadTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
									  `compass` decimal(10,2) DEFAULT '0.00',
									  `accuracy` int(11) DEFAULT NULL,
									  `dateInitStream` datetime DEFAULT '0000-00-00 00:00:00',
									  `online` tinyint(1) NOT NULL DEFAULT '0',
									  `onlinetime` datetime DEFAULT NULL,
									  `userlogin` int(11) DEFAULT NULL,
									  `acceIX` decimal(10,2) DEFAULT NULL,
									  `acceIY` decimal(10,2) DEFAULT NULL,
									  `acceIZ` decimal(10,2) DEFAULT NULL,
									  `travelModel` varchar(45) DEFAULT NULL COMMENT 'DRIVING --> Carretera\nWALKING --> Peaton (Default) \n\n',
									  PRIMARY KEY (`userId`),
									  KEY `uploadTime_index` (`uploadTime`)
								) ENGINE=InnoDB DEFAULT CHARSET=armscii8;";
			
			$crear_tablaUltimaposicion = mysql_query($tablaUltimaposicion,$conexion);
			
			//Comprobamos el proceso de creacion de la tabla
			if(!$crear_tablaUltimaposicion){
				$bbddok=0;
				$txtMail_createTablaError.= 'Error al crear la tabla ultimaposicion <br/>';
			}else{
				$txtMail_createTabla.= 'La tabla ultimaposicion se ha creado correctamente<br/>';
				
				//Ya podemos insertar los datos en la tabla  ultimaposicion 
				
				for($i=1; $i<=$numModulosContratados; $i++){
					$insertarDatosUltimaPosicion="INSERT INTO ultimaposicion (`userId`, `latitude`,`longitude`,`altitude`,`uploadTime`,`compass`,`accuracy`,`dateInitStream`,`online`,`onlinetime`,`userlogin`,`acceIX`,`acceIY`,`acceIZ`,`travelModel`) 
											VALUES (".$i.",0,0,0,CURRENT_TIMESTAMP,0.00,0,'0000-00-00 00:00:00',0,NULL,NULL,NULL,NULL,NULL,'WALKING')";

					$crear_insertarDatosUltimaPosicion=mysql_query($insertarDatosUltimaPosicion,$conexion);
			
					//Comprobamos el proceso de insertar los datos en la tabla
					if(!$crear_insertarDatosUltimaPosicion){
						$bbddok=0;
						$txtMail_createTablaError.= 'Error al insertar los datos en la tabla ultimaposicion, actualmente la tabla estará vacía ('.$i.') <br/>';
					}else{
						$txtMail_createTabla.= 'Los datos se insertaron correctamente en la tabla ultimaposicion ('.$i.') <br/>';
					}
				}//fin for
				
				
				
			}//fin else
			
		
		/********************************************************************************
			Creamos la tabla usersxzones
		********************************************************************************/
			$tablaUsersxzones="CREATE TABLE IF NOT EXISTS `usersxzones` (
								  `userId` int(11) NOT NULL,
								  `zoneId` int(11) NOT NULL
								) ENGINE=InnoDB DEFAULT CHARSET=utf8; ";
			
			$crear_tablaUsersxzones = mysql_query($tablaUsersxzones,$conexion);
			
			//Comprobamos el proceso de creacion de la tabla
			if(!$crear_tablaUsersxzones){
				$bbddok=0;
				$txtMail_createTablaError.= 'Error al crear la tabla usersxzones  <br/>';
			}else{
				$txtMail_createTabla.= 'La tabla usersxzones se creo correctamente<br/>';
			}
		
		
		/********************************************************************************
			Creamos la vista v_avisos
		*******************************************************************************/
			$vistaAvisos="CREATE TABLE IF NOT EXISTS `v_avisos` (
								`id` int(11)
								,`userId` int(11)
								,`name` text
								,`surname` text
								,`zonaini` varchar(100)
								,`zonafin` varchar(100)
								,`tipoavisos` varchar(60)
								,`fechaentera` datetime
								,`dateini` varchar(510)
								,`horaini` time
							); ";
			
			$crear_vistaAvisos = mysql_query($vistaAvisos,$conexion);
			
			//Comprobamos el proceso de creacion de la vista
			if(!$crear_vistaAvisos){
				$bbddok=0;
				$txtMail_createTablaError.= 'Error al crear la vista v_avisos <br/>';
			}else{
				$txtMail_createTabla.= 'La vista v_avisos se creo correctamente<br/>';
			}
		
		
		/********************************************************************************
			Creamos la vista v_grabacions
		********************************************************************************/
			$vistaGrabacions="CREATE TABLE IF NOT EXISTS `v_grabacions` (
								`id` int(11)
								,`userlogin` varchar(250)
								,`name` text
								,`surname` text
								,`dateini` varchar(510)
								,`horaini` time
								,`datefin` date
								,`horafin` time
								,`duracion` time
								,`archivo` varchar(255)
								,`dataordenar` datetime
								,`play` int(11)
								,`delet` int(11)
							); ";
			
			$crear_vistaGrabacions = mysql_query($vistaGrabacions,$conexion);
			
			//Comprobamos el proceso de creacion de la vista
			if(!$crear_vistaGrabacions){
				$bbddok=0;
				$txtMail_createTablaError.= 'Error al crear la vista v_grabacions  <br/>';
			}else{
				$txtMail_createTabla.= 'La vista v_grabacions se ha creado correctamente<br/>';
			}
		
		/********************************************************************************
			Creamos la tabla Wowzalog
		********************************************************************************/
			$tablaWowzalog="CREATE TABLE IF NOT EXISTS `Wowzalog` (
								  `logid` int(10) unsigned NOT NULL AUTO_INCREMENT,
								  `date` varchar(100) DEFAULT NULL,
								  `time` varchar(100) DEFAULT NULL,
								  `tz` varchar(100) DEFAULT NULL,
								  `xevent` varchar(20) DEFAULT NULL,
								  `xcategory` varchar(20) DEFAULT NULL,
								  `xseverity` varchar(100) DEFAULT NULL,
								  `xstatus` varchar(100) DEFAULT NULL,
								  `xctx` varchar(100) DEFAULT NULL,
								  `xcomment` varchar(255) DEFAULT NULL,
								  `xvhost` varchar(100) DEFAULT NULL,
								  `xapp` varchar(100) DEFAULT NULL,
								  `xappinst` varchar(100) DEFAULT NULL,
								  `xduration` varchar(100) DEFAULT NULL,
								  `sip` varchar(100) DEFAULT NULL,
								  `sport` varchar(100) DEFAULT NULL,
								  `suri` varchar(255) DEFAULT NULL,
								  `cip` varchar(100) DEFAULT NULL,
								  `cproto` varchar(100) DEFAULT NULL,
								  `creferrer` varchar(255) DEFAULT NULL,
								  `cuseragent` varchar(100) DEFAULT NULL,
								  `cclientid` varchar(25) DEFAULT NULL,
								  `csbytes` varchar(20) DEFAULT NULL,
								  `scbytes` varchar(20) DEFAULT NULL,
								  `xstreamid` varchar(20) DEFAULT NULL,
								  `xspos` varchar(20) DEFAULT NULL,
								  `csstreambytes` varchar(20) DEFAULT NULL,
								  `scstreambytes` varchar(20) DEFAULT NULL,
								  `xsname` varchar(100) DEFAULT NULL,
								  `xsnamequery` varchar(100) DEFAULT NULL,
								  `xfilename` varchar(100) DEFAULT NULL,
								  `xfileext` varchar(100) DEFAULT NULL,
								  `xfilesize` varchar(100) DEFAULT NULL,
								  `xfilelength` varchar(100) DEFAULT NULL,
								  `xsuri` varchar(255) DEFAULT NULL,
								  `xsuristem` varchar(255) DEFAULT NULL,
								  `xsuriquery` varchar(255) DEFAULT NULL,
								  `csuristem` varchar(255) DEFAULT NULL,
								  `csuriquery` varchar(255) DEFAULT NULL,
								  PRIMARY KEY (`logid`)
							) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1; ";
			
			$crear_tablaWowzalog = mysql_query($tablaWowzalog,$conexion);
			
			//Comprobamos el proceso de creacion de la tabla
			if(!$crear_tablaWowzalog){
				$bbddok=0;
				$txtMail_createTablaError.= 'Error al crear la tabla Wowzalog<br/>';
			}else{
				$txtMail_createTabla.= 'La tabla Wowzalog se ha creado correctamente<br/>';                
			}
		
		/********************************************************************************
			Creamos la tabla zones
		********************************************************************************/
			$tablaZones="CREATE TABLE IF NOT EXISTS `zones` (
							  `pkID` int(11) NOT NULL AUTO_INCREMENT,
							  `name` varchar(100) NOT NULL,
							  `color` varchar(7) NOT NULL,
							  `center` varchar(100) NOT NULL,
							  `zoom` int(11) NOT NULL,
							  PRIMARY KEY (`pkID`)
						) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ;";
			
			$crear_tablaZones = mysql_query($tablaZones,$conexion);
			
			//Comprobamos el proceso de creacion de la tabla
			if(!$crear_tablaZones){
				$bbddok=0;
				$txtMail_createTablaError.= ' Error al crear la tabla zones  <br/>';
			}else{
				$txtMail_createTabla.= 'La tabla zones se ha creado correctamente<br/>';					 
				
				//La tabla se ha creado, ahora insertaremos los datos
				$datos="INSERT INTO `zones` (`pkID`, `name`, `color`, `center`, `zoom`) VALUES
											(0, 'No definida', '#ffffff', '(0, 0)', 13);";
				
				$consulta=mysql_query($datos,$conexion);
				
				//Comprobamos el proceso de insertar los datos en la tabla
				if(!$consulta){
					$txtMail_createTablaError.= 'Error al insertar los datos en la tabla zones, actualmente la tabla estará vacía  <br/>';
				}else{
					$txtMail_createTabla.= 'Los datos se insertaron correctamente en la tabla zones<br/>';
				}
					
				$datos="UPDATE zones SET pkID=0 WHERE pkID=1";
				$consulta=mysql_query($datos,$conexion);
				//Comprobamos el proceso de update (modificacion) los datos en la tabla
				if(!$consulta){
					$txtMail_createTablaError.= 'Error al modificar los datos en la tabla zones <br/>';
				}else{
					$txtMail_createTabla.= 'Los datos se actualizaron correctamente en la tabla zones<br/>';
				}
			}
		
		/********************************************************************************
			Creamos la select para la vista llamadasentrantes
		********************************************************************************/
			$delete=mysql_query("DROP TABLE IF EXISTS `llamadasentrantes`;",$conexion);
			//Comprobamos que se ha eliminado la tabla en caso de existir
			if(!$delete){
				$bbddok=0;
				$txtMail_createTablaError.= ' Error al eliminar la tabla llamadasentratnes  <br/>';
			}else{		
				$txtMail_createTabla.= 'La tabla llamadasentrantes se borro correctamente<br/>';
			}
			
			$vistaELlamadasentrantes="CREATE or replace ALGORITHM=UNDEFINED DEFINER=`adminbd`@`%` SQL SECURITY DEFINER VIEW `llamadasentrantes` AS select `avisos`.`userId` AS `id`,`users`.`name` AS `name`,`users`.`surname` AS `surname`,`users`.`userlogin` AS `userlogin`,`avisos`.`time_ini` AS `fecha` from (`avisos` left join `users` on((`users`.`id` = `avisos`.`userId`))) where ((`avisos`.`tipoAviso` = 2) and (`avisos`.`aviso` = 0) and (`avisos`.`time_ini` >= (now() + interval -(1) minute))); ";
			$crear_vistaELlamadasentrantes = mysql_query($vistaELlamadasentrantes,$conexion);
			
			//Comprobamos el proceso de creacion de la vista
			if(!$crear_vistaELlamadasentrantes){
				$bbddok=0;
				$txtMail_createTablaError.= 'Error al crear la select para la vista llamadasentrantes <br/>';
			}else{
				$txtMail_createTabla.= 'La select para la vista llamadasentrantes se creo correctamente<br/>';
			}
		
		/********************************************************************************
			Creamos la select para la vista llamadasperdidas
		********************************************************************************/
			$delete=mysql_query("DROP TABLE IF EXISTS `llamadasperdidas`;",$conexion);
			//Comprobamos que se ha eliminado la tabla en caso de existir
			if(!$delete){
				$bbddok=0;
				$txtMail_createTablaError.= 'Error al eliminar la tabla llamadasperdidas <br/>';
			}else{
				$txtMail_createTabla.= 'La tabla llamadasperdidas se borro correctamente<br/>';
			}

			$vistaEllamadasperdidas="CREATE or replace ALGORITHM=UNDEFINED DEFINER=`adminbd`@`%` SQL SECURITY DEFINER VIEW `llamadasperdidas` AS select `avisos`.`userId` AS `id`,`users`.`name` AS `name`,`users`.`surname` AS `surname`,`users`.`userlogin` AS `userlogin`,`avisos`.`time_ini` AS `fecha` from (`avisos` left join `users` on((`users`.`id` = `avisos`.`userId`))) where ((`avisos`.`tipoAviso` = 2) and (`avisos`.`aviso` = 0) and (`avisos`.`time_ini` < (now() + interval -(1) minute)));";
			$crear_vistaEllamadasperdidas = mysql_query($vistaEllamadasperdidas,$conexion);
			
			//Comprobamos el proceso de creacion de la vista
			if(!$crear_vistaEllamadasperdidas){
				$bbddok=0;
				$txtMail_createTablaError.= 'Error al crear la select para la vista llamadasperdidas <br/>';
			}else{
				$txtMail_createTabla.= 'La select para la vista llamadasperdidas se creo correctamente<br/>';
			}
		
		/********************************************************************************
			Creamos la select para la vista v_avisos
		********************************************************************************/
			$delete=mysql_query("DROP TABLE IF EXISTS `v_avisos`;",$conexion);
			//Comprobamos que se ha eliminado la tabla en caso de existir
			if(!$delete){
				$bbddok=0;
				$txtMail_createTablaError.= 'Error al eliminar la tabla v_avisos <br/>';
			}else{
				$txtMail_createTabla.= 'La tabla v_avisos se borro correctamente<br/>';
			}
			
			$vistaEv_avisos="CREATE or replace ALGORITHM=UNDEFINED DEFINER=`adminbd`@`%` SQL SECURITY DEFINER VIEW `v_avisos` AS select `avisos`.`pkId` AS `id`,`avisos`.`userId` AS `userId`,`users`.`name` AS `name`,`users`.`surname` AS `surname`,`z`.`name` AS `zonaini`,`z2`.`name` AS `zonafin`,`tipoAvisos`.`nom` AS `tipoavisos`,`avisos`.`time_ini` AS `fechaentera`,date_format(cast(convert_tz(`avisos`.`time_ini`,'+00:00','+1:00') as date),get_format(DATE, 'EUR')) AS `dateini`,cast(convert_tz(`avisos`.`time_ini`,'+00:00','+1:00') as time) AS `horaini` from ((((`avisos` join `users`) join `zones` `z`) join `zones` `z2`) join `tipoAvisos`) where ((`avisos`.`userId` = `users`.`id`) and (`users`.`vigilantia` = 0) and (`z`.`pkID` = `avisos`.`oldZoneId`) and (`z2`.`pkID` = `avisos`.`newZoneId`) and (`avisos`.`tipoAviso` = `tipoAvisos`.`id`));";
			$crear_vistaEv_avisos = mysql_query($vistaEv_avisos,$conexion);
			
			//Comprobamos el proceso de creacion de la vista
			if(!$crear_vistaEv_avisos){
				$bbddok=0;
				$txtMail_createTablaError.= 'Error al crear la select para la vista v_avisos <br/>';
			}else{
				$txtMail_createTabla.= 'La select para la vista v_avisos se creo correctamente<br/>';
			}		
		
		/********************************************************************************		
			Creamos la select para la vista v_grabacions
		********************************************************************************/
			$delete=mysql_query("DROP TABLE IF EXISTS `v_grabacions`;",$conexion);
			//Comprobamos que se ha eliminado la tabla en caso de existir
			if(!$delete){
				$bbddok=0;
				$txtMail_createTablaError.= 'Error al eliminar la tabla v_grabacions <br/>';
			}else{
				$txtMail_createTabla.= 'La tabla v_grabacions se borro correctamente<br/>';
			}
			
			$vistaEv_grabacions="CREATE or replace ALGORITHM=UNDEFINED DEFINER=`adminbd`@`%` SQL SECURITY DEFINER VIEW `v_grabacions` AS select `grabacions`.`id` AS `id`,`grabacions`.`userlogin` AS `userlogin`,`users`.`name` AS `name`,`users`.`surname` AS `surname`,date_format(cast(convert_tz(`grabacions`.`horaini`,'+00:00','+2:00') as date),get_format(DATE, 'EUR')) AS `dateini`,cast(convert_tz(`grabacions`.`horaini`,'+00:00','+2:00') as time) AS `horaini`,cast(convert_tz(`grabacions`.`horafin`,'+00:00','+2:00') as date) AS `datefin`,cast(convert_tz(`grabacions`.`horafin`,'+00:00','+2:00') as time) AS `horafin`,sec_to_time(`grabacions`.`duracion`) AS `duracion`,`grabacions`.`archivo` AS `archivo`,convert_tz(`grabacions`.`horaini`,'+00:00','+2:00') AS `dataordenar`,`grabacions`.`play` AS `play`,`grabacions`.`delet` AS `delet` from (`grabacions` join `users`) where ((`grabacions`.`userlogin` = `users`.`userlogin`) and (`grabacions`.`eliminado` = 0)); ";
			$crear_vistaEv_grabacions = mysql_query($vistaEv_grabacions,$conexion);
			
			//Comprobamos el proceso de creacion de la vista
			if(!$crear_vistaEv_grabacions){
				$bbddok=0;
				$txtMail_createTablaError.= 'Error al crear la select para la vista v_grabacions <br/>';
			}else{
				$txtMail_createTabla.= 'La select para la vista v_grabacions se creo correctamente<br/>';
			}
			
	}//fin else, creacion de la BD

	
	
	/********************************************************************************
		Ahora toca meter los datos del formulario en la bd eude
		Cerramos conexion con la nueva BD.
		Abrimos conexion con la BD Eude.
	********************************************************************************/
	$conexion=mysql_close(); 
	$db = mysql_connect(DB_SERVER2, DB_USER2, DB_PASSWORD2) or die(mysql_error());
	$db = mysql_select_db(DB_DATABASE_EUDE2) or die(mysql_error()) or die(mysql_error());
	
	//Definimos resto datos que hay que insertar en la tabla eude.empresas
	$idPais=12; //Por defecto ponemos España
	
	$subdomini = $usuario;
	$domini = "http://".$subdomini.".controlStream.tv";
	$connapp='jdbc:mysql://vigilantiabd1.cddh4gk18qev.eu-west-1.rds.amazonaws.com:3306/'.$nombreBD;

	$query_insert="INSERT INTO `eude`.`empresas` 
				(`id`, `nombre`,`cif`,`direccion`,`cp`,`poblacion`,`provincia`,`idPais`,`email`,`telefono1`,`telefono2`,`cta`,`activo`,`bbdd`,
				`prefixstream`,`server`,`streamapp`,`domini`,`conn`,`aplicacion`,`num_licencias`,`subdomini`,`numModulosContratados`,`fechaSolicitudAlta`, `razonSocial`, `pass`) 
			VALUES(
				'$newId ',
				'$usuario',
				'$dni',
				'$direccion',
				'$cp',
				'$ciudad',
				'$provincia',
				'$idPais',
				'$email',
				'$tlf',
				'$tlf2',
				'$datosBanco',
				 1,
				'$nombreBD',
				'$prefixstream',
				'rtmp://54.246.96.123',
				'live',
				'$domini',
				'$connapp',
				 1,
				 1,
				'$subdomini',
				'$numModulosContratados',
				'$fechaSolicitudAlta',
				'$empresa',
				'$passEmpresa'
				);";

		 
	$query_insertcon=mysql_query($query_insert);
	
	//Comprobamos si se han insertado bien los datos en la tabla eude.empresas
	if(!$query_insertcon){
		$bbddok=0;
		$txtMail_createTablaError.= 'Error al insertar nueva empresa en EUDE<br/>';
	}else{
		$txtMail_createTabla.= 'El insert nueva empresa en EUDE correctament<br/>';
	}

	/********************************************************************************
		Ahora montamos email para el administrador/es y lo enviamos.
	********************************************************************************/	
	if($bbddok){ //Caso que ha ido todo bien.
		$txtProceso ='OK';
		$txtmail01= '<h1> Se ha creado correctamente la nueva BD '. $nombreBD .'</h1>';
		$txtmail_final = $txtmail01 ." <br/> ". $txtmail_datosEmpresa ;
	}else{ //Caso en el que ha fallado algo.
		$txtProceso ='ERROR';
		$txtmail01.= '<h1> Se ha producido algún error, al crear la nueva  BD '. $nombreBD .'</h1>';
		$txtmail_final = $txtmail01 ." <br/>". $txtMail_createTablaError ." <br/><br/><br/> ". $txtmail_datosEmpresa ;
	}

	$response['success']=$bbddok;

	//Aquí enviamos el email al usuario que ha realizado la compra/alta del producto
	$subject = "[".$txtProceso."] Nueva alta ".$empresa;
	$from = "admin@imks.tv";
	$host = "smtp.mandrillapp.com";
	$username = 'admin@imks.com';
	$password = 'cDt9L1xXFCUpJqm570fXnA';
	$to='marisa@imks.tv';
	$body = $txtmail_final; //Cuerpo del email
	
	//Sacamos un listado a todos los administradores que le queremos enviar el email 
	$query_listado = "SELECT mail FROM backoffice";
	$listado = mysql_query($query_listado) or die(mysql_error());
	
	//Cabecera del email
	$headers = array (
		'From' => $from,
		'To' => $to,
		'Subject' => $subject,
		'MIME-Version' => "1.0",
		'Content-type' => "text/html; charset=utf-8\r\n\r\n"
	);

	try {
		$mandrill = new Mandrill('tssBXB12pEnZKlw2KeF_5A');
		
		while ($row_listado = mysql_fetch_array($listado)){
			//Cuerpo del email
			$message = array(
				'subject' => $subject,
				'from_email' => 'admin@imks.tv',
				'html' => $body,
				'to' => array(array('email' => $row_listado['mail']))
			);
		
			//print_r($mandrill->messages->send($message));
			$mandrill->messages->send($message); //Enviamos email
		}//fin while


	}catch(Mandrill_Error $e) { //En caso de error nos mostrará un mensaje
		echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
		throw $e;
	}  
  
	/********************************************************************************
		Ahora montamos email para la empresa/cliente
	********************************************************************************/	
	if($bbddok){	
		$txtmailCliente = "";
		$txtmailCliente .= 'Enhorabuena, '.$empresa.' ya forma parte de controlStream.tv.';
		$txtmailCliente .= '<br/><br/>';
		$txtmailCliente .= 'Para poder acceder a la plataforma pulse en el link (si  no ve el link copie y pegue en su navegador la dirección) http://'.$usuario.'.controlstream.tv ';
		$txtmailCliente .= '<br/><br/>';
		$txtmailCliente .= 'Los datos para poder acceder son:';
		$txtmailCliente .= '<br/>';
		$txtmailCliente .= '<b>Usuario: </b> ' .$usuario;
		$txtmailCliente .= '<br/>';
		$txtmailCliente .= '<b>Password: </b> ' .$passEmpresa;
		$txtmailCliente .= '<br/><br/>';
		$txtmailCliente .= 'También indicarle que en la dirección http://controlstream.tv/manualUso.pdf  puede descargarse un manual de uso donde le explicamos como funciona la plataforma, y por supuesto cualquier duda que tenga puede ponerse en contacto con nosotros vía email gestion@controlstream.tv';
		$txtmailCliente .= '<br/><br/>';
		$txtmailCliente .= 'Gracias por confiar en nosotros.';
		$txtmailCliente .= '<br/>';
		$txtmailCliente .= 'Un saludo cordial saludo del equipo controlstream.tv';
		$txtmailCliente .= '<br/><br/><br/><br/>';
	}
	
	$to=$email;
	$subject = "Bienvenido a controlStream.tv";
	$headers = array (
		'From' => $from,
		'To' => $to,
		'Subject' => $subject,
		'MIME-Version' => "1.0",
		'Content-type' => "text/html; charset=utf-8\r\n\r\n"
	);
  
  
	try{
		$mandrill = new Mandrill('tssBXB12pEnZKlw2KeF_5A');
		$message = array(
			'subject' => $subject,
			'from_email' => 'admin@imks.tv',
			'html' => $txtmailCliente,
			'to' => array(array('email' => $to))
		);
		$mandrill->messages->send($message);
		
	} catch(Mandrill_Error $e) { // Mandrill errors are thrown as exceptions
		echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
		throw $e;
	}    
	echo json_encode($response);
?> 