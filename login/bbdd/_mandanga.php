<?php
	//Definimos variables
	require_once("db_config.php");
	require 'Mandrill.php';
	$conexion = mysql_connect(DB_SERVER, DB_USER, DB_PASSWORD) or die(mysql_error());
	$bbddok=1; //La bbdd se ha construido correctamente
	$mailtecnico="";
 
	$response = array();

	if(isset($_POST['empresa'])){
		$empresa = $_POST['empresa'];//'Pavos St Pulpis';
		$usuario = $_POST['user'];//'tica';
		$password = $_POST['pass'];//'tica01';
		$email = $_POST['email'];//'tica01';
		$dni = $_POST['dni'];//'tica01';
		$direccion = $_POST['direccion'];//'tica01';
		$ciudad = $_POST['ciudad'];//'tica01';
		$cp = $_POST['cp'];
		$provincia = $_POST['provincia'];
		$tlf = $_POST['tlf'];
		$tlf2 = $_POST['tlf2'];
	}else{
		$empresa = $_GET['empresa'];//'Pavos St Pulpis';
		$usuario = $_GET['user'];//'tica';
		$password = $_GET['pass'];//'tica01';
		$email = $_GET['email'];//'tica01';
		$dni = $_GET['dni'];//'tica01';
		$direccion = $_GET['direccion'];//'tica01';
		$ciudad = $_GET['ciudad'];//'tica01';
		$cp = $_GET['cp'];
		$provincia = $_GET['provincia'];
		$tlf = $_GET['tlf'];
		$tlf2 = $_GET['tlf2'];
	}
		
	$usuario=str_replace(array('(','\'','´',',','.','{','}','+','´','*','¨','[',']','%','&','/','%','\$','#','"','!','?','¡',')',',',';',':','^','`'),'',$usuario);
	$usuario=str_replace(' ', '_', $usuario);

	$mailtecnico.='Empresa:'.$empresa.'<br />';
	$mailtecnico.='Usuario:'.$usuario.'<br />';
	$mailtecnico.='Email:'.$email.'<br />';
	$mailtecnico.='DNI:'.$dni.'<br />';
	$mailtecnico.='Direccion:'.$direccion.'<br />';
	$mailtecnico.='Ciudad:'.$ciudad.'<br />';
	$mailtecnico.='Codigo postal:'.$cp.'<br />';
	$mailtecnico.='Provincia:'.$provincia.'<br />';
	$mailtecnico.='Tlf:'.$tlf.'<br />';
	$mailtecnico.='Tlf2:'.$tlf2.'<br />';

	//Función para limpiar caracteres
	function limpiarCaracteres($string){
		return strtr($string,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
	}

	//Si nos ponen una acento el md5 retornará error
	$nombre_bd = substr($usuario,0,4)."_".substr(md5(limpiarCaracteres($empresa)),0,4);


	//$conexion = mysql_connect($host,$user,$pass);
	$sql="CREATE database $nombre_bd";
	$createBd=mysql_query($sql,$conexion);
	//die($nombre_bd);

	if(!$createBd){
		$mailtecnico.= 'Error al crear la base de datos<br />';
		$bbddok=0;
	}else{
        $mailtecnico.= 'Base de datos creada correctamente<br/>';
      
        //Ya tenemos BD y podemos empezar a crear la estructura
       
	    //Abrimos conexión a la BD creada
        mysql_select_db($nombre_bd,$conexion);
		
		/********************************************************************************
			Creamos la tabla avisos
		********************************************************************************/
		$tablaAvisos="CREATE TABLE IF NOT EXISTS `avisos` (
						`pkId` int(11) NOT NULL AUTO_INCREMENT,
						`userId` int(11) NOT NULL,
						`oldZoneId` int(11) NOT NULL,
						`newZoneId` int(11) NOT NULL,
						`dateOfChange` datetime NOT NULL,
						`tipoAviso` int(10) DEFAULT NULL,
						`estado` int(11) NOT NULL,
						`time_ini` datetime NOT NULL,
						`time_end` datetime NOT NULL,
						`aviso` tinyint(4) NOT NULL,
						`cerrado` tinyint(4) NOT NULL,
						PRIMARY KEY (`pkId`)
					) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
		
		$crear_tablaAvisos = mysql_query($tablaAvisos,$conexion);
        
		if(!$crear_tablaAvisos){
			$bbddok=0;
			$mailtecnico.='Error al crear la tabla Avisos<br/>';
		}else{
			$mailtecnico.= 'La tabla Avisos se creo correctamente<br/>';
		}
		
		/********************************************************************************
			Creamos la tabla emails
		********************************************************************************/
		$tablaEmails="CREATE TABLE IF NOT EXISTS `emails` (
						`id` int(11) NOT NULL AUTO_INCREMENT,
						`email` varchar(50) NOT NULL,
						`fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
						PRIMARY KEY (`id`)
					) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
		
		$crear_tablaEmails = mysql_query($tablaEmails,$conexion);
        
		if(!$crear_tablaEmails){
			$bbddok=0;
			$mailtecnico.= 'Error al crear la tabla Emails<br/>';
		}else{
			$mailtecnico.= 'La tabla Emails se creo correctamente<br/>';
		}
		
		/********************************************************************************
			Creamos la tabla grabacion
		********************************************************************************/
		$tablaGrabacions="CREATE TABLE IF NOT EXISTS `grabacions` (
							`id` int(11) NOT NULL AUTO_INCREMENT,
							`userlogin` varchar(250) NOT NULL,
							`horaini` varchar(200) CHARACTER SET utf8 NOT NULL,
							`horafin` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
							`duracion` int(11) DEFAULT NULL,
							`archivo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
							`gpsini` decimal(10,0) DEFAULT NULL,
							`gpsfin` decimal(10,0) DEFAULT NULL,
							`play` int(11) DEFAULT NULL,
							`delet` int(11) DEFAULT NULL,
							`eliminado` int(11) NOT NULL DEFAULT '0',
							`datedelete` date DEFAULT NULL,
							PRIMARY KEY (`id`)
						) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
		
		$crear_tablaGrabacions = mysql_query($tablaGrabacions,$conexion);
		
		if(!$crear_tablaGrabacions){
			$bbddok=0;
			$mailtecnico.= 'Error al crear la tabla Grabacions<br/>';
		}else{
			$mailtecnico.= 'La tabla Grabacions se creo correctamente<br/>';
		}
		
		/********************************************************************************
			Creamos la tabla LlamadasEntrantes
		********************************************************************************/
		$tablaLlamadasEntrantes="CREATE TABLE IF NOT EXISTS `llamadasentrantes` (
									`id` int(11),
									`name` text,
									`surname` text,
									`userlogin` varchar(50),
									`fecha` datetime
								);";
		
		$crear_tablaLlamadasEntrantes = mysql_query($tablaLlamadasEntrantes,$conexion);
        
		if(!$crear_tablaLlamadasEntrantes){
			$bbddok=0;
			$mailtecnico.= 'Error al crear la tabla LlamadasEntrantes<br/>';
        }else{
			$mailtecnico.= 'La tabla LlamadasEntrantes se creo correctamente<br/>';
		}
		
		
		/********************************************************************************
			Creamos la tabla LlamadasPerdidas
		********************************************************************************/
		$tablaLlamadasPerdidas="CREATE TABLE IF NOT EXISTS `llamadasperdidas` (
									`id` int(11),
									`name` text,
									`surname` text,
									`userlogin` varchar(50),
									`fecha` datetime
								);";
		
		$crear_tablaLlamadasPerdidas = mysql_query($tablaLlamadasPerdidas,$conexion);
        
		if(!$crear_tablaLlamadasPerdidas){
			$bbddok=0;
			$mailtecnico.= 'Error al crear la tabla LlamadasPerdidas<br/>';
		}else{
			$mailtecnico.='La tabla LlamadasPerdidas se creo correctamente<br/>';
		}
		
		/********************************************************************************
			Creamos la tabla Logins
		********************************************************************************/
		$tablaLogins="CREATE TABLE IF NOT EXISTS `logins` (
									`id` int(11) NOT NULL AUTO_INCREMENT,
									`user` varchar(20) NOT NULL,
									`pass` varchar(20) NOT NULL,
									`rol` int(11) NOT NULL,
									`sesion` int(11) NOT NULL DEFAULT '0',
									PRIMARY KEY (`id`)
								) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
		
		$crear_tablaLogins = mysql_query($tablaLogins,$conexion);
        
			if(!$crear_tablaLogins){
				$bbddok=0;
				$mailtecnico.= 'Error al crear la tabla Logins<br/>';
            
			}else{
			
				$mailtecnico.= 'La tabla Logins se creo correctamente<br/>';
                
            }
		
		
		/********************************************************************************
			Creamos la tabla Pointsxzones
		********************************************************************************/			
		$tablaPointsxzones="CREATE TABLE IF NOT EXISTS `pointsxzones` (
								`Y` float NOT NULL,
								`X` float NOT NULL,
								`zoneId` int(11) NOT NULL
							) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
		
		$crear_tablaPointsxzones = mysql_query($tablaPointsxzones,$conexion);
        
		if(!$crear_tablaPointsxzones){
			$bbddok=0;
			$mailtecnico.= 'Error al crear la tabla Pointsxzones<br/>';
		}else{
			$mailtecnico.= 'La tabla Pointsxzones se creo correctamente<br/>';
		}
			
		/********************************************************************************
			Creamos la tabla Posiciones
		********************************************************************************/
		$tablaPosiciones="CREATE TABLE IF NOT EXISTS `posiciones` (
								`id` int(11) NOT NULL AUTO_INCREMENT,
							    `userId` int(11) NOT NULL,
							    `latitude` decimal(10,6) NOT NULL,
							    `longitude` decimal(10,6) NOT NULL,
							    `altitude` float NOT NULL,
							    `uploadTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
							    `compass` decimal(10,2) NOT NULL DEFAULT '0.00',
							    `accuracy` int(11) NOT NULL,
							    `dateInitStream` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
							    `speed` decimal(10,6) DEFAULT NULL,
							    `accelX` decimal(10,2) DEFAULT NULL,
							    `accelY` decimal(10,2) DEFAULT NULL,
							    `accelZ` decimal(10,2) DEFAULT NULL,
							    PRIMARY KEY (`id`,`userId`,`latitude`,`longitude`,`altitude`,`uploadTime`,`compass`,`accuracy`,`dateInitStream`),
							    KEY `index_userid` (`userId`),
							    KEY `uploadTime_index` (`uploadTime`)
							) ENGINE=InnoDB  DEFAULT CHARSET=armscii8 AUTO_INCREMENT=1 ;";

		//Creamos el trigger actualizarUltimaPosicion		
		$triggerPosiciones="CREATE TRIGGER `actualizarUltimaPosicion` AFTER INSERT ON `posiciones` FOR EACH ROW 
		INSERT INTO ultimaposicion SET userId=new.userId,latitude=new.latitude,longitude=new.longitude,
		altitude=new.altitude,uploadTime=new.uploadTime,compass=new.compass,accuracy=new.accuracy,acceIX=new.accelX,acceIY=new.accelY,
		acceIZ=new.accelZ 
		ON DUPLICATE KEY 
		update latitude = new.latitude,longitude = new.longitude,altitude = new.altitude,uploadTime = new.uploadTime,
		compass= new.compass,accuracy = new.accuracy,acceIX=new.accelX,acceIY=new.accelY,acceIZ=new.accelZ";

		$crear_tablaPosiciones = mysql_query($tablaPosiciones,$conexion);
        
		if(!$crear_tablaPosiciones){
			$bbddok=0;
			$mailtecnico.= 'Error al crear la tabla Posiciones<br/>';
		}else{
			$mailtecnico.= 'La tabla Posiciones se creo correctamente<br/>';
			//Si la hemos creado correctamente, creamos el trigger actualizarUltimaPosicion
			$crear_triggerPosiciones = mysql_query($triggerPosiciones,$conexion);
				
			if(!$crear_triggerPosiciones){
				$bbddok=0;
				$mailtecnico.= 'Error al crear el trigger actualizarUltimaPosicion<br/>';
            }else{
				$mailtecnico.= 'El trigger actualizarUltimaPosicion se creo correctamente<br/>';
			}
		}

		
		/********************************************************************************
			Creamos la tabla Registrations
		*******************************************************************************/
		$tablaRegistrations="CREATE TABLE IF NOT EXISTS `registrations` (
								`m_username` varchar(64) NOT NULL,
								`m_identity` varchar(64) DEFAULT NULL,
								`m_updatetime` datetime DEFAULT NULL,
								PRIMARY KEY (`m_username`)
							) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
		
		$crear_tablaRegistrations = mysql_query($tablaRegistrations,$conexion);
        
		if(!$crear_tablaRegistrations){
			$bbddok=0;
			$mailtecnico.= 'Error al crear la tabla Registrations<br/>';
		}else{
			$mailtecnico.= 'La tabla Registrations se creo correctamente<br/>';       
		}
		
		/********************************************************************************
			Creamos la tabla Sesiones
		********************************************************************************/
		$tablaSesiones="CREATE TABLE IF NOT EXISTS `sesiones` (
							`id` int(11) NOT NULL AUTO_INCREMENT,
							`idUser` int(11) NOT NULL,
							`ip` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
							`active` int(1) NOT NULL,
							`fechaEntrada` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
							`fechaUltimoAcceso` timestamp NULL DEFAULT NULL,
							PRIMARY KEY (`id`),
							KEY `idUser` (`idUser`)
						) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
		
		$crear_tablaSesiones = mysql_query($tablaSesiones,$conexion);
        
		if(!$crear_tablaSesiones){
			$bbddok=0;
			$mailtecnico.= 'Error al crear la tabla Sesiones<br/>';
		}else{
			$mailtecnico.= 'La tabla Sesiones se creo correctamente<br/>';  
        }
		
		/********************************************************************************
			Creamos la tabla Streamservers
		********************************************************************************/
		$tablaStreamservers="CREATE TABLE IF NOT EXISTS `streamservers` (
							`id` int(11) NOT NULL AUTO_INCREMENT,
							`server` text NOT NULL,
							`company` text NOT NULL,
							`prefixStream` text,
							`idempresa` int(11) NOT NULL,
							`ipserver` text NOT NULL,
							`streamapp` text NOT NULL,
							`recordapp` text NOT NULL,
							PRIMARY KEY (`id`)
						) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
		
		$crear_tablaStreamservers = mysql_query($tablaStreamservers,$conexion);
        
		if(!$crear_tablaStreamservers){
			$bbddok=0;
			$mailtecnico.= 'Error al crear la tabla Streamservers<br/>';
        }else{
			$mailtecnico.= 'La tabla Streamservers se creo correctamente<br/>';
		}
			
		$datosstream="INSERT INTO `streamservers` (`id`, `server`, `company`, `prefixStream`, `idempresa`, `ipserver`, `streamapp`, `recordapp`) VALUES (1, 'rtmp://ec2-79-125-75-85.eu-west-1.compute.amazonaws.com/liverecord', 'posiciones2', 'po2', 13, 'rtmp://54.246.96.123/', 'live', 'vod');";
		$consultastream=mysql_query($datosstream);
		if(!$consultastream){
			$bbddok=0;
			$mailtecnico.= 'Error al crear al insertar Streamservers<br/>';
		}else{
			$mailtecnico.= 'El insert streamserver guai<br/>';
		}
		
		/********************************************************************************
			Creamos la tabla tipoAvisos
		********************************************************************************/
		$tablaTipoAvisos="CREATE TABLE IF NOT EXISTS `tipoAvisos` (
						`id` int(10) NOT NULL AUTO_INCREMENT,
						`nom` varchar(60) NOT NULL,
						`descrip` varchar(255) DEFAULT NULL,
						PRIMARY KEY (`id`)
					) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
		
		$crear_tablaTipoAvisos = mysql_query($tablaTipoAvisos,$conexion);
        
		if(!$crear_tablaTipoAvisos){
			$bbddok=0;
			$mailtecnico.= 'Error al crear la tabla tipoAvisos<br/>';
        }else{
			$mailtecnico.= 'La tabla tipoAvisos se creo correctamente<br/>';
                  
			// si la tabla se creo correctamente entonces insertarmos los datos           
			$datos="INSERT INTO `tipoAvisos` (`id`, `nom`, `descrip`) VALUES (1, 'Salida zona', 'El usuario ha salido de la zona'),
																				 (2, 'Llamar', 'Petición de chat desde el movil'),
																	             (3, 'P&aacute;nico', 'Petición de assistencia desde el movil');";
			$consulta=mysql_query($datos,$conexion);
           
			if(!$consulta){
				$bbddok=0;
				$mailtecnico.= 'Error al insertar los datos en la tabla tipoAvisos<br/>';
			}else{
                $mailtecnico.= 'Los datos se insertaron correctamente en la tabla tipoAvisos<br/>';
            }
		}
		
		/********************************************************************************
			Creamos la tabla ultimaposicion
		********************************************************************************/
		$tablaUltimaposicion="CREATE TABLE IF NOT EXISTS `ultimaposicion` (
								  `userId` int(11) NOT NULL DEFAULT '0',
								  `latitude` decimal(10,6) DEFAULT NULL,
								  `longitude` decimal(10,6) DEFAULT NULL,
								  `altitude` float DEFAULT NULL,
								  `uploadTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
								  `compass` decimal(10,2) DEFAULT '0.00',
								  `accuracy` int(11) DEFAULT NULL,
								  `dateInitStream` datetime DEFAULT '0000-00-00 00:00:00',
								  `online` tinyint(1) NOT NULL DEFAULT '0',
								  `onlinetime` datetime DEFAULT NULL,
								  `userlogin` int(11) DEFAULT NULL,
								  `acceIX` decimal(10,2) DEFAULT NULL,
								  `acceIY` decimal(10,2) DEFAULT NULL,
								  `acceIZ` decimal(10,2) DEFAULT NULL,
								  PRIMARY KEY (`userId`),
								  KEY `uploadTime_index` (`uploadTime`)
							) ENGINE=InnoDB DEFAULT CHARSET=armscii8;";
		
		$crear_tablaUltimaposicion = mysql_query($tablaUltimaposicion,$conexion);
        
		if(!$crear_tablaUltimaposicion){
			$bbddok=0;
			$mailtecnico.= 'Error al crear la tabla ultimaposicion<br/>';
        }else{
			$mailtecnico.= 'La tabla ultimaposicion se creo correctamente<br/>';
		}	
		
		/********************************************************************************
			Creamos la tabla users
		********************************************************************************/
		$tablaUsers="CREATE TABLE IF NOT EXISTS `users` (
							  `id` int(11) NOT NULL AUTO_INCREMENT,
							  `userlogin` varchar(50) NOT NULL,
							  `name` text NOT NULL,
							  `surname` text NOT NULL,
							  `company` text NOT NULL,
							  `zone` int(11) NOT NULL,
							  `charge` text NOT NULL,
							  `online` int(11) DEFAULT NULL,
							  `onlinetime` date DEFAULT NULL,
							  `email` varchar(50) NOT NULL,
							  `password` text NOT NULL,
							  `admin` int(11) NOT NULL,
							  `vigilantia` int(11) NOT NULL,
							  `activo` int(1) NOT NULL DEFAULT '1' COMMENT 'Si se cierra una pagina cambiar a 0 para que no pueda emitir.',
							  PRIMARY KEY (`id`),
							  UNIQUE KEY `userlogin` (`userlogin`)
						) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
		
		$crear_tablaUsers = mysql_query($tablaUsers,$conexion);
        
		if(!$crear_tablaUsers){
			$bbddok=0;
			$mailtecnico.= 'Error al crear la tabla users<br/>';
        }else{
			$mailtecnico.= 'La tabla users se creo correctamente<br/>';
                  
			// si la tabla se creo correctamente entonces insertarmos los datos           
			$datos="INSERT INTO `users` (`id`, `userlogin`, `name`, `surname`, `company`, `zone`, `charge`, `online`, `onlinetime`, `email`, `password`, `admin`, `vigilantia`, `activo`) VALUES
											 (1, 'user01', 'user01', 'Nombre01', 'x', 0, 'cargo01', 0, NULL, 'user01', '01', 0, 0, 1),
											 (2, 'user02', 'user02', 'Nombre03', 'x', 0, 'cargo01', 0, NULL, 'user02', '02', 0, 0, 1),
											 (3, 'user03', 'user03', 'Nombre03', 'x', 0, 'cargo01', 0, NULL, 'user03', '02', 0, 0, 1),
											 (4, 'user04', 'user04', 'Nombre04', 'x', 0, 'cargo01', 0, NULL, 'user04', '04', 0, 0, 1);";
			$consulta=mysql_query($datos,$conexion);
            
			if(!$consulta){
				$bbddok=0;
				$mailtecnico.= 'Error al insertar los datos en la tabla users<br/>';
			}else{
				$mailtecnico.= 'Los datos se insertaron correctamente en la tabla users<br/>';
			}
		}
		
		
		/********************************************************************************
			Creamos la tabla usersxzones
		********************************************************************************/
		$tablaUsersxzones="CREATE TABLE IF NOT EXISTS `usersxzones` (
							  `userId` int(11) NOT NULL,
							  `zoneId` int(11) NOT NULL
							) ENGINE=InnoDB DEFAULT CHARSET=utf8; ";
		
		$crear_tablaUsersxzones = mysql_query($tablaUsersxzones,$conexion);
        
		if(!$crear_tablaUsersxzones){
			$bbddok=0;
			$mailtecnico.= 'Error al crear la tabla usersxzones<br/>';
		}else{
			$mailtecnico.= 'La tabla usersxzones se creo correctamente<br/>';
		}
		
		/********************************************************************************
			Creamos la vista v_avisos
		*******************************************************************************/
		$vistaAvisos="CREATE TABLE IF NOT EXISTS `v_avisos` (
							`id` int(11)
							,`userId` int(11)
							,`name` text
							,`surname` text
							,`zonaini` varchar(100)
							,`zonafin` varchar(100)
							,`tipoavisos` varchar(60)
							,`fechaentera` datetime
							,`dateini` varchar(510)
							,`horaini` time
						); ";
		
		$crear_vistaAvisos = mysql_query($vistaAvisos,$conexion);
		if(!$crear_vistaAvisos){
			$bbddok=0;
			$mailtecnico.= 'Error al crear la vista v_avisos<br/>';
        }else{
			$mailtecnico.= 'La vista v_avisos se creo correctamente<br/>';
		}
		
		/********************************************************************************
			Creamos la vista v_grabacions
		********************************************************************************/
		$vistaGrabacions="CREATE TABLE IF NOT EXISTS `v_grabacions` (
							`id` int(11)
							,`userlogin` varchar(250)
							,`name` text
							,`surname` text
							,`dateini` varchar(510)
							,`horaini` time
							,`datefin` date
							,`horafin` time
							,`duracion` time
							,`archivo` varchar(255)
							,`dataordenar` datetime
							,`play` int(11)
							,`delet` int(11)
						); ";
		
		$crear_vistaGrabacions = mysql_query($vistaGrabacions,$conexion);
        
		if(!$crear_vistaGrabacions){
			$bbddok=0;
			$mailtecnico.= 'Error al crear la vista v_grabacions<br/>';
		}else{
			$mailtecnico.= 'La vista v_grabacions se creo correctamente<br/>';
        }
		
		/********************************************************************************
			Creamos la tabla Wowzalog
		********************************************************************************/
		$tablaWowzalog="CREATE TABLE IF NOT EXISTS `Wowzalog` (
							  `logid` int(10) unsigned NOT NULL AUTO_INCREMENT,
							  `date` varchar(100) DEFAULT NULL,
							  `time` varchar(100) DEFAULT NULL,
							  `tz` varchar(100) DEFAULT NULL,
							  `xevent` varchar(20) DEFAULT NULL,
							  `xcategory` varchar(20) DEFAULT NULL,
							  `xseverity` varchar(100) DEFAULT NULL,
							  `xstatus` varchar(100) DEFAULT NULL,
							  `xctx` varchar(100) DEFAULT NULL,
							  `xcomment` varchar(255) DEFAULT NULL,
							  `xvhost` varchar(100) DEFAULT NULL,
							  `xapp` varchar(100) DEFAULT NULL,
							  `xappinst` varchar(100) DEFAULT NULL,
							  `xduration` varchar(100) DEFAULT NULL,
							  `sip` varchar(100) DEFAULT NULL,
							  `sport` varchar(100) DEFAULT NULL,
							  `suri` varchar(255) DEFAULT NULL,
							  `cip` varchar(100) DEFAULT NULL,
							  `cproto` varchar(100) DEFAULT NULL,
							  `creferrer` varchar(255) DEFAULT NULL,
							  `cuseragent` varchar(100) DEFAULT NULL,
							  `cclientid` varchar(25) DEFAULT NULL,
							  `csbytes` varchar(20) DEFAULT NULL,
							  `scbytes` varchar(20) DEFAULT NULL,
							  `xstreamid` varchar(20) DEFAULT NULL,
							  `xspos` varchar(20) DEFAULT NULL,
							  `csstreambytes` varchar(20) DEFAULT NULL,
							  `scstreambytes` varchar(20) DEFAULT NULL,
							  `xsname` varchar(100) DEFAULT NULL,
							  `xsnamequery` varchar(100) DEFAULT NULL,
							  `xfilename` varchar(100) DEFAULT NULL,
							  `xfileext` varchar(100) DEFAULT NULL,
							  `xfilesize` varchar(100) DEFAULT NULL,
							  `xfilelength` varchar(100) DEFAULT NULL,
							  `xsuri` varchar(255) DEFAULT NULL,
							  `xsuristem` varchar(255) DEFAULT NULL,
							  `xsuriquery` varchar(255) DEFAULT NULL,
							  `csuristem` varchar(255) DEFAULT NULL,
							  `csuriquery` varchar(255) DEFAULT NULL,
							  PRIMARY KEY (`logid`)
						) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1; ";
		
		$crear_tablaWowzalog = mysql_query($tablaWowzalog,$conexion);
        
		if(!$crear_tablaWowzalog){
			$bbddok=0;
			$mailtecnico.= 'Error al crear la tabla Wowzalog<br/>';
		}else{
			$mailtecnico.= 'La tabla Wowzalog se creo correctamente<br/>';                
		}
		
		/********************************************************************************
			Creamos la tabla zones
		********************************************************************************/
		$tablaZones="CREATE TABLE IF NOT EXISTS `zones` (
						  `pkID` int(11) NOT NULL AUTO_INCREMENT,
						  `name` varchar(100) NOT NULL,
						  `color` varchar(7) NOT NULL,
						  `center` varchar(100) NOT NULL,
						  `zoom` int(11) NOT NULL,
						  PRIMARY KEY (`pkID`)
					) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ;";
		
		$crear_tablaZones = mysql_query($tablaZones,$conexion);
        
		if(!$crear_tablaZones){
			$bbddok=0;
			$mailtecnico.= 'Error al crear la tabla zones<br/>';
        }else{
			$mailtecnico.= 'La tabla zones se creo correctamente<br/>';
                 
			// si la tabla se creo correctamente entonces insertarmos los datos           
			$datos="INSERT INTO `zones` (`pkID`, `name`, `color`, `center`, `zoom`) VALUES
										(0, 'No definida', '#ffffff', '(0, 0)', 13);";
			
			$consulta=mysql_query($datos,$conexion);
			if(!$consulta){
				$mailtecnico.= 'Error al insertar los datos en la tabla zones<br/>';
			}else{
				$mailtecnico.= 'Los datos se insertaron correctamente en la tabla zones<br/>';
			}
                
            $datos="UPDATE zones SET pkID=0 WHERE pkID=1";
			$consulta=mysql_query($datos,$conexion);
			if(!$consulta){
				$mailtecnico.= 'Error al update los datos en la tabla zones<br/>';
			}else{
				$mailtecnico.= 'Los datos se actualizaron correctamente en la tabla zones<br/>';
			}
		}
		
		/********************************************************************************
			Creamos la select para la vista llamadasentrantes
		********************************************************************************/
		$delete=mysql_query("DROP TABLE IF EXISTS `llamadasentrantes`;",$conexion);
		if(!$delete){
			$bbddok=0;
			$mailtecnico.= 'Error al eliminar la tabla llamadasentratnes<br/>';
		}else{		
			$mailtecnico.= 'La tabla llamadasentrantes se borro correctamente<br/>';
		}
		$vistaELlamadasentrantes="CREATE or replace ALGORITHM=UNDEFINED DEFINER=`adminbd`@`%` SQL SECURITY DEFINER VIEW `llamadasentrantes` AS select `avisos`.`userId` AS `id`,`users`.`name` AS `name`,`users`.`surname` AS `surname`,`users`.`userlogin` AS `userlogin`,`avisos`.`time_ini` AS `fecha` from (`avisos` left join `users` on((`users`.`id` = `avisos`.`userId`))) where ((`avisos`.`tipoAviso` = 2) and (`avisos`.`aviso` = 0) and (`avisos`.`time_ini` >= (now() + interval -(1) minute))); ";
		
		$crear_vistaELlamadasentrantes = mysql_query($vistaELlamadasentrantes,$conexion);
        
		if(!$crear_vistaELlamadasentrantes){
			$bbddok=0;
			$mailtecnico.= 'Error al crear la select para la vista llamadasentrantes<br/>';
		}else{
			$mailtecnico.= 'La select para la vista llamadasentrantes se creo correctamente<br/>';
		}
		
		/********************************************************************************
			Creamos la select para la vista llamadasperdidas
		********************************************************************************/
		$delete=mysql_query("DROP TABLE IF EXISTS `llamadasperdidas`;",$conexion);
		if(!$delete){
			$bbddok=0;
			$mailtecnico.= 'Error al eliminar la tabla llamadasperdidas<br/>';
		}else{
			$mailtecnico.= 'La tabla llamadasperdidas se borro correctamente<br/>';
		}
		
		$vistaEllamadasperdidas="CREATE or replace ALGORITHM=UNDEFINED DEFINER=`adminbd`@`%` SQL SECURITY DEFINER VIEW `llamadasperdidas` AS select `avisos`.`userId` AS `id`,`users`.`name` AS `name`,`users`.`surname` AS `surname`,`users`.`userlogin` AS `userlogin`,`avisos`.`time_ini` AS `fecha` from (`avisos` left join `users` on((`users`.`id` = `avisos`.`userId`))) where ((`avisos`.`tipoAviso` = 2) and (`avisos`.`aviso` = 0) and (`avisos`.`time_ini` < (now() + interval -(1) minute)));";
		
		$crear_vistaEllamadasperdidas = mysql_query($vistaEllamadasperdidas,$conexion);
        
		if(!$crear_vistaEllamadasperdidas){
			$bbddok=0;
			$mailtecnico.= 'Error al crear la select para la vista llamadasperdidas<br/>';
		}else{
			$mailtecnico.= 'La select para la vista llamadasperdidas se creo correctamente<br/>';
		}
		
		/********************************************************************************
			Creamos la select para la vista v_avisos
		********************************************************************************/
		$delete=mysql_query("DROP TABLE IF EXISTS `v_avisos`;",$conexion);
		if(!$delete){
			$bbddok=0;
			$mailtecnico.= 'Error al eliminar la tabla v_avisos<br/>';
		}else{
			$mailtecnico.= 'La tabla v_avisos se borro correctamente<br/>';
		}
		
		$vistaEv_avisos="CREATE or replace ALGORITHM=UNDEFINED DEFINER=`adminbd`@`%` SQL SECURITY DEFINER VIEW `v_avisos` AS select `avisos`.`pkId` AS `id`,`avisos`.`userId` AS `userId`,`users`.`name` AS `name`,`users`.`surname` AS `surname`,`z`.`name` AS `zonaini`,`z2`.`name` AS `zonafin`,`tipoAvisos`.`nom` AS `tipoavisos`,`avisos`.`time_ini` AS `fechaentera`,date_format(cast(convert_tz(`avisos`.`time_ini`,'+00:00','+1:00') as date),get_format(DATE, 'EUR')) AS `dateini`,cast(convert_tz(`avisos`.`time_ini`,'+00:00','+1:00') as time) AS `horaini` from ((((`avisos` join `users`) join `zones` `z`) join `zones` `z2`) join `tipoAvisos`) where ((`avisos`.`userId` = `users`.`id`) and (`users`.`vigilantia` = 0) and (`z`.`pkID` = `avisos`.`oldZoneId`) and (`z2`.`pkID` = `avisos`.`newZoneId`) and (`avisos`.`tipoAviso` = `tipoAvisos`.`id`));";
		$crear_vistaEv_avisos = mysql_query($vistaEv_avisos,$conexion);
        
		if(!$crear_vistaEv_avisos){
			$bbddok=0;
			$mailtecnico.= 'Error al crear la select para la vista v_avisos<br/>';
		}else{
			$mailtecnico.= 'La select para la vista v_avisos se creo correctamente<br/>';
		}		
		
		/********************************************************************************		
			Creamos la select para la vista v_grabacions
		********************************************************************************/
		$delete=mysql_query("DROP TABLE IF EXISTS `v_grabacions`;",$conexion);
		if(!$delete){
			$bbddok=0;
			$mailtecnico.= 'Error al eliminar la tabla v_grabacions<br/>';
		}else{
			$mailtecnico.= 'La tabla v_grabacions se borro correctamente<br/>';
		}
		
		$vistaEv_grabacions="CREATE or replace ALGORITHM=UNDEFINED DEFINER=`adminbd`@`%` SQL SECURITY DEFINER VIEW `v_grabacions` AS select `grabacions`.`id` AS `id`,`grabacions`.`userlogin` AS `userlogin`,`users`.`name` AS `name`,`users`.`surname` AS `surname`,date_format(cast(convert_tz(`grabacions`.`horaini`,'+00:00','+2:00') as date),get_format(DATE, 'EUR')) AS `dateini`,cast(convert_tz(`grabacions`.`horaini`,'+00:00','+2:00') as time) AS `horaini`,cast(convert_tz(`grabacions`.`horafin`,'+00:00','+2:00') as date) AS `datefin`,cast(convert_tz(`grabacions`.`horafin`,'+00:00','+2:00') as time) AS `horafin`,sec_to_time(`grabacions`.`duracion`) AS `duracion`,`grabacions`.`archivo` AS `archivo`,convert_tz(`grabacions`.`horaini`,'+00:00','+2:00') AS `dataordenar`,`grabacions`.`play` AS `play`,`grabacions`.`delet` AS `delet` from (`grabacions` join `users`) where ((`grabacions`.`userlogin` = `users`.`userlogin`) and (`grabacions`.`eliminado` = 0)); ";
		$crear_vistaEv_grabacions = mysql_query($vistaEv_grabacions,$conexion);
        
		if(!$crear_vistaEv_grabacions){
			$bbddok=0;
			$mailtecnico.= 'Error al crear la select para la vista v_grabacions<br/>';
		}else{
			$mailtecnico.= 'La select para la vista v_grabacions se creo correctamente<br/>';
		}
	}

	/********************************************************************************
		ahora toca meter los datos del formulario en la bd eude
	********************************************************************************/
	define('DB_USER2', "adminbd"); // db user
	define('DB_PASSWORD2', "evaristo2002++"); // db password (mention your db password here)
	define('DB_DATABASE_EUDE2',"eude");
	define('DB_SERVER2', "vigilantiabd1.cddh4gk18qev.eu-west-1.rds.amazonaws.com"); // db server
	//require_once('db_config');
	$conexion=mysql_close();
	$db = mysql_connect(DB_SERVER2, DB_USER2, DB_PASSWORD2) or die(mysql_error());
	$db = mysql_select_db(DB_DATABASE_EUDE2) or die(mysql_error()) or die(mysql_error());
	$connapp='jdbc:mysql://vigilantiabd1.cddh4gk18qev.eu-west-1.rds.amazonaws.com:3306/'.'xxx';

	$query_insert="INSERT INTO `eude`.`empresas` (`nombre`, `cif`, `direccion`, `cp`, `poblacion`, `provincia`, `idpais`, 
				`email`,`telefono1`,`telefono2`,`cta`,`activo`,`bbdd`,`prefixstream`,`server`,`streamapp`,`domini`,`conn`,
				`aplicacion`,`num_licencias`) VALUES ('$empresa','$dni','$direccion','$cp','$ciudad','$provincia',12,'$email','$tlf','$tlf2',
				'X',1,'$nombre_bd','xxx','rtmp://54.246.96.123','live','http://p.imakestream.com','$connapp',1,1);";

	$query_insertcon=mysql_query($query_insert);
	if(!$query_insertcon){
		$bbddok=0;
		$mailtecnico.= 'Error al insertar nueva empresa en EUDE<br/>';
		//echo 'Error al insertar nueva empresa en EUDE<br/>';
	}else{
		$mailtecnico.= 'El insert nueva empresa en EUDE correctament<br/>';
	}


	if($bbddok){
		$mailtecnico.= "Todo ok en la bbdd<br/>";
	}else{
		$mailtecnico.= "Algo falla en la bbdd<br/>";
	}

	$response['success']=$bbddok;

	//Aquí enviamos el email al usuario que ha realizado la compra/alta del producto
	$subject = "Nuevo grupo de vigilancia";
	$from = "admin@imks.tv";
	$host = "smtp.mandrillapp.com";
	$username = 'admin@imks.com';
	$password = 'cDt9L1xXFCUpJqm570fXnA';
	$to='marisa@imks.tv';
	$body = $mailtecnico;

	$query_listado = "select mail from backoffice";
	$listado = mysql_query($query_listado) or die(mysql_error());

	
	//$to='tica@imks.tv';
	$body=$mailtecnico;
	$headers = array (
		'From' => $from,
		'To' => $to,
		'Subject' => $subject,
		'MIME-Version' => "1.0",
		'Content-type' => "text/html; charset=utf-8\r\n\r\n"
	);

	try {
		$mandrill = new Mandrill('tssBXB12pEnZKlw2KeF_5A');
		
		while ($row_listado = mysql_fetch_array($listado)){
			//echo $row_listado['mail'].'---';
			$message = array(
				'subject' => $subject,
				'from_email' => 'admin@imks.tv',
				'html' => $body,
				'to' => array(array('email' => $row_listado['mail']))
			);
		
			print_r($mandrill->messages->send($message));
		}//fin while


	} catch(Mandrill_Error $e) {
		// Mandrill errors are thrown as exceptions
		//echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
		throw $e;
	}  
  
  

	if(!$bbddok){
		$body='Apreciado cliente, algo ha fallado en el proceso de creacion de su estructura de vigilancia. En breves instantes nuestro equipo tecnico se pondra en contacto con usted. Disculpe las molestias.';
	}else{
		$body ="Apreciado cliente/a,<br/><br/>Le informamos que el proceso de alta se ha realizado correctamente. Usted ahora puede acceder a http://evastream.tv/nombreEmpresa. A continuación hemos creado cuatro usuarios para usted.<br/><br/>";		
		$body .="Usuario = user01<br/>Password = 01<br/><br/>";
		$body .="Usuario = user02<br/>Password = 02<br/>";
		$body .="Usuario = user03<br/>Password = 03<br/>";
		$body .="Usuario = user04<br/>Password = 04<br/><br/>";
		$body .="También deberá descargar las aplicaciones para Smartphone, pinchando en las siguientes direcciones:<br/><br/>";
		$body .="App iOS: www.ios.es";
		$body .="App Android: www.android.es";
	}
	$to=$email;
	$subject = "Bienvenido a vigilancia";
	$headers = array (
		'From' => $from,
		'To' => $to,
		'Subject' => $subject,
		'MIME-Version' => "1.0",
		'Content-type' => "text/html; charset=utf-8\r\n\r\n"
	);
  
  
	try {
		$mandrill = new Mandrill('tssBXB12pEnZKlw2KeF_5A');
		$message = array(
			'subject' => $subject,
			'from_email' => 'admin@imks.tv',
			'html' => $body,
			'to' => array(array('email' => $to))
		);
		print_r($mandrill->messages->send($message));
	
	} catch(Mandrill_Error $e) {
		// Mandrill errors are thrown as exceptions
		//echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
		throw $e;
	}    
	//$mail = $smtp->send($to, $headers, $body);
echo json_encode($response);
?> 