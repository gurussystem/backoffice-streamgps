<html>
	<head>
	
		<!-- libreria jQuery -->
		<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"> </script>

		<script>
			function buscarEmpresa(){
				var nomEmpresa = $("#nomEmpresa").val();
				var idEmpresa = $("#idEmpresa").val();
				console.log("Buscando la empresa, nombre: " +  nomEmpresa + " , id: " + idEmpresa );
		
				$.ajax({
					//	type: 'POST',
					type: 'GET',
					url: 'io/searchBD.php',
					//url: 'io/searchBD.php',
					data: { 
						'nomEmpresa': nomEmpresa,
						'idEmpresa': idEmpresa
					},
					
					success: function(response) {
						var html_txt='';
						var datosEmpresa = $.parseJSON(response);
						
						if(datosEmpresa==''){
							console.log("No hay resultados de busqueda");
							$("#listadoEmpresas").html('No hay resultados de busqueda');
						}else{
							//console.log("hay " + datosEmpresa[i].id);	
							html_txt += "<table>";
								html_txt += "<tr>";
									html_txt += "<th>idEmpresa</th>";
									html_txt += "<th>Nombre</th>";
									html_txt += "<th>Fecha Alta</th>";
									html_txt += "<th>Estado</th>";
								html_txt += "</tr>";
								for(var i=0;i<datosEmpresa.length;i++) {
								//montamos la variable que pasamos
								var parametros = "";
									parametros += datosEmpresa[i].id;
									parametros += " , &quot;"+datosEmpresa[i].razonSocial+"&quot;";
									parametros += " , &quot;"+datosEmpresa[i].nombre+"&quot;";
									parametros += " , &quot;"+datosEmpresa[i].bbdd+"&quot;";
									parametros += " , &quot;"+datosEmpresa[i].pass+"&quot;";
									parametros += " , &quot;"+datosEmpresa[i].cif+"&quot;";
									parametros += " , &quot;"+datosEmpresa[i].direccion+"&quot;";
									parametros += ", " + datosEmpresa[i].cp;
									parametros += " , &quot;"+datosEmpresa[i].poblacion+"&quot;";
									parametros += " , &quot;"+datosEmpresa[i].provincia+"&quot;";
									parametros += " , &quot;"+datosEmpresa[i].email+"&quot;";
									parametros += " , "+datosEmpresa[i].telefono1;
									parametros += " , "+datosEmpresa[i].telefono2;
									parametros += " , &quot;"+datosEmpresa[i].cta+"&quot;";
									parametros += " , &quot;"+datosEmpresa[i].estado+"&quot;";
									parametros += " , &quot;"+datosEmpresa[i].fechaAlta+"&quot;";
									parametros += " , &quot;"+datosEmpresa[i].numModulosContratados+"&quot;";
									parametros += " , &quot;"+datosEmpresa[i].fechaSolicitudAlta+"&quot;";
									
								html_txt += "<tr>";
									html_txt += "<td>"+datosEmpresa[i].id+"</td>";
									html_txt += "<td>"+datosEmpresa[i].nombre+"</td>";
									html_txt += "<td>"+datosEmpresa[i].fechaAlta+"</td>";									
									//html_txt += "<td><a href='#' onclick='seleccionModificar(&quot;"+datosEmpresa[i]+"&quot;);' >"+datosEmpresa[i].estado+"</a></td>";
									html_txt += "<td><a href='#' onclick='seleccionModificar("+parametros+");' >"+datosEmpresa[i].estado+"</a></td>";
									
								html_txt += "</tr>";
								}//fin for
							html_txt += "</table>";
							$("#listadoEmpresas").html(html_txt);							
						}
					}//fin success
				}); //fin ajax
				
			}//fin funcion buscarEmpresa()	
			
			function seleccionModificar(id, razonSocial, nombre, bbdd, pass, cif, direccion, cp, poblacion, provincia, email, telefono1, telefono2, cta, estado, fechaAlta, numModulosContratados, fechaSolicitudAlta){
				console.log("id: " + id );
				console.log("razonSocial: " + razonSocial );
				console.log("Nombre: " + nombre );				
				console.log("Base Datos: " + bbdd );
				console.log("PAss: " + pass );
				console.log("cif: " + cif );
				console.log("direccion: " + direccion );
				console.log("cp: " + cp );
				console.log("poblacion: " + poblacion );
				console.log("provincia: " + provincia );
				console.log("email: " + email );
				console.log("telefono1: " + telefono1 );
				console.log("telefono2: " + telefono2 );
				console.log("cta: " + cta );
				console.log("estado: " + estado );
				console.log("fechaAlta: " + fechaAlta );
				console.log("numModulosContratados: " + numModulosContratados );
				console.log("fechaSolicitudAlta: " + fechaSolicitudAlta );
				
				//console.log("1 Tenemos que modificar la siguiente empresa: " + bdEmpresa );
				
				//ahora podemos enviar 
				
				
			}
		</script>
	</head>
	
	<body>
		<h3>
			Buscador de empresas
		</h3>

		<p>
			Introduce el identificador o el nombre de la empresa para poder modificar sus datos.
		</p>
		<br>


		Introduce el nombre de la empresa: <input type="text" name="nomEmpresa" id="nomEmpresa" ><br>
		Introduce el identificador de la empresa: <input type="text" name="idEmpresa" id="idEmpresa" ><br>
		<input type="button" value="Buscar Empresa" onclick="buscarEmpresa()">
		
		<div>	
			<div id="listadoEmpresas"> </div>
		</div>
	</body>
</html>
