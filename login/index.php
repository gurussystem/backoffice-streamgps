<!DOCTYPE HTML>
<html>
	<head>
		
		
		<link rel="stylesheet" type="text/css" href="css/index.css"> 
		<script type="text/javascript" src="js/scriptLogin.js" ></script>
		<!-- 
			<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		-->
		<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<!--
		<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>	
		-->
		<style>
			table tr td{
				/*border:1px solid !important;*/
			}
		</style>
		<script type="text/javascript">
			/******************************************************************************************************
				ACtivamos los selects, (numero indicado en los checkbox)
			******************************************************************************************************/
			$(document).ready(function(){		
				//listadoTipoCamaras();
				var numModulosContratados;
			});//fin jQuery
			
			
		</script>
	</head>
	
	
	<body>
		<div id="contenedor">
			<div id="header">
				<!--img src="img/headerImakestream.jpg" title="Imakestream.com"/--> 
			</div>
		
			<div id="contenido">
				<table id="tablaContenedora" class="tabli " align='center'> <!--tabla contenedora -->
					<tr>
						<td colspan=3 id="cabecera" >
							Alta en ControlStream.tv
						</td>
					</tr>
				
				
					<tr>
										
						<td colspan=3> 
							<table align="center" class="contacto" id="signin" >
								<tr>
									<td class="marginBottom">
										<label class="texto" title="empresa">Empresa: </label>
									</td>
						
									<td>
										<input type="text" id="empresa" title="Introduce tu empresa" onclick="vaciarInput('empresa')" placeholder="Introduce el nombre de la empresa"/>
										<div id="empresa_div" class="campoObligatorio" style="display:none;"> * Campo Obligatorio, introduce el nombre de la empresa. </div>
									</td>
								</tr>
								
								<tr>
									<td class="marginBottom">
										<label class="texto" title="Usuario">Usuario: </label>
									</td>
						
									<td>
										<input type="text" id="user" title="Introduce tu usuario" onclick="vaciarInput('user')" placeholder="Introduce el nommbre del usuario" />
										<div id="user_div" class="campoObligatorio" style="display:none;"> * Campo Obligatorio, introduce un nombre de usuario </div>
									</td>
								</tr>

								<tr>
									<td class="marginBottom">
										<label class="texto" title="Password">Password: </label>
									</td>
									
									<td>
										<input type="password" id="password" title="Introduce tu password" onKeypress="teclaIntro(this, event);" onclick="vaciarInput('password')" placeholder="Introduce una contrase&ntilde;a" /> 
										<div id="password_div" class="campoObligatorio" style="display:none;"> * Campo Obligatorio, introduce una contrase&ntilde;a </div>
									</td>
								</tr>

								<tr>
									<td class="marginBottom">
										<label class="texto" title="Email">Email: </label>
									</td>
						
									<td>
										<input type="text" id="email" title="Introduce un email donde se env&iacute;aran todos la informaci&oacute;n" onclick="vaciarInput('email')" placeholder="Introduce un email" />
										<div id="email_div" class="campoObligatorio" style="display:none;"> * Campo Obligatorio, introduce un email donde enviarte la informaci&oacute;n</div>
									</td>
								</tr>
								
								<tr>
									<td class="marginBottom">
										<label class="texto" title="DNI">CIF/DNI: </label>
									</td>
						
									<td>
										<input type="text" id="dni" maxlength=9 title="Introduce tu CIF/DNI" onclick="vaciarInput('dni')" placeholder="Introduce el CIF/DNI de la empresa"/>
										<div id="dni_div" class="campoObligatorio" style="display:none;"> * Campo Obligatorio, introduce el CIF/DNI de la empresa </div>
									</td>
								</tr>
								
								<tr>
									<td class="marginBottom">
										<label class="texto" title="Direcci&oacute;n">Direcci&oacute;n: </label>
									</td>
					
									<td>
										<input type="text" id="direccion" title="Introduce la direcci&oacute;n de la empresa" onclick="vaciarInput('direccion')" placeholder="Introduce la direcci&oacute;n donde esta la empresa"/>
										<div id="direccion_div" class="campoObligatorio" style="display:none;"> * Campo Obligatorio, introduce la direcci&oacute;n de la empresa </div>
									</td>
								</tr>

								<tr>
									<td class="marginBottom">
										<label class="texto" title="Ciudad">Poblaci&oacute;n: </label>
									</td>
						
									<td>
										<input type="text" id="ciudad" title="Introduce la poblaci&oacute;n donde esta la empresa" onclick="vaciarInput('ciudad')" placeholder="Introduce la poblaci&oacute;n donde esta la empresa" />
										<div id="ciudad_div" class="campoObligatorio" style="display:none;"> * Campo Obligatorio, introduce la ciudad donde se encuentra la empresa </div>
									</td>
								</tr>

								<tr>
									<td class="marginBottom">
										<label class="texto" title="Codigo postal">Codigo postal: </label>
									</td>
					
									<td>
										<input type="text" id="cp" title="Introduce tu c&oacute;digo postal" maxlength=5 onclick="vaciarInput('cp')" placeholder="Introduce el c&oacute;digo postal" />
										<div id="cp_div" class="campoObligatorio" style="display:none;"> * Campo Obligatorio, c&oacute;digo postal de la empresa </div>
									</td>
								</tr>

								<tr>
									<td class="marginBottom">
										<label class="texto" title="Provincia">Provincia: </label>
									</td>
					
									<td>
										<input type="text" id="provincia" title="Introduce la provincia donde esta la empresa" onclick="vaciarInput('provincia')" placeholder="Introduce la provincia donde esta la empresa" />
										<!-- Añadirlo cuando puedas
										<select >
											<option value='selecciona'>Selecciona</option>             <option value='A Coruña' >A Coruña</option>             <option value='álava'>álava</option>             <option value='Albacete' >Albacete</option>             <option value='Alicante'>Alicante</option>             <option value='Almería' >Almería</option>             <option value='Asturias' >Asturias</option>             <option value='ávila' >Ávila</option>             <option value='Badajoz' >Badajoz</option>             <option value='Barcelona'>Barcelona</option>             <option value='Burgos' >Burgos</option>             <option value='Cáceres' >Cáceres</option>             <option value='Cádiz' >Cádiz</option>             <option value='Cantabria' >Cantabria</option>             <option value='Castellón' >Castellón</option>             <option value='Ceuta' >Ceuta</option>             <option value='Ciudad Real' >Ciudad Real</option>             <option value='Córdoba' >Córdoba</option>             <option value='Cuenca' >Cuenca</option>             <option value='Gerona' >Gerona</option>             <option value='Girona' >Girona</option>             <option value='Las Palmas' >Las Palmas</option>             <option value='Granada' >Granada</option>             <option value='Guadalajara' >Guadalajara</option>             <option value='Guipúzcoa' >Guipúzcoa</option>             <option value='Huelva' >Huelva</option>             <option value='Huesca' >Huesca</option>             <option value='Jaén' >Jaén</option>             <option value='La Rioja' >La Rioja</option>             <option value='León' >León</option>             <option value='Lleida' >Lleida</option>             <option value='Lugo' >Lugo</option>             <option value='Madrid' >Madrid</option>             <option value='Malaga' >Málaga</option>             <option value='Mallorca' >Mallorca</option>             <option value='Melilla' >Melilla</option>             <option value='Murcia' >Murcia</option>             <option value='Navarra' >Navarra</option>             <option value='Orense' >Orense</option>             <option value='Palencia' >Palencia</option>             <option value='Pontevedra'>Pontevedra</option>             <option value='Salamanca'>Salamanca</option>             <option value='Segovia' >Segovia</option>             <option value='Sevilla' >Sevilla</option>             <option value='Soria' >Soria</option>             <option value='Tarragona' >Tarragona</option>             <option value='Tenerife' >Tenerife</option>             <option value='Teruel' >Teruel</option>             <option value='Toledo' >Toledo</option>             <option value='Valencia' >Valencia</option>             <option value='Valladolid' >Valladolid</option>             <option value='Vizcaya' >Vizcaya</option>             <option value='Zamora' >Zamora</option>             <option value='Zaragoza'>Zaragoza</option>
										</select>
										-->
										<div id="provincia_div" class="campoObligatorio" style="display:none;"> * Campo Obligatorio, introduce la provincia donde esta la empresa </div>
									</td>
								</tr>

								<tr>
									<td class="marginBottom">
										<label class="texto" title="telefono 1">Tel&eacute;fono Fijo: </label>
									</td>
					
									<td>
										<input type="text" id="tlf" title="Introduce tu tel&eacute;fono" maxlength=9 onclick="vaciarInput('tlf')" placeholder="Introduce el tel&eacute;fono fijo de la empresa" />
										<div id="tlf_div" class="campoObligatorio" style="display:none;"> * Campo Obligatorio, introduce el tel&eacute;fono fijo de la empresa</div>
									</td>
								</tr>

								<tr>
									<td class="marginBottom">
										<label class="texto" title="telefono 2">Tel&eacute;fono Contacto (Movil): </label>
									</td>
					
									<td>
										<input type="text" id="tlf2" title="Introduce un segundo número de tel&eacute;fono" maxlength=9 onclick="vaciarInput('tlf2')" placeholder="Introduce un segundo n&uacute;mero de tel&eacute;fono"/>
										<div id="tlf2_div" class="campoObligatorio" style="display:none;"> * Campo Obligatorio, un segundo n&uacute;mero de tel&eacute;fono </div>
									</td>
								</tr>
								
								<tr>
									<td class="marginBottom">
										<label class="texto" title="Fecha Solicitud Alta">Fecha Solicitud Alta Cliente: </label>
									</td>
					
									<td>
										<input type="date" id="fechaSolicitudAlta" name="bday" title="Introduce la fecha en que el cliente solicito el alta" onclick="vaciarInput('fechaSolicitudAlta')" > 
										<div id="fechaSolicitudAlta_div" class="campoObligatorio" style="display:none;"> * Campo Obligatorio, introduce la fecha en que se solicito el alta </div>
									</td>
								</tr>

								<!--Apartado banco -->
								<tr>
									<td class="marginBottom">
										<label class="texto" title="Numero de cuenta donde el cliente hara el pago mensual">Domiciliacion Bancaria</label>
									</td>
					
									<td>
										<input type="text" id="iban"  	  class="cta" placeholder="IBAN" 			maxlength=4  title="Introduce el iban" > 
										<input type="text" id="entidad"   class="cta" placeholder="ENTIDAD" 		maxlength=4  title="Introduce la entidad" > 
										<input type="text" id="sucursal"  class="cta" placeholder="SUCURSAL"		maxlength=4  title="Introduce la sucursal" > 
										<input type="text" id="dc"  	  class="cta" placeholder="DC" 				maxlength=2  title="Introduce el DC" > 
										<input type="text" id="numCuenta" class="ctaNumCuenta" placeholder="NUMERO CUENTA"  	maxlength=10 title="Introduce el numero de cuenta" > 
										<div id="datosBancarios_div" class="campoObligatorio" style="display:none;">  </div>
									</td>
								</tr>
								
								<!-- Apartado modulos contratados <tr> <td> <tabla new> -->
								<tr>
									<td id="" >	
											<label> Numero de Modulos a contratar </label>
									</td>
									
									<td>
										<input type="text" id="numModulos_inputext" placeholder="Numero de modulo a contratar" title="Introduce el numero de modulos que quieres contratar"  class="" style="float:left;"> 
										<input type="button" value="cargar modulos" id="numModulos_btn" title="cargar modulos" onclick="cargarModulos()" style=" float: right; " />		
										<div id="numModulosContratar_div" class="campoObligatorio" style="display:none;" > * Campo Obligatorio, selecciona el numero de modulos a contratar</div>
										
									</td>
							
							
								</tr>
							
								<!-- parte de los bloques segun el numero que pongas en el input text 'numModulos_inputext', tantos bloques te apareceran en el form-->
								<tr class="marginBottom" > 
									<td colspan=2 id="bloquesModulos"> </td>
								</tr>
						
								
							
								<!-- Boton para guardar los datos -->
								<tr>
									<td colspan=3 id="">	
										<input type="button" value="Guardar Informaci&oacute;n" title="Guardar Informaci&oacute;n" onclick="validar_datos()" style=" float: right; " />		
									</td>
								</tr>
							</table> <!-- fin tabla formulario datos -->
						</td>
					</tr>
				</table> <!-- tabla principal (la que lo contiene todo) -->
				
				<p>
					<center>
						Aplicaci&oacuten optimizada para los navegadores Firefox y Chrome.
					</center>
				</p>
				
				
			</div> <!-- Fin id=contenido-->
			
			<div id="pie">
				<br/>
			</div> <!-- Fin id=pie -->
		</div> <!-- Fin id=contenido -->
	</body>
</html>