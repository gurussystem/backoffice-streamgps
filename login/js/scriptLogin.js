/*
Muy importante!! 
A la que pueda meter un array mejor porque estará todo mucho más organizado
y falta validar el numero de longitud en los numeros, 
varidar el nif/cif..
solo se ha validado que no esten vacios
*/

	var aux_mod_nom='';
	
		
	var numModulosContratados=0;
	/******************************************************************************************************************
		Comprobamos que no hay campos vacio.
		En caso contrario mostramos un mensaje informativo.
	*******************************************************************************************************************/

	function validar_datos(){	
		var count=0;
		var empresa =document.getElementById('empresa').value;
		var user =document.getElementById('user').value;
		var pass =document.getElementById('password').value;
		var email =document.getElementById('email').value;
		var dni =document.getElementById('dni').value;
		var ciudad =document.getElementById('ciudad').value;
		var cp =document.getElementById('cp').value;
		var provincia =document.getElementById('provincia').value;
		var tlf =document.getElementById('tlf').value;
		var tlf2 =document.getElementById('tlf2').value;
		var direccion =document.getElementById('direccion').value;
		var fechaSolicitudAlta = document.getElementById('fechaSolicitudAlta').value;
		
		//datosBanco
		var iban = document.getElementById('iban').value;
		var entidad = document.getElementById('entidad').value;
		var sucursal = document.getElementById('sucursal').value;
		var dc = document.getElementById('dc').value;
		var numCuenta = document.getElementById('numCuenta').value;
		
		
		//Array de los modelos
		var mod_array_type = [];
		var mod_array_pass = [];
		var mod_array_sim = [];
		var mod_array_imei = [];
		var mod_array_nom = [];
		//Aqui rellenamos los datos de los modulos
		for(var i=1;  i<=numModulosContratados; i++){
			mod_array_type[i] = $("#modulo0"+i+" option:selected").val();
			mod_array_pass[i] = $("#modulo0"+i+"_pass").val();
			mod_array_sim[i]  = $("#modulo0"+i+"_sim").val();
			mod_array_imei[i] = $("#modulo0"+i+"_imei").val();
			//console.log("modulo " +i+ "type " + mod_array_type[i]  + " pass:" + mod_array_pass[i] +  " sim: " + mod_array_sim[i] +" imei" + mod_array_imei[i] );
		}
			
	
		//no me acuerdo para que era esto!! :S
		for(var m = 1; m <=numModulosContratados; m++) { 
			valor = $("#modulo0"+m+" option:selected").val();
			//console.log("#modulo0"+m+":" + valor );
		}
		
		//Controlamos Modulos
		for(i=1;  i<=numModulosContratados; i++){
			
			//Controlamos Modulo campos vacios
			if((mod_array_type[i] == 0) || (mod_array_pass[i] == "" ) || (mod_array_sim[i] == "") || (mod_array_imei[i] == "") ){
				$('#errorMod0'+i).css('display','block');
			}else{
				count++;
				$('#errorMod0'+i).css('display','none');
				mod_array_nom[i]  = nombreModulo(mod_array_type[i]);
				//console.log("nombreModulo a: " +mod_array_nom[i]);
			}
			
			//Controlamos campo vacio PASS
			if(mod_array_pass[i] == "" ){
				$("#modulo0"+i+"_pass").removeClass("campoTrue").addClass("campoFalse");
			}else{
				count++;
				$("#modulo0"+i+"_pass").removeClass("campoFalse").addClass("campoTrue");
			}
			
			//Controlamos campo vacio SIM
			if(mod_array_sim[i] == "" ){
				$("#modulo0"+i+"_sim").removeClass("campoTrue").addClass("campoFalse");
			}else{
				count++;
				$("#modulo0"+i+"_sim").removeClass("campoFalse").addClass("campoTrue");
			}
			
			//Controlamos campo vacio IMEI
			if(mod_array_imei[i] == "" ){
				$("#modulo0"+i+"_imei").removeClass("campoTrue").addClass("campoFalse");
			}else{
				count++;
				$("#modulo0"+i+"_imei").removeClass("campoFalse").addClass("campoTrue");
			}
		
		}
		
		
		//Controlamos que haya seleccionado el numero de modulos a contratar
		if(numModulosContratados == 0){
			$('#numModulosContratar_div').css('display','block');
		}else{
			count++;
		}
		
		
	
		//Controlamos validación nombre de empresa
		if(fechaSolicitudAlta==""){
			$('#fechaSolicitudAlta_div').css('display','block');
			$("#fechaSolicitudAlta").removeClass("campoTrue").addClass("campoFalse");
		}else{
			count++;
			$('#fechaSolicitudAlta_div').css('display','none');
			$("#fechaSolicitudAlta").removeClass("campoFalse").addClass("campoTrue");
		}
		
		//Controlamos validación nombre de empresa
		if(empresa==''){
			$('#empresa_div').css('display','block');
			$("#empresa").removeClass("campoTrue").addClass("campoFalse");
		}else{
			count++;
			$('#empresa_div').css('display','none');
			$("#empresa").removeClass("campoFalse").addClass("campoTrue");
		}
		
		//Controlamos validación nombre de usuario
		if(user ==''){
			$('#user_div').css('display','block');
			$("#user").removeClass("campoTrue").addClass("campoFalse");
		}else{
			count++;		
			$('#user_div').css('display','none');	
			$("#user").removeClass("campoFalse").addClass("campoTrue");
		}
		
		//Controlamos validación password
		if(pass == ''){ 
			$('#password_div').css('display','block');			
			$("#password").removeClass("campoTrue").addClass("campoFalse");
		}else{
			count++;
			$('#password_div').css('display','none');			
			$("#password").removeClass("campoFalse").addClass("campoTrue");
		}
	
		//Controlamos validación email
		if(email==''){
			$('#email_div').css('display','block');	
			$("#email").removeClass("campoTrue").addClass("campoFalse");
		}else{
			count++;
			$('#email_div').css('display','none');			
			$("#email").removeClass("campoFalse").addClass("campoTrue");
		}

		//Controlamos validación CIF/NIF empresa
		if(dni==''){
			$('#dni_div').css('display','block');		
			$("#dni").removeClass("campoTrue").addClass("campoFalse");			
		}else{
			count++;
			$('#dni_div').css('display','none');		
			$("#dni").removeClass("campoFalse").addClass("campoTrue");
		}
		
		//Controlamos validación ciudad
		if(ciudad==''){
			$('#ciudad_div').css('display','block');
			$("#ciudad").removeClass("campoTrue").addClass("campoFalse");
		}else{
			count++;
			$('#ciudad_div').css('display','none');
			$("#ciudad").removeClass("campoFalse").addClass("campoTrue");
		}

		//Controlamos validación CP
		if(cp==''){
			$('#cp_div').css('display','block');
			$("#cp").removeClass("campoTrue").addClass("campoFalse");
		}else{
			count++;
			$('#cp_div').css('display','none');
			$("#cp").removeClass("campoFalse").addClass("campoTrue");
		}
		
		//Controlamos validación provincia
		if(provincia==''){
			$('#provincia_div').css('display','block');
			$("#provincia").removeClass("campoTrue").addClass("campoFalse");
		}else{
			count++;
			$('#provincia_div').css('display','none');
			$("#provincia").removeClass("campoFalse").addClass("campoTrue");
		}

		//Controlamos validación telefono fijo
		if(tlf==''){
			$('#tlf_div').css('display','block');
			$("#tlf").removeClass("campoTrue").addClass("campoFalse");
		}else{
			count++;
			$('#tlf_div').css('display','none');
			$("#tlf").removeClass("campoFalse").addClass("campoTrue");
		}

		//Controlamos validación telefono de contacto
		if(tlf2==''){
			$('#tlf2_div').css('display','block');
			$("#tlf2").removeClass("campoTrue").addClass("campoFalse");
		}else{
			count++;
			$('#tlf2_div').css('display','none');
			$("#tlf2").removeClass("campoFalse").addClass("campoTrue");
		}

		//Controlamos validación direccion
		if(direccion==''){
			$("#direccion_div").css('display','block');
			$("#direccion").removeClass("campoTrue").addClass("campoFalse");
		}else{
			count++;
			$("#direccion_div").css('display','none');
			$("#direccion").removeClass("campoFalse").addClass("campoTrue");
		}

		
		//Controlamos validacion datos Bancarios
		var datosBanco='';
		var countDBanco =0;
		var errorDatosBanco='* Campos Obligatorio, te has olvidado introducir los siguientes campos: ';
		if(iban ==''){
			$("#iban").removeClass("campoTrue").addClass("campoFalse");
			errorDatosBanco += ' iban, ';
		}else{
			countDBanco++;
			datosBanco += iban;			
			$("#iban").removeClass("campoFalse").addClass("campoTrue");
		}
		
		if(entidad ==''){
			$("#entidad").removeClass("campoTrue").addClass("campoFalse");
			errorDatosBanco += ' entidad, ';
		}else{
			countDBanco++;
			datosBanco += '-'+entidad;
			$("#entidad").removeClass("campoFalse").addClass("campoTrue");
		}
		
		if(sucursal ==''){
			$("#sucursal").removeClass("campoTrue").addClass("campoFalse");		
			errorDatosBanco += ' sucursal, ';
		}else{
			countDBanco++;
			datosBanco += '-'+sucursal;
			$("#sucursal").removeClass("campoFalse").addClass("campoTrue");
		}
		
		if(dc ==''){
			$("#dc").removeClass("campoTrue").addClass("campoFalse");		
			errorDatosBanco += ' dc, ';
		}else{
			countDBanco++;
			datosBanco += '-'+dc;
			$("#dc").removeClass("campoFalse").addClass("campoTrue");
		}
		
		if(numCuenta ==''){
			$("#numCuenta").removeClass("campoTrue").addClass("campoFalse");		
			errorDatosBanco += ' numero de cuenta. ';
		}else{
			countDBanco++;
			datosBanco += '-'+numCuenta;
			$("#numCuenta").removeClass("campoFalse").addClass("campoTrue");
		}
		
		if(countDBanco!=5){
			$("#datosBancarios_div").css('display','block');
			$("#datosBancarios_div").html(errorDatosBanco);
		}else{
			$("#datosBancarios_div").css('display','none');
			count++;
		}
		//console.log('datos Bancarios: ' + datosBanco);
		
		//Si todo ha ido bien entramos y ejecutamo la funcion pasandole todos los parametros que contiene la informacion del formulario		
		var numModulosContratadosTotal = eval(numModulosContratados) * 4;
		var numValidaciones = 14 + eval(numModulosContratadosTotal);
		//Controlamos por pantalla que esten todos los campos rellenos
		/*
		console.log("/////////// FORM ALTA /////////////////////////");
		console.log(" Empresa: " + empresa);
		console.log(" Usuario: " + user);
		console.log(" Pass: " + pass);
		console.log(" Email: " + email);
		console.log(" CIF/DNI: " + dni);
		console.log(" direccion: " + direccion);
		console.log(" poblacion: " + ciudad);
		console.log(" C.postal: " + cp);
		console.log(" provincia: " + provincia);
		console.log(" telf fijo: " + tlf);
		console.log(" telf mvl: " + tlf2);
		console.log(" f.alta: " + fechaSolicitudAlta);
		console.log(" d.banco: " + datosBanco);
		console.log(" Num Modulos: " + numModulosContratados);
		
		console.log(" mod_01: " + mod_01 + " pass: " + mod_01pass + " sim:  " + mod_01sim + " Nom: "  + mod_01nom);
		console.log(" mod_02: " + mod_02 + " pass: " + mod_02pass + " sim:  " + mod_02sim + " Nom: "  + mod_02nom);
		console.log(" mod_03: " + mod_03 + " pass: " + mod_03pass + " sim:  " + mod_03sim + " Nom: "  + mod_03nom);
		console.log(" mod_04: " + mod_04 + " pass: " + mod_04pass + " sim:  " + mod_04sim + " Nom: "  + mod_04nom);
		console.log(" mod_05: " + mod_05 + " pass: " + mod_05pass + " sim:  " + mod_05sim + " Nom: "  + mod_05nom);
		console.log(" mod_06: " + mod_06 + " pass: " + mod_06pass + " sim:  " + mod_06sim + " Nom: "  + mod_06nom);
		
		
		console.log("////////////////////////////////////////////////");
		console.log(count + 'Siguiente paso guardar datos, ' + numValidaciones );
		*/
		if(count == numValidaciones){
			//console.log(empresa +" , " + user +" , " + pass +" , " + email +" , " + dni +" , " + direccion +" , " + ciudad +" , " + cp +" , " + provincia +" , " + tlf +" , " + tlf2 +" , " + fechaSolicitudAlta +" , " + datosBanco +" , " + numModulosContratados +" , " + mod_array_type +" , " + mod_array_pass +" , " + mod_array_sim +" , " +	mod_array_imei+" , " +	mod_array_nom);
			login(empresa,user, pass, email, dni, direccion, ciudad, cp, provincia, tlf, tlf2, fechaSolicitudAlta, datosBanco,  numModulosContratados, mod_array_type , mod_array_pass , mod_array_sim ,	mod_array_imei,	mod_array_nom);
		}
	}

	/******************************************************************************************************************
		Vaciamos el input
	*******************************************************************************************************************/
	function vaciarInput(idInput){
		document.getElementById(idInput+'_div').style.display ='none';
	//	document.getElementById(idInput).style.border = "none";
		$('#'+idInput).css('border', 'none');
	}



	/********************************************************************************************************
		Llamamos al fichero mandanga.php que es el que se encarga de:
			- Crear la nueva bd del cliente
			- Añadir la información del formulario en la bd eude tabla 'usuarios'
			- Enviar los email a los usuarios que hay en la tabla 'backoffice' de la bd eude
		Una vez ejecutado nos mostrara por pantalla de como ha ido todo el proceso anterior (bien o no).
	********************************************************************************************************/
	//function login(empresa,user, pass, email, dni, direccion, ciudad, cp, provincia, tlf, tlf2, fechaSolicitudAlta, datosBanco,  numModulosContratados, mod_01, mod_02, mod_03, mod_04, mod_05, mod_06, mod_01pass, mod_02pass, mod_03pass, mod_04pass, mod_05pass, mod_06pass,  mod_01sim, mod_02sim, mod_03sim, mod_04sim, mod_05sim, mod_06sim, mod_01nom, mod_02nom, mod_03nom, mod_04nom, mod_05nom, mod_06nom, mod_01imei, mod_02imei, mod_03imei, mod_04imei, mod_05imei, mod_06imei ){
	function login(empresa,user, pass, email, dni, direccion, ciudad, cp, provincia, tlf, tlf2, fechaSolicitudAlta, datosBanco,  numModulosContratados, mod_array_type , mod_array_pass , mod_array_sim ,	mod_array_imei,	mod_array_nom){
		 //console.log("Funcion loguin!! ");
		 //console.log(empresa +" , " + user +" , " + pass +" , " + email +" , " + dni +" , " + direccion +" , " + ciudad +" , " + cp +" , " + provincia +" , " + tlf +" , " + tlf2 +" , " + fechaSolicitudAlta +" , " + datosBanco +" , numModulosContratados " + numModulosContratados +" , " + mod_array_type +" , " + mod_array_pass +" , " + mod_array_sim +" , " +	mod_array_imei+" , " +	mod_array_nom);
		
		$.ajax({
			type: 'POST',
			url: './bbdd/mandanga.php',
			data: { 
				'empresa': empresa,
				'user': user,
				'pass': pass,
				'email': email,
				'dni': dni,
				'direccion': direccion,
				'ciudad': ciudad,
				'cp': cp,
				'provincia': provincia,
				'tlf': tlf,
				'tlf2': tlf2,
				'fechaSolicitudAlta': fechaSolicitudAlta,
				'datosBanco': datosBanco,
				'numModulosContratados': numModulosContratados,
				'mod_array_type': mod_array_type,				
				'mod_array_pass': mod_array_pass,							
				'mod_array_sim': mod_array_sim,			
				'mod_array_imei': mod_array_imei,				
				'mod_array_nom': mod_array_nom,
			},
			success: function(response) {
				var json = $.parseJSON(response);
				//Datos incorrectos, informamos con un mensje y limpiamos inputs
				if(json['success']==0){
					//alert('Se ha producido un fallo al crear al nuevo usuario, ponte en contacto con el administrador.');
					document.location= "sinexito.php";
				}else{	//Datos correctosm, redirijimos la pagina
					//alert('Todo ha ido bien!! Se ha creado correctamete la bbdd.');
					document.location= "conexito.php";
				}
			}
		});
		
		
	}	
	
	
	
	

	
	/******************************************************************************************************
		Funcion input textbox + tecla intro
	******************************************************************************************************/
	function teclaIntro(inField, e) {
		var charCode;
    
		if(e && e.which){
			charCode = e.which;
		}else if(window.event){
			e = window.event;
			charCode = e.keyCode;
		}

		if(charCode == 13) {  //Hace la misma funcion que el boton buscar
			validar_datos();
		}
	}


	/******************************************************************************************************
		DEvolvemos el nombre del modulo segun su identificador
	******************************************************************************************************/
	function nombreModulo(idMod){
		//console.log('nombreModulo entre y estoy mirando  idMod: ' + idMod);
		//hacemos select que devuelva 	
		/*
		$.ajax({
			data:{
				'idMod' : idMod, 
			},
			type:'GET',
			url: 'http://'+location.hostname+'/io/nombreModulo.php',
			success:function(response){
				var datos = $.parseJSON(response);
				var idTipoCamara = datos['idTipoCamara'];
				var nombreCamara = datos['nombre'];
				var descripcionCamara = datos['descripcion'];
				//console.log('(nombreModulo) idTipoCamara: ' + idTipoCamara);
				//console.log('(nombreModulo) nombre: ' + nombre);
				//console.log('(nombreModulo) descripcionCamara: ' + descripcionCamara);
				return nombreCamara;
			}
		});
		
		*/
		
		if(idMod==1){ return aux_mod_nom='Arnes'; }
		if(idMod==2){ return aux_mod_nom='Mochila'; }
		if(idMod==3){ return aux_mod_nom='Maletin'; }
		if(idMod==4){ return aux_mod_nom='Box_3G_sin_GPS'; }
		if(idMod==5){ return aux_mod_nom='Box_3G_con_GPS'; }
		if(idMod==6){ return aux_mod_nom='Box_3G_PTZ_y_GPS';}
		if(idMod==7){ return aux_mod_nom='Torre_6M';}
		if(idMod==8){ return aux_mod_nom='Smartwatch';}
		
		
	}
	
	

	/******************************************************************************************************
		¿?
	******************************************************************************************************/		
	function seleccionModulo(numModulo){
		// Así accedemos al Texto de la opción seleccionada
		var valor = $("#modulo0"+numModulo+" option:selected").html();
		//console.log('Select seleccionado: ' + valor);
	}
	
	/******************************************************************************************************
		Añadimos los tipos de camaras que hay a los select
	******************************************************************************************************/						
	function listadoTipoCamaras(numModulosContratados){
		$.ajax({
			data:{},
			type:'GET',
			url: 'http://'+location.hostname+'/io/selectTipoCamara.php',
			success:function(response){
				var datos = $.parseJSON(response);
				//console.log('datos entero: ' + datos);						
											
				var txt_add_select ='';
				for(var i = 0; i < datos.length; i++) {  
					var idTipoCamara = datos[i]['idTipoCamara'];
					var nombreCamara = datos[i]['nombre'];
					var descripcionCamara = datos[i]['descripcion'];
					txt_add_select += '<option name="moduloCamara" value="'+ idTipoCamara +'"  title="'+ descripcionCamara  +'">'+ nombreCamara + '</option>';
				}
				
				for(var i = 0; i <=numModulosContratados ; i++) {  
					$("#modulo0"+i).html(txt_add_select);
				}
			}
		});
	}


	/***************************************************************************************************************
		Primero limpiamos y despues llamamos a la funcion que se encarga de cargar lod bloques de los modulos
	***************************************************************************************************************/		
		function cargarModulos(){
			$("#bloquesModulos").html('');
			numModulosContratados =  $("#numModulos_inputext").val();
			//console.log('entre lenghtMod: ' + numModulosContratados);
			anyadirModulos(numModulosContratados);
		}
		
	/***************************************************************************************************************
		Montamos y mostramos los bloques de los modulos
	***************************************************************************************************************/		
		function anyadirModulos(numModulosContratados){
			listadoTipoCamaras(numModulosContratados);
			//console.log(" estoy en anyadirModulos ");
			var bloquesModulosMas = '' ;
			bloquesModulosMas += '<table style="width: 100%;">';
				
			for(var num = 1; num <=numModulosContratados; num++) { 
				bloquesModulosMas += '<tr >';
				bloquesModulosMas += '<td style="border:1px solid;">';
				bloquesModulosMas += '<label class="texto height20"> Modulo 0'+num+': </label>';					
				bloquesModulosMas += '</td>';									
				bloquesModulosMas += '<td id="tr_box_modulo0'+num+'" class="" style="border:1px solid;">';
				bloquesModulosMas += '<div>';		
				bloquesModulosMas += '<b>Nombre modulo:</b> <select id="modulo0'+num+'" onclick="seleccionModulo('+num+');" style="margin-right: 10px;"></select>';
				bloquesModulosMas += '<div id="modulo0'+num+'_desc"></div>';
				bloquesModulosMas += '</div>';
				bloquesModulosMas += '<div class="datosMod">';
				bloquesModulosMas += '<b>PASS:</b> <input type="text" id="modulo0'+num+'_pass" placeholder="Password modulo 0'+num+'" title="Introduce una contrasenya para la camara"  class="inputPassMod medidasInputMod marginL10"> ';
				bloquesModulosMas += '<br/>';
				bloquesModulosMas += '<b>SIM: </b>  <input type="text" id="modulo0'+num+'_sim"  placeholder="Num SIM modulo 0'+num+'" title="Introduce el num de tarjeta sim de la camara"   class="inputSimMod medidasInputMod marginL21"> ';
				bloquesModulosMas += '<br/>';
				bloquesModulosMas += '<b>IMEI: </b>  <input type="text" id="modulo0'+num+'_imei"  placeholder="Num IMEI modulo 0'+num+'" title="Introduce el num imei del dispositivo"  class="medidasInputMod marginL19"> ';
				bloquesModulosMas += '</div>';
				bloquesModulosMas += '<div id="errorMod0'+num+'" class="campoObligatorio"  style="display:none;">* Campos Obligatorios, asegurate que has rellenado todo.</div>';
				bloquesModulosMas += '</td>';
				bloquesModulosMas += '</tr>';
			}
			bloquesModulosMas += '</table>';
			$("#bloquesModulos").append(bloquesModulosMas);
		}