
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>StreamGPS</title>
		<link rel="shortcut icon" href="http://streamgps.com/backoffice/favicon.ico">
		<meta name="description" content="description">
		<meta name="author" content="DevOOPS">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="plugins/bootstrap/bootstrap.css" rel="stylesheet">
		<link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
		<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<link href='http://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'>
		<link href="plugins/fancybox/jquery.fancybox.css" rel="stylesheet">
		<link href="plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
		<link href="plugins/xcharts/xcharts.min.css" rel="stylesheet">
		<link href="plugins/select2/select2.css" rel="stylesheet">
		<link href="plugins/justified-gallery/justifiedGallery.css" rel="stylesheet">
		<link href="css/style_v3.css" rel="stylesheet">
		<link href="plugins/chartist/chartist.min.css" rel="stylesheet">
		<link href="plugins/bootstrap-fileinput/css/fileinput.css" rel="stylesheet">
		<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=false"></script>
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
				<script src="http://getbootstrap.com/docs-assets/js/html5shiv.js"></script>
				<script src="http://getbootstrap.com/docs-assets/js/respond.min.js"></script>
		<![endif]-->
<style>
		
		#content ,
		.box-content { 
			background: grey none repeat scroll 0% 0%;
			color:#fff
		}
		
		#breadcrumb,
		#sidebar-left		{
			background: #404040 none repeat scroll 0% 0%;
		}
		
		#breadcrumb{
			line-height: 14px;
			padding-bottom: 10px;
		}
		
		.expand-link,
		.close-link,
		.collapse-link {
			padding-top: 4px !important;
		}
		
		.bg-success,
		.bg-warning,
		.bg-danger{
    padding: 15px;
    border-radius: 5px;
    text-align: center;
}


	legend{
		color: #fff;
		font-weight:bold;
	}
	
	.main-menu .dropdown-menu > li > a {
		padding: 9px 15px 9px 30px;
		color: #F0F0F0;
	}
	
	.nivel3-menu {
		padding-left: 15px;
		color: #fff;
	}
	
	
		</style>
		
	</head>
	
	<body>
		<?php 
			session_start(); 
			//Miramos si hay o no sesion, 
			if(empty($_SESSION)){
				header('Location: http://streamgps.com/backoffice/');
			}
			/*
			else{
				echo "<pre>";print_r ($_SESSION);echo "</pre>";			
			}
			*/
			
			
		?>
		<!--Start Header-->
		<div id="screensaver">
			<canvas id="canvas"></canvas>
			<i class="fa fa-lock" id="screen_unlock"></i>
		</div>

		<div id="modalbox">
			<div class="devoops-modal">
				<div class="devoops-modal-header">
					<div class="modal-header-name">
						<span>Basic table</span>
					</div>
					<div class="box-icons">
						<a class="close-link">
							<i class="fa fa-times"></i>
						</a>
					</div>
				</div>
				<div class="devoops-modal-inner"></div>
				<div class="devoops-modal-bottom"></div>
			</div>
		</div>
		
		<header class="navbar">
			<div class="container-fluid expanded-panel">
				<div class="row">
					<div id="logo" class="col-xs-12 col-sm-2" style="display:none;">
						<a href="index_v1.html"> StreamGPS </a>
					</div>
					<div id="top-panel" class="col-xs-12 "> <!--col-sm-10 -->
						<div class="row">
					
							<div class="col-xs-8 col-sm-4">
								<!--
								<div id="search">
									<input type="text" placeholder="search"/>
									<i class="fa fa-search"></i>
								</div>
								-->
							</div>
					
							<div class="col-xs-4 col-sm-8 top-panel-right">
								<?php require("menu_dropdown.php"); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>
		<!--End Header-->

		<!--Start Container-->
		<div id="main" class="container-fluid">
			<div class="row">
				<div id="sidebar-left" class="col-xs-2 col-sm-2">
					<?php require("menu_main.php"); ?>			
				</div>
				<!--Start Content-->
				<div id="content" class="col-xs-12 col-sm-10">
					<div id="about">
						<div class="about-inner">
							<h4 class="page-header">Open-source admin theme for you</h4>
							<p>DevOOPS team</p>
							<p>Homepage - <a href="http://devoops.me" target="_blank">http://devoops.me</a></p>
							<p>Email - <a href="mailto:devoopsme@gmail.com">devoopsme@gmail.com</a></p>
							<p>Twitter - <a href="http://twitter.com/devoopsme" target="_blank">http://twitter.com/devoopsme</a></p>
							<p>Donate - BTC 123Ci1ZFK5V7gyLsyVU36yPNWSB5TDqKn3</p>
						</div>
					</div>
					<div class="preloader">
						<img src="img/devoops_getdata.gif" class="devoops-getdata" alt="preloader"/>
					</div>
					<!--Start Breadcrumb-->
					<div class="row">
						<div id="breadcrumb" class="col-xs-12">
							<a href="#" class="show-sidebar">
								<i class="fa fa-bars"></i>
							</a>
							<ol id="contenidoBreadcrumb" class="breadcrumb pull-left">			
							</ol>
							<div id="social" class="pull-right"></div>
						</div>
					</div>

					<!--End Breadcrumb-->
					<div id="ajax-content"></div>
				</div>
				<!--End Content-->
			</div>
		</div>
		<!--End Container-->


		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<!--<script src="http://code.jquery.com/jquery.js"></script>-->
		<!--<script src="plugins/jquery/jquery.min.js"></script>-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
		<script type="text/javascript" src="plugins/jquery-numeric/jquery.numeric.js" ></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		
		<script src="plugins/bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
        <script src="plugins/bootstrap-fileinput/js/fileinput_locale_fr.js" type="text/javascript"></script>
        <script src="plugins/bootstrap-fileinput/js/fileinput_locale_es.js" type="text/javascript"></script>
		
		
		
		<script src="plugins/bootstrap/bootstrap.min.js"></script>
		<script src="plugins/justified-gallery/jquery.justifiedGallery.min.js"></script>
		<script src="plugins/tinymce/tinymce.min.js"></script>
		<script src="plugins/tinymce/jquery.tinymce.min.js"></script>
		
		<!-- All functions for this theme + document.ready processing -->
		<script src="js/devoops.js"></script>
		<script src="js/scriptStreamgps.js"> </script>
		<script src="js/scriptTextos.js"> </script>
		<script src="js/scriptInputs.js"> </script>
		<script src="js/menuLateral.js"> </script>
		
		<!--
		
        
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
       
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js" type="text/javascript"></script>
		-->
		<script> 
		$(document).ready(function() { cargar_menu_laterl(); });
		</script> 
		
	</body>
</html>
