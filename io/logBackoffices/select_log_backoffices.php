<?php 
	require_once("../funciones.php");
	conectar_bbdd('eude');
	require_once("../conexion/dbi_connect.php");
		
	# Recogemos datos
	$idSeleccionado = comprobarParametros('idSelecionado');
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	# echo "GET<pre>"; print_r($_GET); echo "</pre>"; echo "POS<pre>"; print_r($_POST); echo "</pre>";
	
	# Preparamos y ejecutamos la consulta
	$sql = "SELECT id, idUser, nameUser, ip, fecha_inicio, fecha_fin FROM eude.log_backoffice_sessions ";

	
	if($idSeleccionado){
		$consulta = $sql . " WHERE  id = ? 	ORDER BY fecha_inicio DESC";		
		$stmt = $mysqli->prepare($consulta);		
		$stmt->bind_param("i", $idSeleccionado);
	}else{
		$consulta = $sql . " ORDER BY fecha_inicio DESC";
		$stmt = $mysqli->prepare($consulta);		
	}
	$stmt->execute();
	$stmt->bind_result($id, $idUser, $nameUser, $ip, $fecha_inicio, $fecha_fin);
	
	while( $stmt->fetch() ) {
		
		$data[] = array(
			"id"=>$id, 
			"idUser"=>$idUser, 
			"nameUser"=>$nameUser, 
			"fecha"=>desfaseHorario($fecha_inicio, $tiempoDesfase, "fecha", "suma") , 
			"fechaFin"=>controlFechaModificacion($fecha_fin, $tiempoDesfase, "fecha", "suma"), 
			"ip"=>$ip, 
			"accion"=> "select",
			"tabla"=> "log_backoffices",
			"nomFichero"=> "select_log_backoffices.php"		
		);
	}	
	
	$stmt->close();
	# echo '<pre>'; print_r($data);echo '</pre>';
	echo json_encode($data); 
	$mysqli->close();
?>