<?php 
	require_once("../funciones.php");
	conectar_bbdd('eude');
	include_once("../conexion/dbi_connect.php");
	$data = array();
	
	# Recogemos datos
	$idSelect = comprobarParametros ('idSelect');
	$tiempoDesfase = comprobarParametros ('tiempoDesfase');
	// echo "GET<pre>"; print_r($_GET); echo "</pre>"; echo "POS<pre>"; print_r($_POST); echo "</pre>";

	# Consulta
	$sql = "SELECT
					de.id,
					de.nombre,
					de.descripcion,
					de.numSerie,
					de.modelo,
					de.versionSO,
					de.fechaAlta,
					de.fechaModificacion,
					de.asignado
				FROM eude.dispositivo_emisor as de;";
	
	# Preparamos y ejecutamos la consulta
	if($idSelect){
		$consulta = $sql . " where de.id = ?";
		$stmt = $mysqli->prepare($consulta);		
		$stmt->bind_param("i", $idSelect);
	}else{	
		$stmt = $mysqli->prepare($sql);		
	}
	$stmt->execute();
	
	# Vincular variables a la sentencia preparada
	$stmt->bind_result($id, $nombre, $descripcion, $numSerie,  $modelo,  $versionSO, $fechaAlta, $fechaModificacion, $asignado  );
	
	#  Obtenemos los valores
	while($stmt->fetch() ) {	
				
		$data[] = array(
			"id"=>$id, 
			"nombre"=>utf8_decode($nombre), 
			"descripcion"=>utf8_decode($descripcion), 
			"numSerie"=>utf8_decode($numSerie), 
			"modelo"=>utf8_decode($modelo), 
			"versionSO"=>utf8_decode($versionSO), 
			"fechaAlta"=>desfaseHorario($fechaAlta,  $tiempoDesfase, "entera", "suma"),			
			"fechaModificacion"=>controlFechaModificacion($fechaModificacion, $tiempoDesfase, "entera", "suma"),
			"asignado"=>$asignado, 			
			"accion"=> "select",
			"tabla"=> "dispositivoEmision",
			"nomFichero"=> "select_dispositivo_emisor.php"							
		);
	}	
	
	$stmt->close();
	# echo "<pre>"; print_r($data); echo "</pre>";
	echo json_encode(utf8ize($data));
	# $stmt->mysqli();
?>