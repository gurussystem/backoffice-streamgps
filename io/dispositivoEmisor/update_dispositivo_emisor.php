 <?php
	require_once("../funciones.php");
	conectar_bbdd('eude');
	include_once("../conexion/dbi_connect.php");
	$data = array();
	
	# Recogemos datos
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	$idSelect = comprobarParametros('idSelect');
	$nombre = comprobarParametros('nombre');
	$descripcion = comprobarParametros('descripcion');
	$numSerie = comprobarParametros('numSerie');
	$modelo = comprobarParametros('modelo');
	$versionSO = comprobarParametros('versionSO');
	# echo "GET<pre>"; print_r($_GET); echo "</pre>"; 	echo "POS<pre>"; print_r($_POST); echo "</pre>";
	
	# Preparamos y ejecutamos la consulta	
	$stmt = $mysqli->prepare("UPDATE eude.dispositivo_emisor
												SET
													nombre = ?,
													descripcion = ?,
													numSerie= ?,
													modelo=?,
													versionSO= ?,
													fechaModificacion = CURRENT_TIMESTAMP
												WHERE
													id = ?");
													
	$stmt->bind_param("sssssi", $nombre, $descripcion, $numSerie, $modelo, $versionSO, $idSelect);
	$stmt->execute();
	
	
	# Evaluamos is ha ido todo bien o habido algun fallo
	if($stmt->errno){
		$response=0;
		$proceso = 'error';
	}else{
		$response=1;
		$proceso = 'ok';
	}
		
	# Montamos el array 
	$data[] = array(
		"id"=>$idSelect, 
		"nombre"=>$nombre, 
		"descripcion"=>$descripcion , 
		"numSerie"=>$numSerie , 
		"modelo"=>$modelo , 
		"versionSO"=>$versionSO , 	
		"response" => $response,
		"proceso" => $proceso,
		"accion"=> "update",
		"tabla"=> "dispositivoEmisor",
		"nomFichero"=> "update_dispositivo_emisor.php",
	);

	$stmt->close();
	echo json_encode($data); 
	$mysqli->close();
?>