<?php
	require_once("../funciones.php");
	conectar_bbdd('eude');
	include_once("../conexion/dbi_connect.php");
	$data = array();
		
	# Recogemos datos
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	$nombre = comprobarParametros('nombre');
	$descripcion = comprobarParametros('descripcion');
	$numSerie = comprobarParametros('numSerie');
	$modelo = comprobarParametros('modelo');
	$versionSO = comprobarParametros('versionSO');

	# echo "GET<pre>"; print_r($_GET); echo "</pre>"; echo "POS<pre>"; print_r($_POST); echo "</pre>";
		
	# Preparamos y ejecutamos la consulta
	$stmt = $mysqli->prepare("INSERT INTO eude.dispositivo_emisor (nombre, descripcion, numSerie, modelo, versionSO)  VALUES (?, ?,?,?,?)");		
	$stmt->bind_param("sssss", $nombre, $descripcion, $numSerie, $modelo, $versionSO);
	$stmt->execute();
	
	# Evaluamos is ha ido todo bien o habido algun fallo
	if($stmt->errno){
		$response=0;
		$proceso = 'error';
	}else{
		$response=1;
		$proceso = 'ok';
	}
	
	# Consultamos el ultimo id insertado.
	$result=$mysqli->query("SELECT LAST_INSERT_ID() AS ultimoID");		
	$row=$result->fetch_assoc();
		
	# Montamos el array 
	$data[] = array(
		"id"=>$row['ultimoID'], 
		"nombre"=>$nombre, 
		"descripcion"=>$descripcion , 
		"numSerie"=>$numSerie , 
		"modelo"=>$modelo , 
		"versionSO"=>$versionSO , 
		"response" => $response,
		"proceso" => $proceso,
		"accion"=> "insert",
		"tabla"=> "dispositivoEmisor",
		"nomFichero"=> "insert_dispositivo_emisor.php",
	);
	
	
	$stmt->close();
	echo json_encode($data); 
	$mysqli->close();	
?>