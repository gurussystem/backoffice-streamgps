<?php
	require_once("../funciones.php");
	include_once("funcionesProductos.php");
	conectar_bbdd('eude');
	include_once("../conexion/dbi_connect.php");
	$data = array();

	# Recogemos datos
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	$idProducto = comprobarParametros('idProducto');
	$nombre = comprobarParametros('nombre'); 
	$descripcion = comprobarParametros('descripcion'); 
	$idTipoProducto = comprobarParametros('idTipoProducto'); 
	$idCamara = comprobarParametros('idCamara'); 
	$idRouter = comprobarParametros('idRouter'); 
	$idSim = comprobarParametros('idSim'); 	
	$idDispositivoEmisor = comprobarParametros('idDispositivoEmisor'); 	
	$idEmpresa = comprobarParametros('idClienteSeleccion'); 	

	$idCamaraAux = comprobarParametros('idCamaraAux'); 
	$idRouterAux = comprobarParametros('idRouterAux'); 
	$idSimAux = comprobarParametros('idSimAux'); 
	$idDispositivoEmisorAux = comprobarParametros('idDispositivoEmisorAux'); 
	$idEmpresaAux = comprobarParametros('idClienteSeleccionAux'); 
	
	$nameTipoProducto = comprobarParametros('nameTipoProducto'); 
	$nameCamara = comprobarParametros('nameCamara'); 
	$nameRouter = comprobarParametros('nameRouter'); 
	$nameSim = comprobarParametros('nameSim'); 
	$nameDispositivoEmisor = comprobarParametros('nameDispositivoEmisor'); 
	$nameEmpresa = comprobarParametros('nameClienteSeleccion'); 
	
	$usuario = comprobarParametros('usuario'); 				
	$clave = comprobarParametros('clave'); 				
	$email = comprobarParametros('email'); 		

	$serialnum_camara = "";
	$dyndns_router = "";	
	 # echo " <pre>"; print_r($_GET); echo "</pre"; echo "<pre>"; print_r($_POST); echo "</pre>";
		
	
	# TipoProducto 'CAJA'
	if(($idTipoProducto == '1') || ($idTipoProducto == '4')){ // caja hikvision o caja dahua
		
		if($idCamaraAux != $idCamara){
			# 2.1 Liberar camara anterior -> Update en eude.camara asignar = 0 where camOld 
			asignar_camara($mysqli, $idCamaraAux, 0);
		
			# 2.2 Asignar nueva camara -> Update en eude.camara asignar = 1 where camNew
			asignar_camara($mysqli, $idCamara, 1);
		}
		
		if($idRouterAux != $idRouter){
			# 3.1 Liberar router anterior -> Update en eude.routers asignar = 0 where routerOld 
			asignar_router($mysqli, $idRouterAux, 0);

			# 3.2 Asignar nueva router -> Update en eude.routers asignar = 1 where routerNew
			asignar_router($mysqli, $idRouter, 1);
		}
		
		
		# Sacamos el campo 'serialnum_camara' de la tabla camara
		$serialnum_camara = select_serialNum_camara($mysqli, $idCamara);
				
		# Sacamos el campo 'dyndns_router' de la tabla router	
		$dyndns_router = select_dyndns_router($mysqli, $idRouter);
		
	
	}

	# TipoProducto 'EMISION PERSONAL'
	if($idTipoProducto == '3'){
		if($idDispositivoEmisorAux != $idDispositivoEmisor){
			# 5.1 Liberar dispositivoEmisor anterior -> Update en eude.sim asignar = 0 where dispositivoEmisor Old 
			asignar_dispositivoEmisor($mysqli, $idDispositivoEmisorAux, 0);
			
			# 5.2 Asignar nueva dispositivoEmisor  -> Update en eude.dispositivoEmisor  asignar = 1 where dispositivoEmisor New
			asignar_dispositivoEmisor($mysqli, $idDispositivoEmisor, 1);
		}
	}

	# TipoProducto 'CAJA' o 'EMISION PERSONAL'
	if(($idTipoProducto == '1') || ($idTipoProducto == '3') ||  ($idTipoProducto == '4')){
		# 4.1 Liberar sim anterior -> Update en eude.sim asignar = 0 where simOld 
		asignar_sim($mysqli, $idSimAux, 0);
		
		# 4.2 Asignar nueva sim-> Update en eude.sim asignar = 1 where simNew
		asignar_sim($mysqli, $idSim, 1);
	}
		
	# TipoProducto 'APP'	
	if($idTipoProducto == '2'){
		# 6.1 cambiar usuario, contraseña, email --> Update en bd.login_users
		
	}

	
	# 7.3 Sacar bd_old con idEmpresa_old
	$nombreBDAux = select_bbdd($mysqli, $idEmpresaAux);
		
	# 7.4 Sacar bd_new con idEmpresa_new
	$nombreBD = select_bbdd($mysqli, $idEmpresa);
	
	
	# Si la empresa ha cambiado
	if($idEmpresa != $idEmpresaAux){
		# 7.1 Restar 1 en eude.empresa.licencia where idEmpresa_old
		$num_licenciasAux = count_licencias($mysqli, $idEmpresaAux);
		add_licencia($mysqli, $idEmpresaAux, 'eude', $num_licenciasAux);	
		
		# 7.2 Sumar 1 en eude.empresa.licencia where idEmpresa_new
		$num_licencias = count_licencias($mysqli, $idEmpresa);
		add_licencia($mysqli, $idEmpresa, 'eude', $num_licencias);

			
		# Conectamos con la BD eude para eliminar el producto 'la empresa old'
		$mysqli2=new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, $nombreBDAux);
	
		# 7.5 Eliminar el producto de la tabla producto_contratados de la bd_old
		delete_productos_contratados($mysqli2, $nombreBDAux, $idProducto);
		
		# 7.7 Eliminar el producto de la tabla ultimaPosicion de la bd_old
		delete_ultimaposicion($mysqli2, $nombreBDAux, $idProducto);
		
		# 7.9 Restar 1 en bd_old tabla info_cliente.licencia where idEmpresa_old
		add_licencia($mysqli2, $idEmpresa, $nombreBDAux, $num_licenciasAux);
		
		# 7.11 eliminar el producto en bd_old tabla login_users where idProducto
		if($idTipoProducto == '2') delete_login_users($mysqli2, $nombreBDAux, $idProducto);
			
		# Cerramos la conexion 'nombreBDAux'
		$mysqli2 -> close();
	
		
		///////// HACEMOS LAS CONSULTAS CON 'la empresa new'
		$mysqli3=new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, $nombreBD);
		
		# 7.6 Añadir el producto de la tabla producto_contratados de la bd_new
		add_productoContratado($mysqli3, $nombreBD, $idProducto, $idEmpresa, $serialnum_camara, $dyndns_router, $idTipoProducto,  $nameEmpresa );
		
		# 7.8 Añadir el producto de la tabla ultimaPosicion de la bd_new
		add_ultimaposicion($mysqli3, $nombreBD, $idProducto);
				
		# 7.10 Sumar 1 en bd_old tabla info_cliente.licencia where idEmpresa_new
		add_licencia($mysqli3, $idEmpresa, $nombreBD, $num_licencias);
		
		# 7.12 insertar el producto en bd_new tabla login_users where idProducto
		if($idTipoProducto == '2'){			
			include_once '../conexion/functions.php';
			if($clave== ""){$clave = $usuario; };
			 login_register_app($usuario,$email,$clave,$idEmpresa,"1",$mysqli3, $nombreBD, $idProducto);
			# if(loginUser($usuario,$clave,$mysqli3, $nombreBD)) echo "<br> ok login"; else echo "<br> LOGIN FAIL!!!";
		}
		
		# Cerramos la conexion 'nombreBD'
		$mysqli3 -> close();
	}

	else{
		
		# Conectamos con la BD eude para eliminar el producto 'la empresa old'
		$mysqli2=new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, $nombreBDAux);
		
		# 7.11 eliminar el producto en bd_old tabla login_users where idProducto
		if($idTipoProducto == '2') update_login_users($mysqli2, $nombreBDAux, $idProducto,  $usuario, $email);			
		
		
		if(($idTipoProducto == '1') ||  ($idTipoProducto == '4')){
			# 7.13 Modificamos el campo 'serialnum_camara' de la tabla producto_contratado 
			update_serialNum_productoContratado($mysqli2, $nombreBDAux, $serialnum_camara, $idProducto);
			
			# 7.14 Modificamos el campo 'dyndns_router' de la tabla producto_contratado 
			update_dyndns_router_productoContratado($mysqli2, $nombreBDAux, $dyndns_router, $idProducto);
			
		}
		
		$mysqli2 -> close();
		
	}


	
	
	# Modificar el producto de eude.productos
	/*
	1.0 Cambiar nombre, descripcion -> Update en eude.producto
	2.0 Cambiar idCamara -> Update en eude.producto
	3.0 Cambiar idRouter -> Update en eude.producto
	4.0 Cambiar idSim -> Update en eude.producto
	5.0 Cambiar idDispositivoEmisor -> Update en eude.producto
	6.0 cambiar usuario, contraseña, email -->  Update en eude.producto
	7.0 cambiar idEmpresa de eude.producto
	*/
	# volvemos a conectar con eude
	$mysqli=new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, 'eude');
	# echo "<br> idProd: " . $idProducto;
	$response = update_producto($mysqli, $idTipoProducto, $nombre, $descripcion, $idCamara, $idRouter,  $idSim, $idEmpresa, $idProducto, $idDispositivoEmisor, $usuario, $email);

	if($response == 1)
		$proceso = 'ok';
	else
		$proceso = 'error';
	
	# Montamos el array 
	$data[] = array(
		"id"=>$idProducto, 
		"nombre"=>$nombre, 
		"descripcion"=>$descripcion, 
		"idTipoProducto"=>$idTipoProducto, 
		"idCamara"=>$idCamara, 
		"idRouter"=>$idRouter, 
		"idSim"=>$idSim , 
		"idDispositivoEmisor"=>$idDispositivoEmisor , 
		"nameTipoProducto"=>$nameTipoProducto,
		"nameCamara"=>$nameCamara ,
		"nameRouter"=>$nameRouter,
		"nameSim"=>$nameSim, 
		"nameDispositivoEmisor"=>$nameDispositivoEmisor,
		"usuario"=>$usuario,
		"clave"=>$clave,
		"email"=>$email,	
		"response" => $response,
		"proceso" => $proceso,
		"accion"=> "update",
		"tabla"=> "producto",
		"nomFichero"=> "update_producto.php",
	);
	
	echo json_encode($data); 
	$mysqli->close();
	?>