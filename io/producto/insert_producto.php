<?php
	require_once("../funciones.php");
	include_once("funcionesProductos.php");
	conectar_bbdd('eude');
	include_once("../conexion/dbi_connect.php");
	$data = array();
	
	# Recogemos datos
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	$nombre = comprobarParametros('nombre'); 
	$descripcion = comprobarParametros('descripcion'); 
	$idTipoProducto = comprobarParametros('idTipoProducto');
	$idCamara = comprobarParametros('idCamara'); 
	$idRouter = comprobarParametros('idRouter'); 
	$idSim = comprobarParametros('idSim'); 	
	$nameTipoProducto = comprobarParametros('nameTipoProducto');
	$nameCamara = comprobarParametros('nameCamara'); 
	$nameRouter = comprobarParametros('nameRouter'); 
	$nameSim = comprobarParametros('nameSim'); 
	
	# nuevos
	$idDispositivoEmisor = comprobarParametros('idDispositivoEmisor'); 
	$nameEmpresa = comprobarParametros('nameClienteSeleccion'); 
	$idEmpresa = comprobarParametros('idClienteSeleccion'); 
	$usuario = comprobarParametros('usuario'); 
	$clave = comprobarParametros('clave'); 	
	$email = comprobarParametros('email'); 
	# echo "G<pre>"; print_r($_GET); echo "</pre>"; echo "P<pre>"; print_r($_POST); echo "</pre>";

	# Segun el 'tipo de producto' preparamos la consulta en 'eude.productos'
	$estado = 1;
	switch($idTipoProducto){
		case '1': 
		case '4': 
			# cajas = idCamara + idRouter + idSim
			$sql = "INSERT INTO eude.productos(nombre, descripcion, idTipoProducto, idCamara, idRouter, idSim, idEmpresa, estado) VALUES(?,?,?,?,?,?,?,?)";
			$stmt = $mysqli->prepare($sql);		
			$stmt->bind_param("ssiiiiii",$nombre, $descripcion, $idTipoProducto, $idCamara, $idRouter,$idSim, $idEmpresa, $estado);
			$stmt->execute();
			# echo " <br> (caja) ";
			break;		
		
		case '2': 
			# APPS = idEmpresa + usuario + clave
			$sql = "INSERT INTO eude.productos (nombre , descripcion , idTipoProducto , idEmpresa , estado , usuario_app , clave_app , email_app) VALUES (? , ? , ? , ? , ? , ? , ? , ?)";
			$stmt = $mysqli->prepare($sql);		
			$stmt->bind_param("ssiiisss",$nombre, $descripcion, $idTipoProducto, $idEmpresa, $estado, $usuario, $clave, $email);
			$stmt->execute();
			# echo " <br> (APP) ";
			break;
			
		case '3':
			# Emision personal = idDispositivoEmisor + idSim
			$sql = "INSERT INTO eude.productos(nombre, descripcion, idTipoProducto, idDispositivoEmisor, idSim, idEmpresa, estado) VALUES(?,?,?,?,?,?,?) ";	
			$stmt = $mysqli->prepare($sql);		
			$stmt->bind_param("ssiiiii",$nombre, $descripcion, $idTipoProducto, $idDispositivoEmisor, $idSim, $idEmpresa, $estado);
			$stmt->execute();
			# echo "<br> (Emision personal) ";
			break;


		case '5': 
			# cajas = idCamara  + idSim
			$sql = "INSERT INTO eude.productos(nombre, descripcion, idTipoProducto, idCamara, idSim, idEmpresa, estado) VALUES(?,?,?,?,?,?,?)";
			$stmt = $mysqli->prepare($sql);		
			$stmt->bind_param("ssiiiii",$nombre, $descripcion, $idTipoProducto, $idCamara, $idSim, $idEmpresa, $estado);
			$stmt->execute();
			# echo " <br> (caja) ";
			break;					
	}
	
	# Evaluamos is ha ido todo bien o habido algun fallo
	if($stmt->errno){
		$response=0;
		$proceso = 'error';
	}else{
		$response=1;
		$proceso = 'ok';
	}
	
	# Consultamos el ultimo id insertado.
	$result=$mysqli->query("SELECT LAST_INSERT_ID() AS ultimoID;");		
	$row=$result->fetch_assoc();
	$idProducto = $row['ultimoID'];
	
	
	# Montamos el array 
	$data[] = array(
		"id"=>$row['ultimoID'], 
		"nombre"=>$nombre, 
		"descripcion"=>$descripcion, 
		"idTipoProducto"=>$idTipoProducto, 
		"idCamara"=>$idCamara, 
		"idRouter"=>$idRouter, 
		"idSim"=>$idSim , 
		"idDispositivoEmisor"=>$idDispositivoEmisor , 
		"idEmpresa"=>$idEmpresa , 
		"usuario"=>$usuario , 
		"email"=>$email , 
		"clave"=>$clave , 
		"nameEmpresa"=>$nameEmpresa,
		"nameTipoProducto"=>$nameTipoProducto,
		"nameCamara"=>$nameCamara ,
		"nameRouter"=>$nameRouter,
		"nameSim"=>$nameSim, 			
		"response" => $response,
		"proceso" => $proceso,
		"accion"=> "insert",
		"tabla"=> "producto",
		"nomFichero"=> "insert_producto.php",
	);	
	
	# Respuesta
	echo json_encode($data); 
	
	
	switch($idTipoProducto){
		case '1': 
		case '4': 
			# Modificamos la camara asignada = 1
			asignar_camara($mysqli, $idCamara, 1);	
			
			# Modificamos la sim asignada
			asignar_sim($mysqli, $idSim, 1);
			
			# Modificamos el router asignado	
			asignar_router($mysqli, $idRouter, 1);
			
			# Sacamos el campo 'serialnum_camara' de la tabla camara
			$serialnum_camara = select_serialNum_camara($mysqli, $idCamara);
			
			# Sacamos el campo 'dyndns_router' de la tabla router	
			$dyndns_router = select_dyndns_router($mysqli, $idRouter);	
			
			break;		
		
		case '2': 
			$serialnum_camara = "";
			$dyndns_router = "";	
			break;
			
		case '3':
			$serialnum_camara = "";
			$dyndns_router = "";	
			
			# Modificamos la sim asignada
			asignar_sim($mysqli, $idSim, 1);
			
			# Modificamos el dispositivo emision
			asignar_dispositivoEmisor($mysqli, $idDispositivoEmisor, 1);
			break;
		
			
		case '5':
			# Modificamos la camara asignada = 1
			asignar_camara($mysqli, $idCamara, 1);	
			
			# Modificamos la sim asignada
			asignar_sim($mysqli, $idSim, 1);
			
			# Sacamos el campo 'serialnum_camara' de la tabla camara
			$serialnum_camara = select_serialNum_camara($mysqli, $idCamara);
			
			# Sacamos el campo 'dyndns_router' de la tabla router	
			$dyndns_router = "";
			break;
			
		
	}
	
	
	# Sacamos la bd para cualquiera de los diferentes tipos
	$nombreBD = select_bbdd($mysqli, $idEmpresa);
	# echo "<br> nomEmpresa: " . $nombreBD;	
	
	# Añadimos una licencia contratada  en  eude.empresa	
	$num_licencias = count_licencias($mysqli, $idEmpresa);
	add_licencia($mysqli, $idEmpresa, 'eude', $num_licencias);

	# Cerramos la conexion de 'eude'
	$mysqli->close();
	
	# cambiamos la conexion al del cliente
	$mysqli2=new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, $nombreBD);
	
	# insertar nuevo producto_contratado
	add_productoContratado($mysqli2, $nombreBD, $idProducto, $idEmpresa, $serialnum_camara, $dyndns_router, $idTipoProducto,  $nameEmpresa );
		
	# registar en login_user (app)
	if($idTipoProducto == '2'){		
		include_once '../conexion/functions.php';
		login_register_app($usuario,$email,$clave,$idEmpresa,"1",$mysqli2, $nombreBD, $idProducto);
		# if(loginUser($usuario,$clave,$mysqli2, $nombreBD)) echo "<br> ok login"; else echo "<br> LOGIN FAIL!!!";
	}
	
	
	# cambios en info_cliente campo licencia 
	add_licencia($mysqli2, $idEmpresa, $nombreBD, $num_licencias);
		
	# añadir una posicion en ultimaposicion
	add_ultimaposicion($mysqli2, $nombreBD, $idProducto);

	# Cerramos la conexion de '$nombreBD'
	$mysqli2->close();
?>