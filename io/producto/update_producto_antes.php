<?php
	require_once("../funciones.php");
	conectar_bbdd('eude');
	include_once("../conexion/dbi_connect.php");
	$data = array();

	# Recogemos datos
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	$idSelect = comprobarParametros('idSelect');
	$nombre = comprobarParametros('nombre'); 
	$descripcion = comprobarParametros('descripcion'); 
	$idTipoProducto = comprobarParametros('idTipoProducto'); 
	$idCamara = comprobarParametros('idCamara'); 
	$idRouter = comprobarParametros('idRouter'); 
	$idSim = comprobarParametros('idSim'); 	
	$idDispositivoEmisor = comprobarParametros('idDispositivoEmisor'); 	
	$idEmpresa = comprobarParametros('idClienteSeleccion'); 	

	$idCamaraAux = comprobarParametros('idCamaraAux'); 
	$idRouterAux = comprobarParametros('idRouterAux'); 
	$idSimAux = comprobarParametros('idSimAux'); 
	$idDispositivoEmisorAux = comprobarParametros('idDispositivoEmisorAux'); 
	$idEmpresaAux = comprobarParametros('idClienteSeleccionAux'); 
	
	$nameTipoProducto = comprobarParametros('nameTipoProducto'); 
	$nameCamara = comprobarParametros('nameCamara'); 
	$nameRouter = comprobarParametros('nameRouter'); 
	$nameSim = comprobarParametros('nameSim'); 
	$nameDispositivoEmisor = comprobarParametros('nameDispositivoEmisor'); 
	$nameEmpresa = comprobarParametros('nameClienteSeleccion'); 
	
	$usuario = comprobarParametros('usuario'); 				
	$clave = comprobarParametros('clave'); 				
	$email = comprobarParametros('email'); 				
	 # echo " <pre>"; print_r($_GET); echo "</pre"; echo "<pre>"; print_r($_POST); echo "</pre>";
	
	# Segun el 'tipo de producto' preparamos la consulta en 'eude.productos'
	switch($idTipoProducto){
		case '1': 
			# cajas = idCamara + idRouter + idSim
			$sql = "UPDATE eude.productos SET nombre = ?, descripcion =?, idCamara = ?, idRouter = ?, idSim = ? , idEmpresa = ?, fecha_modificacion = CURRENT_TIMESTAMP WHERE id = ?";
			$stmt = $mysqli->prepare($sql);	
			$stmt->bind_param("ssiiiii" ,$nombre ,$descripcion, $idCamara, $idRouter,  $idSim, $idEmpresa, $idSelect);
			$stmt->execute();
			# echo " <br> (caja) ";
			break;		
		
		case '2': 
			# APPS = idEmpresa + usuario + clave
			# la clave no la cambiaremos desde aqui
			$sql = "UPDATE eude.productos SET nombre = ?, descripcion =?, usuario_app = ?, email_app = ? , idEmpresa =?, fecha_modificacion = CURRENT_TIMESTAMP WHERE id = ?";
			$stmt = $mysqli->prepare($sql);	
			$stmt->bind_param("ssssii" ,$nombre ,$descripcion, $usuario, $email, $idEmpresa, $idSelect);
			$stmt->execute();
			# echo " <br> (APP) ";
			break;
			
		case '3':
			# Emision personal = idDispositivoEmisor + idSim
			$sql = "UPDATE eude.productos SET nombre = ?, descripcion =?, idDispositivoEmisor = ?, idSim=? , idEmpresa =?, fecha_modificacion = CURRENT_TIMESTAMP WHERE id = ?";
			$stmt = $mysqli->prepare($sql);	
			$stmt->bind_param("ssiiii" ,$nombre ,$descripcion, $idDispositivoEmisor, $idSim, $idEmpresa, $idSelect);
			$stmt->execute();
			# echo "<br> (Emision personal) ";
			break;
	}
	
	# Evaluamos is ha ido todo bien o habido algun fallo
	if($stmt->errno){
		$response=0;
		$proceso = 'error';
	}else{
		$response=1;
		$proceso = 'ok';
	}
	
	# Montamos el array 
	$data[] = array(
		"id"=>$idSelect, 
		"nombre"=>$nombre, 
		"descripcion"=>$descripcion, 
		"idTipoProducto"=>$idTipoProducto, 
		"idCamara"=>$idCamara, 
		"idRouter"=>$idRouter, 
		"idSim"=>$idSim , 
		"idDispositivoEmisor"=>$idDispositivoEmisor , 
		"nameTipoProducto"=>$nameTipoProducto,
		"nameCamara"=>$nameCamara ,
		"nameRouter"=>$nameRouter,
		"nameSim"=>$nameSim, 
		"nameDispositivoEmisor"=>$nameDispositivoEmisor,
		"usuario"=>$usuario,
		"clave"=>$clave,
		"email"=>$email,	
		"response" => $response,
		"proceso" => $proceso,
		"accion"=> "update",
		"tabla"=> "producto",
		"nomFichero"=> "update_producto.php",
	);
	
	#  Liberamos la camara anterior a 0
	if($idTipoProducto == '1'){		
		$stmt = $mysqli->prepare("UPDATE eude.camaras SET asignada = 0 WHERE idcamaras = ?");		
		$stmt->bind_param("i", $idCamaraAux);
		$stmt->execute();
	}	
		
	#  Modificamos la camara asignada a 1
	if($idTipoProducto == '1'){		
		$stmt = $mysqli->prepare("UPDATE eude.camaras SET asignada = 1 WHERE idcamaras = ?");		
		$stmt->bind_param("i", $idCamara);
		$stmt->execute();
	}
	
	#  Liberamos el router anterior a 0
	if($idTipoProducto == '1'){	
		$stmt = $mysqli->prepare("	UPDATE eude.routers SET  asignado = 0 WHERE idrouters = ?");		
		$stmt->bind_param("i", $idRouterAux);
		$stmt->execute();
	}
		
	#  Modificamos el router asignado a 1
	if($idTipoProducto == '1'){	
		$stmt = $mysqli->prepare("UPDATE eude.routers SET  asignado = 1 WHERE idrouters  = ?");		
		$stmt->bind_param("i", $idRouter);
		$stmt->execute();
	}
	
	#  Liberamos la sim anterior a 0
	if(($idTipoProducto == '1') || ($idTipoProducto == '3')){		
		$stmt = $mysqli->prepare("UPDATE eude.sim SET asignada = 0  WHERE idSim = ?");		
		$stmt->bind_param("i", $idSimAux);
		$stmt->execute();
	}
		
	#  Modificamos la sim  asignada a 1
	if(($idTipoProducto == '1') || ($idTipoProducto == '3')){	
		$stmt = $mysqli->prepare("UPDATE eude.sim SET asignada = 1 WHERE idSim = ?");		
		$stmt->bind_param("i", $idSim);
		$stmt->execute();
	}
	
	# Libertamos el dispositivo emisión
	if($idTipoProducto == '3'){	
		$stmt = $mysqli->prepare("UPDATE eude.dispositivo_emisor SET  asignado = 0 WHERE id = ?");	
		$stmt->bind_param("i", $idDispositivoEmisorAux);
		$stmt->execute();
	}
	
	# Modificamos el dispositivo emision
	if($idTipoProducto == '3'){	
		$stmt = $mysqli->prepare("UPDATE eude.dispositivo_emisor SET  asignado = 1 WHERE id = ?");	
		$stmt->bind_param("i", $idDispositivoEmisor);
		$stmt->execute();
	}
	
	# Modificamos el app
	if($idTipoProducto == '2'){	
		# Sacamos la bd  antigua
		$stmt2 = $mysqli->prepare("select bbdd from eude.empresas where id=? limit 1");			
		$stmt2->bind_param("i", $idEmpresaAux); 
		$stmt2->execute();
		$stmt2->bind_result($nombreBDAux);
		$stmt2->fetch();
		$stmt2->close();
		# echo "<br> idEmpresaAux: " . $idEmpresaAux . " - nomEmpresaAux: " . $nombreBDAux;	
		
		# Sacamos la bd  nueva
		$stmt3 = $mysqli->prepare("select bbdd from eude.empresas where id=? limit 1");			
		$stmt3->bind_param("i",  $idEmpresa); 
		$stmt3->execute();
		$stmt3->bind_result($nombreBD);
		$stmt3->fetch();
		$stmt3->close();
		# echo "<br> idEmpresa: " . $idEmpresa . " - nomEmpresa: " . $nombreBD;	
		
		
		# Eliminamos el registro de user_login de la bd antigua		
		$sql = "DELETE FROM ".$nombreBDAux.".login_users WHERE idProducto = ".$idSelect ;
		# echo "<br> ". $sql;
		$mysqli->query($sql);			
	# echo "<br> ";
	
		
		# Añadimos registro de user_login en la bd nueva		
		# registar en login_user
		 $mysqli2=new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, $nombreBD);	
		include_once '../conexion/functions.php';
		# echo "<br>  email: ". $email;
		login_register_app($usuario,$email,$clave,$idEmpresa,"1",$mysqli2, $nombreBD, $idSelect);
		

	} // fin if
	
	$stmt->close();
	echo json_encode($data); 
	$mysqli->close();
	?>