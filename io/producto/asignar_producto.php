<?php
	require_once("../funciones.php");
	conectar_bbdd('eude');
	include_once("../conexion/dbi_connect.php");
	$data = array();

	# Recogemos datos
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	$idProducto = comprobarParametros('idProducto');
	$idEmpresa = comprobarParametros('idEmpresa'); 
	$nameEmpresa = comprobarParametros('nameEmpresa');
	$nameProducto = comprobarParametros('nameProducto'); 
	$idTipoProducto = comprobarParametros('idTipoProducto'); 
	$nameTipoProducto = comprobarParametros('nameTipoProducto'); 
	$idCamara = comprobarParametros('idCamara'); 
	$nameCamara = comprobarParametros('nameCamara'); 
	$idRouter = comprobarParametros('idRouter'); 
	$nameRouter = comprobarParametros('nameRouter'); 
	$idSim = comprobarParametros('idSim'); 
	$nameSim = comprobarParametros('nameSim'); 
	$nombre = comprobarParametros('nombre'); 
	$descripcion = comprobarParametros('descripcion');  	
	$user = $idEmpresa."_".$idProducto;
	$pass = $idEmpresa."_".$idProducto;
	# echo "<pre>"; print_r($_GET);echo "</pre>"; echo "<pre>"; print_r($_POST);echo "</pre>";
	
	# Preparamos y ejecutamos la consulta	
	# nombre base de datos del cliente
	# echo "<br> bd idEmpresa: " .$idEmpresa;
	$stmt = $mysqli->prepare("SELECT bbdd FROM eude.empresas where id = ?");
	$stmt->bind_param('i', $idEmpresa);
	$stmt->execute();
	$stmt->bind_result($nameBDCliente);
	$stmt->fetch();	
	# echo "<br> bd cliente: " .$nameBDCliente;
	$stmt->close();
	
	# cambios en eude.producto 
	$stmt = $mysqli->prepare("UPDATE
												eude.productos
											SET
												idEmpresa = ?,
												nombre= ? ,
												estado = 1 ,
												fecha_modificacion = CURRENT_TIMESTAMP
											WHERE
												id= ? ");	
	$stmt->bind_param("isi" ,$idEmpresa ,$nameProducto, $idProducto);
	$stmt->execute();	
	# Comprobamos si se han insertado los datos.
	# printf("<br> Affected rows (update eude.producto ): %d\n", $mysqli->affected_rows);
	$stmt->close();	
	
	 # Añadimos una licencia contratada  en  eude.empresa
   	$stmt = $mysqli->prepare(" UPDATE eude.empresas SET licencias = licencias+1 WHERE id = ?");
	$stmt->bind_param('i', $idEmpresa);
	$stmt->execute();
	
	# Sacamos el campo 'serialnum_camara' de la tabla camara 
	# echo "<br> bd idEmpresa: " .$idCamara;
	$stmt = $mysqli->prepare("SELECT serialnum FROM eude.camaras where idcamaras = ? ");
	$stmt->bind_param('i', $idCamara);
	$stmt->execute();
	$stmt->bind_result($serialnum_camara);
	$stmt->fetch();	
	# echo "<br> serialnum camara: " .$serialnum_camara;
	$stmt->close();
	
	# Sacamos el campo 'dyndns_router' de la tabla router 
	# echo "<br> bd idEmpresa: " .$idRouter;
	$stmt = $mysqli->prepare("SELECT dyndns FROM eude.routers where idrouters =  ? ");
	$stmt->bind_param('i', $idRouter);
	$stmt->execute();
	$stmt->bind_result($dyndns_router);
	$stmt->fetch();	
	# echo "<br> dyndns router : " .$dyndns_router;
	$stmt->close();
	
	
	$mysqli->close();

	
	
	# cambiamos la conexion al del cliente
	$mysqli2=new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, $nameBDCliente);
	
	
	
	# insertar nuevo producto_contratado
	$idZona = 0;
	$estado = 1;
	$online = 0;
	$active = 1;
	$orden = 12;
	$stmt = $mysqli2->prepare("INSERT INTO  ".$nameBDCliente.".productos_contratados(idProducto, nameProducto ,idTipoProducto, idZona, user, pass, estado, online, active, orden, serialnum, dyndns) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
	$stmt->bind_param('isiissiiiiss', $idProducto, $nameEmpresa, $idTipoProducto, $idZona, $user, $pass, $estado, $online, $active, $orden, $serialnum_camara, $dyndns_router);
	$stmt->execute();
	# Comprobamos si se han insertado los datos.
	# printf("<br> Affected rows (insert productos_contratados): %d\n", $mysqli2->affected_rows);
	$stmt->close();
	
	
	# cambios en info_cliente campo licencia 
	$stmt = $mysqli2->prepare("UPDATE ".$nameBDCliente.".info_cliente SET licencias = licencias+1");
	$stmt->execute();
	# Comprobamos si se han insertado los datos.
	# printf("<br> Affected rows (UPDATE info_cliente): %d\n", $mysqli2->affected_rows);
	$stmt->close();
	
	# añadir una posicion en ultimaposicion
	$latitude=0;
	$longitude=0;
	$stmt = $mysqli2->prepare("INSERT INTO  ".$nameBDCliente.".ultimaposicion(userId, latitude, longitude) VALUES (?, ?, ?);");
	$stmt->bind_param('iii', $idProducto, $latitude, $longitude);
	$stmt->execute();
	# Comprobamos si se han insertado los datos.
	# printf("<br> Affected rows (insert ultimaposicion): %d\n", $mysqli2->affected_rows);
	$stmt->close();
	
	

	//Evaluamos is ha ido todo bien o habido algun fallo
	if($stmt->errno){
		$response=0;
		$proceso = 'error';
		$error = $stmt->errno;
	}else{
		$response=1;
		$proceso = 'ok';
		$error ="";
	}

	
	//Montamos el array 
	$data[] = array(
		"id"=>$idProducto,
		"idEmpresa"=>$idEmpresa, 
		"nameEmpresa"=>$nameEmpresa, 
		"idTipoProducto"=>$idTipoProducto,
		"idCamara"=>$idCamara,
		"idRouter"=>$idRouter,	
		"nameProducto"=>$nameProducto, 
		"idSim"=>$idSim,
		"nombre"=>$nombre ,
		"descripcion"=>$descripcion,
		"nameTipoProducto"=>$nameTipoProducto,
		"nameCamara"=>$nameCamara ,
		"nameRouter"=>$nameRouter,
		"nameSim"=>$nameSim, 
		"fecha_alta_europea"=>date("d-m-Y"),			
		"fechaHora_alta_europea"=>date("Y-m-d  h:m:s"),			
		"response" => $response,
		"proceso" => $proceso,
		"accion"=> "asignar",
		"tabla"=> "producto",
		"nomFichero"=> "asignar_producto.php",
		"error"=> $error 
	);

	
	echo json_encode($data); 	
	$mysqli2->close();
?>