<?php

# count numLicencias
function count_licencias($mysqli, $idEmpresa){

	$stmt = $mysqli->prepare("select count(*) from eude.productos where idEmpresa = ?");
	$stmt->bind_param('i', $idEmpresa);

	$stmt->execute();
	$stmt->bind_result($num_licencias);
	$stmt->fetch();	
	# echo "<br> num_licencias : " .$num_licencias;
	$stmt->close();

	return $num_licencias;
	
	# Comprobamos si se han insertado los datos.
	# printf("<br> Affected rows (UPDATE info_cliente): %d\n", $mysqli->affected_rows);
	
}


# Sumar una licencia 
function add_licencia($mysqli, $idEmpresa, $nombreBD, $num_licencias){
	# echo "<br>add_licencia(" . $idEmpresa . " , " . $nombreBD .")<br> ";

	if($nombreBD == 'eude'){
		$stmt = $mysqli->prepare("UPDATE eude.empresas SET licencias = ? WHERE id = ? ");
		$stmt->bind_param('ii', $num_licencias, $idEmpresa);
	}else{
		$stmt = $mysqli->prepare("UPDATE ".$nombreBD.".info_cliente SET licencias = ? ");
		$stmt->bind_param('i', $num_licencias);
	}
	
	$stmt->execute();
	$stmt->close();
	# $mysqli->close();
	
	# Comprobamos si se han insertado los datos.
	# printf("<br> Affected rows (UPDATE info_cliente): %d\n", $mysqli->affected_rows);
}


# Sumar una licencia 
function add_licencia_($mysqli, $idEmpresa, $nombreBD){
	# echo "<br>add_licencia(" . $idEmpresa . " , " . $nombreBD .")<br> ";

	if($nombreBD == 'eude'){
		$sql = "UPDATE eude.empresas SET licencias = licencias+1 WHERE id = ? ";
		$stmt = $mysqli->prepare($sql);
		$stmt->bind_param('i', $idEmpresa);
	}else{
		$sql = "UPDATE ".$nombreBD.".info_cliente SET licencias = licencias+1";
		$stmt = $mysqli->prepare($sql);
	}
	$stmt->execute();
	$stmt->close();
	# $mysqli->close();
	
	# Comprobamos si se han insertado los datos.
	# printf("<br> Affected rows (UPDATE info_cliente): %d\n", $mysqli->affected_rows);
}

# Restar una licencia 
function remove_licencia_($mysqli, $idEmpresa, $nombreBD){
	# echo "<br>  remove_licencia(".$mysqli.", ".$idEmpresa.", ".$nombreBD.") <br> ";
	if($nombreBD == 'eude'){
		$sql = "UPDATE eude.empresas SET licencias = licencias-1 WHERE id = ? ";
		$stmt = $mysqli->prepare($sql);
		$stmt->bind_param('i', $idEmpresa);
	}else{
		$sql = "UPDATE ".$nombreBD.".info_cliente SET licencias = licencias-1";
		$stmt = $mysqli->prepare($sql);
	}
	$stmt->execute();
	$stmt->close();	
}
	
# Sacamos el campo 'serialnum_camara' de la tabla camara 
function select_serialNum_camara($mysqli, $idCamara){
	$stmt = $mysqli->prepare("SELECT serialnum FROM eude.camaras where idcamaras = ? ");
	$stmt->bind_param('i', $idCamara);
	$stmt->execute();
	$stmt->bind_result($serialnum_camara);
	$stmt->fetch();	
	# echo "<br> serialnum camara: " .$serialnum_camara;
	$stmt->close();

	return $serialnum_camara;
}

# Modificamos el campo 'serialnum_camara' de la tabla producto_contratado 
function update_serialNum_productoContratado($mysqli, $nombreBD, $serialnum_camara, $idProducto){
	# echo "<br>  $nombreBD, $serialnum_camara, $idProducto <br>  ";
	$stmt = $mysqli->prepare("UPDATE ".$nombreBD.".productos_contratados SET  serialnum = ?,  fechaModificacion = CURRENT_TIMESTAMP WHERE idProducto = ?"); 
	$stmt->bind_param('si', $serialnum_camara, $idProducto);
	$stmt->execute();
	# Comprobamos si se han insertado los datos.
	# printf("<br> Affected rows (insert productos_contratados): %d\n", $mysqli->affected_rows);
	$stmt->close();
	
}

# Modificamos el campo 'dyndns_router' de la tabla producto_contratado 
function update_dyndns_router_productoContratado($mysqli, $nombreBD, $dyndns_router, $idProducto){
	$stmt = $mysqli->prepare("UPDATE ".$nombreBD.".productos_contratados SET  dyndns = ?,  fechaModificacion = CURRENT_TIMESTAMP WHERE idProducto = ?"); 
	$stmt->bind_param('si' , $dyndns_router, $idProducto);
	$stmt->execute();
	# Comprobamos si se han insertado los datos.
	# printf("<br> Affected rows (insert productos_contratados): %d\n", $mysqli->affected_rows);
	$stmt->close();
	
}

# Modificamos la camara asignada (0, 1)
function asignar_camara($mysqli, $idCamara, $valor){
	$stmt = $mysqli->prepare("UPDATE eude.camaras SET asignada = ? , fecha_modificacion = CURRENT_TIMESTAMP  WHERE idcamaras = ?");		
	$stmt->bind_param("ii", $valor, $idCamara);
	$stmt->execute();
	$stmt->close();
}

# Modificamos la router asignada (0, 1)
function asignar_router($mysqli, $idRouter, $valor){
	$stmt = $mysqli->prepare("UPDATE eude.routers SET asignado=?, fecha_modificacion = CURRENT_TIMESTAMP  WHERE idrouters = ?");	
	$stmt->bind_param("ii", $valor, $idRouter);
	$stmt->execute();
	$stmt->close();
}


# Sacamos el campo 'dyndns_router' de la tabla router 
function select_dyndns_router($mysqli, $idRouter){
	$stmt = $mysqli->prepare("SELECT dyndns FROM eude.routers where idrouters =  ?  ");
	$stmt->bind_param('i', $idRouter);
	$stmt->execute();
	$stmt->bind_result($dyndns_router);
	$stmt->fetch();	
	# echo "<br> dyndns router : " .$dyndns_router;
	$stmt->close();

	return $dyndns_router;
}

# cambiar dyndns_router  
// function update_dyndns_router($mysqli, $idRouter){}



# Modificamos la sim asignada (0, 1)
function asignar_sim($mysqli, $idSim, $valor){
	$stmt = $mysqli->prepare("UPDATE eude.sim SET asignada = ? , fecha_modificacion = CURRENT_TIMESTAMP  WHERE idSim = ?");		
	$stmt->bind_param("ii", $valor, $idSim);
	$stmt->execute();
	$stmt->close();
}

# Modificamos la sim asignada (0, 1)
function asignar_dispositivoEmisor($mysqli, $idDispositivoEmisor, $valor){
	$stmt = $mysqli->prepare("UPDATE eude.dispositivo_emisor SET  asignado = ?,  fechaModificacion = CURRENT_TIMESTAMP  WHERE id = ?");		
	$stmt->bind_param("ii", $valor, $idDispositivoEmisor);
	$stmt->execute();
	$stmt->close();
}

function select_bbdd($mysqli, $idEmpresa){
	$stmt = $mysqli->prepare("SELECT bbdd FROM eude.empresas where id = ? ");
	$stmt->bind_param('i', $idEmpresa);
	$stmt->execute();
	$stmt->bind_result($nombreBD);
	$stmt->fetch();	
	# echo "<br> bd cliente: " .$nombreBD;
	$stmt->close(); 
	
	return $nombreBD;
}


# insertar nuevo producto_contratado
function add_productoContratado($mysqli, $nombreBD, $idProducto, $idEmpresa, $serialnum_camara, $dyndns_router, $idTipoProducto,  $nameEmpresa ){
	$user = $idEmpresa."_".$idProducto;
	$pass = $idEmpresa."_".$idProducto;
	$estado = 1;
	$idZona = 0;
	$online = 0;
	$active = 1;
	$orden = 12;
	$stmt = $mysqli->prepare("INSERT INTO  ".$nombreBD.".productos_contratados(idProducto, nameProducto ,idTipoProducto, idZona, user, pass, estado, online, active, orden, serialnum, dyndns) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
	$stmt->bind_param('isiissiiiiss', $idProducto, $nameEmpresa, $idTipoProducto, $idZona, $user, $pass, $estado, $online, $active, $orden, $serialnum_camara, $dyndns_router);
	$stmt->execute();
	# Comprobamos si se han insertado los datos.
	# printf("<br> Affected rows (insert productos_contratados): %d\n", $mysqli->affected_rows);
	$stmt->close();
}




# añadir una posicion en ultimaposicion
function add_ultimaposicion($mysqli, $nombreBD, $idProducto){
	$latitude=0;
	$longitude=0;
	$stmt = $mysqli->prepare("INSERT INTO  ".$nombreBD.".ultimaposicion(userId, latitude, longitude) VALUES (?, ?, ?)");
	$stmt->bind_param('iii', $idProducto, $latitude, $longitude);
	$stmt->execute();
	# Comprobamos si se han insertado los datos.
	# printf("<br> Affected rows (insert ultimaposicion): %d\n", $mysqli2->affected_rows);
	$stmt->close();
}


# Modificamos usuario, email del producto si el tipoProducto = 2
function update_login_users($mysqli, $nombreBD, $idProducto,  $usuario, $email){
	$stmt = $mysqli->prepare("UPDATE ".$nombreBD.".login_users  SET user = ?, mail =?, modified_ts = CURRENT_TIMESTAMP  WHERE idProducto = ? ");
	$stmt->bind_param('ssi', $usuario, $email, $idProducto);
	$stmt->execute();
	# Comprobamos si se han insertado los datos.
	# printf("<br> Affected rows (insert ultimaposicion): %d\n", $mysqli->affected_rows);
	$stmt->close();

}

# Eliminar en user_login el producto si el tipoProducto = 2
function delete_login_users($mysqli, $nombreBD, $idProducto){
	// $stmt = $mysqli->prepare("DELETE FROM ".$nombreBD.".login_users WHERE idProducto = ?");
	$stmt = $mysqli->prepare("UPDATE  ".$nombreBD.".login_users SET idProducto = '-1' , modified_ts = CURRENT_TIMESTAMP    WHERE idProducto = ?");	
	$stmt->bind_param('i', $idProducto);
	$stmt->execute();
	# Comprobamos si se han insertado los datos.
	# printf("<br> Affected rows (insert ultimaposicion): %d\n", $mysqli->affected_rows);
	$stmt->close();
}

# Elimiar el producto de la tabla productos_contratados
function delete_productos_contratados($mysqli, $nombreBD, $idProducto){
	$stmt = $mysqli->prepare("DELETE FROM ".$nombreBD.".productos_contratados WHERE idProducto=?");
	$stmt->bind_param('i', $idProducto);	
	$stmt->execute();
	# Comprobamos si se han insertado los datos.
	# printf("<br> Affected rows (delete productos_contratados): %d\n", $mysqli->affected_rows);
	$stmt->close();
}

# Eliminar el producto de la tabla 'utimaPosicion'
function delete_ultimaposicion($mysqli, $nombreBD, $idProducto){
	$stmt = $mysqli->prepare("DELETE FROM ".$nombreBD.".ultimaposicion WHERE userId=? ");
	$stmt->bind_param('i', $idProducto);
	$stmt->execute();
	# Comprobamos si se han insertado los datos.
	# printf("<br> Affected rows (delete ultimaposicion): %d\n", $mysqli->affected_rows);
	$stmt->close();
}


# Eliminar el producto de eude.productos
function delete_productos($mysqli, $idProducto){
	$stmt = $mysqli->prepare("DELETE FROM eude.productos WHERE id=? ");
	$stmt->bind_param('i', $idProducto);
	$stmt->execute();
	# Comprobamos si se han insertado los datos.
	# printf("<br> Affected rows (delete ultimaposicion): %d\n", $mysqli->affected_rows);
	if($stmt->errno){
		return $response=0;
		# $proceso = 'error';
	}else{
		return $response=1;
		# $proceso = 'ok';
	}
	
	$stmt->close();
	
}


# Cambiar nombre, descripcion de producto
function update_producto($mysqli, $idTipoProducto, $nombre, $descripcion, $idCamara, $idRouter,  $idSim, $idEmpresa, $idProducto, $idDispositivoEmisor, $usuario, $email){
# Segun el 'tipo de producto' preparamos la consulta en 'eude.productos'
	switch($idTipoProducto){
		case '1': 
			# cajas = idCamara + idRouter + idSim
			$sql = "UPDATE eude.productos SET nombre = ?, descripcion =?, idCamara = ?, idRouter = ?, idSim = ? , idEmpresa = ?, fecha_modificacion = CURRENT_TIMESTAMP WHERE id = ?";
			$stmt = $mysqli->prepare($sql);	
			$stmt->bind_param("ssiiiii" ,$nombre ,$descripcion, $idCamara, $idRouter,  $idSim, $idEmpresa, $idProducto);
			$stmt->execute();
			# echo " <br> (caja) ";
			break;		
		
		case '2': 
			# APPS = idEmpresa + usuario + clave
			# la clave no la cambiaremos desde aqui
			$sql = "UPDATE eude.productos SET nombre = ?, descripcion =?, usuario_app = ?, email_app = ? , idEmpresa =?, fecha_modificacion = CURRENT_TIMESTAMP WHERE id = ?";
			$stmt = $mysqli->prepare($sql);	
			$stmt->bind_param("ssssii" ,$nombre ,$descripcion, $usuario, $email, $idEmpresa, $idProducto);
			$stmt->execute();
			# echo " <br> (APP) ";
			break;
			
		case '3':
			# Emision personal = idDispositivoEmisor + idSim
			$sql = "UPDATE eude.productos SET nombre = ?, descripcion =?, idDispositivoEmisor = ?, idSim=? , idEmpresa =?, fecha_modificacion = CURRENT_TIMESTAMP WHERE id = ?";
			$stmt = $mysqli->prepare($sql);	
			$stmt->bind_param("ssiiii" ,$nombre ,$descripcion, $idDispositivoEmisor, $idSim, $idEmpresa, $idProducto);
			$stmt->execute();
			# echo "<br> (Emision personal) ";
			break;
	}
	
	if($stmt->errno){
		return $response=0;
		# $proceso = 'error';
	}else{
		return $response=1;
		# $proceso = 'ok';
	}
	
	$stmt->close();
}

?>