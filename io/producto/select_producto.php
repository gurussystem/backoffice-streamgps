<?php 
	require_once("../funciones.php");
	conectar_bbdd('eude');
	include_once("../conexion/dbi_connect.php");
	$data = array();
	
	# Recogemos datos
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	$idSelect = comprobarParametros('idSelect'); 
	$opcion = comprobarParametros('opcion'); 
	# echo "GET<pre>"; print_r($_GET); echo "</pre>"; echo "POS<pre>"; print_r($_POST); echo "</pre>";
	
	# consulta 
	$sql = "SELECT
	p.id,
    p.nombre,
    p.descripcion,
    p.idTipoProducto,
    p.idCamara,
    p.idRouter,
    p.idSim,
    p.idDispositivoEmisor,
    p.idEmpresa,
    p.estado,
    p.fecha_alta,
    p.fecha_modificacion,
	(SELECT c.name FROM eude.camaras AS c WHERE c.idCamaras = p.idCamara )AS nombreCamara,
	(SELECT r.name FROM eude.routers AS r WHERE r.idrouters = p.idrouter) AS nombreRouter,
	tp.nombre AS nombreTipoProducto,
	tp.tp_gps,
	tp.tp_wifi,
	tp.tp_3G,
	tp.tp_4G,
	(SELECT s.numSim01 FROM eude.sim AS s WHERE s.idSim = p.idSim ) AS nombreSim,
	(SELECT de.nombre FROM eude.dispositivo_emisor as de WHERE p.idDispositivoEmisor = de.id  ) as nombreDispositivoEmisor, 
	p.usuario_app AS usuario, 
	p.clave_app AS clave, 
	p.email_app AS email
	
FROM
	eude.productos AS p
    INNER JOIN configstream.tipo_productos AS tp ON (tp.idTipoProducto = p.idTipoProducto)";
	
	# Preparamos y ejecutamos la consulta
	switch($opcion){
		case 1: 
			# Todo el listado			
			$consulta = $sql . " ORDER BY p.id asc";
			break;
			
		case 2: 
			# Consultando un producto 			
			$consulta = $sql . "WHERE id = ? LIMIT 1";
			break;
			
		case 3: 
			# Ficha producto para asignar a empresa			
			$consulta = $sql . " WHERE p.idEmpresa = ? ";
			break;
			
		case 4: 	
			# Todos los productos sin asignar		
			$consulta = $sql . " WHERE p.estado = 0 ORDER BY p.id asc";
			break;
	}//fin switch	
	
	if($idSelect){
		$stmt = $mysqli->prepare($consulta);	
		$stmt->bind_param("i", $idSelect);
	}else{
		$stmt = $mysqli->prepare($consulta);	
	}
	$stmt->execute();
	$stmt->bind_result($id, $nombre, $descripcion, $idTipoProducto, $idCamara, $idRouter, $idSim, $idDispositivoEmisor, $idEmpresa, $estado, $fecha_alta, $fecha_modificacion,  $nombreCamara, $nombreRouter, $nombreTipoProducto, $tp_gps, $tp_wifi, $tp_3G, $tp_4G, $nombreSim, $nombreDispositivoEmisor, $usuario, $clave, $email);
	
	while($stmt->fetch()) {
		
		$data[] = array(
			"id"=>$id, 
			"nombre"=>$nombre, 
			"descripcion"=>$descripcion, 
			"idTipoProducto"=>$idTipoProducto, 
			"idCamara"=>$idCamara, 
			"idRouter"=>$idRouter, 
			"idEmpresa"=>$idEmpresa, 
			"idSim"=>$idSim, 
			"estado"=>$estado, 
			"fecha_alta"=>$fecha_alta, 			
			"fecha_mod"=>$fecha_modificacion,
			//"fecha_alta"=>desfaseHorario($fecha_alta, $tiempoDesfase, "entera", "suma") 	, 			
			//"fecha_mod"=>controlFechaModificacion($fecha_modificacion, $tiempoDesfase, "entera", "suma"),
			"idDispositivoEmisor"=>$idDispositivoEmisor, 
			"nombreRouter"=>$nombreRouter, 
			"nombreCamara"=>$nombreCamara, 
			"nombreSim"=>$nombreSim, 
			"nombreTipoProducto"=>$nombreTipoProducto,
			"nombreDispositivoEmisor"=>$nombreDispositivoEmisor,
			"usuario"=>$usuario,
			"clave"=>$clave,
			"email"=>$email,
			"tp_gps"=>$tp_gps,
			"tp_wifi"=>$tp_wifi,
			"tp_3G"=>$tp_3G,
			"tp_4G"=>$tp_4G,
			"accion"=> "select",
			"tabla"=> "producto",
			"nomFichero"=> "select_producto.php",
			"result"=> 1,
		);
	}
	
	# comprobamos si el array esta vacio 
	if(!$data){
	# 	echo "<br> data if <br>";
		$data[] = array(
			"accion"=> "select",
			"tabla"=> "producto",
			"nomFichero"=> "select_producto.php",
			"result"=> 0,
		);
	}
	
	$stmt->close();
	# echo "<pre>"; print_r($data); echo "</pre>";			
	echo json_encode($data); 
	$mysqli->close();
?>
