<?php
	require_once("../funciones.php");
	include_once("funcionesProductos.php");
	conectar_bbdd('eude');
	include_once("../conexion/dbi_connect.php");
	$data = array();

	# Recogemos datos
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	$idProducto = comprobarParametros('idProducto'); 
	$idCamara = comprobarParametros('idCamara'); 
	$idRouter = comprobarParametros('idRouter'); 
	$idSim = comprobarParametros('idSim'); 	
	$idEmpresa = comprobarParametros('idEmpresa'); 
	$idDispositivoEmisor = comprobarParametros('idDispositivoEmisor'); 	
	$idTipoProducto = comprobarParametros('idTipoProducto'); 	
	
	
	if($idTipoProducto == '1'){
		# Liberar el idCamara -> asignado = 0 si el tipoProducto = 1
		asignar_camara($mysqli, $idCamara, 0);	
		
		# Liberar el idRouter -> asignado = 0 si el tipoProducto = 1
		asignar_router($mysqli, $idRouter, 0);
	}
	
    
	# Liberar el idSim -> asignado = 0 si el tipoProducto = 1 o 3
	if(($idTipoProducto == '1') || ($idTipoProducto == '3')){	
		asignar_sim($mysqli, $idSim, 0);
	}
    
	# Liberar el idDispositivoEmisor -> asignado = 0 si el tipoProducto = 3
	if($idTipoProducto == '3'){	
		asignar_dispositivoEmisor($mysqli, $idDispositivoEmisor, 0);
	}
	
	
	# Restar una licencia a eude.empresa.licencia según idProducto
	$num_licencias = count_licencias($mysqli, $idEmpresa);
	add_licencia($mysqli, $idEmpresa, 'eude', $num_licencias);
	

	# Sacar el nombre de la BD con el idEmpresa
	$nombreBD = select_bbdd($mysqli, $idEmpresa);
	
	
	$mysqli->close();
	
	# Conectamos con la BD del cliente segun idEmpresa
	$mysqli2=new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, $nombreBD);
	
	# Eliminar en user_login el producto si el tipoProducto = 2
    if($idTipoProducto == '2'){
		delete_login_users($mysqli2, $nombreBD, $idProducto);		
	}
	
	# Elimiar el producto de la tabla productos_contratados
    if(($idTipoProducto == '1') || ($idTipoProducto == '2') || ($idTipoProducto == '3') ){
		delete_productos_contratados($mysqli2, $nombreBD, $idProducto);
	}
	
	# Eliminar el producto de la tabla 'utimaPosicion'
	delete_ultimaposicion($mysqli2, $nombreBD, $idProducto);
	
	# Restar una licencia a info_cliente.licencia
	add_licencia($mysqli2, $idEmpresa, $nombreBD, $num_licencias);
  
	# Cerramos la conexion 
	$mysqli2->close();
	
	# Conectamos con la BD eude para eliminar el producto
	$mysqli3=new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, 'eude');
	
	# Eliminar el producto de eude.productos
	$response = delete_productos($mysqli3, $idProducto);

	if($response == 1)
		$proceso = 'ok';
	else
		$proceso = 'error';
	
	# Evaluamos is ha ido todo bien o habido algun fallo
	/*if($stmt->errno){
		$response=0;
		$proceso = 'error';
	}else{
		$response=1;
		$proceso = 'ok';
	}

	*/
	
	# Montamos el array 	
	$data[] = array(
		"id"=>$idProducto,
		"idEmpresa"=>$idEmpresa, 
		"idCamara"=>$idCamara, 
		"idRouter"=>$idRouter, 
		"idSim"=>$idSim, 
		"idDispositivoEmisor"=>$idDispositivoEmisor, 
		"idTipoProducto"=>$idTipoProducto, 		
		"response" => $response,
		"proceso" => $proceso,
		"accion"=> "desasignar",
		"tabla"=> "producto",
		"nomFichero"=> "desasignar_producto.php"
	);
	
	
	echo json_encode($data); 
	$mysqli3->close();

?>