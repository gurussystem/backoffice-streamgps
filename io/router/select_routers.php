<?php 
	require_once("../funciones.php");
	conectar_bbdd('eude');
	include_once("../conexion/dbi_connect.php");
	# 	datos_conexion();
	
	# Recogemos datos
	$idRouterSelect = comprobarParametros('idRouter');	
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	#  echo "GET: <pre>"; print_r($_GET); echo "</pre>"; echo "POST: <pre> "; print_r($_POST); echo "</pre>";
	
	# consulta
	$sql = "SELECT
	r.idrouters as idrouters,
    r.name as name,
    r.type as type,
    r.serialnum as serialnum,
    r.mac as mac,
    r.user as user,
    r.pass as pass,
    r.ipstatic as ipstatic,
    r.dyndns as dyndns,
	(Select p.idEmpresa from empresas as e inner join productos as p on(p.idEmpresa = e.id)  where p.idRouter = r.idrouters) as idPropietario,
	(Select e.nombre from empresas as e inner join productos as p on(p.idEmpresa = e.id)  where p.idRouter = r.idrouters) as nomPropietario,
    r.asignado as asignado,
	r.fecha_alta as fecha_alta,
    r.fecha_modificacion as fecha_modificacion
FROM
	eude.routers as r ";
	
	# Preparamos y ejecutamos la consulta
	if($idRouterSelect){
		$consulta = $sql . "where r.idrouters = ? order by r.idrouters asc";
		$stmt = $mysqli->prepare($consulta);		
		$stmt->bind_param("i", $idRouterSelect);
	}else{
		$consulta = $sql . " where	r.idrouters != 0	order by r.idrouters asc";
		$stmt = $mysqli->prepare($consulta);		
	}
	$stmt->execute();
	
	# vincular variables a la sentencia preparada
	$stmt->bind_result($idrouters, $name, $idTipoRouter, $serialnum, $mac, $user, $pass, $ipstatic, $dyndns , $idPropietario, $nomPropietario, $asignado, $fecha_alta, $fecha_mod);
	
	# Obtenemos los valores
	while($stmt->fetch() ) {
		$data[] = array(
			"idRouters"=>$idrouters, 
			"nameRouters"=>$name, 
			"idTipoRouter"=>$idTipoRouter, 
			"serialnumRouters"=>$serialnum, 
			"macRouters"=>$mac, 
			"userRouters"=>$user,
			"passRouters"=>$pass,
			"ipstaticRouters"=>$ipstatic,
			"dyndnsRouters"=> $dyndns,
			"idPropietario"=> $idPropietario,
			"nomPropietario"=> $nomPropietario,
			"asignado"=> $asignado,
			"fecha_alta"=> $fecha_alta,
			"fecha_mod"=> $fecha_mod,
			"accion"=> "select",
			"tabla"=> "router",
			"nomFichero"=> "select_router.php",
			"id"=> $idrouters
		);
	}		
	$stmt->close();
	#  echo "<pre>"; print_r($data); echo "</pre>";
	echo json_encode(utf8ize($data));
	# $stmt->mysqli();
?>