<?php
	require_once("../funciones.php");
	conectar_bbdd('eude');
	include_once("../conexion/dbi_connect.php");
	$data = array();
	
	# Recogemos datos
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	$idRouterSelect = comprobarParametros('idRouterSelect'); 
	$nombreRouter = comprobarParametros('nombreRouter'); 
	$idTipoRouter = comprobarParametros('idTipoRouter'); 
	$numSerialRouter = comprobarParametros('numSerialRouter'); 
	$macRouter = comprobarParametros('macRouter'); 
	$userRouter = comprobarParametros('userRouter'); 
	$passRouter = comprobarParametros('passRouter'); 
	$ipStaticRouter = comprobarParametros('ipStaticRouter'); 
	$dyndnsRouter = comprobarParametros('dyndnsRouter'); 
	$fechaA = comprobarParametros('fechaA'); 
	$fechaM = comprobarParametros('fechaM'); 
	# echo "GET<pre>"; print_r($_GET); echo "</pre>"; 	echo "POS<pre>"; print_r($_POST); echo "</pre>";
	
	# Preparamos y ejecutamos la consulta	
	$stmt = $mysqli->prepare("UPDATE eude.routers
												SET
													name = ?,
													type = ?,
													serialnum = ?,
													mac= ?,
													user=?,
													pass= ?,
													ipstatic= ?,
													dyndns = ?,
													fecha_modificacion = CURRENT_TIMESTAMP
												WHERE
													idrouters = ?");		
	$stmt->bind_param("ssssssssi", $nombreRouter, $idTipoRouter, $numSerialRouter, $macRouter, $userRouter, $passRouter, $ipStaticRouter, $dyndnsRouter, $idRouterSelect);
	$stmt->execute();
	
	
	# Evaluamos is ha ido todo bien o habido algun fallo
	if($stmt->errno){
		$response=0;
		$proceso = 'error';
	}else{
		$response=1;
		$proceso = 'ok';
	}
		
	# Montamos el array 
	$data[] = array(
		"id"=>$idRouterSelect, 
		"nameRouters"=>$nombreRouter, 
		"idTipoRouter"=>$idTipoRouter , 
		"serialnumRouters"=>$numSerialRouter , 
		"macRouters"=>$macRouter , 
		"userRouters"=>$userRouter , 
		"passRouters"=>$passRouter , 
		"ipstaticRouters"=>$ipStaticRouter , 
		"dyndnsRouters"=>$dyndnsRouter , 
		"fecha_alta_europea"=>$fechaA,			
		"response" => $response,
		"proceso" => $proceso,
		"accion"=> "update",
		"tabla"=> "router",
		"nomFichero"=> "update_router.php",
	);

	$stmt->close();
	echo json_encode($data); 
	$mysqli->close();
?>