<?php
	require_once("../funciones.php");
	conectar_bbdd('eude');
	include_once("../conexion/dbi_connect.php");
	$data = array();
	
	# Recogemos datos
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	$nombreRouter = comprobarParametros('nombreRouter');
	$idTipoRouter = comprobarParametros('idTipoRouter');
	$numSerialRouter = comprobarParametros('numSerialRouter');
	$macRouter = comprobarParametros('macRouter');
	$userRouter = comprobarParametros('userRouter');
	$passRouter = comprobarParametros('passRouter');
	$ipStaticRouter = comprobarParametros('ipStaticRouter');
	$dyndnsRouter = comprobarParametros('dyndnsRouter');
	# echo "GET<pre>"; print_r($_GET); echo "</pre>"; echo "POS<pre>"; print_r($_POST); echo "</pre>";
	
	# Preparamos y ejecutamos la consulta
	$stmt = $mysqli->prepare("INSERT INTO eude.routers(name, type, serialnum, mac, user, pass, ipstatic, dyndns) VALUES (?,?,?,?,?,?,?,?)");		
	$stmt->bind_param("ssssssss", $nombreRouter, $idTipoRouter, $numSerialRouter, $macRouter, $userRouter, $passRouter, $ipStaticRouter, $dyndnsRouter);
	$stmt->execute();
	
	# Evaluamos is ha ido todo bien o habido algun fallo
	if($stmt->errno){
		$response=0;
		$proceso = 'error';
	}else{
		$response=1;
		$proceso = 'ok';
	}
	
	# Consultamos el ultimo id insertado.
	$result=$mysqli->query("SELECT LAST_INSERT_ID() AS ultimoID");		
	$row=$result->fetch_assoc();
		
	# Montamos el array 
	$data[] = array(
		"id"=>$row['ultimoID'], 
		"nameRouters"=>$nombreRouter, 
		"idTipoRouter"=>$idTipoRouter , 
		"serialnumRouters"=>$numSerialRouter , 
		"macRouters"=>$macRouter , 
		"userRouters"=>$userRouter , 
		"passRouters"=>$passRouter , 
		"ipstaticRouters"=>$ipStaticRouter , 
		"dyndnsRouters"=>$dyndnsRouter , 
		"response" => $response,
		"proceso" => $proceso,
		"accion"=> "insert",
		"tabla"=> "router",
		"nomFichero"=> "insert_router.php",
	);
	
	
	$stmt->close();
	echo json_encode($data); 
	$mysqli->close();	
?>