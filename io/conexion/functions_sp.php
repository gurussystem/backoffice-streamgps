<?php
include_once 'db_config_sp.php';
include_once 'secure-hash-class.php';
function sec_session_start() {
    $session_name = 'sec_session_id';   // Set a custom session name
    $secure = SECURE;
    // This stops JavaScript being able to access the session id.
    $httponly = true;
    // Forces sessions to only use cookies.
    if (ini_set('session.use_only_cookies', 1) === FALSE) {
        header("Location: ../error.php?err=Could not initiate a safe session (ini_set)");
        exit();
    }
    // Gets current cookies params.
    $cookieParams = session_get_cookie_params();
    session_set_cookie_params($cookieParams["lifetime"],
        $cookieParams["path"], 
        $cookieParams["domain"], 
        $secure,
        $httponly);
    // Sets the session name to the one set above.
    session_name($session_name);
    session_start();            // Start the PHP session 
    session_regenerate_id(true);    // regenerated the session, delete the old one. 
}
function loginUser($user, $password,$mysqli) {

    // Using prepared statements means that SQL injection is not possible. 
    if ($stmt = $mysqli->prepare("SELECT id, user, pass, salt 
        FROM users WHERE user = ? LIMIT 1")) {
        $stmt->bind_param('s', $user);  // Bind "$user" to parameter.
        $stmt->execute();   // Execute the prepared query.
        $stmt->store_result();
 
        // get variables from result.
        $stmt->bind_result($user_id, $user, $db_password, $salt);
        $stmt->fetch();
    
        if ($stmt->num_rows == 1) {
            // If the user exists we check if the account is locked
            // from too many login attempts 
           
            if (checkbrute($user_id,$mysqli) == true) {
                // Account is locked 
                // Send an email to user saying their account is locked
              
                
                return false;
            } else {
                // Check if the password in the database matches
                // the password the user submitted.
                // load the class
              
                $secure = new SecureHash();        

                // check for password match
                if ($secure->validate_hash($password, $db_password, $salt)) {
                    // Password is correct!
                    // Get the user-agent string of the user.
                    $user_browser = $_SERVER['HTTP_USER_AGENT'];
                    // XSS protection as we might print this value
                    $user_id = preg_replace("/[^0-9]+/", "", $user_id);
                    $_SESSION['user_id'] = $user_id;
                    // XSS protection as we might print this value
                    $user = preg_replace("/[^a-zA-Z0-9_\-]+/", 
                                                                "", 
                                                                $user);
                    $_SESSION['user'] = $user;
                    $_SESSION['login_string'] = hash('sha512', 
                              $password . $user_browser);
                    // Login successful.
                    return true;
                } else {
                    // Password is not correct
                    // We record this attempt in the database
                    $now = time();
                    $mysqli->query("INSERT INTO ".DB_CLIENTE.".login_attempts(user_id, time)
                                    VALUES ('$user_id', '$now')");
                    return false;
                }
            }
        } else {
            // No user exists.
            return false;
        }
    }
}
function loginEmail($mail, $password,$mysqli) {
    // Using prepared statements means that SQL injection is not possible. 
    if ($stmt = $mysqli->prepare("SELECT id, user, password, salt 
        FROM users
       WHERE mail = ?
        LIMIT 1")) {
        $stmt->bind_param('s', $mail);  // Bind "$mail" to parameter.
        $stmt->execute();    // Execute the prepared query.
        $stmt->store_result();
 
        // get variables from result.
        $stmt->bind_result($user_id, $user, $db_password, $salt);
        $stmt->fetch();
 
        if ($stmt->num_rows == 1) {
            // If the user exists we check if the account is locked
            // from too many login attempts 
 
            if (checkbrute($user_id,$mysqli) == true) {
                // Account is locked 
                // Send an email to user saying their account is locked
                return false;
            } else {
                // Check if the password in the database matches
                // the password the user submitted.
                // load the class
                $secure = new SecureHash();        

                // check for password match
                if ($secure->validate_hash($password, $db_password, $salt)) {
                    // Password is correct!
                    // Get the user-agent string of the user.
                    $user_browser = $_SERVER['HTTP_USER_AGENT'];
                    // XSS protection as we might print this value
                    $user_id = preg_replace("/[^0-9]+/", "", $user_id);
                    $_SESSION['user_id'] = $user_id;
                    // XSS protection as we might print this value
                    $user = preg_replace("/[^a-zA-Z0-9_\-]+/", 
                                                                "", 
                                                                $user);
                    $_SESSION['user'] = $user;
                    $_SESSION['login_string'] = hash('sha512', 
                              $password . $user_browser);
                    // Login successful.
                    return true;
                } else {
                    // Password is not correct
                    // We record this attempt in the database
                    $now = time();
                    $mysqli->query("INSERT INTO ".DB_CLIENTE.".login_attempts(user_id, time)
                                    VALUES ('$user_id', '$now')");
                    return false;
                }
            }
        } else {
            // No user exists.
            return false;
        }
    }
}
function login_register($user,$mail,$pass,$idEmpresa,$idRol,$mysqli){
// load the class
$secure = new SecureHash();

// the password to encrypt
//$pass = 'example p@ssword';

// salt is passed by reference and generated on the fly
$salt = '';

// the encrypted version of the password (for database storage)
$encrypted = $secure->create_hash($pass, $salt);

// salt is now populated
//echo $salt . PHP_EOL;

// now store both the $encrypted and $salt values in the database
 $mysqli->query("INSERT INTO users(user,pass,salt,idEmpresa,idRol)
                                    VALUES ('$user', '$encrypted','$salt','$idEmpresa','$idRol')");

}


function login_register_all($newId, $userName, $email, $pass, $idRol, $mysqli, $telefono,  $session, $disabled, $token, $tokenExpire, $urlLive, $social_name, $total_minutes, $minutes_left , $address, $streamName, $autostar, $fileRtsp, $fileHls, $fileHlsMob, $fileHlsAdmin, $fbAppId, $fbAppUrl, $host, $titulo, $server, $showJugadas, $urlPlayer, $geoBloqueo){
	
	// now store both the $encrypted and $salt values in the database
 	// load the class
	$secure = new SecureHash();

	// salt is passed by reference and generated on the fly
	$salt = '';
	
	// the encrypted version of the password (for database storage)
	$encrypted = $secure->create_hash($pass, $salt);


	// salt is now populated
	//echo $salt . PHP_EOL;

	$mysqli->query("INSERT INTO streamsports.users (id, idRol, user, pass, mail, phone, salt, session , disabled, token, token_expire,urlLive, social_name, total_minutes, minutes_left, address, streamName, autostart, fileRtsp, fileHls, fileHlsMob , fileHlsAdmin, fbAppId, fbAppUrl, host, title , server, showJugadas, urlPlayer, geobloqueo)  
VALUE( '$newId','$idRol','$userName','$encrypted', '$email', '$telefono', '$salt', '$session', '$disabled' , '$token', '$tokenExpire', '$urlLive', '$social_name', '$total_minutes', '$minutes_left' , '$address', '$streamName', '$autostar', '$fileRtsp', '$fileHls', '$fileHlsMob', '$fileHlsAdmin', '$fbAppId','$fbAppUrl', '$host', '$titulo', '$server', '$showJugadas', '$urlPlayer', '$geoBloqueo')");

	
}


function checkbrute($user_id,$mysqli) {
    // Get timestamp of current time 
    $now = time();
 
    // All login attempts are counted from the past 2 hours. 
    $valid_attempts = $now - (2 * 60 * 60);
 
    if ($stmt = $mysqli->prepare("SELECT time 
                             FROM ".DB_CLIENTE.".login_attempts 
                             WHERE user_id = ? 
                            AND time > '$valid_attempts'")) {
        $stmt->bind_param('i', $user_id);
 
        // Execute the prepared query. 
        $stmt->execute();
        $stmt->store_result();
 
        // If there have been more than 5 failed logins 
        if ($stmt->num_rows > 5) {
            return true;
        } else {
            return false;
        }
    }
}
function login_check($mysqli) {
    // Check if all session variables are set 
    if (isset($_SESSION['user_id'], 
                        $_SESSION['username'], 
                        $_SESSION['login_string'])) {
 
        $user_id = $_SESSION['user_id'];
        $login_string = $_SESSION['login_string'];
        $username = $_SESSION['username'];
 
        // Get the user-agent string of the user.
        $user_browser = $_SERVER['HTTP_USER_AGENT'];
 
        if ($stmt = $mysqli->prepare("SELECT password 
                                      FROM members 
                                      WHERE id = ? LIMIT 1")) {
            // Bind "$user_id" to parameter. 
            $stmt->bind_param('i', $user_id);
            $stmt->execute();   // Execute the prepared query.
            $stmt->store_result();
 
            if ($stmt->num_rows == 1) {
                // If the user exists get variables from result.
                $stmt->bind_result($password);
                $stmt->fetch();
                $login_check = hash('sha512', $password . $user_browser);
 
                if ($login_check == $login_string) {
                    // Logged In!!!! 
                    return true;
                } else {
                    // Not logged in 
                    return false;
                }
            } else {
                // Not logged in 
                return false;
            }
        } else {
            // Not logged in 
            return false;
        }
    } else {
        // Not logged in 
        return false;
    }
}
?>