<?php
	require_once("../funciones.php");
	conectar_bbdd('eude');
	require_once("../conexion/dbi_connect.php");
	$data = array();

	# Recogemos datos
	$idSelect = comprobarParametros('idCamaraSelect');
	$nombreCamara = comprobarParametros('nombreCamara');
	$idTipoCamara = comprobarParametros('idTipoCamara');
	$numSerialCamara = comprobarParametros('numSerialCamara');
	$macCamara = comprobarParametros('macCamara');
	$userCamara = comprobarParametros('userCamara');
	$passCamara = comprobarParametros('passCamara');
	$locationIpCamara = comprobarParametros('locationIpCamara');
	$rtsp_1 = comprobarParametros('rtsp_1');
	$kbps_1 = comprobarParametros('kbps_1');
	$resolution_1 = comprobarParametros('resolution_1');
	$rtsp_2 = comprobarParametros('rtsp_2');	
	$kbps_2 = comprobarParametros('kbps_2');
	$resolution_2 = comprobarParametros('resolution_2');
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	$fechaA = comprobarParametros('fechaA'); 
	$fechaM = comprobarParametros('fechaM'); 
	 # echo "GET<pre>"; print_r($_GET); echo "</pre>"; echo "POS<pre>"; print_r($_POST); echo "</pre>";
	
	# Preparamos y ejecutamos la consulta	
	$consulta = "UPDATE	eude.camaras 
						SET
							name=?,
							type=?,
							serialnum=?,
							mac=?,
							user=?,
							pass=?,
							localip=?,
							rtsp_main=?,
							kbps_main=?,
							resolution_main=?,
							rtsp_second=?,
							kbps_second=?,
							resolution_second=?,
							fecha_modificacion = CURRENT_TIMESTAMP
						WHERE
							idcamaras=?;"; 
	$stmt = $mysqli->prepare($consulta);	
	$stmt->bind_param("sssssssssssssi" , $nombreCamara ,$idTipoCamara, $numSerialCamara, $macCamara, $userCamara,  $passCamara, $locationIpCamara, $rtsp_1, $kbps_1, $resolution_1, $rtsp_2, $kbps_2, $resolution_2,  $idSelect);  
	$stmt->execute();
	
	# Evaluamos is ha ido todo bien o habido algun fallo
	if($stmt->errno){
		$response=0;
		$proceso = 'error';
	}else{
		$response=1;
		$proceso = 'ok';
	}
	
			
	# Montamos el array 
	$data[] = array(
		"id"=>$idSelect, 
		"nameCamara"=>$nombreCamara, 
		"idTipoCamara"=>$idTipoCamara, 
		"serialnumCamara"=>$numSerialCamara, 
		"macCamara"=>$macCamara, 
		"userCamara"=>$userCamara,
		"passCamara"=>$passCamara,
		"localipCamara"=>$locationIpCamara,
		"rtspMainCamara"=>$rtsp_1,
		"kbpsMainCamara"=>$kbps_1,		
		"resolutionMainCamara"=>$resolution_1,
		"rtspSecondCamara"=>$rtsp_2,		
		"kbpsSecondCamara"=>$kbps_2,
		"resolutionSecondCamara"=>$resolution_2,		
		"response" => $response,
		"proceso" => $proceso,
		"accion"=> "update",
		"tabla"=> "camara",
		"nomFichero"=> "update_camara.php",
	);
		
	$stmt->close();
	echo json_encode($data); 
	$mysqli->close();
?>