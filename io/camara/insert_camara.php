<?php
	require_once("../funciones.php");
	conectar_bbdd('eude');
	include_once("../conexion/dbi_connect.php");
	$data = array();
	
	# Recogemos datos	
	$nombreCamara = comprobarParametros('nombreCamara'); 
	$idTipoCamara = comprobarParametros('idTipoCamara');
	$numSerialCamara = comprobarParametros('numSerialCamara');
	$macCamara = comprobarParametros('macCamara');
	$userCamara = comprobarParametros('userCamara');
	$passCamara = comprobarParametros('passCamara');
	$locationIpCamara = comprobarParametros('locationIpCamara');
	$rtspMain = comprobarParametros('rtspMain');
	$resolutionMain = comprobarParametros('resolutionMain');
	$kbpsMain = comprobarParametros('kbpsMain');
	$rtspSecond = comprobarParametros('rtspSecond');
	$resolutionSecond = comprobarParametros('resolutionSecond');
	$kbpsSecond = comprobarParametros('kbpsSecond'); 
	$tiempoDesfase = comprobarParametros ('tiempoDesfase'); 
	# echo "GET<pre>"; print_r($_GET); echo "</pre>"; echo "POS<pre>"; print_r($_POST); echo "</pre>";
	
	# Preparamos y ejecutamos la consulta	
	$consulta = "INSERT INTO	eude.camaras( name, type, serialnum, mac, user, pass, localip, rtsp_main, rtsp_second, resolution_main,     kbps_main, resolution_second, kbps_second) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	$stmt = $mysqli->prepare($consulta);		
	$stmt->bind_param("sssssssssssss",$nombreCamara,$idTipoCamara, $numSerialCamara, $macCamara, $userCamara,$passCamara,$locationIpCamara,$rtspMain,$rtspSecond,$resolutionMain, $kbpsMain,$resolutionSecond, $kbpsSecond);
	$stmt->execute();
	
	# Evaluamos is ha ido todo bien o habido algun fallo
	if($stmt->errno){
		$response=0;
		$proceso = 'error';
	}else{
		$response=1;
		$proceso = 'ok';
	}
	
	# Consultamos el ultimo id insertado.
	$result=$mysqli->query("SELECT LAST_INSERT_ID() AS ultimoID;");		
	$row=$result->fetch_assoc();
		
	# Montamos el array 
	$data[] = array(
		"id"=>$row['ultimoID'], 
		"nameCamara"=>$nombreCamara, 
		"idTipoCamara"=>$idTipoCamara, 
		"serialnumCamara"=>$numSerialCamara, 
		"macCamara"=>$macCamara, 
		"userCamara"=>$userCamara,
		"passCamara"=>$passCamara,
		"localipCamara"=>$locationIpCamara,
		"rtspMainCamara"=>$rtspMain,
		"rtspSecondCamara"=>$rtspSecond,
		"resolutionMainCamara"=>$resolutionMain,
		"kbpsMainCamara"=>$kbpsMain,
		"resolutionSecondCamara"=>$resolutionSecond,
		"kbpsSecondCamara"=>$kbpsSecond,		
		"response" => $response,
		"proceso" => $proceso,
		"accion"=> "insert",
		"tabla"=> "camara",
		"nomFichero"=> "insert_camara.php",
	);	

	$stmt->close();
	echo json_encode($data); 
	$mysqli->close();
?>