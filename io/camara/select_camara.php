<?php 
	require_once("../funciones.php");
	conectar_bbdd('eude');
	include_once("../conexion/dbi_connect.php");
	$data = array();
	
	# Recogemos datos
	$idCamara_selecionada = comprobarParametros ('idCamara_selecionada');
	$tiempoDesfase = comprobarParametros ('tiempoDesfase');
	// echo "GET<pre>"; print_r($_GET); echo "</pre>"; echo "POS<pre>"; print_r($_POST); echo "</pre>";

	# Consulta
	$sql = "SELECT
		c.idcamaras as idcamaras ,
		c.name as name,
		c.type as type,
		c.serialnum as serialnum,
		c.mac as mac,
		c.user as user,
		c.pass as pass,
		c.localip as localip,
		c.rtsp_main as rtsp_main,
		c.rtsp_second as rtsp_second,
		c.resolution_main as resolution_main,
		c.kbps_main as kbps_main,
		c.resolution_second as resolution_second,
		c.kbps_second as kbps_second,
        c.asignada as asignada,
		c.fecha_alta as fecha_alta,
		c.fecha_modificacion as fecha_modificacion,
		(Select p.idEmpresa from empresas as e inner join productos as p on(p.idEmpresa = e.id)  where p.idCamara = c.idcamaras) as idPropietario,
		(Select e.nombre from empresas as e inner join productos as p on(p.idEmpresa = e.id)  where p.idCamara = c.idcamaras) as nomPropietario
	FROM
		eude.camaras as c";
	
	# Preparamos y ejecutamos la consulta
	if($idCamara_selecionada){
		$consulta = $sql . " where c.idCamaras = ?";
		$stmt = $mysqli->prepare($consulta);		
		$stmt->bind_param("i", $idCamara_selecionada);
	}else{	
		$stmt = $mysqli->prepare($sql);		
	}
	$stmt->execute();
	
	# Vincular variables a la sentencia preparada
	$stmt->bind_result($idcamaras, $nameCamara, $idTipoCamara, $serialnum, $mac, $user, $pass, $localip, $rtsp_main,  $rtsp_second, $resolution_main, $kbps_main, $resolution_second, $kbps_second, $asignada, $fecha_alta, $fecha_modificacion, $idPropietario, $nomPropietario);
	
	#  Obtenemos los valores
	while($stmt->fetch() ) {	
						
		$data[] = array(
			"idCamara"=>$idcamaras, 
			"id"=>$idcamaras, 
			"nameCamara"=>$nameCamara, 
			"idTipoCamara"=>$idTipoCamara, 
			"serialnumCamara"=>$serialnum, 
			"macCamara"=>$mac, 
			"userCamara"=>$user,
			"passCamara"=>$pass,
			"localipCamara"=>$localip,
			"rtspMainCamara"=>$rtsp_main,
			"rtspSecondCamara"=>$rtsp_second,
			"resolutionMainCamara"=>$resolution_main,
			"kbpsMainCamara"=>$kbps_main,
			"resolutionSecondCamara"=>$resolution_second,
			"kbpsSecondCamara"=>$kbps_second,
			"idPropietario"=>$idPropietario,
			"nomPropietario"=>$nomPropietario,		
			"asignada"=>$asignada,
			# "fecha_alta"=>date("d-m-Y",strtotime($row["fecha_alta"])),			
			"fecha_alta"=>desfaseHorario($fecha_alta, $tiempoDesfase, "entera", "suma"),			
			"fecha_mod"=>controlFechaModificacion($fecha_modificacion, $tiempoDesfase, "entera", "suma"),
			"accion"=> "select",
			"tabla"=> "camara",
			"nomFichero"=> "select_camara.php"							
		);
	}	
	
	$stmt->close();
	# echo "<pre>"; print_r($data); echo "</pre>";
	echo json_encode(utf8ize($data));
	# $stmt->mysqli();
?>