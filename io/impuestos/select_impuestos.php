<?php 
	require_once("../funciones.php");
	conectar_bbdd('configstream');
	include_once("../conexion/dbi_connect.php");
	$data = array();
		
	# Recogemos datos
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	$idSeleccionado = comprobarParametros('idSelecionado'); 
	# echo "GET<pre>"; print_r($_GET); echo "</pre>"; echo "POS<pre>"; print_r($_POST); echo "</pre>";

	# Preparamos y ejecutamos la consulta
	$sql = "SELECT id, nombre, valor, descripcion, fecha_alta, fecha_modificacion FROM configstream.impuestos"; 	
	if($idSeleccionado){
		$consulta = $sql . " WHERE id = ?";
		$stmt = $mysqli->prepare($consulta);		
		$stmt->bind_param("i", $idSeleccionado);
	}else{
		$stmt = $mysqli->prepare($sql);				
	}
	$stmt->execute();
	$stmt->bind_result($id, $nombre, $valor, $descripcion, $fecha_alta, $fecha_modificacion);
	
	while($stmt->fetch()) {
		$data[] = array(
				"id"=>$id, 
				"nombre"=>$nombre, 
				"valor"=>$valor, 
				"descripcion"=>$descripcion, 
				"fecha_alta"=>desfaseHorario($fecha_alta, $tiempoDesfase, "entera", "suma") ,
				"fecha_mod"=>controlFechaModificacion($fecha_modificacion, $tiempoDesfase, "entera", "suma"),
				"accion"=> "select",
				"tabla"=> "impuestos",
				"nomFichero"=> "select_impuestos.php",
			);
		}	
	
	$stmt->close();
	# echo "<pre>"; print_r($data); echo "</pre>";
echo json_encode(utf8ize($data));
	 # json_errores();
	$mysqli->close();
?>
