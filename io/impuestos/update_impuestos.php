<?php
	require_once("../funciones.php");
	conectar_bbdd('configstream');
	include_once("../conexion/dbi_connect.php");
	$data = array();

	# Recogemos datos
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	$idSelect = comprobarParametros('idSelect'); 
	$nombre = comprobarParametros('nombre'); 
	$descripcion = comprobarParametros('descripcion'); 
	$valor = comprobarParametros('valor'); 	
	# echo "GET<pre>"; print_r($_GET); echo "</pre>"; echo "POS<pre>"; print_r($_POST); echo "</pre>";
	
	#Preparamos y ejecutamos la consulta				
	$consulta = "UPDATE configstream.impuestos	SET nombre = ?,  valor = ?,   descripcion = ?,  fecha_modificacion = CURRENT_TIMESTAMP
    WHERE	id = ?" ;
	$stmt = $mysqli->prepare($consulta);		
	$stmt->bind_param("sisi", $nombre, $valor, $descripcion, $idSelect);
	$stmt->execute();
	
	# Evaluamos is ha ido todo bien o habido algun fallo
	if($stmt->errno){
		$response=0;
		$proceso = 'error';
	}else{
		$response=1;
		$proceso = 'ok';
	}
	
	# Montamos el array 
	$data[] = array(
		"id"=>$idSelect, 
		"nombre"=>$nombre, 
		"valor"=>$valor, 
		"descripcion"=>$descripcion, 	
		"response" => $response,
		"proceso" => $proceso,
		"accion"=> "update",
		"tabla"=> "impuestos",
		"nomFichero"=> "update_impuestos.php",
	);
		
	$stmt->close();
	echo json_encode($data); 
	$mysqli->close();	
?>