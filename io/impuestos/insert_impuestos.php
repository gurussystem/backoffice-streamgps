<?php
	require_once("../funciones.php");
	conectar_bbdd('configstream');
	include_once("../conexion/dbi_connect.php");
	$data = array();
	
	# Recogemos datos
	$nombre = comprobarParametros('nombre'); 
	$valor = comprobarParametros('valor'); 
	$descripcion = comprobarParametros('descripcion'); 
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	# echo "GET<pre>"; print_r($_GET); echo "</pre>"; echo "POS<pre>"; print_r($_POST); echo "</pre>";
	
	# Preparamos y ejecutamos la consulta	
	$consulta = "INSERT INTO	configstream.impuestos (nombre, valor, descripcion) VALUES (?,?,?)";
	$stmt = $mysqli->prepare($consulta);		
	$stmt->bind_param("sis", $nombre, $valor, $descripcion );
	$stmt->execute();	
	
	# Evaluamos is ha ido todo bien o habido algun fallo
	if($stmt->errno){
		$response=0;
		$proceso = 'error';
	}else{
		$response=1;
		$proceso = 'ok';
	}
	
	# Consultamos el ultimo id insertado.
	$result=$mysqli->query("SELECT LAST_INSERT_ID() AS ultimoID;");		
	$row=$result->fetch_assoc();

	
	# Montamos el array 
	$data[] = array(
		"id"=>$row['ultimoID'], 
		"nombre"=>$nombre, 
		"valor"=>$valor, 
		"descripcion"=>$descripcion, 	
		"response" => $response,
		"proceso" => $proceso,
		"accion"=> "insert",
		"tabla"=> "impuestos",
		"nomFichero"=> "insert_impuesto.php",
	);
	

	$stmt->close();
	echo json_encode($data);
	$mysqli->close();	
?>