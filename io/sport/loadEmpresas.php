<?php
	// Conectamos con la BD
	require_once("../conexion/dbi_connectSport.php");
	
	// Tenemos que sacar el id del ultimo usuario
	# Consultamos el ultimo id de empresa insertado para añadir el nuevo  
	$stmt = $mysqli->prepare("SELECT id, user FROM streamsports.users");		
	$stmt->execute();
	$result = $stmt->get_result(); 

	//Evaluamos is ha ido todo bien o habido algun fallo
	if($stmt->errno){
		$response=0;
		$proceso = 'error';
	}else{
		$response=1;
		$proceso = 'ok';
	}
	
	while($row=$result->fetch_assoc()) {
		
		//Montamos el array 
		$data[] = array(
			"id"=>$row['id'], 		
			"userName" => $row['user'],
			"response" => $response,
			"proceso" => $proceso,
			"accion"=> "select",
			"tabla"=> "userSport",
			"nomFichero"=> "loadEmpresas.php",
		);
	
	}
	// echo "<br> newId: " . $newId;

	$stmt->close();
	echo json_encode($data); 
	

?>