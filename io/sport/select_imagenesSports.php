<?php
	// Conectamos con la BD
	require_once("../conexion/dbi_connectSport.php");
	//$data = array();
	require_once("../funciones.php");
	require_once("../conexion/functions.php");
	
	
	//comprobamos que sea una petición ajax
	$idUser=comprobarParametros('idUser'); 

	// echo "POST: <pre>";print_r($_POST);echo "</pre> GET <pre>";print_r($_GET);echo "</pre>";

	// Tenemos que sacar el id del ultimo usuario
	# Consultamos el ultimo id de empresa insertado para añadir el nuevo  
	$stmt = $mysqli->prepare("SELECT poster, logoPlayer, logo_mosca, logo_mosca_on, logoPosition FROM streamsports.users where id=?");		
	$stmt->bind_param("i", $idUser);  
	$stmt->execute();
	$result = $stmt->get_result(); 
	
	//Evaluamos is ha ido todo bien o habido algun fallo
	if($stmt->errno){
		$response=0;
		$proceso = 'error';
	}else{
		$response=1;
		$proceso = 'ok';
	}
	
	while($row=$result->fetch_assoc()) {
		//Montamos el array 
		$data[] = array(
			"id"=>$idUser, 		
			"logoPoster"=>$row['poster'], 		
			"logoPlayer"=>$row['logoPlayer'], 		
			"logoMosca"=>$row['logo_mosca'], 		
			"logoMosca_on"=>$row['logo_mosca_on'], 		
			"logoPosition"=>$row['logoPosition'], 		
			"response" => $response,
			"proceso" => $proceso,
			"accion"=> "select",
			"tabla"=> "userSport",
			"nomFichero"=> "select_imagenesSports.php",
		);
	}


	$stmt->close();
	echo json_encode($data); 
	

?>