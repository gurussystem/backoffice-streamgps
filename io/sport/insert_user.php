<?php
	 // Conectamos con la BD
	include("../conexion/dbi_connectSport.php");
	
	//$data = array();
	require_once("../funciones.php");

	//comprobamos que sea una petición ajax
	$idRol =comprobarParametros('idRol'); 
	$userName =comprobarParametros('userName');
	$pass =comprobarParametros('clave');
	$email =comprobarParametros('email');
	$telefono =comprobarParametros('telefono');
	$session =comprobarParametros('session');
	$disabled =comprobarParametros('estado');
	$total_minutes =comprobarParametros('total_minutes');
	$minutes_left =comprobarParametros('minutes_left');
	$address =comprobarParametros('address');
	$autostar =comprobarParametros('autostar');
	$titulo =comprobarParametros('titulo');
	$live =comprobarParametros('live');
	$server =comprobarParametros('server');
	$port =comprobarParametros('port');
	$showJugadas =comprobarParametros('showJugadas');
	$geoBloqueo =comprobarParametros('geoBloqueo');
	// fb datos
	$social_name = comprobarParametros("social_name");
	$token = comprobarParametros("token");
	$tokenExpire = comprobarParametros("tokenExpire");
	$fbAppId = comprobarParametros("fbAppId");
	$fbAppUrl = comprobarParametros("fbAppUrl");	
	 // echo "POST: <pre>";print_r($_POST);echo "</pre> GET <pre>";print_r($_GET);echo "</pre>";


	// Tenemos que sacar el id del ultimo usuario
	# Consultamos el ultimo id de empresa insertado para añadir el nuevo  
	$stmt = $mysqli->prepare("SELECT id FROM streamsports.users order by id desc limit 1");		
	$stmt->execute();
	$result = $stmt->get_result(); 
	$res=$result->fetch_assoc();
	$newId = $res['id'] + 1;
	// echo "<br> newId: " .$newId;

	$urlLive ="http://".$server.":".$port."/live/".$newId."_".$userName."/playlist.m3u8";
	$streamName =$newId."_".$userName."out_720p";
	$fileRtsp ="rtsp://".$server.":".$port."/livecentral/".$newId."_".$userName."_out"; 
	$fileHls ="http://d1tzvl7r79u4qr.cloudfront.net/livecentral/ngrp:".$newId."_".$userName."_out_all/playlist.m3u8";
	$fileHlsAdmin ="http://".$server.":".$port."/livecentral/ngrp:".$newId."_".$userName."_out_mobile/playlist.m3u8"; 
	$fileHlsMob  ="http://d1tzvl7r79u4qr.cloudfront.net/livecentral/ngrp:".$newId."_".$userName."_out_mobile/playlist.m3u8"; 
	$host =$userName.".streamsports.es"; 
	$urlPlayer ="http://".$host."/directo/indexMobile.php";
 	
	/*
	echo "<br> newId: " .$newId;
	echo "<br> idRol: " . $idRol;
	echo "<br> userName: " . $userName;
	echo "<br> clave: " . $pass;
	echo "<br> email: " . $email;
	echo "<br> telefono: " . $telefono;
	echo "<br> session: " . $session;
	echo "<br> disabled: " . $disabled;
	echo "<br> token: " . $token;
	echo "<br> token_expire: " . $token_expire;
	echo "<br> urlLive: " . $urlLive;
	echo "<br> social_name: " . $social_name;
	echo "<br> total_minutes: " . $total_minutes;
	echo "<br> minutes_left: " . $minutes_left;
	echo "<br> address: " . $address;
	echo "<br> streamName: " . $streamName;
	echo "<br> autostar: " . $autostar;
	echo "<br> fileRtsp: " . $fileRtsp;
	echo "<br> fileHls: " . $fileHls;
	echo "<br> fileHlsMob: " . $fileHlsMob;
	echo "<br> fileHlsAdmin: " . $fileHlsAdmin;
	echo "<br> fbAppId: " . $fbAppId;
	echo "<br> fbAppUrl: " . $fbAppUrl;
	echo "<br> host: " . $host;
	echo "<br> titulo: " . $titulo;
	echo "<br> server: " . $server;
	echo "<br> showJugadas: " . $showJugadas;
	echo "<br> urlPlayer: " . $urlPlayer;
	echo "<br> geoBloqueo: " . $geoBloqueo;
*/
	require_once("../conexion/functions_sp.php");
	login_register_all($newId, $userName, $email, $pass, $idRol, $mysqli, $telefono,  $session, $disabled, $token, $tokenExpire, $urlLive, $social_name, $total_minutes, $minutes_left , $address, $streamName, $autostar, $fileRtsp, $fileHls, $fileHlsMob, $fileHlsAdmin, $fbAppId, $fbAppUrl, $host, $titulo, $server, $showJugadas, $urlPlayer, $geoBloqueo);
	
	//Montamos el array 
	$data[] = array(
		"id"=>$newId, 		
		"response" => 1,
		"proceso" => 'ok',
		"accion"=> "insert",
		"tabla"=> "userSport",
		"nomFichero"=> "insert_user.php",
	);
	
	
	$stmt->close();
	echo json_encode($data); 
	$mysqli->close();

?>