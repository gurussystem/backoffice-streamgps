<?php
	// Conectamos con la BD
	require_once("../conexion/dbi_connectSport.php");
	//$data = array();
	require_once("../funciones.php");
	require_once("../conexion/functions.php");
	
	
	//comprobamos que sea una petición ajax
	$idUser=comprobarParametros('idUser'); 
	
	// fb datos
	/*
	$social_name = comprobarParametros("social_name");
	$token = comprobarParametros("token");
	$tokenExpire = comprobarParametros("tokenExpire");
	$fbAppId = comprobarParametros("fbAppId");
	$fbAppUrl = comprobarParametros("fbAppUrl");	
	*/
	// echo "POST: <pre>";print_r($_POST);echo "</pre> GET <pre>";print_r($_GET);echo "</pre>";

	// Tenemos que sacar el id del ultimo usuario
	# Consultamos el ultimo id de empresa insertado para añadir el nuevo  
	$stmt = $mysqli->prepare("SELECT social_name, token, token_expire, fbAppId, fbAppUrl FROM streamsports.users where id=?");		
	$stmt->bind_param("i", $idUser);  
	$stmt->execute();
	$result = $stmt->get_result(); 
	
	//Evaluamos is ha ido todo bien o habido algun fallo
	if($stmt->errno){
		$response=0;
		$proceso = 'error';
	}else{
		$response=1;
		$proceso = 'ok';
	}
	
	while($row=$result->fetch_assoc()) {
		//Montamos el array 
		$data[] = array(
			"id"=>$idUser, 		
			"social_name"=>$row['social_name'], 		
			"token"=>$row['token'], 		
			"token_expire"=>$row['token_expire'], 		
			"fbAppId"=>$row['fbAppId'], 		
			"fbAppUrl"=>$row['fbAppUrl'], 		
			"response" => $response,
			"proceso" => $proceso,
			"accion"=> "select",
			"tabla"=> "userSport",
			"nomFichero"=> "select_redesSociales.php",
		);
	}


	$stmt->close();
	echo json_encode($data); 
	

?>