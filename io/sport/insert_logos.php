<?php
// Conectamos con la BD
	require_once("../conexion/dbi_connectSport.php");
	
$data['server'] = $_SERVER;
//comprobamos que sea una petición ajax
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	
	require_once("../funciones.php");
	$idUser=comprobarParametros('idUser'); 
	$logo_mosca_on=comprobarParametros('logo_mosca_on'); 
	$posicionLogo=comprobarParametros('posicionLogo'); 
	$logoMosca_name=comprobarParametros('logoMosca_name'); 
	$logoPlayer_name=comprobarParametros('logoPlayer_name'); 
	$logoPoster_name=comprobarParametros('logoPoster_name'); 


 
	$data['idUser'] = $idUser;	
	$data['posicionLogo'] = $posicionLogo;	
	$data['logo_mosca_on'] = $logo_mosca_on;	
	$data['logoMosca_name'] = $logoMosca_name;
	$data['logoPlayer_name'] = $logoPlayer_name;
	$data['logoPoster_name'] = $logoPoster_name;
	
    
	// hacemos una consulta para sacar el nameUser
	$stmt = $mysqli->prepare("SELECT user FROM streamsports.users where id= $idUser ");		
	$stmt->execute();
	$result = $stmt->get_result(); 
	$res=$result->fetch_assoc();
	$data['userName'] = $res['user'];
	$nameUser  = $res['user'];
	$stmt->close();
	
	
	$data['rutaImagen'] = $_SERVER['REQUEST_SCHEME']."://".$_SERVER['HTTP_HOST']."/io/sport/logosSports/".$nameUser;
	
    //comprobamos si existe un directorio para subir el archivo si no es así, lo creamos
	existDir(); //carpeta principal
	existDir2($nameUser); // carpeta del cliente
	
	// Por cada archivo de imagen
	
	// Logo Mosca
	if($logoMosca_name == ""){
		$data['logoMosca']  = subirFile( $_FILES['logoMosca']['name'], $_FILES['logoMosca']['tmp_name'], $nameUser, $_FILES['logoMosca']);	
		$logoMosca = $data['rutaImagen']."/".$_FILES['logoMosca']['name'];
	}else{
		$logoMosca = $logoMosca_name;
	}
	
	// Logo poster
	if($logoPlayer_name == ""){
		$data['logoPlayer']  = subirFile( $_FILES['logoPlayer']['name'], $_FILES['logoPlayer']['tmp_name'], $nameUser, $_FILES['logoPlayer']);
		$logoPlayer = $data['rutaImagen']."/".$_FILES['logoPlayer']['name'];
	}else{
		$logoPlayer = $logoPlayer_name;
	}
	
	// Logo poster
	if($logoPoster_name == ""){
		$data['logoPoster']  = subirFile( $_FILES['logoPoster']['name'], $_FILES['logoPoster']['tmp_name'], $nameUser, $_FILES['logoPoster']);
		$logoPoster = $data['rutaImagen']."/".$_FILES['logoPoster']['name'];
	}else{
		$logoPoster = $logoPoster_name;		
	}
	
	
	//galeriaLogos($nameUser);
	$stmt = $mysqli->prepare("UPDATE streamsports.users  SET logo_mosca = ?, poster = ?, logoPlayer =?, logoPosition = ?, logo_mosca_on = ? WHERE id = ?  ");	
	$stmt->bind_param("ssssii", $logoMosca , $logoPoster, $logoPlayer, $posicionLogo , $logo_mosca_on , $idUser);										
	$stmt->execute();
	
	echo json_encode($data); 
	
}else{
    throw new Exception("Error Processing Request", 1);   
}


function existDir(){
	 if(!is_dir("logosSports/")) 
        mkdir("logosSports/", 0777);
      sleep(3);
}


function existDir2($nameUser){    
	 if(!is_dir("logosSports/".$nameUser)) 
        mkdir("logosSports/".$nameUser, 0777);
   sleep(3);	
}


function subirFile($nameFile, $tmp_name, $nameUser, $arrayFile){
	if ($nameFile && move_uploaded_file($tmp_name, "logosSports/".$nameUser."/".$nameFile)){
       sleep(3);//retrasamos la petición 3 segundos
	   return $arrayFile;
    }
} 

