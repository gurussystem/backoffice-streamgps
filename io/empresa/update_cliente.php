<?php
	require_once("../funciones.php");
	conectar_bbdd('eude');
	include_once("../conexion/dbi_connect.php");
	$data = array();
	
	# Recogemos datos
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 	
								
	# Datos en comun tablas eude.empresas y bdCliente.info_cliente
	$idSelect =  comprobarParametros('idClienteSelect'); 
	$dominio = comprobarParametros('dominio'); 
	$razonSocial = comprobarParametros('razonSocial');
	$cif = comprobarParametros('dni');		
	$email = comprobarParametros('email');
	$direccion =  comprobarParametros('direccion');		
	$poblacion =  comprobarParametros('poblacion');
	$nomProvincia=comprobarParametros('nombreProvincia');
	$provincia =  comprobarParametros('provincia');
	$idPais =  comprobarParametros('idPais');	
	$cp =  comprobarParametros('cp');
	$tlf =  comprobarParametros('tlf_fijo');
	$tlf2 =  comprobarParametros('tlf_movil');
	$ipServer=  comprobarParametros('ipServer');
	$puertoServer=  comprobarParametros('puertoServer');
	$serverNew = $ipServer.":".$puertoServer;
	$conn=  comprobarParametros('conn');
	$videoAnalisis=  comprobarParametros('videoAnalisis');
	#	tambien tenemo en comun:	lat, lng, licencias, emailAlert, telefonoAlert, aplicacion		
	# echo "GET<pre>"; print_r($_GET); echo "</pre>"; echo "POS<pre>"; print_r($_POST); echo "</pre>"; 
	
	
	# Modifico en eude.empresas
	
	$stmt = $mysqli->prepare("UPDATE eude.empresas
												SET
													razonSocial = ?,
													cif= ?,
													email= ?,
													direccion= ?,
													cp= ?,
													poblacion = ?,
													provincia= ?,
													telefono1= ?,
													telefono2= ?,
													server= ?,
													conn= ?,
													idPais= ?,
													videoAnalisis= ?
												WHERE
													id = ?");
	$stmt->bind_param("ssssissssssiii", $razonSocial, $cif, $email, $direccion, $cp, $poblacion, $provincia, $tlf, $tlf2, $serverNew, $conn, $idPais, $videoAnalisis, $idSelect );
	
	$stmt->execute();
	# Comprobamos si se han insertado los datos.
	#printf("<br> Affected rows (UPDATE eude.empresas ): %d\n", $mysqli->affected_rows);
	$stmt->close();
	
	
	# nombre base de datos del cliente
	$stmt = $mysqli->prepare("SELECT bbdd FROM eude.empresas where id = ? ");
	$stmt->bind_param('i', $idSelect);
	$stmt->execute();
	$stmt->bind_result($nameBDCliente);
	$stmt->fetch();
	$stmt->close();
	$mysqli->close();	
	#echo "<br> bbdd: ".$nameBDCliente;
	
	$mysqli2=new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, $nameBDCliente);
	
	# Modifico tabla info_cliente
	$stmt = $mysqli2->prepare("UPDATE ".$nameBDCliente.".info_cliente SET  direccion = ?, cp = ?, poblacion = ?, provincia = ?, idPais = ?, email = ?, telefono1 =?, telefono2 = ?, server = ?,   conn  = ?, videoAnalisis = ? WHERE idEmpresa = ?; ");	
	$stmt->bind_param("ssssisssssii", $direccion,$cp,$poblacion,$provincia,$idPais, $email, $tlf, $tlf2, $serverNew, $conn, $videoAnalisis, $idSelect);
	$stmt->execute();
	# Comprobamos si se han insertado los datos.
	# printf("<br> Affected rows (UPDATE info_cliente): %d\n", $mysqli2->affected_rows);
	$stmt->close();
	
	
	# Modifico tabla info_server  campo 'ipserver'
	$server = "rtmp://".$ipServer.":".$puertoServer."/vod";
	$stmt = $mysqli2->prepare("UPDATE  ".$nameBDCliente.".info_server SET server = ? , ipserver = ?; ");
	$stmt->bind_param("ss", $server, $ipServer);
	$stmt->execute();
	# Comprobamos si se han insertado los datos.
	# printf("<br> Affected rows (UPDATE - info_server ): %d\n", $mysqli2->affected_rows);
	
	# Evaluamos is ha ido todo bien o habido algun fallo
	if($stmt->errno){
		$response=0;
		$proceso = 'error';
	}else{
		$response=1;
		$proceso = 'ok';
	}
	$stmt->close();

	
	# Montamos el array 
	$data[] = array(
		"id"=>$idSelect, 
		"nombreCliente"=>$dominio, 
		"razonSocialCliente"=>$razonSocial, 
		"cifCliente"=>$cif , 
		"emailCliente"=>$email , 
		"direccionCliente"=>$direccion, 
		"poblacionCliente"=>$poblacion, 
		"provinciaCliente"=>$provincia,
		"nomProvincia"=>$nomProvincia, 
		"idPais"=>$idPais, 		
		"cpCliente"=>$cp, 
		"telefonoFijo"=>$tlf, 
		"telefonoMovil"=>$tlf2, 
		"ipServer"=>$ipServer,
		"puertoServer"=>$puertoServer,			
		"connCliente"=>$conn, 	
		"videoAnalisis"=>$videoAnalisis,
		"response" => $response,
		"proceso" => $proceso,
		"accion"=> "update",
		"tabla"=> "empresa",
		"nomFichero"=> "update_cliente.php"
	);
	
	
	echo json_encode($data); 
	$mysqli2->close();
?>