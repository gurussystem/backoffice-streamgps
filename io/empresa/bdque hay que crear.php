<?php

	/********************************************************************************
	* Creamos la procedure (productoContratado_delete)
	*******************************************************************************/
	$procedure_productoContratado_delete= ("CREATE  PROCEDURE  ".$nombreBD.".productoContratado_delete ( _idProductoNew int(11), _idProducto int(11))
	BEGIN
		UPDATE
			  ".$nombreBD.".productos_contratados
		SET
			idProducto = _idProductoNew,
			estado = 0
		WHERE 
			idProducto = _idProducto;
	END");
	
	$resultado_productoContratado_delete= mysqli_query($datos_de_conexion, $procedure_productoContratado_delete);	
	validar_tabla($resultado_productoContratado_delete, 'productoContratado_delete', 'procedure'); #Comprobamos el proceso de creacion 
	
	
	
	/********************************************************************************
	* Creamos la procedure (productoContratado_insert)
	*******************************************************************************/
	$procedure_productoContratado_insert=("CREATE  PROCEDURE   ".$nombreBD.".productoContratado_insert (_idProducto int(11), _nameProducto varchar(50), _idEmpresa int(11), _idTipoProducto int(11), _idCamara int(11), _idRouter int(11), _idSim int(11), _user varchar(50), _pass varchar(45))
	BEGIN
		INSERT INTO  ".$nombreBD.".productos_contratados(idProducto,nameProducto,idEmpresa,idTipoProducto,idCamara,idRouter,idSim,user,pass, estado) 
		VALUES (_idProducto, _nameProducto, _idEmpresa, _idTipoProducto, _idCamara, _idRouter, _idSim, _user, _pass, 1);		
	END");
	
	//$crearP_productoContratadoInsert = mysql_query($procedure_productoContratadoInsert,$db_nueva);
	//validar_tabla($crearP_productoContratadoInsert, 'p_productoContratado_insert', 'procedure');
	
	$resultado_productoContratado_insert= mysqli_query($datos_de_conexion, $procedure_productoContratado_insert);	
	validar_tabla($resultado_productoContratado_insert, 'productoContratado_insert', 'procedure'); #Comprobamos el proceso de creacion 
	
	
		
	/********************************************************************************
	* Creamos la procedure (infoServer_update)
	*******************************************************************************/
	$procedure_infoServer_update=("CREATE PROCEDURE  ".$nombreBD.".infoServer_update (_server varchar(250), _ipserver varchar(25))
	BEGIN
		UPDATE  ".$nombreBD.".info_server SET server = _server , ipserver = _ipserver;
	END");

	//$crearP_infoServerUpdate = mysql_query($procedure_infoServerUpdate,$db_nueva);
	//validar_tabla($crearP_infoServerUpdate, 'p_infoServer_update', 'procedure');

	$resultado_infoServer_update= mysqli_query($datos_de_conexion, $procedure_infoServer_update);	
	validar_tabla($resultado_infoServer_update, 'infoServer_update', 'procedure'); #Comprobamos el proceso de creacion 

	
	/********************************************************************************
	* Creamos la procedure (ultimaPosicion_delete)
	*******************************************************************************/
	$procedure_ultimaPosicion_delete =("CREATE  PROCEDURE  ".$nombreBD.".ultimaPosicion_delete (_idProductoNew int(11),_idProducto int(11))
	BEGIN
	UPDATE ".$nombreBD.".ultimaposicion
	SET
	userId =_idProductoNew,
	estado= 0
	WHERE userId = _idProducto;
	END");

	//$crearP_ultimaPosicionDelete= mysql_query($procedure_ultimaPosicion_delete,$db_nueva);
	//validar_tabla($crearP_ultimaPosicionDelete, 'p_ultimaPosicion_delete', 'procedure');

	$resultado_ultimaPosicion_delete= mysqli_query($datos_de_conexion, $procedure_ultimaPosicion_delete);	
	validar_tabla($resultado_ultimaPosicion_delete, 'ultimaPosicion_delete', 'procedure'); #Comprobamos el proceso de creacion 
	
	/********************************************************************************
	* Creamos la procedure (ultimaPosicion_insert_default)
	*******************************************************************************/
	$procedure_ultimaPosicion_insert_default=("CREATE  PROCEDURE  ".$nombreBD.".ultimaPosicion_insert_default (_userId int(11), _latitude decimal(10,6), _longitude decimal(10,6), _zoom int(11))
	BEGIN
		INSERT INTO ".$nombreBD.".ultimaposicion (userId, latitude, longitude, zoom, estado) VALUES (_userId, _latitude, _longitude, _zoom, 1);
	END");


	//$crearP_ultimaPosicionIinsertDefault= mysql_query($procedure_ultimaPosicionIinsertDefault,$db_nueva);
	//validar_tabla($crearP_ultimaPosicionIinsertDefault, 'p_ultimaPosicion_insert_default', 'procedure');

	$resultado_ultimaPosicion_insert_default= mysqli_query($datos_de_conexion, $procedure_ultimaPosicion_insert_default);	
	validar_tabla($resultado_ultimaPosicion_insert_default, 'ultimaPosicion_insert_default', 'procedure'); #Comprobamos el proceso de creacion 
?>