<?php
	require_once("../funciones.php");
	conectar_bbdd('eude');
	include_once("../conexion/dbi_connect.php");
	$data = array();
	
	# Recogemos datos
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	$idCliente_selecionado = comprobarParametros('idCliente_selecionado');	
	$operacionSQL = comprobarParametros('operacionSQL');	
	$fichero = comprobarParametros('fichero');	
	#  echo "<pre>g:"; print_r($_GET);  echo "<br>p:"; print_r($_POST); echo "</pre>";
	
	# Consulta 
	$sql = "SELECT
		e.id as id,
		e.nombre as nombre,
		e.razonSocial as razonSocial,
		e.cif as cif,
		e.email as email,
		e.pass as pass,
		e.direccion as direccion,
		e.poblacion as poblacion,
		e.provincia as provincia,
		(SELECT pro.nombre FROM configstream.provincias as pro where pro.id=e.provincia) as nameProvincia,
		e.cp as cp,
		e.telefono1 as telefono1,
		e.telefono2 as telefono2,
		e.fechaAlta as fechaAlta,
		e.fechaSolicitudAlta as fechaSolicitudAlta,
		e.activo as estadoCliente,
		e.prefixstream as prefixstream,
		e.server as servidor,
		e.streamapp as streamapp,
		e.domini as domini,
		e.subdomini as subdomini,
		e.conn as conn,
		e.bbdd as bdCliente,
		e.licencias as licencias,
		cta_e.titularCuenta as titularCuenta,
		cta_e.entidad as entidad,
		cta_e.sucursal as sucursal,
		cta_e.dc as dc,
		cta_e.numCuenta as numCuenta,
		cta_e.iban as iban,
		cta_e.swift as swift,
		e.videoAnalisis as videoAnalisis,
        e.lat as lat,
        e.lng as lng,
        e.emailAlerta as emailAlerta,
        e.telefonoAlert as telefonoAlert,
		e.idPais as idPais,
        cta_e.fecha_alta as cta_fechaA,
        cta_e.fecha_modificacion as cta_fechaM
	FROM
		eude.empresas AS e
	INNER JOIN eude.cta_empresas AS cta_e ON (cta_e.idCta = e.idCta) ";
	
	# Preparamos y ejecutamos la consulta
	if($idCliente_selecionado != ""){ 
		$consulta = $sql . " WHERE	e.id = ? AND e.id != 0	Limit 1";
		$stmt = $mysqli->prepare($consulta);		
		$stmt->bind_param("i", $idCliente_selecionado);	
		
	}else{ 
		$consulta = $sql . " WHERE e.id != 0 ";
		$stmt = $mysqli->prepare($consulta);		
	}
	
	$stmt->execute();
	$stmt->bind_result($id, $nombre, $razonSocial, $cif, $email, $pass, $direccion, $poblacion, $provincia, $nameProvincia, $cp, $telefono1, $telefono2, $fechaAlta, $fechaSolicitudAlta, $estadoCliente, $prefixstream, $servidor,  $streamapp, $domini, $subdomini, $conn, $bdCliente, $licencias, $titularCuenta, $entidad, $sucursal, $dc, $numCuenta, $iban, $swift,  $videoAnalisis, $lat, $lng, $emailAlerta, $telefonoAlert, $idPais, $cta_fechaA, $cta_fechaM);
	
	# Obtenemos los valores
	while( $stmt->fetch() ) {		
	
		# Separamos el server para obtener la ip y el puerto en campos diferentes
		$separarServer = explode(":", $servidor);
		$ipServer = $separarServer[0]; 
		$puertoServer = $separarServer[1];
		
		
		$data[] = array(
			# Datos Empresa
			"idCliente"=>$id,			
			"nombreCliente"=>$nombre,  # es el dominio
			"razonSocialCliente"=>$razonSocial, 
			"cifCliente"=>$cif, 
			"emailCliente"=>$email, 
			"passCliente"=>$pass, 
			"direccionCliente"=>$direccion, 
			"poblacionCliente"=>$poblacion, 
			"provinciaCliente"=>$provincia, 
			"idPais"=>$idPais, 
			"nameProvinciaCliente"=>$nameProvincia, 
			"cpCliente"=>$cp, 
			"telefonoFijo"=>$telefono1, 
			"telefonoMovil"=>$telefono2, 			
			"fechaAltaCliente"=> desfaseHorario($fechaAlta, $tiempoDesfase, "entera", "suma"), 
			# fechaModCliente"=> desfaseHorario($fechaMod, $tiempoDesfase, "entera", "suma") , 
			"fechaSolicitudAlta"=>$fechaSolicitudAlta, 			
			"estadoCliente"=>$estadoCliente, 
			//Otros datos
			"prefixstreamCliente"=>$prefixstream,
			"ipServer"=>$ipServer,
			"puertoServer"=>$puertoServer,			
			"streamappCliente"=>$streamapp, 
			"dominioCliente"=>$domini, 
			"subdominioCliente"=>$subdomini,
			"connCliente"=>$conn, 
			"bdCliente"=>$bdCliente ,
			"licencias"=>$licencias, 			
			# Datos Bancarios
			"titularCuenta"=> $titularCuenta, 
			"entidad"=> $entidad, 
			"sucursal"=> $sucursal, 
			"dc"=>$dc, 
			"numCuenta"=>$numCuenta, 
			"iban"=>$iban, 
			"swift"=>$swift, 
			"accion"=> "select",
			"tabla"=> "empresa",
			"nomFichero"=> "select_cliente.php",
			"id"=>$id	,
			"videoAnalisis"=>$videoAnalisis	,
			"lat"=>$lat	,
			"lng"=>$lng	,
			"emailAlerta"=>$emailAlerta	, 
			"telefonoAlert"=>$telefonoAlert,	
			"cta_fechaA"=>$cta_fechaA	,
			"cta_fechaM"=>$cta_fechaM	
		);		
		//	echo "<pre>"; print_r($data);echo "</pre>";
	}	
	
	$stmt->close();

	echo json_encode(utf8ize($data));
	#echo json_encode($data); 
	# json_errores();

	
	
	$mysqli->close();
?>