<?php
	require_once("../funciones.php");
	conectar_bbdd('eude');
	include_once("../conexion/dbi_connect.php");
	$data = array();
	
	# Recogemos datos
	$idClienteSelect = comprobarParametros('idClienteSelect'); 
	$titularCuenta = comprobarParametros('titularCuenta');
	$iban= comprobarParametros('iban');
	$swift= comprobarParametros('swift');
	$entidad= comprobarParametros('entidad');
	$sucursal= comprobarParametros('sucursal');
	$dc= comprobarParametros('dc');
	$numCuenta= comprobarParametros('numCuenta');
	$fechaA= comprobarParametros('fechaA');
	$fechaM= comprobarParametros('fechaM');
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 	
	# echo "POST <pre>";print_r($_POST); echo "</pre>"; echo "GET  <pre>";print_r($_GET); echo "</pre>";
	
	# Preparamos y ejecutamos la consulta
	$stmt = $mysqli->prepare("UPDATE eude.cta_empresas
												SET
													titularCuenta = ?,
													swift = ?,
													iban = ?,
													entidad = ?,
													sucursal = ?,
													dc = ?,
													numCuenta = ?													
												WHERE
													idCta = ?");		
	$stmt->bind_param("sssiiiii", $titularCuenta, $swift, $iban, $entidad , $sucursal, $dc, $numCuenta, $idClienteSelect );
	$stmt->execute();
	
	# Evaluamos si ha ido todo bien o habido algun fallo
	if($stmt->errno){
		$response=0;
		$proceso = 'error';
	}else{
		$response=1;
		$proceso = 'ok';
	}
		
	# Montamos el array 
	$data[] = array(
		"id"=>$idClienteSelect, 
		"titularCuenta"=> $titularCuenta , 
		"entidad"=> $entidad, 
		"sucursal"=> $sucursal, 
		"dc"=>$dc, 
		"numCuenta"=>$numCuenta, 
		"iban"=>$iban, 
		"swift"=>$swift, 
		"fecha_alta_europea"=>$fechaA,			
		# "fechaHora_alta_europea"=>date("Y-m-d  h:m:s"),			
		"response" => $response,
		"proceso" => $proceso,
		"accion"=> "update",
		"tabla"=> "cta_empresas",
		"nomFichero"=> "update_cta_cliente.php",
	);
	
	
	$stmt->close();
	echo json_encode($data); 
?>