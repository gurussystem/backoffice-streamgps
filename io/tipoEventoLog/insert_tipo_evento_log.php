<?php
	require_once("../funciones.php");
	conectar_bbdd('eude');
	include_once("../conexion/dbi_connect.php");
	$data = array();
	
	# Recogemos datos
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	$nombre = comprobarParametros('nombre'); 
	$descripcion = comprobarParametros('descripcion'); 
	# echo "GET<pre>"; print_r($_GET); echo "</pre>"; echo "POS<pre>"; print_r($_POST); echo "</pre>";
	
					
	# Preparamos y ejecutamos la consulta
	$stmt = $mysqli->prepare("INSERT INTO eude.tipo_evento_log (evento, descripcion ) VALUES(?,?)");		
	$stmt->bind_param("ss", $nombre, $descripcion );
	$stmt->execute();
	
	
	# Evaluamos is ha ido todo bien o habido algun fallo
	if($stmt->errno){
		$response=0;
		$proceso = 'error';
	}else{
		$response=1;
		$proceso = 'ok';
	}
	
	# Consultamos el ultimo id insertado.
	$result=$mysqli->query("SELECT LAST_INSERT_ID() AS ultimoID");		
	$row=$result->fetch_assoc();
	
	date_default_timezone_set("Europe/Madrid");
	
	# Montamos el array 
	$data[] = array(
		"id"=>$row['ultimoID'], 
		"nom"=>$nombre, 
		"descrip"=>$descripcion, 
		"fecha_alta_europea"=>date("d-m-Y"),			
		"fechaHora_alta_europea"=>date("Y-m-d  h:m:s"),			
		"response" => $response,
		"proceso" => $proceso,
		"accion"=> "insert",
		"tabla"=> "tipo_evento_log",
		"nomFichero"=> "insert_tipo_evento_log.php",
	);
	
	
	$stmt->close();
	echo json_encode($data); 
	$mysqli->close();
?>