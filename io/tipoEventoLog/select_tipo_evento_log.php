<?php 
	require_once("../funciones.php");
	conectar_bbdd('eude');
	include_once("../conexion/dbi_connect.php");
	$data = array();
	
	# Recogemos datos
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	$idSeleccionado = comprobarParametros('idSelecionado'); 
	# echo "GET<pre>"; print_r($_GET); echo "</pre>"; echo "POS<pre>"; print_r($_POST); echo "</pre>";
	

	# Preparamos y ejecutamos la consulta
	$sql = "SELECT 	id, evento, descripcion, fecha_alta, fecha_modificacion	FROM eude.tipo_evento_log ";
	
	if($idSeleccionado){
		$consulta = $sql . " WHERE id = ? Limit 1";
		$stmt = $mysqli->prepare($consulta);		
		$stmt->bind_param("i", $idSeleccionado);
	}else{	
		$stmt = $mysqli->prepare($sql);		
	}
	$stmt->execute();
	$stmt->bind_result($id, $evento, $descripcion, $fecha_alta, $fecha_modificacion);
	while($stmt->fetch()) {
		
		$data[] = array(
			"id"=>$id, 
			"nombre"=>$evento, 
			"descripcion"=>$descripcion, 
			"fecha_alta"=>desfaseHorario($fecha_alta, $tiempoDesfase, "fecha", "suma") 	,			
			"fecha_mod"=>controlFechaModificacion($fecha_modificacion, $tiempoDesfase, "fecha", "suma"),
			"accion"=> "select",
			"tabla"=> "tipo_evento_log",
			"nomFichero"=> "select_tipo_evento_log.php",				
		);
	}	
	
	$stmt->close();
	# echo "<pre>"; print_r($data); echo "</pre>";
	echo json_encode(utf8ize($data));
	# $stmt->mysqli();
	$mysqli->close();
?>