<?php
	require_once("../funciones.php");
	conectar_bbdd('eude');
	include_once("../conexion/dbi_connect.php");
	$data = array();
		
	# Recogemos datos
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	$numSim01 = comprobarParametros('numSim01'); 
	$numSim02 = comprobarParametros('numSim02'); 
	$pinSim = comprobarParametros('pinSim'); 
	$punkSim = comprobarParametros('punkSim'); 
	$modelSim = comprobarParametros('modelSim'); 
	# echo "GET<pre>"; print_r($_GET); echo "</pre>"; echo "POS<pre>"; print_r($_POST); echo "</pre>";
	
	# Preparamos y ejecutamos la consulta
	$stmt = $mysqli->prepare("INSERT INTO eude.sim (numSim01, numSim02, pin, punk, model ) VALUES ( ?,?,?,?,?) ");		
	$stmt->bind_param("iiiis", $numSim01, $numSim02, $pinSim, $punkSim, $modelSim);
	$stmt->execute();
	
	
	# Evaluamos is ha ido todo bien o habido algun fallo
	if($stmt->errno){
		$response=0;
		$proceso = 'error';
	}else{
		$response=1;
		$proceso = 'ok';
	}
		
	# Consultamos el ultimo id insertado.
	$result=$mysqli->query("SELECT LAST_INSERT_ID() AS ultimoID");		
	$row=$result->fetch_assoc();
	
	# Montamos el array 
	$data[] = array(
		"id"=>$row['ultimoID'], 
		"numSim01"=>$numSim01, 
		"numSim02"=>$numSim02, 
		"pinSim"=>$pinSim, 
		"punkSim"=>$punkSim , 
		"modelSim"=>$modelSim ,
		"response" => $response,
		"proceso" => $proceso,
		"accion"=> "insert",
		"tabla"=> "sim",
		"nomFichero"=> "insert_sim.php",
	);
	
	
	$stmt->close();
	echo json_encode($data); 
	$mysqli->close();
?>