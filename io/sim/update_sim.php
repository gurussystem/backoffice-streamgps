<?php
	require_once("../funciones.php");
	conectar_bbdd('eude');
	include_once("../conexion/dbi_connect.php");
	$data = array();
	
	# Recogemos datos
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	$idSelect = comprobarParametros('idSelect'); 
	$numSim01 = comprobarParametros('numSim01'); 
	$numSim02 = comprobarParametros('numSim02'); 
	$pinSim = comprobarParametros('pinSim'); 
	$punkSim = comprobarParametros('punkSim'); 
	$modelSim = comprobarParametros('modelSim'); 
	$fechaA = comprobarParametros('fechaA'); 
	$fechaM = comprobarParametros('fechaM'); 
	# echo "GET<pre>"; print_r($_GET); echo "</pre>"; echo "POS<pre>"; print_r($_POST); echo "</pre>";
	
	# Preparamos y ejecutamos la consulta
	$stmt = $mysqli->prepare("UPDATE eude.sim
												SET
													numSim01 = ? ,
													numSim02 = ?,
													pin = ?,
													punk = ?,
													model = ?,
													fecha_modificacion = CURRENT_TIMESTAMP
												WHERE
													idSim = ?");		
	$stmt->bind_param("iiiisi", $numSim01, $numSim02, $pinSim, $punkSim, $modelSim,  $idSelect);
	$stmt->execute();
	
	
	# Evaluamos is ha ido todo bien o habido algun fallo
	if($stmt->errno){
		$response=0;
		$proceso = 'error';
	}else{
		$response=1;
		$proceso = 'ok';
	}
	
	
	# Montamos el array 
	$data[] = array(
		"id"=>$idSelect, 
		"numSim01"=>$numSim01, 
		"numSim02"=>$numSim02, 
		"pinSim"=>$pinSim, 
		"punkSim"=>$punkSim , 
		"modelSim"=>$modelSim ,
		"fecha_alta_europea"=>$fechaA,			
		"response" => $response,
		"proceso" => $proceso,
		"accion"=> "update",
		"tabla"=> "sim",
		"nomFichero"=> "update_sim.php",
	);
	
	
	$stmt->close();
	echo json_encode($data); 
	$mysqli->close();
?>