<?php 
	require_once("../funciones.php");
	conectar_bbdd('eude');
	include_once("../conexion/dbi_connect.php");
	$data = array();
	
	 # Recogemos datos
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	$idSeleccionado = comprobarParametros('idSelecionado'); 
	# echo "GET<pre>"; print_r($_GET); echo "</pre>"; echo "POS<pre>"; print_r($_POST); echo "</pre>";

	# Preparamos y ejecutamos la consulta
	
	$sql ="SELECT
	s.idSim as idSim ,
    s.numSim01 as numSim01,
    s.numSim02 as numSim02,
    s.pin as pin,
    s.punk as punk,
    s.model as model,
    s.fecha_alta as fecha_alta,
    s.fecha_modificacion as fecha_modificacion,
	(select p.idEmpresa from productos as p  where s.idSim = p.idSim) as idPropietario,
	(Select e.nombre from empresas as e inner join productos as p on(p.idEmpresa = e.id)  where p.idSim = s.idSim) as nomPropietario,
    s.asignada as asignada
	FROM	eude.sim as s";
	
	if($idSeleccionado){
		$consulta =$sql . "  WHERE s.idSim = ? ";
		// $stmt = $mysqli->prepare("call sim_select(?)");		
		$stmt = $mysqli->prepare($consulta);		
		$stmt->bind_param("i", $idSeleccionado);
	}else{
		//$stmt = $mysqli->prepare("call sim_selectOption()");			
		$stmt = $mysqli->prepare($sql);			
	}
	$stmt->execute();
	
	$stmt->bind_result($idSim , $numSim01, $numSim02,  $pin, $punk, $model, $fecha_alta, $fecha_modificacion, $idPropietario,   $nomPropietario, $asignada);

	while($stmt->fetch()) {
			// echo "<br> hola " . $idPropietario;
		$data[] = array(
			"idSim"=>$idSim, 
			"numSim01"=>$numSim01, 
			"numSim02"=>$numSim02, 
			"pin"=>$pin, 
			"punk"=>$punk, 
			"model"=>$model,
			"idPropietario"=> $idPropietario,
			"nomPropietario"=> $nomPropietario,		
			"asignada"=> $asignada,		
			"fecha_alta"=> $fecha_alta,
			"fecha_mod"=>$fecha_modificacion,	
		////	"fecha_alta"=> desfaseHorario($fecha_alta, $tiempoDesfase, "entera", "suma"),			
			//"fecha_mod"=>desfaseHorario($fecha_modificacion, $tiempoDesfase, "entera", "suma") ,			
			"accion"=> "select",
			"tabla"=> "sim",
			"nomFichero"=> "select_sim.php",
			"id"=>$idSeleccionado, 
		);
		
	}	

	$stmt->close();
	//echo "<pre>"; print_r($data); echo "</pre>";
	echo json_encode($data); 
	$mysqli->close();
?>