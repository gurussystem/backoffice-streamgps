<?php 
	require_once("Mandrill.php");
	$mandrill = new Mandrill("pI7YSRggNJxpMihxy3s-ZA");
	
	//Recogemos datos pasados por parametro
	if($_POST['comprobar']){
		$datosArray = json_decode($_POST['datosArray'],true);
		$arrayLabels = json_decode($_POST['arrayLabels'],true);
	}else{
		$datosArray = json_decode($_GET['datosArray'],true);
		$arrayLabels = json_decode($_GET['arrayLabels'],true);
	}

	//echo "<br> POST: <pre>" ; print_r($_POST); echo "</pre>" ;  echo "<br> GET: <pre>" ; 	print_r($_GET);  echo "</pre>" ;
	//echo "<br> array: <pre>" ;   print_r($datosArray );echo "</pre>" ;
	//echo "<br> array: <pre>" ;   print_r($arrayLabels );echo "</pre>" ;

	//Preparamos el mensaje del mail

	$html_message = '<h2>Los datos introducidos en el formulario son los siguientes:  </h2> <br />';
	$html_message.='<b> Acción: </b> '.$datosArray[0]['accion'].		'<br />';
	
	switch($datosArray[0]['tabla']){
			case 'backoffice':  
				$html_message.='<b>Identificador: </b> '.$datosArray[0]['id'].		'<br />';
				$html_message.='<b>Nombre: </b> '.$datosArray[0]['name'].	'<br />';
				$html_message.='<b>Clave: </b> '.$datosArray[0]['pass'].	'<br />';
				$html_message.='<b>Email: </b> '.$datosArray[0]['mail'].	'<br />';
				$html_message.='<b>Rol Asignado: </b> '.$datosArray[0]['nameRol'].	'<br />';
			break;
			
			case 'camara':  	
				$html_message.='<b>Identificador: </b> '.$datosArray[0]['id'].		'<br />';
				$html_message.='<b>Nombre: </b> '.$datosArray[0]['nameCamara'].	'<br />';
				$html_message.='<b>Tipo cámara: </b> '.$datosArray[0]['idTipoCamara'].	'<br />';
				$html_message.='<b>Número serial: </b> '.$datosArray[0]['serialnumCamara'].	'<br />';
				$html_message.='<b>Dirección mac: </b> '.$datosArray[0]['macCamara'].	'<br />';
				$html_message.='<b>Usuario: </b> '.$datosArray[0]['userCamara'].	'<br />';
				$html_message.='<b>Clave: </b> '.$datosArray[0]['passCamara'].	'<br />';
				$html_message.='<b>Ip local: </b> '.$datosArray[0]['localipCamara'].	'<br />';
				$html_message.='<b>RTSP Main: </b> '.$datosArray[0]['rtspMainCamara'].	'<br />';
				$html_message.='<b>Resolución Main: </b> '.$datosArray[0]['resolutionMainCamara'].	'<br />';
				$html_message.='<b>KBPS Main: </b> '.$datosArray[0]['kbpsMainCamara'].	'<br />';
				$html_message.='<b>RTSP Second: </b> '.$datosArray[0]['rtspSecondCamara'].	'<br />';
				$html_message.='<b>Resolución Second: </b> '.$datosArray[0]['resolutionSecondCamara'].	'<br />';
				$html_message.='<b>KBPS Second: </b> '.$datosArray[0]['kbpsSecondCamara'].	'<br />';
			break;
			
			case 'empresa':   // cliente
				if($datosArray[0]['estadoCliente'] == 0 ) {
					$estadoCliente = "Desactivado";
				}else{
					$estadoCliente = "Activado";
				}
				
				$html_message.='<b>Identificador: </b> '.$datosArray[0]['id'].		'<br />';
				$html_message.='<b>Estado cliente: </b> '.$estadoCliente .	'<br />';
				$html_message.='<b>Razón social: </b> '.$datosArray[0]['razonSocialCliente'].	'<br />';
				$html_message.='<b>Cif/Nif/Dni: </b> '.$datosArray[0]['cifCliente'].	'<br />';
				$html_message.='<b>Email: </b> '.$datosArray[0]['emailCliente'].	'<br />';
				$html_message.='<b>Clave: </b> '.$datosArray[0]['passCliente'].	'<br />';
				$html_message.='<b>Dirección: </b> '.$datosArray[0]['direccionCliente'].	'<br />';
				$html_message.='<b>Población: </b> '.$datosArray[0]['poblacionCliente'].	'<br />';
				$html_message.='<b>Provincia: </b> '.$datosArray[0]['nomProvincia'].	'<br />';
				$html_message.='<b>Código Postal: </b> '.$datosArray[0]['cpCliente'].	'<br />';
				$html_message.='<b>Télefono 1: </b> '.$datosArray[0]['telefonoFijo'].	'<br />';
				$html_message.='<b>Télefono 2: </b> '.$datosArray[0]['telefonoMovil'].	'<br />';
				$html_message.='<b>Prefijo stream: </b> '.$datosArray[0]['prefixstreamCliente'].	'<br />';
				$html_message.='<b>Ip Server / puerto: </b> '.$datosArray[0]['ipServer'].	'/'. $datosArray[0]['puertoServer'] .'<br />';
				$html_message.='<b>Stream App: </b> '.$datosArray[0]['streamappCliente'].	'<br />';
				$html_message.='<b>Nombre: </b> '.$datosArray[0]['nombreCliente'].	'<br />';
				$html_message.='<b>Subdominio: </b> '.$datosArray[0]['subdominioCliente'].	'<br />';
				$html_message.='<b>Conn: </b> '.$datosArray[0]['connCliente'].	'<br />';
				$html_message.='<b>Licencias contratadas: </b> '.$datosArray[0]['licencias'].	'<br />';			
			break;
			
			case 'cta_empresas': 
				// Controlamos la cuenta bancaria a la hora de mandar el email
				$cta = "";
				$tipoCta ="";
				if($iban != "" ){
					$tipoCta ="IBAN";
					$cta = $datosArray[0]['iban'];
				}else if ($swift != "") {
					$cta =  $datosArray[0]['swift'];
					$tipoCta ="BIC/SWIFT";
				}else {
					$tipoCta ="CCC";
					$cta = $datosArray[0]['entidad'].'-'. $datosArray[0]['sucursal'].'-'.$datosArray[0]['dc'].'-'. $datosArray[0]['numCuenta'];	
				}
		
				$html_message.='<b>Identificador: </b> '.$datosArray[0]['id'].		'<br />'; 
				$html_message.='Titular de la cuenta: </b> '.$datosArray[0]['titularCuenta'].'<br />';
				$html_message.='<b> Tipo Cuenta: </b> '.$tipoCta. '<br />';
				$html_message.='<b>Número/Código Bancario: </b> '.$cta.'<br />';
			break;
			
			case 'producto':  	
				if($datosArray[0]['accion'] == 'desasignar'){
					$html_message.='<b>Identificador: </b> '.$datosArray[0]['id'].		'<br />';
					$html_message.='<b>Nombre: </b> '.$datosArray[0]['nombre'].	'<br />';
					$html_message.='<b>Nombre Ahora: </b> '.$datosArray[0]['nombreAhora'].	'<br />';
					$html_message.='<b>Identificador Empresa: </b> '.$datosArray[0]['idEmpresa'].	'<br />';
				}else{
					$html_message.='<b>Identificador: </b> '.$datosArray[0]['id'].		'<br />';
					$html_message.='<b>Nombre: </b> '.$datosArray[0]['nombre'].	'<br />';
					$html_message.='<b>Descripción: </b> '.$datosArray[0]['descripcion'].	'<br />';
					$html_message.='<b>Tipo Producto </b> '.$datosArray[0]['nameTipoProducto'].	'<br />';
					$html_message.='<b>Cámara asociada: </b> '.$datosArray[0]['nameCamara'].	'<br />';
					$html_message.='<b>Router asociado: </b> '.$datosArray[0]['nameRouter'].	'<br />';
					$html_message.='<b>Sim asociada: </b> '.$datosArray[0]['nameSim'].	'<br />';
				
					if($datosArray[0]['accion'] == 'asignar'){
						$html_message.='<b>Empresa Asignada: </b> '.$datosArray[0]['nameEmpresa'].'('.$datosArray[0]['idEmpresa'].')<br />';
					}
				}	
					
			break;
			
			
			case 'router':  
				$html_message.='<b>Identificador: </b> '.$datosArray[0]['id'].	'<br />';
				$html_message.='<b>Nombre: </b> '.$datosArray[0]['nameRouters'].	'<br />';
				$html_message.='<b>Tipo Router: </b> '.$datosArray[0]['tipoRouters'].	'<br />';
				$html_message.='<b>Número Serial: </b> '.$datosArray[0]['serialnumRouters'].	'<br />';
				$html_message.='<b>Dirección mac: </b> '.$datosArray[0]['macRouters'].	'<br />';
				$html_message.='<b>Usuario: </b> '.$datosArray[0]['userRouters'].	'<br />';
				$html_message.='<b>Clave: </b> '.$datosArray[0]['passRouters'].	'<br />';
				$html_message.='<b>Ip Estatica: </b> '.$datosArray[0]['ipstaticRouters'].	'<br />';
				$html_message.='<b>Dyndns: </b> '.$datosArray[0]['dyndnsRouters'].	'<br />';
			break;
			
			case 'sim': 			
				$html_message.='<b>Identificador: </b> '.$datosArray[0]['id'].					'<br />';
				$html_message.='<b>Número Sim 1: </b> '.$datosArray[0]['numSim01'].	'<br />';
				$html_message.='<b>Número Sim 2: </b> '.$datosArray[0]['numSim02'].	'<br />';
				$html_message.='<b>Pin: </b> '.$datosArray[0]['pinSim'].			'<br />';
				$html_message.='<b>Punk: </b> '.$datosArray[0]['punkSim'].		'<br />';
				$html_message.='<b>Modelo: </b> '.$datosArray[0]['modelSim'].	'<br />';
			break;
			
			case 'rol_user':   
			case 'tipo_avisos':	
			case 'tipo_evento_log':	
				$html_message.='<b>Identificador:  </b> '.$datosArray[0]['id'].		'<br />';
				$html_message.='<b>Nombre: </b> '.$datosArray[0]['nom'].	'<br />';
				$html_message.='<b>Descripción: </b> '.$datosArray[0]['descrip'].'<br />';
			break;
			
			
			case 'impuestos': 
				$html_message.='<b>Identificador: </b> '.$datosArray[0]['id'].		'<br />';
				$html_message.='<b>Nombre: </b> '.$datosArray[0]['nombre'].	'<br />';
				$html_message.='<b>Descripción: </b> '.$datosArray[0]['descripcion'].	'<br />';
				$html_message.='<b>Valor: </b> '.$datosArray[0]['valor'].	'<br />';

			break;

			case 'pais':		
				$html_message.='<b>Identificador: </b> '.$datosArray[0]['id'].	'<br />';
				$html_message.='<b>Nombre: </b> '.$datosArray[0]['nombre'].	'<br />';
				$html_message.='<b>Code Calling: </b> '.$datosArray[0]['codeCalling'].	'<br />';
				$html_message.='<b>Capital: </b> '.$datosArray[0]['capital'].	'<br />';
				$html_message.='<b>Continente: </b> '.$datosArray[0]['continente'].	'<br />';
				$html_message.='<b>Latitud: </b> '.$datosArray[0]['lat'].	'<br />';
				$html_message.='<b>Longuitid: </b> '.$datosArray[0]['lng'].	'<br />';
				$html_message.='<b>Code ISO2: </b> '.$datosArray[0]['codeISO2'].	'<br />';
				$html_message.='<b>Code ISO3: </b> '.$datosArray[0]['codeISO3'].	'<br />';		
				break;
		
			
			case 'provincias':  
				$html_message.='<b>Identificador: </b> '.$datosArray[0]['id'].		'<br />';
				$html_message.='<b>Nombre: </b> '.$datosArray[0]['nombre'].	'<br />';
				$html_message.='<b>Código Postal: </b> '.$datosArray[0]['codigoPostal'].'<br />';
			break;		
			
			
			case 'tipo_productos':  	
				//Cambiamos los 1-0 por Si-No
				if($datosArray[0]['tpGPS'] == 1){	$tp_gps_txt  = "Si"; }else{	$tp_gps_txt  = "No"; 	}
				if($datosArray[0]['tp3G']  == 1){	$tp_3g_txt   = "Si"; 	}else{	$tp_3g_txt   = "No"; 	}
				if($datosArray[0]['tp4G'] 	== 1){		$tp_4g_txt   = "Si"; 	}else{	$tp_4g_txt   = "No"; 	}
				if($datosArray[0]['tpWIFI']== 1){	$tp_wifi_txt  = "Si"; 	}else{	$tp_wifi_txt = "No"; 	}
	
				$html_message.='<b>Identificador:  </b> '.$datosArray[0]['id'].		'<br />';
				$html_message.='<b>Nombre:   </b> '.$datosArray[0]['nombre'].		'<br />';
				$html_message.='<b>Descripción:   </b> '.$datosArray[0]['desc'].		'<br />';
				$html_message.='<b>Tipo Live:   </b> '.$datosArray[0]['tipoLive'].		'<br />';
				$html_message.='<b> Características :   </b> <br />';
				$html_message.='<ul>';
					$html_message.='<li><b>GPS: </b> '. $tp_gps_txt. ' </li>';
					$html_message.='<li><b>WIFI: </b>'. $tp_wifi_txt . ' </li>';
					$html_message.='<li><b>3G: </b>'. $tp_3g_txt . ' </li>';
					$html_message.='<li><b>4G: </b>'. $tp_4g_txt . ' </li>';
				$html_message.='</ul>';
			break;
			
			case 'tipo_router':	
				$html_message.='<b>Identificador: </b> '.$datosArray[0]['id'].		'<br />';
				$html_message.='<b>Nombre: </b> '.$datosArray[0]['nombre'].	'<br />';
				$html_message.='<b>Descripción: </b> '.$datosArray[0]['descripcion'].'<br />';
				break;
				
			case 'tipo_camara':	
				$html_message.='<b>Identificador: </b> '.$datosArray[0]['id'].'<br />';
				$html_message.='<b>Nombre: </b> '.$datosArray[0]['nombre'].'<br />';
				$html_message.='<b>Descripción: </b> '.$datosArray[0]['descripcion'].'<br />';
				break;
				
		//	case 'dispositivo_emisor': $html_message.='<b>Identificador: </b> '.$datosArray[0]['id']. '<br />';				break
	}//fin switch	
	
	//echo "<br> mensaje : <br> " . $html_message;
	

	//Enviamos mail
	if($_SERVER['SERVER_NAME'] !=  'streamgps.backoffice.com') {
		$message = new stdClass();
		$message->html = $html_message;
		$message->subject = "[".$proceso."] ".  $datosArray[0]['accion'] . "  ". $datosArray[0]['tabla'];
		$message->from_email = "info@streamgps.com"; //de quien
		$message->from_name  = "Backoffice StreamGPS";
		$message->to = array(array("email" => "marisa@streamgps.com"), array("email" => "marisaQ02@gmail.com")); //para quien cambiar cuando funcione info@streamgps.com
		$message->track_opens = true;
		$mandrill->messages->send($message);
	}else{	
		echo $html_message;
	}
	?>
