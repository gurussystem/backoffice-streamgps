<?php
	require_once("../funciones.php");
	conectar_bbdd('configstream');
	include_once("../conexion/dbi_connect.php");
	$data = array();
	
	# Recogemos datos
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	$idSeleccionado = comprobarParametros('idSelecionado'); 	
	# echo "GET<pre>"; print_r($_GET); echo "</pre>"; echo "POS<pre>"; print_r($_POST); echo "</pre>";

	#Preparamos y ejecutamos la consulta
	$sql = "SELECT id, codeCalling, nombre, capital, continente, lat, lng, codeISO2, codeISO3 ,fecha_alta, fecha_modificacion FROM configstream.paises ";
	if($idSeleccionado){
		$consulta = $sql . " WHERE id = ? ";
		$stmt = $mysqli->prepare($consulta);
		$stmt->bind_param("i", $idSeleccionado);
	}else{	
		$stmt = $mysqli->prepare($sql);		
	}
	$stmt->execute();	
	$stmt->bind_result($id, $codeCalling, $nombre, $capital, $continente, $lat, $lng, $codeISO2, $codeISO3, $fecha_alta, $fecha_modificacion );
	
	while($stmt->fetch()) {				
		$data[] = array(
			"id"=>$id, 
			"codeCalling"=>$codeCalling, 
			"nombre"=>$nombre, 
			"capital"=> $capital, 	 // campoVacio($capital), 	
			"continente"=> $continente, //campoVacio($continente), 	
			"lat"=> $lat,  // campoVacio($lat), 	
			"lng"=> $lng, // campoVacio($lng), 	
			"codeISO2"=> $codeISO2, // campoVacio($codeISO2), 	
			"codeISO3"=> $codeISO2, // campoVacio($codeISO2), 	
			"fecha_alta"=> desfaseHorario($fecha_alta, $tiempoDesfase, "entera", "suma") 	, 
			"fecha_mod"=>controlFechaModificacion($fecha_modificacion, $tiempoDesfase, "entera", "suma"),			
			"accion"=> "select",
			"tabla"=> "pais",
			"nomFichero"=> "select_pais.php",			
		);
	}	
	
	$stmt->close();
	# echo "<pre>"; print_r($data); echo "</pre>";
	echo json_encode(utf8ize($data));
	# $stmt->mysqli();
	$mysqli->close();
?>