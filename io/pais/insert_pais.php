<?php
	require_once("../funciones.php");
	conectar_bbdd('configstream');
	include_once("../conexion/dbi_connect.php");
	$data = array();
	
	
	# Recogemos datos
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	$nombre = comprobarParametros('nombre'); 
	$codeCalling = comprobarParametros('codeCalling'); 
	$capital = comprobarParametros('capital'); 
	$continente = comprobarParametros('continente'); 
	$latPais = comprobarParametros('latPais'); 
	$lngPais = comprobarParametros('lngPais'); 
	$codeISO2 = comprobarParametros('codeISO2'); 
	$codeISO3 = comprobarParametros('codeISO3'); 
	# echo "GET<pre>"; print_r($_GET); echo "</pre>"; echo "POS<pre>"; print_r($_POST); echo "</pre>";

	# Preparamos y ejecutamos la consulta
	$stmt = $mysqli->prepare("INSERT INTO configstream.paises (codeCalling, nombre, capital, continente, lat, lng , codeISO2, codeISO3) VALUES (?,?,?,?,?,?,?,?)");		
	$stmt->bind_param("ssssddss", $codeCalling, $nombre, $capital, $continente, $latPais, $lngPais , $codeISO2, $codeISO3);
	$stmt->execute();	
	
	
	# Evaluamos is ha ido todo bien o habido algun fallo
	if($stmt->errno){
		$response=0;
		$proceso = 'error';
	}else{
		$response=1;
		$proceso = 'ok';
	}


	# Consultamos el ultimo id insertado.
	$result=$mysqli->query("SELECT LAST_INSERT_ID() AS ultimoID;");		
	$row=$result->fetch_assoc();
		
	# Montamos el array 
	$data[] = array(
		"id"=>$row['ultimoID'], 
		"codeCalling"=>$codeCalling, 
		"nombre"=>$nombre, 
		"capital"=> $capital, 	
		"continente"=> $continente, 	
		"lat"=> $latPais, 	
		"lng"=> $lngPais, 	
		"codeISO2"=> $codeISO2, 	
		"codeISO3"=> $codeISO3, 		
		"response" => $response,
		"proceso" => $proceso,
		"accion"=> "insert",
		"tabla"=> "pais",
		"nomFichero"=> "insert_pais.php",
	);
		
	$stmt->close();
	echo json_encode($data); 
	$mysqli->close();
	
?>