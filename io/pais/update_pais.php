<?php
	require_once("../funciones.php");
	conectar_bbdd('configstream');
	include_once("../conexion/dbi_connect.php");
	$data = array();
	
	# Recogemos datos
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	$idSelect = comprobarParametros('idSelect'); 
	$nombre = comprobarParametros('nombre'); 
	$codeCalling = comprobarParametros('codeCalling'); 
	$capital = comprobarParametros('capital'); 
	$continente = comprobarParametros('continente'); 
	$latPais = comprobarParametros('latPais'); 
	$lngPais = comprobarParametros('lngPais'); 
	$codeISO2 = comprobarParametros('codeISO2'); 
	$codeISO3 = comprobarParametros('codeISO3'); 
	$fechaA = comprobarParametros('fechaA'); 
	$fechaM = comprobarParametros('fechaM'); 
	#echo "GET<pre>"; print_r($_GET); echo "</pre>"; 	echo "POS<pre>"; print_r($_POST); echo "</pre>";
	
	# Preparamos y ejecutamos la consulta
	# lat y long son 'd' porque tienen 'decimales'
	$stmt = $mysqli->prepare("UPDATE configstream.paises SET
												codeCalling = ?,
												nombre = ?,
												capital = ?,
												continente = ?,
												lat = ?,
												lng = ?, 
												codeISO2 = ?,
												codeISO3 = ?,
												fecha_modificacion = CURRENT_TIMESTAMP
												WHERE id = ? ");		
	$stmt->bind_param("ssssddssi", $codeCalling, $nombre,  $capital, $continente, $latPais, $lngPais, $codeISO2, $codeISO3, $idSelect);
	$stmt->execute();	
	
	# Evaluamos is ha ido todo bien o habido algun fallo
	if($stmt->errno){
		$response=0;
		$proceso = 'error';
	}else{
		$response=1;
		$proceso = 'ok';
	}
	
	
	date_default_timezone_set("Europe/Madrid");
	
	# Montamos el array 
	$data[] = array(
		"id"=>$idSelect, 
		"codeCalling"=>$codeCalling, 
		"nombre"=>$nombre, 
		"capital"=> $capital, 	
		"continente"=> $continente, 	
		"lat"=> $latPais, 	
		"lng"=> $lngPais, 	
		"codeISO2"=> $codeISO2, 	
		"codeISO3"=> $codeISO3, 	
		"response" => $response,
		"proceso" => $proceso,
		"accion"=> "update",
		"tabla"=> "pais",
		"nomFichero"=> "update_pais.php",
	);
	
	
	$stmt->close();
	echo json_encode($data); 
	$mysqli->close();
?>