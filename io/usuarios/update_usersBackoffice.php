<?php
	require_once("../funciones.php");
	conectar_bbdd('eude');
	include_once("../conexion/dbi_connect.php");
	$data = array();

	# Recogemos datos
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	$idSelect = comprobarParametros('idSelect'); 
	$nombre = comprobarParametros('nombre'); 
	$pass = comprobarParametros('pass'); 
	$mail = comprobarParametros('mail'); 
	$rid = comprobarParametros('rid'); 		
	$nameRol = comprobarParametros('nameRol'); 		
	# echo "GET<pre>"; print_r($_GET); echo "</pre>"; echo "POS<pre>"; print_r($_POST); echo "</pre>";
	
	# Preparamos y ejecutamos la consulta		
	# $stmt = $mysqli->prepare("call usersBackoffice_update(?,?,?,?,?) ");		
	$stmt = $mysqli->prepare("UPDATE	eude.backoffice 
											    SET	name = ?,  pass = ?,  mail = ?, rid = ?, fecha_modificacion = CURRENT_TIMESTAMP
												WHERE uid = ?;");		
	$stmt->bind_param("sssii", $nombre, $pass, $mail, $rid, $idSelect);
	$stmt->execute();	
	
	# Evaluamos is ha ido todo bien o habido algun fallo
	if($stmt->errno){
		$response=0;
		$proceso = 'error';
	}else{
		$response=1;
		$proceso = 'ok';
	}
	
	date_default_timezone_set("Europe/Madrid");
	
	# Montamos el array 
	$data[] = array(
		"id"=>$idSelect, 
		"name"=>$nombre, 
		"pass"=>$pass, 
		"mail"=>$mail , 
		"rid"=>$rid, 
		"nameRol"=>$nameRol, 
		"fecha_alta_europea"=>date("d-m-Y"),			
		"fechaHora_alta_europea"=>date("Y-m-d  h:m:s"),			
		"response" => $response,
		"proceso" => $proceso,
		"accion"=> "update",
		"tabla"=> "backoffice",
		"nomFichero"=> "update_usersBackoffice.php",
	);
		
	$stmt->close();
	echo json_encode($data); 
	$mysqli->close();
?>