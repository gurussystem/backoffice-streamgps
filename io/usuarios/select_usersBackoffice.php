<?php 
	require_once("../funciones.php");
	conectar_bbdd('eude');
	include_once("../conexion/dbi_connect.php");
	$data = array();
	
	# Recogemos datos
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	$idSeleccionado = comprobarParametros('idSelect'); 
	# echo "GET<pre>"; print_r($_GET); echo "</pre>"; echo "POS<pre>"; print_r($_POST); echo "</pre>";

	# Preparamos y ejecutamos la consulta
	$sql = "SELECT
		b.uid,
        b.name,
        b.pass,
        b.mail,
        b.rid,
        b.fecha_alta,
        b.fecha_modificacion,
        r.nom AS nomRol
	FROM
		eude.backoffice AS b
	INNER  JOIN configstream.rol_user AS r ON (r.id = b.rid)";
	
	if($idSeleccionado){
		$consulta = $sql . "WHERE b.uid= ?";
		$stmt = $mysqli->prepare($consulta);		
		$stmt->bind_param("i", $idSeleccionado);			
	}else{
		$stmt = $mysqli->prepare($sql);		
	}
	
	$stmt->execute();
	$stmt->bind_result($uid, $name, $pass, $mail, $rid, $fecha_alta, $fecha_modificacion, $nomRol);
	
	while($stmt->fetch()) {		
		$data[] = array(
			"uid"=>$uid, 
			"name"=>$name, 
			"pass"=>$pass, 
			"mail"=>$mail, 
			"rid"=>$rid, 
			"nomRol"=>$nomRol, 
			"fecha_alta"=> desfaseHorario($fecha_alta, $tiempoDesfase, "fecha", "suma") 	, 
			"fecha_mod"=>controlFechaModificacion($fecha_modificacion, $tiempoDesfase, "fecha", "suma"),
			"accion"=> "select",
			"tabla"=> "backoffice",
			"nomFichero"=> "select_usersBackoffice.php",
			"id"=>$idSeleccionado, 
		);
	}
	
	$stmt->close();
	echo json_encode($data); 
	$mysqli->close();
?>