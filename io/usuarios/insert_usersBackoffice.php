<?php
	require_once("../funciones.php");
	conectar_bbdd('eude');
	require_once("../conexion/dbi_connect.php");
	 //datos_conexion();
	
	# Recogemos datos
	$nombre = comprobarParametros('nombre');
	$pass = comprobarParametros('pass');
	$mail = comprobarParametros('mail');
	$nameRol = comprobarParametros('nameRol');
	$rid = sacarID_rolUser($nameRol);
	$tiempoDesfase = comprobarParametros('tiempoDesfase');
	# echo "GET<pre>"; print_r($_GET); echo "</pre>"; echo "POS<pre>"; print_r($_POST); echo "</pre>";
	
	# Preparamos y ejecutamos la consulta	
	$stmt = $mysqli->prepare("INSERT INTO eude.backoffice (name, pass, mail, rid ) VALUES (?,?, ?, ?)");			
	# $stmt = $mysqli->prepare("call usersBackoffice_insert(?,?,?,?)");			
	$stmt->bind_param("sssi", $nombre, $pass, $mail, $rid);
	$stmt->execute();

	# Evaluamos is ha ido todo bien o habido algun fallo
	if($stmt->errno  ==  1062){
		$response=0;
		$proceso = 'duplicado';
	}else if($stmt->errno){
		$response=0;
		$proceso = 'error';
	}else{
		$response=1;
		$proceso = 'ok';
	}
	

	# Consultamos el ultimo id insertado.
	$result=$mysqli->query("SELECT LAST_INSERT_ID() AS ultimoID;");		
	 $row=$result->fetch_assoc();
		
	# Montamos el array 
	$data[] = array(
		"id"=>$row['ultimoID'], 
		"name"=>$nombre , 
		"pass"=>$pass , 
		"mail"=>$mail , 
		"rid"=>$rid, 
		"nameRol"=>$nameRol, 		
		"response" => $response,
		"proceso" => $proceso,
		"accion"=> "insert",
		"tabla"=> "backoffice",
		"nomFichero"=> "insert_usersBackoffice.php",
	);
	
	$stmt->close();
	echo json_encode($data); 
	# echo "<pre>"; print_r($mysqli); echo "</pre>";
	$mysqli->close();
?>