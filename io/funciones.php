<?php
	# Función para limpiar caracteres
	function limpiarCaracteres($string){
		return strtr($string,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
	}
	
	
	# Hacemos tratamiento al nombre de la empresas, en caso que tenga algun caracter extraño se lo quitamos	
	function limpiarVariable($variable){
		$variable=str_replace(array('(','\'','´',',','.','{','}','+','´','*','¨','[',']','%','&','/','%','\$','#','"','!','?','¡',')',',',';',':','^','`'),'',$variable);
		$variable=str_replace(' ', '_', $variable);
		//echo " Usuario limpio: " . $variable . " <br> ";
		return $variable;
	}
	
	function comprobarParametros($param){
		// echo "<bR>".$param;
		
		if(isset($_GET[$param])){ 	
			return ($_GET[$param]); 
		}
		else if(isset($_POST[$param])) {
			return ($_POST[$param]);    
		}
		else if(isset($_REQUEST[$param])) {
			return ($_REQUEST[$param]);    		
		}
		//echo "GET<pre>"; print_r($_GET); echo "</pre>"; echo "POS<pre>"; print_r($_POST); echo "</pre>";
	}
	
	
	/*******************************************************************
	* Sumo o resto el desfase horario según en que parte del mundo estemos
	*******************************************************************/ 
	function desfaseHorario($fecha, $tDesfase, $opcion, $operacion){			
		// echo "<br> desfaseHorario( " . $fecha . " ," .  $tDesfase . " , " . $opcion  . " , " .  $operacion . " ) ";
	
		
		$pos = strpos($fecha, 'T'); // devuelve falso o la posicion del texto que se busca
		//	echo "<br>pos: " . $pos;

		if ($pos === false) {
		   $dtime = DateTime::createFromFormat("Y-m-d H:i:s", $fecha , new DateTimeZone("UTC"));
		  //  echo '<br>if '.$dtime->format('Y-m-d H:i:s') .'<br>';
		} 
		else {
			$dtime = new DateTime($fecha);
			//  echo '<br> else '.$dtime->format('Y-m-d\TH:i:sP') .'<br>';
		}		
		$fecha_timestamp = $dtime->getTimestamp();	// convierto la fecha en timestamp
		
		// Calculo en milisegundos los minutos
		$milisegundos_desfase = $tDesfase * 60;
		
		// Sumo los milisegundos a la fecha
		if($operacion == 'suma'){
			$fecha_timestamp_new = $fecha_timestamp + $milisegundos_desfase; 
		}else{
			$fecha_timestamp_new = $fecha_timestamp - $milisegundos_desfase; 
		}
		
		// Convierto al formato normal y devuelvo el resultado		
		switch($opcion){
			case 'entera':
				$fecha_result = gmdate('Y-m-d H:i:s',$fecha_timestamp_new);
				break;
				
			case 'hora':
				$fecha_result = gmdate('H:i:s',$fecha_timestamp_new);
				break;
				
			case 'fecha':
				$fecha_result = gmdate('Y-m-d',$fecha_timestamp_new);
				break;	
		}
	
		return $fecha_result;
	
	}
	
	
	function controlFechaModificacion($fechaMod, $tiempoDesfase, $opcion, $operacion){
		if(isset($fechaMod))
			return desfaseHorario($fechaMod, $tiempoDesfase, $opcion, $operacion);
		else
			return "-";
	}
	
	function definir_dominio(){
		//Recogemos del nombre del servidor lo que hay antes del punto
		$exp = explode('.', $_SERVER['SERVER_NAME']);
		$subdominio = $exp[0];
		return $exp[0]; // if streamgpsnew
		//echo '<pre>'; 		print_r($exp); 		echo '</pre>';	
	}
	
	
	function campoVacio($campo){
		if(isset($campo))
			return $campo;
		else
			return "-";
	}
	
	
	function definir_dbServer(){
		$dominio = definir_dominio();
		# echo "definir_dbServer(".$dominio.")" ;

		switch($dominio){
			case 'streamgpsnew': 
			case 'backoffice':
			case 'streamcontrol':
				#define('DB_USER', 'backoffice'); 
				define('DB_SERVER', 'localhost'); 
				break;
			
			case 'streamgps':
				#define('DB_USER', 'topo'); 
				define('DB_SERVER', 'sgclients.cisjdxpoil63.eu-west-1.rds.amazonaws.com'); 
				break;
					
		} // fin switch 'subdominio'	
	} // fin funcion 
	
	
	function conectar_bbdd($nameDB){
		 //echo 'conectar_bbdd('.$nameDB.')' ;
		
		// definimos  el usuario y la constraseña
		// define('DB_USER', 'backoffice'); 
		define('DB_USER', 'topo'); 
		define('DB_PASSWORD', '4r8icq9ypvstc2kx'); 
		//definir_dbServer();
		switch($_SERVER['SERVER_NAME']){
			case 'streamgps.backoffice.com': 
				define('DB_SERVER', 'localhost'); 
				break;
			
			case 'streamgps.com':
				define('DB_SERVER', 'sgclients.cisjdxpoil63.eu-west-1.rds.amazonaws.com'); 
				break;
					
		} // fin switch 'subdominio'	
		define('DB_CLIENTE', $nameDB );		
		// datos_conexion();	
		// conexion_activa();		
	}
	
	function datos_conexion(){
		echo "<br>  DB_SERVER: " . DB_SERVER ;
		echo "<br>  DB_USER: " . DB_USER ;
		echo "<br>  DB_PASSWORD: " . DB_PASSWORD ;
		echo "<br>  DB_CLIENTE: " . DB_CLIENTE ;
		echo "<br>  SERVER_NAME: " . $_SERVER['SERVER_NAME'] ;
		echo "<br>  dominio: " . definir_dominio() ;
	}
	
	function conexion_activa(){
		/* comprobar si el servidor sigue vivo */
		if ($mysqli->ping()) {
			printf ("¡La conexión está bien!\n");
		} else {
			printf ("Error: %s\n", $mysqli->error);
		}
	}
	
	
	function sacarID_rolUser($rolUser){
		$porciones = explode(" - ", $rolUser);
		return $porciones[0]; 
	}
	
	
	//Mostramos los datos bancarios solo si es el fichero cta_empresa, de lo contrario enviaremos un null
	function comprobarFichero($campo , $fichero){
		if($fichero == "cta_empresa"){
			return $campo;
		}else{
			return null;
		}
	} // fin funcion
	
	//Solo cuando la consulta es query mostramos los siguientes datos	
	function operacionSQL(  $campo , $operacionSQL){
		if ($operacionSQL == 'query'){	
			return $campo ;
		}else{
			return null;
		}	
	}
	
	function tipoAccionLog($idAfectado, $accion){
		if(isset($idAfectado)){
				return "Consultando id " . $idAfectado;
		}else{
			switch($accion){
				case 'select':
					return "Consulta General";
					break;
					
				case 'insert':
					return "Haciendo un alta....";
					break;
			}//fin switch
		}	
			
	} // fin funcion
	
	function json_errores(){
		switch(json_last_error()) {
			case JSON_ERROR_NONE:
				echo ' - Sin errores';
			break;
			case JSON_ERROR_DEPTH:
				echo ' - Excedido tamaño máximo de la pila';
			break;
			case JSON_ERROR_STATE_MISMATCH:
				echo ' - Desbordamiento de buffer o los modos no coinciden';
			break;
			case JSON_ERROR_CTRL_CHAR:
				echo ' - Encontrado carácter de control no esperado';
			break;
			case JSON_ERROR_SYNTAX:
				echo ' - Error de sintaxis, JSON mal formado';
			break;
			case JSON_ERROR_UTF8:
				echo ' - Caracteres UTF-8 malformados, posiblemente están mal codificados';
			break;
			default:
				echo ' - Error desconocido';
			break;
		}
	}
	
	function utf8ize($d) {
    if (is_array($d)) {
        foreach ($d as $k => $v) {
            $d[$k] = utf8ize($v);
        }
    } else if (is_string ($d)) {
        return utf8_encode($d);
    }
    return $d;
}



?>