<?php
	require_once("funciones.php");
	conectar_bbdd('eude');
	include_once("conexion/dbi_connect.php");
	//datos_conexion();
	
	//Asigno los parametros a unas variables	
	$user = comprobarParametros('user');	
	$pass = comprobarParametros('pass');	
	//cho '<br> user: ' . $user;
	// echo '<br> pass: ' . $pass;	
		
	$response = array();
	$data = array();
	$ip = $_SERVER['REMOTE_ADDR'];		
	
	//Consulto si existen los datos
	$stmt = $mysqli->prepare("SELECT uid,name,pass,mail,rid FROM eude.backoffice WHERE  name=? AND pass=?");		
	$stmt->bind_param("ss", $user, $pass);
	$stmt->execute();
	$result = $stmt->get_result();
	$row=$result->fetch_assoc();	
//	echo '<pre>';print_r($data);echo '</pre>';
		
	if($row){
	//	echo "hay datos";
		$response["success"] = 1;
			
		//hago un insert`para el log_backoffices
		$stmt = $mysqli->prepare("INSERT INTO eude.log_backoffice_sessions (idUser, nameUser, ip) VALUES (?,?,?)");		
		$stmt->bind_param("iss", $row["uid"], $user, $ip);
		$stmt->execute();
		$result = $stmt->get_result();
	
		//Inicio la sesi�n
		session_start();
	
		$_SESSION['user']=$row["name"];
		$_SESSION['idUser']=$row["uid"];
		$_SESSION['rol']=$row["rid"];
		$_SESSION["ultimoAcceso"] =date("Y-n-j H:i:s");
		$_SESSION['uniqueIdUser']=$row["uid"];
	}else{
		$response["success"] = 0;
		//echo 'no hay datos';
	}
	
	$stmt->close();
	echo json_encode($response);
	$mysqli->close();
?>
