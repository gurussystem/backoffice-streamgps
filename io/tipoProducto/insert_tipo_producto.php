<?php
	require_once("../funciones.php");
	conectar_bbdd('configstream');
	include_once("../conexion/dbi_connect.php");
	$data = array();
	
	# Recogemos datos
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	$nombre = comprobarParametros('nombreTipoProducto'); 
	$desc = comprobarParametros('descTipoProducto');
	$tipoLive = comprobarParametros('tipoLiveTipoProducto');
	$tp_gps = comprobarParametros('tp_gps');
	$tp_wifi = comprobarParametros('tp_wifi');
	$tp_3g = comprobarParametros('tp_3g');
	$tp_4g = comprobarParametros('tp_4g');
	# echo "GET<pre>"; print_r($_GET); echo "</pre>"; echo "POS<pre>"; print_r($_POST); echo "</pre>";	
					
	# Preparamos y ejecutamos la consulta	
	$consulta ="INSERT INTO configstream.tipo_productos (nombre, descr, tipoLive, tp_gps, tp_3G,  tp_4G, tp_wifi)VALUES(?,?,?,?,?,?,?)";
	$stmt = $mysqli->prepare($consulta);		
	$stmt->bind_param("sssiiii", $nombre, $desc, $tipoLive, $tp_gps, $tp_3g, $tp_4g, $tp_wifi);
	$stmt->execute();
	
	
	# Evaluamos is ha ido todo bien o habido algun fallo
	if($stmt->errno){
		$response=0;
		$proceso = 'error';
	}else{
		$response=1;
		$proceso = 'ok';
	}
	
	# Consultamos el ultimo id insertado.
	$result=$mysqli->query("SELECT LAST_INSERT_ID() AS ultimoID;");		
	$row=$result->fetch_assoc();

	# Montamos el array 
	$data[] = array(
		"id"=>$row['ultimoID'], 
		"nombre"=>$nombre, 
		"desc"=>$desc, 
		"tipoLive"=>$tipoLive, 
		"tpGPS"=>$tp_gps, 
		"tpWIFI"=>$tp_wifi, 
		"tp3G"=>$tp_3g, 
		"tp4G"=>$tp_4g, 				
		"response" => $response,
		"proceso" => $proceso,
		"accion"=> "insert",
		"tabla"=> "tipo_productos",
		"nomFichero"=> "insert_tipo_productos.php",
	);
	
	$stmt->close();
	echo json_encode($data); 
	$mysqli->close();
?>