<?php
	require_once("../funciones.php");
	conectar_bbdd('configstream');
	include_once("../conexion/dbi_connect.php");
	$data = array();
	
	# Recogemos datos
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	$idSelect = comprobarParametros('idSelect'); 
	$nombre = comprobarParametros('nombre'); 
	$desc = comprobarParametros('desc'); 
	$tipoLive = comprobarParametros('tipoLive'); 
	$tp_gps = comprobarParametros('tp_gps'); 
	$tp_wifi = comprobarParametros('tp_wifi'); 
	$tp_3g = comprobarParametros('tp_3g'); 
	$tp_4g = comprobarParametros('tp_4g'); 	
	# echo "G <pre>"; print_r($_GET);  echo "</pre>"; echo "P <pre>"; print_r($_POST); echo "</pre>";
	
	# Preparamos y ejecutamos la consulta
	$consulta ="UPDATE
		configstream.tipo_productos
	SET
		nombre=?,
        descr=?,
        tipoLive=?,
		tp_gps=?,
        tp_3G=?,
        tp_4G=?,
        tp_wifi=?,
        fecha_modificacion = CURRENT_TIMESTAMP
	WHERE
        idTipoProducto = ?";
	$stmt = $mysqli->prepare($consulta);		
	$stmt->bind_param("sssiiiii", $nombre, $desc, $tipoLive, $tp_gps, $tp_3g, $tp_4g, $tp_wifi, $idSelect);
	$stmt->execute();
	
	
	# Evaluamos is ha ido todo bien o habido algun fallo
	if($stmt->errno){
		$response=0;
		$proceso = 'error';
	}else{
		$response=1;
		$proceso = 'ok';
	}
		
	# Montamos el array 
	$data[] = array(
		"id"=>$idSelect, 
		"nombre"=>$nombre, 
		"desc"=>$desc, 
		"tipoLive"=>$tipoLive, 
		"tpGPS"=>$tp_gps, 
		"tpWIFI"=>$tp_wifi, 
		"tp3G"=>$tp_3g, 
		"tp4G"=>$tp_4g, 
		"response" => $response,
		"proceso" => $proceso,
		"accion"=> "update",
		"tabla"=> "tipo_productos",
		"nomFichero"=> "update_tipo_producto.php",
	);
	
	
	$stmt->close();
	echo json_encode($data); 
	$mysqli->close();
?>