<?php 
	require_once("../funciones.php");
	conectar_bbdd('configstream');
	include_once("../conexion/dbi_connect.php");
	$data = array();
	
	# Recogemos datos
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	$idSeleccionado = comprobarParametros('idSelecionado'); 
	# echo "GET<pre>"; print_r($_GET); echo "</pre>"; echo "POS<pre>"; print_r($_POST); echo "</pre>";
	
	# Preparamos y ejecutamos la consulta	
	$sql ="SELECT idTipoProducto, nombre , descr, tipoLive, estado, tp_gps, tp_3G, tp_4G, tp_wifi, fecha_alta, fecha_modificacion   FROM configstream.tipo_productos ";
	
	if($idSeleccionado){
		$consulta = $sql . " idTipoProducto = ? Limit 1";
		$stmt = $mysqli->prepare($consulta);		
		$stmt->bind_param("i", $idSeleccionado);			
	}else{
		$stmt = $mysqli->prepare($sql);		
	}
	$stmt->execute();
	$stmt->bind_result($idTipoProducto, $nombre , $descr, $tipoLive, $estado, $tp_gps, $tp_3G, $tp_4G, $tp_wifi, $fecha_alta, $fecha_modificacion);

	while($stmt->fetch()) {	
		$data[] = array(
			"idTipoProducto"=>$idTipoProducto, 
			"nombre"=>utf8_decode($nombre), 
			"descr"=>$descr, 
			"tipoLive"=>$tipoLive, 
			"estado"=>$estado, 
			"tp_gps"=>$tp_gps, 
			"tp_3G"=>$tp_3G, 
			"tp_4G"=>$tp_4G, 
			"tp_wifi"=>$tp_wifi,
			"fecha_alta"=>$fecha_alta,
			"fecha_mod"=>$fecha_modificacion,
			"accion"=> "select",
			"tabla"=> "tipo_productos",
			"nomFichero"=> "select_tipo_producto.php",
			"id"=>$idTipoProducto, 
			"fecha_alta"=>desfaseHorario($fecha_alta, $tiempoDesfase, "entera", "suma"),			
			"fecha_mod"=>controlFechaModificacion($fecha_modificacion, $tiempoDesfase, "entera", "suma")
		);
	}	
	
	$stmt->close();
	# echo "<pre>"; print_r($data); echo "</pre>";
	echo json_encode(utf8ize($data));
	 # json_errores();
	$mysqli->close();
?>