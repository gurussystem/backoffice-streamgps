<?php 
	require_once("../funciones.php");
	conectar_bbdd('configstream');
	include_once("../conexion/dbi_connect.php");

	# Recogemos datos
	$idSeleccionado = comprobarParametros('idSelecionado');	
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	# echo "GET<pre>"; print_r($_GET); echo "</pre>"; echo "POS<pre>"; print_r($_POST); echo "</pre>";

	# Preparamos y ejecutamos la consulta
	$sql = "SELECT id, nom, descrip, fecha_alta, fecha_modificacion FROM	configstream.rol_user";
	
	if($idSeleccionado){
		$consulta = $sql . " WHERE id= ?";
		$stmt = $mysqli->prepare($consulta);		
		$stmt->bind_param("i", $idSeleccionado);	
	}else{
		$stmt = $mysqli->prepare($sql);		
	}
	$stmt->execute();
	$stmt->bind_result($id, $nom, $descrip, $fecha_alta, $fecha_modificacion );
	
	while($stmt->fetch()) {				
		$data[] = array(
			"id"=>$id, 
			"nom"=>$nom, 
			"descrip"=>$descrip, 		
			"fecha_alta"=> desfaseHorario($fecha_alta, $tiempoDesfase, "entera", "suma") ,			
			"fecha_mod"=>controlFechaModificacion($fecha_modificacion, $tiempoDesfase, "entera", "suma"), 
			"accion"=> "select",
			"tabla"=> "rol_user",
			"nomFichero"=> "select_rol_user.php",
		);
	}	
	
	$stmt->close();
	# echo "<pre>"; print_r($data); echo "</pre>";
	echo json_encode(utf8ize($data));
	 # json_errores();
	$mysqli->close();
?>