<?php
	require_once("../funciones.php");
	conectar_bbdd('configstream');
	include_once("../conexion/dbi_connect.php");
	$data = array();
	
	# Recogemos datos
	$nombre = comprobarParametros('nombre');	
	$descripcion = comprobarParametros('descripcion');
	$tiempoDesfase = comprobarParametros ('tiempoDesfase'); 
		
	# Preparamos y ejecutamos la consulta
	$consulta = "INSERT INTO configstream.rol_user (nom, descrip ) VALUES (?,?)";	
	$stmt = $mysqli->prepare($consulta);		
	$stmt->bind_param("ss", $nombre, $descripcion );
	$stmt->execute();
	
	if($stmt->errno){
		$response=0;
		$proceso = 'error';
	}else{
		$response=1;
		$proceso = 'ok';
	}
	
	# Consultamos el ultimo id insertado.
	$result=$mysqli->query("SELECT LAST_INSERT_ID() AS ultimoID;");		
	$row=$result->fetch_assoc();
	
	# Montamos el array 
	$data[] = array(
		"id"=>$row['ultimoID'], 
		"nom"=>$nombre, 
		"descrip"=>$descripcion,
		"response" => $response,
		"proceso" => $proceso,
		"accion"=> "insert",
		"tabla"=> "rol_user",
		"nomFichero"=> "insert_rol_user.php",
	);

	$stmt->close();
	echo json_encode($data); 
	$mysqli->close();	
?>