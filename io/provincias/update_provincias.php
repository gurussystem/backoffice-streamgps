<?php
	require_once("../funciones.php");
	conectar_bbdd('configstream');
	include_once("../conexion/dbi_connect.php");
	$data = array();
	
	# Recogemos datos
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	$idSelect = comprobarParametros('idSelect'); 
	$nombre = comprobarParametros('nombre'); 
	$codigoPostal = comprobarParametros('codigoPostal'); 
	$fechaA = comprobarParametros('fechaA'); 
	$fechaM = comprobarParametros('fechaM'); 
	# echo "GET<pre>"; print_r($_GET); echo "</pre>"; 	echo "POS<pre>"; print_r($_POST); echo "</pre>";
	
	# Preparamos y ejecutamos la consulta
	$consulta = "UPDATE configstream.provincias SET nombre = ?, codigo_postal = ?, fecha_modificacion = CURRENT_TIMESTAMP
	WHERE id = ? ";
	$stmt = $mysqli->prepare($consulta);		
	$stmt->bind_param("ssi", $nombre, $codigoPostal,  $idSelect);
	$stmt->execute();	
	
	# Evaluamos is ha ido todo bien o habido algun fallo
	if($stmt->errno){
		$response=0;
		$proceso = 'error';
	}else{
		$response=1;
		$proceso = 'ok';
	}
	
	# Montamos el array 
	$data[] = array(
		"id"=>$idSelect, 
		"nombre"=>$nombre, 
		"codigoPostal"=>$codigoPostal,  
		"fecha_alta_europea"=>$fechaA,			
		"response" => $response,
		"proceso" => $proceso,
		"accion"=> "update",
		"tabla"=> "provincias",
		"nomFichero"=> "update_provincias.php",
	);

	$stmt->close();
	echo json_encode($data); 
	$mysqli->close();
?>