CREATE PROCEDURE `provincias_insert`(_nombre varchar(50), _cp varchar(2))
BEGIN
	INSERT INTO
		configstream.provincias (nombre, codigo_postal)
	VALUES
		(_nombre, _cp);
END