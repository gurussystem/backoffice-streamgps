<?php 
	require_once("../funciones.php");
	conectar_bbdd('configstream');
	include_once("../conexion/dbi_connect.php");
	$data = array();
	
	# Recogemos datos
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	$idSeleccionado = comprobarParametros('idSelecionado'); 
	# echo "GET<pre>"; print_r($_GET); echo "</pre>"; echo "POS<pre>"; print_r($_POST); echo "</pre>";
	
	# Preparamos y ejecutamos la consulta
	$sql = "SELECT id, nombre, codigo_postal, fecha_alta, fecha_modificacion FROM configstream.provincias ";
	if($idSeleccionado){
		$consulta = $sql . "  where id = ? ";
		$stmt = $mysqli->prepare($consulta);		
		$stmt->bind_param("i", $idSeleccionado);
	}else{	
		$consulta = "call provincias_selectOption()";
		$stmt = $mysqli->prepare($sql);		
	}
	$stmt->execute();	
	$stmt->bind_result($id, $nombre, $codigo_postal, $fecha_alta, $fecha_modificacion);
	
	while($stmt->fetch()) {
				
		$data[] = array(
			"id"=>$id, 
			"nombre"=>$nombre, 
			"codigoPostal"=> campoVacio($codigo_postal), 	
			"fecha_alta"=> desfaseHorario($fecha_alta, $tiempoDesfase, "entera", "suma") 	, 
			"fecha_mod"=>controlFechaModificacion($fecha_modificacion, $tiempoDesfase, "entera", "suma"),			
			"accion"=> "select",
			"tabla"=> "provincias",
			"nomFichero"=> "select_provincias.php",			
		);
	}	
	
	$stmt->close();
	# echo "<pre>"; print_r($data); echo "</pre>";
echo json_encode(utf8ize($data));
	 # json_errores();
	$mysqli->close();
?>