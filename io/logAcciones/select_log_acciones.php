<?php 
	require_once("../funciones.php");
	conectar_bbdd('eude');
	include_once("../conexion/dbi_connect.php");
	$data = array();
	
	# Recogemos datos
	$idSeleccionado = comprobarParametros('idSelecionado'); 
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	# echo "GET<pre>"; print_r($_GET); echo "</pre>"; echo "POS<pre>"; print_r($_POST); echo "</pre>";
	
	# Preparamos y ejecutamos la consulta
	$sql = "SELECT  id, idUser, nameUser, fecha, ip, accion, fichero, idAfectado FROM  eude.log_backoffice_acciones ";
		
	if($idSeleccionado){
		$consulta = $sql ." WHERE 	id = ? ORDER BY 	id DESC Limit 1";
		$stmt = $mysqli->prepare($consulta);		
		$stmt->bind_param("i", $idSeleccionado);
	}else{
		$consulta = $sql . " ORDER BY id DESC ";
		$stmt = $mysqli->prepare($consulta);		
	}
	$stmt->execute();
	$stmt->bind_result($id, $idUser, $nameUser, $fecha, $ip, $accion, $fichero, $idAfectado);

	while($stmt->fetch()) {

		$data[] = array(
			"id"=>$id, 
			"idUser"=>$idUser, 
			"nameUser"=>$nameUser, 
			"fecha"=> desfaseHorario($fecha, $tiempoDesfase, "fecha", "suma") , 
			"ip"=>$ip, 
			"accion"=>$accion, 
			"fichero"=>$fichero, 
			"idAfectado"=>tipoAccionLog($idAfectado, $accion), 
			"accion"=> "select",
			"tabla"=> "log_acciones",
			"nomFichero"=> "select_log_acciones.php",
		);
	}	
	
	$stmt->close();
	# echo '<pre>'; print_r($data);echo '</pre>';
	echo json_encode($data); 
	$mysqli->close();
?>
