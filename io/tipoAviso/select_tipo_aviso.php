<?php 
	require_once("../funciones.php");
	conectar_bbdd('configstream');
	include_once("../conexion/dbi_connect.php");
	$data = array();
	
	# Recogemos datos
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	$idSelect = comprobarParametros('idSelecionado'); 
	# echo "GET<pre>"; print_r($_GET); echo "</pre>"; echo "POS<pre>"; print_r($_POST); echo "</pre>";
	
	# Preparamos y ejecutamos la consulta
	$sql ="	SELECT id, nom, descrip, fecha_alta, fecha_modificacion FROM configstream.tipo_avisos";
	if($idSelect){
		$consulta = $sql . " where id = ? Limit 1";
		$stmt = $mysqli->prepare($consulta);		
		$stmt->bind_param("i", $idSelect);
	}else{
		$stmt = $mysqli->prepare($sql );			
	}
	$stmt->execute();
	$stmt->bind_result($id, $nom, $descrip, $fecha_alta, $fecha_modificacion);

	while($stmt->fetch()) {
			
		$data[] = array(
			"id"=>$id, 
			"nom"=>$nom, 
			"descrip"=>$descrip, 
			"fecha_alta"=>desfaseHorario($fecha_alta, $tiempoDesfase, "entera", "suma"),			
			"fecha_mod"=>controlFechaModificacion($fecha_modificacion, $tiempoDesfase, "entera", "suma"),
			"accion"=> "select",
			"tabla"=> "tipo_avisos",
			"nomFichero"=> "select_tipo_avisos.php",
		);
	}	#  fin while
	
	$stmt->close();
	#  echo "<pre>"; print_r($data); echo "</pre>";
	echo json_encode(utf8ize($data));
	 # json_errores();
	$mysqli->close();
?>