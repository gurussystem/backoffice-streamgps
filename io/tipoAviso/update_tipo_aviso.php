<?php
	require_once("../funciones.php");
	conectar_bbdd('configstream');
	include_once("../conexion/dbi_connect.php");
	$data = array();
	
	# Recogemos datos
	$tiempoDesfase = comprobarParametros('tiempoDesfase'); 
	$idSelect = comprobarParametros('idSelect'); 
	$nombre = comprobarParametros('nombre'); 
	$descripcion = comprobarParametros('descripcion'); 
	$fechaA = comprobarParametros('fechaA'); 
	$fechaM = comprobarParametros('fechaM'); 
	# echo "GET<pre>"; print_r($_GET); echo "</pre>"; 	echo "POS<pre>"; print_r($_POST); echo "</pre>";
	
	# Preparamos y ejecutamos la consulta
	$consulta ="UPDATE configstream.tipo_avisos	SET nom = ?, descrip = ?, fecha_modificacion = CURRENT_TIMESTAMP WHERE	id = ?";
	$stmt = $mysqli->prepare($consulta);		
	$stmt->bind_param("ssi", $nombre, $descripcion,  $idSelect);
	$stmt->execute();	
	
	# Evaluamos is ha ido todo bien o habido algun fallo
	if($stmt->errno){
		$response=0;
		$proceso = 'error';
	}else{
		$response=1;
		$proceso = 'ok';
	}

	# Montamos el array 
	$data[] = array(
		"id"=>$idSelect, 
		"nom"=>$nombre, 
		"descrip"=>$descripcion, 
		"fecha_alta_europea"=>$fechaA,			
		"response" => $response,
		"proceso" => $proceso,
		"accion"=> "update",
		"tabla"=> "tipo_avisos",
		"nomFichero"=> "update_tipo_avisos.php",
	);
	
	
	$stmt->close();
	echo json_encode($data); 
	$mysqli->close();
?>