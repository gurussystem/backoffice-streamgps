CREATE PROCEDURE `router_selectOption`()
BEGIN
SELECT
	r.idrouters as idrouters,
    r.name as name,
    r.type as type,
    r.serialnum as serialnum,
    r.mac as mac,
    r.user as user,
    r.pass as pass,
    r.ipstatic as ipstatic,
    r.dyndns as dyndns,
	(select p.idEmpresa from productos as p  where r.idrouters = p.idRouter) as idPropietario,
	(Select e.nombre from empresas as e inner join productos as p on(p.idEmpresa = e.id)  where p.idRouter = r.idrouters) as nomPropietario,
    r.asignado as asignado,
    r.fecha_alta as fecha_alta,
    r.fecha_modificacion as fecha_modificacion

FROM
	eude.routers as r

 where
	r.idrouters != 0
	order by r.idrouters asc;
END