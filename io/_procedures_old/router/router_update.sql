CREATE   PROCEDURE `router_update`(_name varchar(45), _type varchar(45), _serialnum varchar(45), _mac varchar(45), _user varchar(45), _pass varchar(45), _ipstatic varchar(45), _dyndns varchar(45),_id int(11))
BEGIN
	UPDATE
		eude.routers
	SET
		name = _name,
		type = _type,
        serialnum = _serialnum,
        mac= _mac,
        user=_user,
        pass= _pass,
        ipstatic= _ipstatic,
        dyndns = _dyndns,
        fecha_modificacion = CURRENT_TIMESTAMP
	WHERE
        idrouters = _id;

END