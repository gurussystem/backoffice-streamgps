CREATE   PROCEDURE `router_insert`(_name varchar(45), _type varchar(45), _serialnum varchar(45), _mac varchar(45), _user varchar(45), _pass varchar(45), _ipstatic varchar(45), _dyndns varchar(45))
BEGIN
INSERT INTO eude.routers(name, type, serialnum, mac, user, pass, ipstatic, dyndns)
		 VALUES (
			_name ,
            _type,
            _serialnum ,
            _mac ,
            _user ,
            _pass ,
            _ipstatic ,
            _dyndns );
END