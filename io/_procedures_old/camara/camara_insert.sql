CREATE  PROCEDURE `camara_insert`(
 _nombre varchar(45), 
 _tipo varchar(45),
 _numSerial varchar(45),
 _mac varchar(45) , 
 _user varchar(45), 
 _clave varchar(45),
 _ip varchar(45), 
 _rtspMain varchar(45),
 _rtspSecond varchar(45),
 _resolucionMain varchar(45),
 _resolucionSecond varchar(45),
 _kbpsMain varchar(45),
 _kbpsSecond varchar(45)
 )
 
BEGIN
	INSERT INTO
		eude.camaras(
			name,
            type,
            serialnum,
            mac,
            user,
            pass,
            localip,
            rtsp_main,
            rtsp_second,
            resolution_main,
            kbps_main,
            resolution_second,
            kbps_second)
	VALUES (
		_nombre,
        _tipo,
        _numSerial,
        _mac,
        _user,
        _clave,
        _ip,
        _rtspMain,
        _rtspSecond,
        _resolucionMain,
        _kbpsMain,
        _resolucionSecond,
        _kbpsSecond);
END