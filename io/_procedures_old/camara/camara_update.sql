CREATE  PROCEDURE `camara_update`(
	_nombre varchar(45),
	_tipo varchar(45),
    _numSerial varchar(45),
    _mac varchar(45) ,
    _user varchar(45),
    _clave varchar(45),
    _ip varchar(45),
    _rtsp_1 varchar(45),
    _kbps_1 varchar(45),
    _resolucion_1 varchar(45),
    _rtsp_2 varchar(45),
    _kbps_2 varchar(45),
    _resolucion_2 varchar(45),
    _idCam int(11))
BEGIN
	UPDATE
		eude.camaras
	SET
		name=_nombre,
        type=_tipo,
        serialnum=_numSerial,
        mac=_mac,
        user=_user,
        pass=_clave,
        localip=_ip,
        rtsp_main=_rtsp_1,
        kbps_main=_kbps_1,
        resolution_main=_resolucion_1,
        rtsp_second=_rtsp_2,
        kbps_second=_kbps_2,
        resolution_second=_resolucion_2,
        fecha_modificacion = CURRENT_TIMESTAMP
	WHERE
		idcamaras=_idCam;
END