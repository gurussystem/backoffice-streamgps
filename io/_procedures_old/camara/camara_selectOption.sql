CREATE PROCEDURE `camara_selectOption`()
BEGIN
	SELECT
		c.idcamaras as idcamaras ,
		c.name as name,
		c.type as type,
		c.serialnum as serialnum,
		c.mac as mac,
		c.user as user,
		c.pass as pass,
		c.localip as localip,
		c.rtsp_main as rtsp_main,
		c.rtsp_second as rtsp_second,
		c.resolution_main as resolution_main,
		c.kbps_main as kbps_main,
		c.resolution_second as resolution_second,
		c.kbps_second as kbps_second,
		c.asignada as asignada,
		c.fecha_alta as fecha_alta,
		c.fecha_modificacion as fecha_modificacion,
        (select p.idEmpresa from productos as p  where c.idcamaras = p.idcamara) as idPropietario,
		(Select e.nombre from empresas as e inner join productos as p on(p.idEmpresa = e.id)  where p.idCamara = c.idcamaras) as nomPropietario
	FROM
		eude.camaras as c
		;
	END