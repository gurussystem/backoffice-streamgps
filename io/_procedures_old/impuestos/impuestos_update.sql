CREATE  PROCEDURE `impuestos_update`(_nombre varchar(25), _valor int(2), _descripcion varchar(100), _id int(11))
BEGIN
	UPDATE
		configstream.impuestos
	SET
		nombre = _nombre,
		valor = _valor,
        descripcion = descripcion,
        fecha_modificacion = CURRENT_TIMESTAMP
    WHERE
		id = _id ;
END