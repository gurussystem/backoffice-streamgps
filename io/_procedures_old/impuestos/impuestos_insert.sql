CREATE  PROCEDURE `impuestos_insert`(_nombre varchar(25), _valor int(2), _descripcion varchar(100))
BEGIN
	INSERT INTO
		configstream.impuestos (nombre, valor, descripcion)
	VALUES
		(_nombre, _valor, _descripcion);
END