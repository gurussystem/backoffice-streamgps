CREATE   PROCEDURE `impuestos_select`(_id int(11))
BEGIN
	SELECT
		id, nombre, valor, descripcion, fecha_alta, fecha_modificacion
	FROM
		configstream.impuestos
	WHERE id = _id;
END