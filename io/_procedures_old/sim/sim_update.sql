CREATE  PROCEDURE `sim_update`(_numSim01 int(11), _numSim02 int(11), _pin int(11), _punk int(11), _model varchar (45), _id int(11))
BEGIN
	UPDATE
		eude.sim
    SET
		numSim01 = _numSim01 ,
        numSim02 = _numSim02,
		pin = _pin,
        punk = _punk,
		model = _model,
		fecha_modificacion = CURRENT_TIMESTAMP
    WHERE
		idSim = _id;
END