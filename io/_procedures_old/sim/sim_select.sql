CREATE PROCEDURE `sim_select`( _id int)
BEGIN
SELECT
	s.idSim as idSim ,
    s.numSim01 as numSim01,
    s.numSim02 as numSim02,
    s.pin as pin,
    s.punk as punk,
    s.model as model,
    s.fecha_alta as fecha_alta,
    s.fecha_modificacion as fecha_modificacion,
	(select p.idEmpresa from productos as p  where s.idSim = p.idSim) as idPropietario,
	(Select e.nombre from empresas as e inner join productos as p on(p.idEmpresa = e.id)  where p.idSim = s.idSim) as nomPropietario,
    s.asignada as asignada
FROM
	eude.sim as s
WHERE
	s.idSim = _id ;
END