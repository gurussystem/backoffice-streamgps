CREATE   PROCEDURE `empresa_update`(_razonSocial varchar(45), _cif varchar(50), _email varchar(100),  _pass varchar(15), _direccion varchar(150), _cp varchar(50),  _poblacion varchar(150), _provincia varchar(150), _telefono1 varchar(100), _telefono2 varchar(100), _activo int(1), _prefixstream varchar(10),  _server text,  _streamapp varchar(250), _domini text, _subdomini text,_conn varchar(250), _id int(11), _idPais int(11), _videoAnalisis int(1)  )
BEGIN
	UPDATE
		eude.empresas
	SET
		razonSocial = _razonSocial,
		cif= _cif,
		email= _email,
		pass= _pass,
		direccion= _direccion,
		cp= _cp,
		poblacion = _poblacion,
		provincia= _provincia,
		telefono1= _telefono1,
		telefono2= _telefono2,
		activo= _activo,
		prefixstream= _prefixstream,
		server= _server,
		streamapp= _streamapp,
		domini= _domini,
		subdomini= _subdomini,
		conn= _conn,
		idPais= _idPais,
		videoAnalisis= _videoAnalisis
	WHERE
		id = _id;
END