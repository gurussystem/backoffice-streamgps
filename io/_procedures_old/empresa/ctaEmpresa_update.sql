CREATE   PROCEDURE `ctaEmpresa_update`( _titularCuenta int(11), _swift varchar(11), _iban varchar(24), _entidad int(4), _sucursal int(4), _dc int(2), _numCuenta int(10), _idCta int(11))
BEGIN
	UPDATE
		eude.cta_empresas
	SET
		titularCuenta = _titularCuenta,
        swift = _swift,
        iban = _iban,
        entidad = _entidad,
        sucursal = _sucursal,
        dc = _dc,
        numCuenta = _numCuenta,
        fecha_modificacion = CURRENT_TIMESTAMP
	WHERE
        idCta = _idCta;
END