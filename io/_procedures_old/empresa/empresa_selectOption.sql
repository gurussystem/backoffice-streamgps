CREATE  PROCEDURE `empresa_selectOption`()
BEGIN
	SELECT
		e.id as id,
		e.nombre as nombre,
		e.razonSocial as razonSocial,
		e.cif as cif,
		e.email as email,
		e.pass as pass,
		e.direccion as direccion,
		e.poblacion as poblacion,
        e.provincia as provincia,
		(SELECT pro.nombre FROM configstream.provincias as pro where pro.id=e.provincia) as nameProvincia,
		e.cp as cp,
		e.telefono1 as telefono1,
		e.telefono2 as telefono2,
		e.fechaAlta as fechaAlta,
		e.fechaSolicitudAlta as fechaSolicitudAlta,
		e.activo as estadoCliente,
		e.prefixstream as prefixstream,
		e.server as servidor,
		e.streamapp as streamapp,
		e.domini as domini,
		e.subdomini as subdomini,
		e.conn as conn,
		e.bbdd as bdCliente,
		e.licencias as licencias,
		cta_e.titularCuenta as titularCuenta,
		cta_e.entidad as entidad,
		cta_e.sucursal as sucursal,
		cta_e.dc as dc,
		cta_e.numCuenta as numCuenta,
		cta_e.iban as iban,
		cta_e.swift as swift,
        e.videoAnalisis as videoAnalisis,
        e.lat as lat,
        e.lng as lng,
        e.emailAlerta as emailAlerta,
        e.telefonoAlert as telefonoAlert,
        e.idPais as idPais,
        cta_e.fecha_alta as cta_fechaA,
		cta_e.fecha_modificacion as cta_fechaM
	FROM
		eude.empresas AS e
        INNER JOIN eude.cta_empresas AS cta_e ON (cta_e.idCta = e.idCta)
	WHERE
		e.id !=0;
END