CREATE   PROCEDURE `ctaEmpresas_insert`( _idCta int(11), _idEmpresa int(11), _titularCuenta varchar(250), _entidad int(4), _sucursal int(4), _dc int(2), _numCuenta int(10), _iban varchar(24), _swift varchar(11))
BEGIN
	INSERT INTO  eude.cta_empresas (idCta, idEmpresa, titularCuenta, entidad, sucursal, dc, numCuenta, iban, swift)
	VALUE ( _idCta, _idEmpresa, _titularCuenta, _entidad, _sucursal, _dc, _numCuenta, _iban, _swift);
END