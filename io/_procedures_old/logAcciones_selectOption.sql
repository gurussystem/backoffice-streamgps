CREATE   PROCEDURE `logAcciones_selectOption`()
BEGIN
	SELECT
		id, idUser, nameUser, fecha, ip, accion, fichero, idAfectado
	FROM
		eude.log_backoffice_acciones
    ORDER BY
		id DESC;
END