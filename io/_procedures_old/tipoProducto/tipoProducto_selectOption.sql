CREATE  PROCEDURE `tipoProducto_selectOption`()
BEGIN
	SELECT
		idTipoProducto, nombre , descr, tipoLive, estado, tp_gps, tp_3G, tp_4G, tp_wifi, fecha_alta, fecha_modificacion
    FROM
		configstream.tipo_productos ;
END