CREATE   PROCEDURE `tipoProducto_insert`(_nombre varchar(100), _descr varchar(250), _tipoLive varchar(10), _tp_gps int(1), _tp_3G int(1),  _tp_4G int(1), _tp_wifi int(1))
BEGIN
	INSERT INTO
		configstream.tipo_productos (nombre, descr, tipoLive, tp_gps, tp_3G,  tp_4G, tp_wifi)
	VALUES
		(_nombre, _descr, _tipoLive, _tp_gps, _tp_3G,  _tp_4G, _tp_wifi);
END