CREATE   PROCEDURE `tipoProducto_select`(_id int(11))
BEGIN
	SELECT
		idTipoProducto, nombre , descr, tipoLive, estado, tp_gps, tp_3G, tp_4G, tp_wifi, fecha_alta, fecha_modificacion
    FROM
		configstream.tipo_productos
	WHERE
		idTipoProducto = _id
	Limit 1;
END