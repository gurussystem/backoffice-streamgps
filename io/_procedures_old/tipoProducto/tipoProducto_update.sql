CREATE   PROCEDURE `tipoProducto_update`(_nombre varchar(100), _descr varchar(250), _tipoLive varchar(10), _estado int(1), _tp_gps int(1), _tp_3G int(1),  _tp_4G int(1), _tp_wifi int(1), _id int(11))
BEGIN
	UPDATE
		configstream.tipo_productos
	SET
		nombre=_nombre,
        descr=_descr,
        tipoLive=_tipoLive,
        estado=_estado,
        tp_gps=_tp_gps,
        tp_3G=_tp_3G,
        tp_4G=_tp_4G,
        tp_wifi=_tp_wifi,
        fecha_modificacion = CURRENT_TIMESTAMP
	WHERE
        idTipoProducto = _id ;
END