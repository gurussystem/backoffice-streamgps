CREATE  PROCEDURE `impuestos_selectOption`()
BEGIN
	SELECT
		id, nombre, valor, descripcion, fecha_alta, fecha_modificacion
	FROM configstream.impuestos;
END