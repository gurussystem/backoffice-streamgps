CREATE  PROCEDURE `tipoEventoLog_selectOption`()
BEGIN
	SELECT
		id, evento, descripcion, fecha_alta, fecha_modificacion
    FROM
		eude.tipo_evento_log;
END