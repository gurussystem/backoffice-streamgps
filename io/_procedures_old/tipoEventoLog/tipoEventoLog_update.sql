CREATE  PROCEDURE `tipoEventoLog_update`(_evento varchar(100), _des  varchar(150), _id int(1))
BEGIN
	UPDATE
		eude.tipo_evento_log
	SET
		evento=_evento,
        descripcion=_des,
        fecha_modificacion = CURRENT_TIMESTAMP
	WHERE
		id = _id;
END