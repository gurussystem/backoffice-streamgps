CREATE  PROCEDURE `tipoEventoLog_select`(_id int(11))
BEGIN
	SELECT
		id, evento, descripcion, fecha_alta, fecha_modificacion
	FROM
		eude.tipo_evento_log
	WHERE
		id = _id
	Limit 1;
END