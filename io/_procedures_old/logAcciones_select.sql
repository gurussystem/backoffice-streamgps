CREATE  PROCEDURE `logAcciones_select`(_id int)
BEGIN
	SELECT
		id, idUser, nameUser, fecha, ip, accion, fichero, idAfectado
	FROM
		eude.log_backoffice_acciones
	WHERE
		id = _id
	ORDER BY
		id DESC
	Limit 1;
END