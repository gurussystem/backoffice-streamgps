CREATE   PROCEDURE `tipoAviso_select`( _id int)
BEGIN
	SELECT
		id, nom, descrip, fecha_alta, fecha_modificacion
	FROM
		configstream.tipo_avisos
	WHERE
		id = _id
    Limit 1;
END