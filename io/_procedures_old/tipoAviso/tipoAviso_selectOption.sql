CREATE   PROCEDURE `tipoAviso_selectOption`()
BEGIN
	SELECT
		id, nom, descrip, fecha_alta, fecha_modificacion
    FROM
		configstream.tipo_avisos ;
END