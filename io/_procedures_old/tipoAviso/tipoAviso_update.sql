CREATE  PROCEDURE `tipoAviso_update`(_nom varchar (60), _descrip varchar (255) , _id int(10))
BEGIN
	UPDATE
		configstream.tipo_avisos
	SET
		nom = _nom,
        descrip = _descrip,
        fecha_modificacion = CURRENT_TIMESTAMP
	WHERE
		id = _id;
END