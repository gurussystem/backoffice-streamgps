CREATE  PROCEDURE `provincias_update`(_nombre varchar(50), _cp varchar(2), _id int(11))
BEGIN
	UPDATE
		configstream.provincias
	SET
		nombre = _nombre,
        codigo_postal = _cp,
        fecha_modificacion = CURRENT_TIMESTAMP
	WHERE
		id = _id;
END