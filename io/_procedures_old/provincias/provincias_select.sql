CREATE  PROCEDURE `provincias_select`(_id int(11))
BEGIN
	SELECT id, nombre, codigo_postal, fecha_alta, fecha_modificacion
    FROM configstream.provincias

    where id = _id  ;
END