CREATE  PROCEDURE `productos_selectOption`()
BEGIN
SELECT
	p.id,
    p.nombre,
    p.descripcion,
    p.idTipoProducto,
    p.idCamara,
    p.idRouter,
    p.idSim,
    p.idEmpresa,
    p.estado,
    p.fecha_alta,
    p.fecha_modificacion,
	c.name AS nombreCamara,
	r.name AS nombreRouter,
	tp.nombre AS nombreTipoProducto,
	tp.tp_gps,
	tp.tp_wifi,
	tp.tp_3G,
	tp.tp_4G,
	s.numSim01 AS nombreSim
FROM
	eude.productos AS p
	INNER JOIN eude.camaras AS c ON (c.idCamaras = p.idCamara)
	INNER JOIN eude.routers AS r ON (r.idrouters = p.idrouter)
	INNER JOIN configstream.tipo_productos AS tp ON (tp.idTipoProducto = p.idTipoProducto)
	INNER JOIN eude.sim AS s ON (s.idSim = p.idSim)
ORDER BY
	p.id;
END