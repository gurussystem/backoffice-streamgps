CREATE   PROCEDURE `productos_asignado_update`(_idEmpresa int(11), _nombre varchar(50) ,  _id int(11))
BEGIN
	/* cambiamos algunos datos de la empresa*/
	UPDATE
		eude.productos
	SET
		idEmpresa = _idEmpresa,
		nombre= _nombre ,
		estado = 1 ,
		fecha_modificacion = CURRENT_TIMESTAMP
	WHERE
		id= _id ;

    -- Añadimos una licencia contratada
    UPDATE eude.empresas SET licencias = licencias+1 WHERE id = _idEmpresa;
END