CREATE   PROCEDURE `productos_update`(_nombre varchar(50), _descripcion text , _idTipoProducto int(11) ,_idCamara int(11) ,_idRouter int(11),_idSim int(11),_estado int(1), _id int(11), _idCamaraAux int(11) ,_idRouterAux int(11), _idSimAux int(11))
BEGIN
	-- Modificamos el producto
	UPDATE
		eude.productos
	SET
		nombre = _nombre,
        descripcion =_descripcion,
        idTipoProducto = _idTipoProducto,
        idCamara = _idCamara,
        idRouter = _idRouter,
        idSim = _idSim,
        estado= _estado,
        fecha_modificacion = CURRENT_TIMESTAMP
	WHERE
		id = _id;

-- Modificamos la camara asignada a 1 y liberamos la camara anterior a 0
	UPDATE eude.camaras SET asignada = 0 WHERE idcamaras = _idCamaraAux;
    UPDATE eude.camaras SET asignada = 1 WHERE idcamaras = _idCamara;

 -- Modificamos la sim asignada a 1 y liberamos la sim anterior a 0
	UPDATE eude.sim SET asignada = 0 WHERE idSim = _idSimAux;
    UPDATE eude.sim SET asignada = 1 WHERE idSim = _idSim;


 -- Modificamos el router asignado a 1 y liberamos el router anterior a 0
	UPDATE eude.routers SET  asignado = 0 WHERE idrouters = _idRouterAux;
    UPDATE eude.routers SET  asignado = 1 WHERE idrouters = _idRouter;


END