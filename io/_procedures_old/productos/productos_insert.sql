CREATE   PROCEDURE `productos_insert`(_nombre varchar(50), _descripcion text , _idTipoProducto int(11) ,_idCamara int(11) ,_idRouter int(11),_idSim int(11) )
BEGIN
	INSERT INTO
		eude.productos(nombre,descripcion,idTipoProducto,idCamara,idRouter,idSim)
	VALUES
		(_nombre,_descripcion,_idTipoProducto,_idCamara,_idRouter,_idSim);

 -- Modificamos la camara asignada
	UPDATE eude.camaras SET asignada = 1 WHERE idcamaras = _idCamara;

 -- Modificamos la sim asignada
	UPDATE eude.sim SET asignada = 1 WHERE idSim = _idSim;

 -- Modificamos el router asignado
	UPDATE eude.routers SET  asignado = 1 WHERE idrouters = _idRouter;
END