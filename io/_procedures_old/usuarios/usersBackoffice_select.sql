CREATE   PROCEDURE `usersBackoffice_select`(_id INT)
BEGIN
	SELECT
		b.uid,
        b.name,
        b.pass,
        b.mail,
        b.rid,
        b.fecha_alta,
        b.fecha_modificacion,
        r.nom as nomRol
	FROM
		eude.backoffice as b
	inner join
		configstream.rol_user as r on (r.id = b.rid)
	where
		b.uid= _id;
END