CREATE  PROCEDURE `usersBackoffice_update`(_nombre varchar(60), _clave varchar(128), _email varchar(320), _idRol INT, _id INT)
BEGIN
	UPDATE
		eude.backoffice
	SET
		name = _nombre,
        pass = _clave,
        mail = _email,
        rid = _idRol,
        fecha_modificacion = CURRENT_TIMESTAMP
	WHERE
		uid = _id;
END