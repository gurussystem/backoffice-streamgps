CREATE   PROCEDURE `pais_select`(_id int(11))
BEGIN
	SELECT
		id, codeCalling, nombre, capital, continente, lat, lng, codeISO2, codeISO3 ,fecha_alta, fecha_modificacion
	FROM
		configstream.paises
	WHERE
		id = _id ;
END