CREATE   PROCEDURE `pais_selectOption`()
BEGIN
	SELECT
		id, codeCalling, nombre, capital, continente, lat, lng, codeISO2, codeISO3 ,fecha_alta, fecha_modificacion
	FROM
		configstream.paises;
END