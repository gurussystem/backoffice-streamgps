CREATE  PROCEDURE `logSesiones_selectOption`()
BEGIN
    SELECT
		id, idUser, nameUser, ip, fecha_inicio, fecha_fin
	FROM
		eude.log_backoffice_sessions
	ORDER BY
        fecha_inicio DESC;
END