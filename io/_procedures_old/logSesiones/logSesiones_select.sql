CREATE   PROCEDURE `logSesiones_select`(_id int(11))
BEGIN
    SELECT
		id, idUser, nameUser, ip, fecha_inicio, fecha_fin
    FROM
		eude.log_backoffice_sessions
	WHERE
		id = _id
	ORDER BY
		fecha_inicio DESC;
END