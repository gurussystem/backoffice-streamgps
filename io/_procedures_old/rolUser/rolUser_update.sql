CREATE PROCEDURE `rolUser_update`( _nom varchar(45), _descrip varchar(200),  _id int(11))
BEGIN
	UPDATE
		configstream.rol_user
    SET
		nom = _nom,
        descrip = _descrip,
        fecha_modificacion = CURRENT_TIMESTAMP
	WHERE
        id = _id;
END