CREATE  PROCEDURE `rolUser_select`(_idRol INT)
BEGIN
	SELECT
		id, nom, descrip, fecha_alta, fecha_modificacion
	FROM
		configstream.rol_user
	WHERE
		id= _idRol;
END