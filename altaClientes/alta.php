<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>StreamGPS</title>
		<link rel="shortcut icon" href="http://streamgps.com/backoffice/favicon.ico">
		<meta name="description" content="description">
		<meta name="author" content="DevOOPS">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta charset="utf-8">
			
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="../js/funcionesGenerales.js"></script>
		
		<style>
		 .form-group {
			margin-left: 70px;
			margin-bottom: 15px;
			float: left;
			width: 100%;
		}
		
		#infoMensajes {
			float: left;
			padding: 15px;
		}
		</style>
				
	</head>
	
	<body>
		
		<div id="contenedor"> 
			<div class="bg-info" style="padding: 5px; ">
				<p class="bg-info" id="sms_info" style="padding-left: 5px; ">
					<span class='fa fa-info' title='Alarma no vista ' style='color:#E95043;'></span>
					Recuerda! 
					<ul>
						<li> Mínimo 10 caracteres. </li>
						<li> No sea igual que el subdominio. </li>
					</ul>
				</p>
			</div>
		
			<legend id="legend_form"> Alta Empresa </legend>					

			<!-- nombre -->
			<div class="form-group">	
				<label class="col-sm-3 control-label" id="nombreEmpresa_lbl">Nombre Empresa: </label>
				<div class="col-sm-5">
					<input type="text" id="nombreEmpresa" class="form-control"  placeholder="Introduce un nombre de empresa" maxlength="40" onfocus="freez(this, 'Introduce un nombre de empresa')" onblur="freez(this, 'Introduce un nombre de empresa')">
					
					<label id="nombreEmpresa_dominio"> <span id="nombreEmpresa_subdominio"></span>.streamgps.com</label>
					<p class="bg-danger" id="nombreEmpresa_error"  style="display:none;">  </p>
				</div>
			</div>
			

									
			<!-- pass  01 -->
			<div class="form-group">	
				<label class="col-sm-3 control-label" id="pass01_lbl">Contraseña: </label>
				<div class="col-sm-5">
					<input type="password" id="pass01" class="form-control" placeholder="Introduce la contraseña" maxlength="20" onfocus="freez(this, 'Introduce la contraseña')" onblur="freez(this, 'Introduce la contraseña')">
					<p class="bg-danger" id="pass01_error"  style="display:none;">  </p>
				</div>
			</div>
				
			<!-- pass  02 -->	
			<div class="form-group">	
				<label class="col-sm-3 control-label" id="pass02_lbl">Repetir Contraseña: </label>
				<div class="col-sm-5">
					<input type="password" id="pass02" class="form-control" placeholder="Introduce la misma contraseña" maxlength="20" onfocus="freez(this, 'Introduce la misma contraseña')" onblur="freez(this, 'Introduce la misma contraseña')">
					<p class="bg-danger" id="pass02_error"  style="display:none;">  </p>
				</div>
			</div>
				
							
			<!-- email -->
			<div class="form-group">	
				<label class="col-sm-3 control-label" id="email_lbl">Email: </label>
				<div class="col-sm-5">
					<input type="text" id="email" class="form-control" placeholder="Introduce un email" maxlength="60" onfocus="freez(this, 'Introduce un email')" onblur="freez(this, 'Introduce un email')">
					<p class="bg-danger" id="email_error"  style="display:none;">  </p>
				</div>
			</div>
				
			<!-- telefono movil 				
			<div class="form-group">	
				<label class="col-sm-3 control-label" id="telefono_lbl">Teléfono: </label>
				<div class="col-sm-5">
					<input type="text" id="telefono" class="form-control"  placeholder="Introduce un número de contacto" maxlength="9" onkeypress="return soloNumeros(event);"  onfocus="freez(this, 'Introduce un número de contacto')" onblur="freez(this, 'Introduce un número de contacto')">
					<p class="bg-danger" id="telefono_error"  style="display:none;">  </p>
				</div>
			</div>
			-->		

			<!--  Opcion con videos o sin videos -->
			
			<div class="form-group">	
				<label class="col-sm-3 control-label" id="tipoPlataforma_lbl"> Tipo de plataforma: </label>
				<div class="col-sm-5">
					<select id="tipoPlataforma" class="form-control">
						<option value="0" selected> Selecciona un tipo de plataforma </option>
						<option value="1"> Plataforma con visores y geolocalización </option>
						<option value="2"> Plataforma solo con geolocalización </option>						
					</select>
					<p class="bg-danger" id="tipoPlataforma_error"  style="display:none;">  </p>
				</div>
			</div>


			<!-- 	Botones	-->
			<div class="form-group">
				<div class="col-sm-9 col-sm-offset-3">
					<button type="submit" id="submit" class="btn btn-primary" onclick="validar_datos()"> Alta </button>
					<button type="reset" id="reset" class="btn btn-primary" onclick="borrar_datos_form()"> Borrar </button>
					<div id="guardandoDatos" class="cargarDatos txtLeft">  <img src="../img/ajaxLoader/ajaxLoaderBig.gif" class="imgLoaderBig"/> </div> 
				</div>
			</div>
		</div>
		
		<!-- mensaje  informativo -->
		<div class="form-group">
			<div id="infoMensajes"  style="display:none;" class="col-sm-10">  </div>			
		</div>
		
		
<script> 
	var streamgpsEmail = "<a href='mailto:info@streamgps.com' > info@streamgps.com </a> ";
	var streamgpsTelefono = "+ 34 93 018 52 14";
				
	/*************************************************
	* Primera carga
	*************************************************/
	$(document).ready(function() {	
		$("#infoMensajes").hide();	
		limpiarForm();
	});
	
	
	/*************************************************
	* Mostramos / escondemos los botones y la img de reload
	*************************************************/
	function reloadImg(opcion){ 
		if(opcion=='desactivar'){
			$("#guardandoDatos").hide();
			$("#submit").show();
			$("#reset").show();					
		}else{
			$("#guardandoDatos").show();
			$("#submit").hide();
			$("#reset").hide();							
		}			
	}
	
	/*****************************************************************
	* Añadimos el nombre en nombreEmpresa_subdominio para que se vea como quedará
	*****************************************************************/
	window.onload = function() {
		document.onkeyup = muestraInformacion;
		document.onkeypress = muestraInformacion;
		document.onkeydown = muestraInformacion;
	}
	
	
	/*****************************************************************
	* 
	*****************************************************************/			
	function muestraInformacion(elEvento) {
		var evento = window.event || elEvento;
		/*
		console.log( "Tipo de evento: " + evento.type);
		console.log( "Propiedad keyCode: " + evento.keyCode);
		console.log( "Propiedad charCode: " + evento.charCode  );
		console.log( "Carácter pulsado: " + String.fromCharCode(evento.charCode) );
		*/	
		
		//  Si no hemos pulsado espacio 
		if ( (evento.type == 'keypress') && (( evento.keyCode ==  13) && ( evento.charCode == 0))){
			validar_datos();
		}else{
			$("#nombreEmpresa_subdominio").html($("#nombreEmpresa").val());		
		}
	}
		
		
	/******************************************
	* Limpiamos todos los campos que hay en el formulario
	*******************************************/
	function limpiarForm(){								
		$("#nombreEmpresa").val('');
		$("#nombreEmpresa_subdominio").empty();
		$("#pass01").val('');				
		$("#pass02").val('');
		$("#email").val('');
		$("#tipoPlataforma").val(0);
		//$("#telefono").val('');
		$("#nombreEmpresa_error").css("display","none");			
		$("#pass01_error").css("display","none");			
		$("#pass02_error").css("display","none");			
		$("#email_error").css("display","none");			
		$("#tipoPlataforma_error").css("display","none");			
		//	$("#telefono_error").css("display","none");		
		// img y botones
		reloadImg('desactivar'); //desactivamos el reload
	}
	
	
	/****************************************************		
	* Recogemos datos y los comprobamos que el formato sea correcto
	* 
	* Comprobar contraseñas:
	* 1. Que no esten vacias
	* 2. Minimo 10 caracteres
	* 3. Diferente del nombreEmpresa / subdominio
	*
	****************************************************/
	var contador_validaciones;
	function validar_datos(){
		reloadImg('activar'); // activamos el reload
		contador_validaciones = 0;
		
		var nombreEmpresa = campoVacio("nombreEmpresa", "text");					
		var pass01 = campoVacio("pass01", "text");
		var pass02 = campoVacio("pass02", "text");
		var email = campoVacio("email", "email");
		var tipoPlataforma = campoVacio("tipoPlataforma", "text");
		 console.log("validar datos - tipoPlataforma: " + tipoPlataforma) ;
		// var telefono = campoVacio("telefono", "numeric");
	
		// Comprobamos que ha seleccionado un tipo de plataforma
		if(tipoPlataforma == 0 ){
			$("#tipoPlataforma_error").css("display","block");
			$("#tipoPlataforma_error").html("Selecciona un tipo de plataforma.");
		}else{
			contador_validaciones ++;
		}
		
		// Comprobar tamaño de la contraseña
		if(pass01){
			if(pass01.length >= 10){
				// console.log("mayor");
				
				if(nombreEmpresa == pass01){
					$("#pass01_error").css("display","block");
					$("#pass01_error").html("La clave tiene que ser diferente al 'Nombre Empresa'");
					contador_validaciones --;
				}else{
					contador_validaciones ++;
				}
				
			}else{
				// console.log("menos de 10 caracteres : " + pass01.length );
				$("#pass01_error").css("display","block");
				$("#pass01_error").html("Mínimo 10 caracteres.");
				contador_validaciones --;
			}
		}			
		
		
	
		// Comprobar si las dos contraseñas son iguales	
		if(pass01 != pass02){
			$("#pass02_error").css("display","block");
			$("#pass02_error").html("Las contraseñas son diferentes");
			contador_validaciones --;
		}else{
			$("#pass02_error").css("display","none");
		}
				
	
		// Comprobamos el nombre unico
		nombreUnico(nombreEmpresa, 'nombreEmpresa', pass01, email, tipoPlataforma);
	}

	
	/******************************************
	* Validamos los inputs vacios
	*******************************************/	
	function campoVacio(nomDiv, opcion){
		var nom = $("#"+nomDiv).val();
		contador_validaciones ++; // por defecto lo sumo 
	
		if(nom){
			$("#"+nomDiv+"_error").css("display","none");					
				
			switch(opcion){
				case 'text': 
					return  getCleanedString(nom);  								
					break;
						
				case 'email':
					return validarEmail(nom, nomDiv);					
					break;
							
				default: 
					return nom; 
					break;
				
			} // fin switch
					
		}else{	
			contador_validaciones --; // Si esta vacio lo restpo de la suma previa
			$("#"+nomDiv+"_error").css("display","block");
			$("#"+nomDiv+"_error").html("Campo Obligatorio");
		}				
	} // fin funcion 
			
	
	/***********************************************
	* Comprobamos que el nombreEmpresa no exista en la bd
	* Si no existe llamamos a 'altaCliente()' sino mostramos error
	***********************************************/			
	// function nombreUnico(nombreEmpresa, nomDiv, pass01, email, telefono){				
	function nombreUnico(nombreEmpresa, nomDiv, pass01, email, tipoPlataforma){				
		// console.log("nombreUnico("+nombreEmpresa +","+ nomDiv +","+ pass01 +"," + email +","+tipoPlataforma)");	
		
		$.ajax({
			async:true, 
			cache:false,
			type: 'GET',
			data: { 
				'nom' : nombreEmpresa			
			},
			url:  'io/subdominioUnico.php',  // falta hacer la consulta
			success: function(response){
				// console.log("response nombreUnico: " + response);
				if(response == 1){
					contador_validaciones --;
					$("#"+nomDiv+"_error").css("display","block");
					$("#"+nomDiv+"_error").html("El nombre de empresa ya esta dado de alta.");	
					reloadImg('desactivar'); //desactivamos el reload					
				}else{
					// console.log("contador_validaciones: " + contador_validaciones);
					if(contador_validaciones == 7){
						altaCliente(nombreEmpresa, pass01, email , tipoPlataforma);
					}else{
						reloadImg('desactivar'); //desactivamos el reload
					}
				}
				
			} // fin success
		}); // fin ajax
	} // fin funcion
			
			
	/***********************************************
	* Damos de alta al cliente - empresa
	***********************************************/			
	var respuestaAlta = new Array();
	function altaCliente(nombreEmpresa, pass, email, tipoPlataforma){
		//alert(latitud +','+ +longitud);
		// console.log("altaCliente: " + nombreEmpresa  + "  , " + pass + "  , " + email+ "  , " + tipoPlataforma);
			
		$.ajax({
			async:true, 
			cache:false,
			// type: 'GET',
			type: 'POST',
			data: { 
				'nameCompany' : nombreEmpresa,
				'password' : pass,
				'email' :email,
				'tiempoDesfase' : desfaseHoraria(),  	
				'tipoPlataforma' : tipoPlataforma			
			},
			
			url:  'https://api7.streamgps.com/rest/v1_marisa/register',  // falta hacer la consulta
			success: function(response){
				respuestaAlta = response;				
				$("#infoMensajes").show();			
			
				if(respuestaAlta.datos[0].response == 1){
					
					$("#infoMensajes").removeClass("bg-danger").addClass("bg-success");					
					var link_pruebas = "https://"+nombreEmpresa+".streamgps.com:444";
					var link = "https://"+nombreEmpresa+".streamgps.com";
					var mensajito ="Se ha dado de alta correctamente. <br> A continuación le enviaremos un email con los pasos a seguir. <br> Cualquier duda pongase en contacto con nosotros en "+streamgpsEmail+". <br>";
					// mensajito += " Para entrar a la plataforma <a href='"+link_pruebas+"' > click aquí </a>";					
					limpiarForm();		
					
					// Enviamos un email  de confirmación de que se ha modificado alguna cosa del backoffice
					enviar_email_confirmacion(nombreEmpresa, email, tipoPlataforma);	// enviamos email al cliente	
					enviar_email_confirmacion_streamgps(nombreEmpresa, email, tipoPlataforma);	// enviamos email al cliente	
					
				}else{
					$("#infoMensajes").removeClass("bg-success").addClass("bg-danger");				
					var mensajito = "Lo sentimos pero ha sucedido un error, ponte en contacto con nosotros en "+streamgpsEmail+"  o llamando al "+streamgpsTelefono+ " y te ayudaremos.";
				}
				
				$("#infoMensajes").html(mensajito);
				
			} // fin success
		});
	}
				
		
		
	/************************************************************************************
	* activar cuando pase a produccion
	************************************************************************************/		
	function localize(){	
		if (navigator.geolocation){
			navigator.geolocation.getCurrentPosition(mapa,error);
		}
		else{
			alert('Tu navegador no soporta geolocalizacion.');
		}
	}

 
	var latitud, longitud, precision;
	function mapa(pos){
		latitud = pos.coords.latitude;
		longitud = pos.coords.longitude;
		precision = pos.coords.accuracy;
	}

 

	function error(errorCode){
		if(errorCode.code == 1)
			alert("No has permitido buscar tu localizacion")
		else if (errorCode.code==2)
			alert("Posicion no disponible")
		else
			alert("Ha ocurrido un error")
	}
	
	
	function escondite_sms(){
		$("#infoMensajes").hide();
	}
	
	
	// hace que los inputs escondan o muestren el placeholder
	// <input name="Name2" value="Enter Your Name" onfocus="freez(this, 'Enter Your Name')" onblur="freez(this, 'Enter Your Name')">
	function freez(obj, valor){
		// console.log(obj);
		if(obj.value==''){
			obj.placeholder=valor;
			// obj.value=valor;
		}
		else if(obj.value == valor){
			obj.value='';
		}
  
		$("#infoMensajes").hide();
	}
	

	function enviar_email_confirmacion(nombreEmpresa, email, tipoPlataforma){
			
		$.ajax({
			type: 'GET',
			url: 'io/enviarEmail.php',
			data: { 
					'nombreEmpresa': nombreEmpresa,
					'email': email, 
					'tipoPlataforma': tipoPlataforma
			},
			success: function(response) {		
				
			}
		});	
	}
	
	function enviar_email_confirmacion_streamgps(nombreEmpresa, email, tipoPlataforma){
			
		$.ajax({
			type: 'GET',
			url: 'io/enviarEmail_info.php',
			data: { 
					'nombreEmpresa': nombreEmpresa,
					'email': email, 
					'tipoPlataforma': tipoPlataforma
			},
			success: function(response) {		
				
			}
		});	
	}
	
	
</script> 
		
	</body>
</html>
