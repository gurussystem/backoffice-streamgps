<?php 
	require_once("Mandrill.php");
	$mandrill = new Mandrill("pI7YSRggNJxpMihxy3s-ZA");
	
	# Recogemos datos pasados por parametro
	if($_POST['comprobar']){
		$datosArray = json_decode($_POST['datosArray'],true);
		$arrayLabels = json_decode($_POST['arrayLabels'],true);
	}else{
		$datosArray = json_decode($_GET['datosArray'],true);
		$arrayLabels = json_decode($_GET['arrayLabels'],true);
	}

	# echo "<br> POST: <pre>" ; print_r($_POST); echo "</pre>" ;  echo "<br> GET: <pre>" ; 	print_r($_GET);  echo "</pre>" ;
	# echo "<br> array: <pre>" ;   print_r($datosArray );echo "</pre>" ;
	# echo "<br> array: <pre>" ;   print_r($arrayLabels );echo "</pre>" ;
	

	/********************************************************************************
	 #
	 # Obtenemos el numero de la cuenta bancaria, segun el tipo de cuenta que han. (ccc, iban, swift)
	 #
	*****************************************************************************/
	$numCta = "";
	if(($datosArray[0]['iban'] != "" ) || ($datosArray[0]['iban'] != null) ){
		$numCta =$datosArray[0]['iban'];
	}else if(($datosArray[0]['swift'] != "" ) || ($datosArray[0]['swift'] != null) ){
		$numCta = $datosArray[0]['swift'];
	}else {
		$numCta =	$datosArray[0]['entidad'].'-'.$datosArray[0]['sucursal'].'-'.$datosArray[0]['dc'].'-'.$datosArray[0]['numCuenta'];	
	}
	# echo '<br> numCta:  ' . $numCta ;
	
	/********************************************************************************
	 #
	 # Datos introducidos en el formulario
	 #
	*****************************************************************************/
	$datosFormulario = "";
	$datosFormulario.='<h4> Los datos intoducidos en el formulario son los siguientes:  </h4> <br />';

	$datosFormulario.='<b> Dominio :  </b> '.$datosArray[0]['domini'].      ' (en caso de no verse como link, copie y pegue en el navegador) <br />';
	$datosFormulario.='<b> Ip/ PuertoServer :  </b> '.$datosArray[0]['ipServer'].  '<br />';
	$datosFormulario.='<b> Email:     </b> '.$datosArray[0]['email'].		'<br />';
	$datosFormulario.='<b> Password:  </b> '.$datosArray[0]['pass'] .'<br />';	
	$datosFormulario.='<b> Tlf fijo:  </b> '.$datosArray[0]['telefono'].			'<br />';
	$datosFormulario.='<br/> <br/>';
	
	# echo "<br>txtmail_datosEmpresa: " .$txtmail_datosEmpresa;
	
	
	/********************************************************************************
	 #
	 # Firma del email
	 #
	*****************************************************************************/
	$mail_firma ="";
	$mail_firma .= '<table width="100%" border="0" cellspacing="10" cellpadding="0" style="border-top-width:3px; border-top-style:solid; border-top-color:#a2c3e3;">';
		$mail_firma .= '<tr>';
			$mail_firma .= '<td>&nbsp;</td> ';
			$mail_firma .= '<td></td>';
		$mail_firma .= '</tr>';
			
		$mail_firma .= '<tr>';
			$mail_firma .= '<td width="200" align="right" valign="top">';
				$mail_firma .= '<img src="http://streamgps.com/wp-content/uploads/2015/02/logo2_streamgps.png" alt="StreamGPS logo" hspace="5"  style="padding-top: 8px;"/>';
			$mail_firma .= '</td>  ';
				
			$mail_firma .= '<td align="left" valign="top" style="padding:0px;">';
				$mail_firma .= '<p>';
					$mail_firma .= '<font face="Arial, Helvetica, sans-serif" color="#05233d"  style="font-size:14px;">StreamGPS.com</font>';
					$mail_firma .= '<br />';    
					$mail_firma .= '<font face="Arial, Helvetica, sans-serif" color="#416886"  style="font-size:12px;">C/ Llacuna, 162 <br/> 08012 - Barcelona (España)</font>';
					$mail_firma .= '<br />';
					$mail_firma .= '<font face="Arial, Helvetica, sans-serif" color="#416886" size="-1">Tlf: <strong>';
					$mail_firma .= '<font color="#1b4260">+34 93 018 52 14</font></strong> ';
					$mail_firma .= '<br />	';
					$mail_firma .= '<font face="Arial, Helvetica, sans-serif" color="#1b4260" size="-1">';
						$mail_firma .= '<a style="color: #09C;text-decoration: none;" href="mailto:info@streamgps.com">info@streamgps.com</a> ';
						$mail_firma .= '<br /> ';
						$mail_firma .= '<a style="color: #09C;text-decoration: none;" href="http://www.streamgps.com">www.streamgps.com</a>';
					$mail_firma .= '</font>';
				$mail_firma .= '</p>';
			$mail_firma .= '</td>';
		$mail_firma .= '</tr>';
	$mail_firma .= '</table>';
	# echo "<br><br><br>mail_firma: " .$mail_firma;
	
	/********************************************************************************
	 #
	 # Texto para el cliente
	 #
	*****************************************************************************/	
	//Email para el cliente/empresa
	$usuario = $datosArray[0]['nombreEmpresa']; 
	$enlace = 'http://'.$usuario.'.streamgps.com';  
	$guia_rapida = "http://StreamGPS.com/manualUso.pdf";
	$txtmailCliente = "";
	$txtmailCliente .= "Estimado cliente, ".$usuario;
	$txtmailCliente .= "<br/><br/>";
	$txtmailCliente .= "El equipo de StreamGPS.com le da la bienvenida y le agradece su interés.";
	$txtmailCliente .= "<br/><br/>";
	$txtmailCliente .= "A continuación le indicamos los pasos a seguir para poder entrar en su plataforma:";
	$txtmailCliente .= "<ol>";
		$txtmailCliente .= "<li>Entre en el siguiente enlace: ".$enlace." </li>";
		$txtmailCliente .= "<li>Introduzca sus datos de acceso:";
					$txtmailCliente .= "<ul>";
					$txtmailCliente .= "<li> Usuario: ". $usuario." </li>";
					$txtmailCliente .= "<li> Clave: ". $datosArray[0]['pass']." </li>";
					$txtmailCliente .= "</ul>";
		$txtmailCliente .= "</li>";
		$txtmailCliente .= "<li> Puede echar vistazo a nuestra <a href=".$guia_rapida."> GUÍA RÁPIDA </a> donde le explicamos de forma muy sencilla como funciona la plataforma StreamGPS.com </li>";
	$txtmailCliente .= "</ol>";
		$txtmailCliente .= "<br/>";
	$txtmailCliente .= "De todas formas, si usted tiene alguna duda puede ponerse en contacto con nosotros vía email info@streamgps.com o bien, marcando el numero de teléfono 93 172 78 93.";
	$txtmailCliente .= "<br/><br/>";
	$txtmailCliente .= "Saludos cordiales,";
	$txtmailCliente .= "El equipo de StreamGPS.com";
	$txtmailCliente .= "<br><br>".$mail_firma;
	
	#  echo "<br>******************************************************************************************** " ;
	#  echo "***************************** TEXTO PARA EL CLIENTE ****************************************** " ;
	#  echo "******************************************************************************************** <br> " ;
	# echo  $txtmailCliente;
	
	
	/********************************************************************************
	 #
	 # Controlamos el estado de la consulta
	 #
	*****************************************************************************/		
	$nombreBD  = $datosArray[0]['proceso']; 
	if( $datosArray[0]['proceso'] == "ok"){ //Caso que ha ido todo bien.
		$txtProceso ='[OK]';
		$txtmail01= '<h1> Se ha creado correctamente la nueva BD '. $nombreBD .'</h1>'; 
	}else{ //Caso en el que ha fallado algo.
		$txtProceso =' [ERROR]';
		$txtmail01.= '<h1> Se ha producido algún error, al crear la nueva  BD '. $nombreBD .'</h1>';		
	}
	$txtmail_final = $txtmail01 ." <br/> ". $datosFormulario . "<br> <br> <br> " .  $datosArray[0]['txtMail_create'];
	
	# echo "<br>******************************************************************************************** " ;
	# echo "***************************** TEXTO PARA EL ADMIN  ****************************************** " ;
	# echo "******************************************************************************************** <br> " ;
	# echo $txtmail_final;
	
	
	# Sacamos un listado de todos los emails (tabla backoffice) a los que le queremos enviar el email 
	require_once("../funciones.php");
	conectar_bbdd('eude');
	include_once("../conexion/dbi_connect.php");
	$array_mails = array(); #Declaramos email
	
	$stmt = $mysqli->prepare("SELECT mail FROM backoffice");		
	$stmt->execute();
	$result = $stmt->get_result(); 
	while ($res=$result->fetch_assoc()){
		$array_mails[] = array(
			"email"=>$res["mail"]
		);
	}
	# echo "array_mails <pre>";print_r($array_mails);echo "</pre>";
	
	/********************************************************************************
	 #
	 # Enviamos mail (nuevo) 
	 #
	*****************************************************************************/		
	if($_SERVER['SERVER_NAME'] !=  'streamgps.backoffice.com') {
		# Enviamos email al/los administrador/es	
		try {	
			$message = new stdClass();
			$message->html = $txtmail_final; //Cuerpo del email
			$message->subject = "[".$txtProceso."] Nueva alta ".$usuario;
			$message->from_email = "info@streamgps.com"; //de quien
			$message->from_name  = "Alta cliente StreamGPS";
			$message->to = $array_mails; //para quien
			$message->track_opens = true;
			$mandrill->messages->send($message);
		}catch(Mandrill_Error $e) { //En caso de error nos mostrará un mensaje
			echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
			throw $e;
		} 

		# Enviamos mail al cliente
		try {
			$message = new stdClass();
			$message->html = $txtmailCliente; //Cuerpo del email
			$message->subject = "Bienvenido ".$usuario." a StreamGPS.com ";
			$message->from_email = "info@streamgps.com"; //de quien
			$message->from_name  = "Alta cliente StreamGPS";
			$message->to = array(array("email" => $datosArray[0]['email'] )); //para quien
			$message->track_opens = true;
			$mandrill->messages->send($message);		
		}catch(Mandrill_Error $e) { //En caso de error nos mostrará un mensaje
			echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
			throw $e;
		} 
		
	}else{	
			echo "Email Cliente " . $txtmailCliente;
			echo " *********************************************************************** ";
			echo " *********************************************************************** ";
			echo " *********************************************************************** ";
			echo " *********************************************************************** ";
			echo "<br> Email Administrador " . $txtmail_final;
	}
?>