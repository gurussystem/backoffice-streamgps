<?php
	require_once("../funciones.php");
	conectar_bbdd('eude');
	include_once("../conexion/dbi_connect.php");
	$data = array();
	$txt_createBD = ""; 
	 
	# $mysqli->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);
	$usuario = limpiarVariable(comprobarParametros('nombreEmpresa')); //dominio de la empresa
	$pass = comprobarParametros('pass');
	$email = comprobarParametros('email');
	$telefono = comprobarParametros('telefono');
	$tiempoDesfase = comprobarParametros('tiempoDesfase');
	
	# Comprobamos que el 'nombreEmpresa'  sea como maximo 40 caracteres
	if(strlen($usuario) > 40){
		echo json_encode("Error, el nombre es demasiado largo, máximo 40 caracteres."); 	
		die();
	}
	
	# Comprobamos que el 'pass'  sea como maximo 20 caracteres
	if(strlen($pass) > 20){
		echo json_encode("Error, la clave es demasiado larga, máximo 20 caracteres."); 	
		die();
	}
	
	# Comprobamos que el 'email'  sea como maximo 60 caracteres
	if(strlen($email) > 60){
		echo json_encode("Error, el email es demasiado largo, máximo 60 caracteres."); 	
		die();
	}	
	
	# Comprobamos que el 'telefono'  sea como maximo 9 caracteres
	if(strlen($telefono) > 9){
		echo json_encode("Error, el teléfono es demasiado largo, máximo 9 digitos."); 	
		die();
	}
	
	# Comprobamos que el 'telefono'  es numerico
	if(!is_numeric($telefono)){
		echo json_encode("Error, el teléfono debe ser numerico compuesto por 9 digitos"); 	
		die();
	}


	# comprobamos que es un email 
	// echo "<br> ($email) - cEmail:  " .  verificar_email($email);
	if(verificar_email($email) != 1){
		echo json_encode("Error, el email es incorrecto."); 	
		die();
	}

	

	// campos que necesitamos
	# Consultamos el ultimo id de empresa insertado para añadir el nuevo
	$stmt = $mysqli->prepare("SELECT id FROM eude.empresas order by id desc limit 1");			
	$stmt->execute();
	$stmt->bind_result($id);
	$stmt->fetch();
	$idUsuario = $id + 1;
	// $idUsuario = idUsuario();
	// echo "<br>  idUsuario: " . $idUsuario;	
	$stmt->close();
	
	
	$nombreBD = $usuario.'_'.$idUsuario;
	// $newId_cta = ultimo_idCta();
	$ipServer = '54.229.60.25';
	$puertoServer = '1935';
	$newServer = $ipServer.":".$puertoServer;
	$subdomini = $usuario;
	$domini = "http://".$subdomini.".streamgps.com";
	$streamapp ='live';
	$aplicacion=1;
	$licencias = 0;
	$activo = 1;
	$recordapp = "vod";
	$txtMail_create ="";
	
	$prefix = substr($usuario,0,3);	
	$prefixstream = $prefix.$idUsuario;	
	
	$lat=0;
	$lng=0;
	$bdn = 'sgclients';
	$connapp='jdbc:mysql://'.$bdn.'.cisjdxpoil63.eu-west-1.rds.amazonaws.com:3306/'.$nombreBD;
	
	

	
	# Funcion para validar si esta bien o no la creación de la bd
	function validar_tabla($sqlValuar, $nombreTabla, $opcion){
		
		switch($opcion){
			case 'create':
				$txt_error = '[ERROR],	al crear la tabla '. $nombreTabla .' <br/>';
				$txt_ok = '[OK], la tabla '.$nombreTabla.' se ha creado correctamente. <br/>';
				break;
		
			case 'insert':
				$txt_error = '[ERROR],	al insertar los datos en la tabla '. $nombreTabla .', actualmente estará vacía. <br/>';
				$txt_ok = '[OK], se han insertado correctamente los datosn en la tabla '.$nombreTabla.'. <br/>';
				break;
				
			case 'trigger':
				$txt_error = '[ERROR], al crear el trigger '. $nombreTabla .' <br/>';
				$txt_ok = '[OK],el trigger '. $nombreTabla .' se creo correctamente<br/>' ;
				break;
			
			case 'update':
				$txt_error = '[ERROR], al crear modificar la tabla '. $nombreTabla .' <br/>';
				$txt_ok = '[OK], los datos de la tabla '. $nombreTabla .' se actualizarón correctamente<br/>' ;
				break;
				
			case 'vista':
				$txt_error = '[ERROR], al crear la vista '. $nombreTabla .' <br/>';
				$txt_ok = '[OK], la vista '. $nombreTabla .' se ha creado  correctamente<br/>' ;
				break;
			
			case 'insert_eude':
				$txt_error = '[ERROR], al dar de alta a la nueva empresa en EUDE.<br/> Comprueba que la empresa no exista ya en la Base de datos.';
				$txt_ok = '[OK], Se ha creado la nueva empresa en EUDE correctament<br/>' ;
				break;
				
			case 'cta_empresas':
				$txt_error = '[ERROR], al insertar en la tabla cta.empresas';
				$txt_ok = '[OK], Se ha insertado correctamente<br/>' ;
				break;
				
			case 'procedure':
				$txt_error = '[ERROR], al crear el procedure '. $nombreTabla .' <br/>';
				$txt_ok = '[OK], el procedure '. $nombreTabla .' se ha creado  correctamente<br/>' ;
				break;	
		}
	
		if(!$sqlValuar){
			$bbddok=0;
			$txtMail_create = $txt_error;
		}else{
			$txtMail_create = $txt_ok ;
			
		}
	
		
		if (!$sqlValuar) {
			die('<br> Consulta no válida: ' . $mysqli->error);
		}
		
			# echo "<br> txtMail_create: " . $txtMail_create;
	}		
	
	
	
	# Guardamos los datos en eude.empresas
	$consulta = "INSERT INTO eude.empresas(
						id,
						nombre,
						pass,
						email,
						telefono1,
						activo,
						bbdd,
						prefixstream,
						server,
						streamapp,
						domini,
						conn,
						aplicacion,
						subdomini,
						licencias,									
						lat,
						lng						
						)	VALUES (	?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"; // 17 campos
	$stmt = $mysqli->prepare($consulta);	
	$stmt->bind_param('isssiissssssisiss', $idUsuario,$usuario,$pass, $email,$telefono, $activo, $nombreBD, $prefixstream, $newServer,$streamapp, $domini, $connapp, $aplicacion,  $subdomini, $licencias, $lat, $lng );
	$stmt->execute();
	
	
	if($stmt->errno){
		$response=0;
		$proceso = 'error';
	}else{
		$response=1;
		$proceso = 'ok';
	}
		$stmt->close(); 


	#Crear la nueva BD con la clase mysqli
	$resultado = mysqli_query($mysqli,  "CREATE DATABASE $nombreBD");
	
	if ($resultado){
		# echo "Nueva base de datos creada correctamente";
		require("crearNewDB.php");
		#  if($bbddok!=0){	$txt_createBD = 'Base de datos '. $nombreBD . 'se ha creado correctamente <br/>';	}
	}else{
		$bbddok=0;	
		$txt_createBD = mysqli_error($mysqli);
	}


	//Montamos el array 
	$data[] = array(
		"id"=>$idUsuario, 
		"nombreEmpresa"=>$usuario, 
		"pass"=>$pass, 
		"email"=>$email, 
		"telefono"=>$telefono, 
		"estado"=>$activo, 
		"nombreBD"=>$nombreBD, 
		"prefixstream"=>$prefixstream, 
		"ipServer"=>$newServer, 
		"streamapp"=>$streamapp, 
		"domini"=>$domini, 
		"connapp"=>$connapp, 
		"aplicacion"=>$aplicacion, 
		"subdomini"=>$subdomini, 
		"licencias"=>$licencias, 
		"lat"=>$lat, 
		"lng"=>$lng, 	
		"response" => $response,
		"proceso" => $proceso,
		"accion"=> "insert",
		"tabla"=> "empresa",
		"nomFichero"=> "insert_cliente.php",
		"txtMail_create"=> $txtMail_create,			
		"txt_createBD"=> $txt_createBD,			
	);
	
	
	echo json_encode($data); 	
	// echo "<pre>";	print_r($data); echo "</pre>";
	$mysqli->close();
?> 