<?php
	# Abrimos la conexion con la bd seleccionada
	# echo "<br> DB_SERVER: " .  DB_SERVER;
	# echo "<br> DB_USER: " .  DB_USER;
	# echo "<br> DB_PASSWORD: " .  DB_PASSWORD;
	$mysqli2=new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, $nombreBD);		

	
	/********************************************************************************
	* Creamos la acceso_temporal_cam
	********************************************************************************/
	$tabla_acceso_temporal_cam="CREATE TABLE IF NOT EXISTS ".$nombreBD.".`acceso_temporal_cam` (
													  `id` int(11) NOT NULL AUTO_INCREMENT,
													  `idProducto` int(11) DEFAULT NULL,
													  `token` varchar(245) DEFAULT NULL,
													  `email` varchar(245) DEFAULT NULL,
													  `fechaCreacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
													  `activo` int(11) DEFAULT '1',
													  PRIMARY KEY (`id`),
													  KEY `index_acceso_temporal_cam` (`token`,`activo`)
													) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	
	$resultado_acceso_temporal_cam = mysqli_query($mysqli2, $tabla_acceso_temporal_cam);
	# Comprobamos el proceso de creacion de la tabla
	validar_tabla($resultado_acceso_temporal_cam, 'acceso_temporal_cam', 'create'); 
	
	/********************************************************************************
	* Creamos la alarma_movimiento
	********************************************************************************/
	$tabla_alarma_movimiento="CREATE TABLE IF NOT EXISTS ".$nombreBD.".`alarma_movimiento` (
												  `id` int(11) NOT NULL AUTO_INCREMENT,
												  `idCam` int(11) DEFAULT NULL,
												  `img` varchar(245) DEFAULT NULL,
												  `time` varchar(245) DEFAULT NULL,
												  `idMail` varchar(245) DEFAULT NULL,
												  `visto` int(1) DEFAULT '0' COMMENT '0 no se ha visto la alerta\n1 se ha visto la alerta ',
												  `eliminado` int(1) DEFAULT '0' COMMENT '0 no se ha eliminado la alerta\n1 se ha eliminado la alerta ',
												  PRIMARY KEY (`id`),
												  KEY `index_alarma_movimiento` (`id`,`time`,`visto`,`eliminado`)
						) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8";
	
	$resultado_alarma_movimiento = mysqli_query($mysqli2, $tabla_alarma_movimiento);
	# Comprobamos el proceso de creacion de la tabla
	validar_tabla($resultado_alarma_movimiento, 'alarma_movimiento', 'create'); 

	/********************************************************************************
	* Creamos la tabla avisos
	********************************************************************************/
	$tabla_avisos="CREATE TABLE IF NOT EXISTS ".$nombreBD.".`avisos` (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `idUser` int(11) NOT NULL,
				  `idZona_old` int(11) NOT NULL,
				  `idZona_new` int(11) NOT NULL,
				  `idTipoAviso` int(10) DEFAULT NULL,
				  `fecha_modificacion` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
				  `time_ini` datetime NOT NULL,
				  `time_end` datetime NOT NULL,
				  `estado` int(11) NOT NULL,
				  `aviso` tinyint(4) NOT NULL,
				  `cerrado` tinyint(4) NOT NULL,
				  PRIMARY KEY (`id`)
		) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8";
	
	$resultado_avisos = mysqli_query($mysqli2, $tabla_avisos);
	# Comprobamos el proceso de creacion de la tabla
	validar_tabla($resultado_avisos, 'avisos', 'create'); 
	
	
	/********************************************************************************
	* Creamos la tabla empleados
	********************************************************************************/
	$tabla_empleados="CREATE TABLE IF NOT EXISTS ".$nombreBD.".`empleados` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`tipoRol` int(2) DEFAULT NULL,
			`nombre` varchar(45) DEFAULT NULL,
			`pass` varchar(45) DEFAULT NULL,
			`telefono` varchar(15) DEFAULT NULL,
			`email` varchar(45) DEFAULT NULL,
			`fecha_alta` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
			`fecha_modificacion` timestamp NULL DEFAULT NULL,
			`eliminado` int(1) NOT NULL DEFAULT '0' COMMENT '0 --> El empleado NO esta eliminado\n1 --> El empleado esta eliminado',
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8";
	
	$resultado_empleados = mysqli_query($mysqli2, $tabla_empleados);	
	# Comprobamos el proceso de creacion de la tabla
	validar_tabla($resultado_empleados, 'empleados', 'create'); 
	
	/********************************************************************************
	* Creamos la tabla grabacion
	********************************************************************************/
	$tabla_grabacions="CREATE TABLE IF NOT EXISTS ".$nombreBD.".grabacions(
									  `id` int(11) NOT NULL AUTO_INCREMENT,
									  `idProducto` int(10) unsigned NOT NULL,
									  `horaini` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
									  `horafin` timestamp NULL DEFAULT NULL,
									  `duracion` int(11) DEFAULT NULL,
									  `archivo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
									  `play` int(11) DEFAULT NULL,
									  `delet` int(11) DEFAULT NULL,
									  `eliminado` int(11) NOT NULL DEFAULT '0',
									  `datedelete` timestamp NULL DEFAULT NULL,
									  `size_bytes` double DEFAULT NULL,
									  `num_segment` int(10) unsigned DEFAULT NULL,
									  `tieneGPS` int(1) DEFAULT NULL,
									  `tieneAlerta` int(1) DEFAULT NULL COMMENT '1 -> tiene alertas\n0 -> No tiene alertas',
									   `descargado_estado` int(1) DEFAULT '0',
										`descargado_usuario` varchar(245) DEFAULT 'null',
										`descargado_fecha` timestamp NULL DEFAULT NULL,
									   PRIMARY KEY (`id`,`idProducto`),
									  KEY `index_grabacions` (`id`,`idProducto`,`horaini`,`eliminado`)
					) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ";
					
				//	echo "<br>".$tabla_grabacions."<br>";
	$resultado_grabacions = mysqli_query($mysqli2, $tabla_grabacions);	
	# Comprobamos el proceso de creacion de la tabla
	validar_tabla($resultado_grabacions, 'grabacions', 'create'); 	
	
	
	# add trigger
	$trigger_grabacions_before_insert="CREATE  TRIGGER ".$nombreBD.".`grabacions_BEFORE_INSERT` BEFORE INSERT ON `grabacions` FOR EACH ROW
	BEGIN
	-- Indica si la grabacion tiene GPS 
	DECLARE tieneAlerta Integer;
	DECLARE tieneGps Integer;

    SELECT count(distinct(posiciones.id)) into @tieneGps FROM ".$nombreBD.".posiciones  inner join ".$nombreBD.".grabacions on (posiciones.userId = NEW.idProducto)  WHERE  uploadTime BETWEEN NEW.horaini AND NEW.horafin;
    
    IF @tieneGps = 0 THEN
		SET NEW.tieneGPS = 0;
	ELSE
       	SET NEW.tieneGPS = 1;
	END IF; 
    
    -- Indica si la grabacion tiene Alertas 
    SELECT count(distinct(alarma_movimiento.id)) into @tieneAlerta 
    FROM ".$nombreBD.".alarma_movimiento  inner join ".$nombreBD.".grabacions on (alarma_movimiento.idCam = NEW.idProducto)  
    WHERE time BETWEEN NEW.horaini AND NEW.horafin;
    
    IF @tieneAlerta = 0 THEN
		SET NEW.tieneAlerta = 0;
	ELSE
       	SET NEW.tieneAlerta = 1;
	END IF; 
END";

	$resultado_grabacions_before_insert = mysqli_query($mysqli2, $trigger_grabacions_before_insert);	
	# Comprobamos el proceso de creacion de la tabla
	validar_tabla($resultado_grabacions_before_insert, 'grabacions_BEFORE_INSERT', 'trigger'); 
	
	# trigger 2
	$trigger_grabacions_AINS="CREATE TRIGGER ".$nombreBD.".`grabacions_AINS` AFTER INSERT ON `grabacions` FOR EACH ROW
begin
		DECLARE id_exists Boolean;
		-- Check log_consumo_cajas table exist row for actual billing period.
		DECLARE diaFactSim Integer;
		SELECT 1
		INTO @id_exists
		FROM ".$nombreBD.".log_consumo_producto
		where 
			fecha_facturacion_sim>curdate() AND id_productos_contratados=NEW.idProducto;
		SELECT diaFacturacionSim INTO @diaFactSim from ".$nombreBD.".productos_contratados where idProducto=NEW.idProducto;
		IF STR_TO_DATE(CONCAT(@diaFactSim,',',month(curdate()),',',year(curdate())),'%d,%m,%Y')>curdate()
			THEN
				SET @fechaFact=STR_TO_DATE(CONCAT(@diaFactSim,',',month(curdate()),',',year(curdate())),'%d,%m,%Y');
			else
				SET @fechaFact=STR_TO_DATE(CONCAT(@diaFactSim,',',month(curdate()),',',year(curdate())),'%d,%m,%Y')+INTERVAL 1 MONTH;
			END IF;
		IF @id_exists = 1
		THEN
			UPDATE ".$nombreBD.".log_consumo_producto
			SET segundos_consumidos = segundos_consumidos+NEW.duracion,
			   bytes_consumidos = bytes_consumidos+NEW.size_bytes
			WHERE id_productos_contratados = NEW.idProducto AND fecha_facturacion_sim=@fechaFact;
		else
			INSERT INTO ".$nombreBD.".log_consumo_producto VALUES  ( default,NEW.idProducto,NEW.duracion,NEW.size_bytes,@fechaFact);
        END IF;
end";
	
	$resultado_grabacions_AINS = mysqli_query($mysqli2, $trigger_grabacions_AINS);	
	# Comprobamos el proceso de creacion de la tabla
	validar_tabla($resultado_grabacions_AINS, 'grabacions_AINS', 'trigger'); 
	
	
	
	/********************************************************************************
	* Creamos la tabla historial_alertas
	********************************************************************************/
	$tabla_historial_alertas="CREATE TABLE IF NOT EXISTS ".$nombreBD.". `historial_alertas` (
		  `idHistorico` int(11) NOT NULL AUTO_INCREMENT,
		  `id` int(11) DEFAULT NULL,
		  `idCam` int(11) DEFAULT NULL,
		  `img` varchar(245) DEFAULT NULL,
		  `time` timestamp NULL DEFAULT NULL,
		  `idMail` varchar(254) DEFAULT NULL,
		  `visto` int(1) DEFAULT NULL COMMENT '0 no se ha visto la alerta\n1 se ha visto la alerta ',
		  `eliminado` int(1) DEFAULT NULL COMMENT '0 no se ha eliminado la alerta\n1 se ha eliminado la alerta ',
		  PRIMARY KEY (`idHistorico`),
		  UNIQUE KEY `idHistorico_UNIQUE` (`idHistorico`),
		  UNIQUE KEY `id_UNIQUE` (`id`),
		   KEY `index_historial_alertas` (`idHistorico`,`id`)
		) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

	$resultado_historial_alertas = mysqli_query($mysqli2, $tabla_historial_alertas);	
	# Comprobamos el proceso de creacion de la tabla
	validar_tabla($resultado_historial_alertas, 'historial_alertas', 'create'); 
	
	
	/********************************************************************************
	* Creamos la tabla historial_grabacions
	********************************************************************************/
	$tabla_historial_grabacions = "CREATE TABLE IF NOT EXISTS ".$nombreBD.". `historial_grabacions` (
	  `idHistorico` int(11) NOT NULL AUTO_INCREMENT,
	  `idGrabacion` int(11) DEFAULT NULL,
	  `idProducto` int(10) DEFAULT NULL,
	  `horaini` timestamp NULL DEFAULT NULL ,
	  `horafin` timestamp NULL DEFAULT NULL,
	  `duracion` int(11) DEFAULT NULL,
	  `archivo` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
	  `play` int(11) DEFAULT NULL,
	  `delet` int(11) DEFAULT NULL,
	  `eliminado` int(11) DEFAULT NULL,
	  `datedelete` timestamp NULL DEFAULT NULL,
	  `size_bytes` double DEFAULT NULL,
	  `num_segment` int(10) DEFAULT NULL,
	  `tieneGPS` int(1) DEFAULT NULL,
	  `tieneAlerta` int(1) DEFAULT NULL,
	  PRIMARY KEY (`idHistorico`),
	  UNIQUE KEY `idHistorico_UNIQUE` (`idHistorico`),
	  UNIQUE KEY `idGrabacion_UNIQUE` (`idGrabacion`),
	  KEY `index_historial_grabacions` (`idGrabacion`)
	) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

	$resultado_historial_grabacions = mysqli_query($mysqli2, $tabla_historial_grabacions);	
	# Comprobamos el proceso de creacion de la tabla
	validar_tabla($resultado_historial_grabacions, 'historial_grabacions', 'create');
	
	/********************************************************************************
	* Creamos la tabla historial_posiciones
	********************************************************************************/
	$tabla_historial_posiciones = "CREATE TABLE IF NOT EXISTS ".$nombreBD.". `historial_posiciones` (
	  `idHistorico` int(11) NOT NULL AUTO_INCREMENT,
	  `idPosicion` int(11) DEFAULT NULL,
	  `userId` int(11) DEFAULT NULL,
	  `latitude` decimal(10,6) DEFAULT NULL,
	  `longitude` decimal(10,6) DEFAULT NULL,
	  `altitude` float DEFAULT NULL,
	  `uploadTime` timestamp NULL DEFAULT NULL,
	  `compass` decimal(10,2) DEFAULT NULL,
	  `accuracy` int(11) DEFAULT NULL,
	  `dateInitStream` datetime DEFAULT '0000-00-00 00:00:00',
	  `speed` decimal(10,6) DEFAULT NULL,
	  `accelX` varchar(45) DEFAULT NULL,
	  `accelY` varchar(45) DEFAULT NULL,
	  `accelZ` varchar(45) DEFAULT NULL,
	  `travelModel` varchar(45) DEFAULT NULL COMMENT 'DRIVING --> Carretera\nWALKING --> Peaton (Default) \n\n',
	  `tipoPosicion` int(1) DEFAULT NULL COMMENT '0 -> Automatico, caso por defecto\n1 -> Manual, lo pone el usuario.',
	  `zoom` int(11) DEFAULT NULL,
	  PRIMARY KEY (`idHistorico`),
	  UNIQUE KEY `idHistorico_UNIQUE` (`idHistorico`),
	  UNIQUE KEY `idPosicion_UNIQUE` (`idPosicion`),
	  KEY `index_historial_posiciones` (`idPosicion`,`userId`,`uploadTime`)
	) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";
	$resultado_historial_posiciones = mysqli_query($mysqli2, $tabla_historial_posiciones);	
	# Comprobamos el proceso de creacion de la tabla
	validar_tabla($resultado_historial_posiciones, 'historial_posiciones', 'create');
	
	/********************************************************************************
	* Creamos la tabla info_cliente
	********************************************************************************/
	$tabla_info_cliente ="CREATE TABLE IF NOT EXISTS ".$nombreBD.".`info_cliente` (
	  `idEmpresa` int(11) NOT NULL COMMENT 'identificador de la empresa.',
	  `subdomini` text NOT NULL COMMENT 'nombre de la empresa.',
	  `direccion` varchar(150) NOT NULL,
	  `cp` varchar(50) NOT NULL,
	  `poblacion` varchar(150) NOT NULL,
	  `provincia` varchar(150) NOT NULL,
	  `idPais` int(11) NOT NULL,
	  `email` varchar(100) NOT NULL,
	  `telefono1` varchar(100) NOT NULL,
	  `telefono2` varchar(100) DEFAULT NULL,
	  `prefixstream` varchar(10) NOT NULL,
	  `server` text NOT NULL,
	  `streamapp` varchar(250) DEFAULT NULL,
	  `domini` text NOT NULL,
	  `conn` varchar(250) DEFAULT NULL COMMENT 'se usa en wowza, para acceder a BD.',
	  `aplicacion` int(11) NOT NULL,
	  `licencias` int(10) NOT NULL,
	  `latEmpresa` decimal(10,6) DEFAULT NULL,
	  `lngEmpresa` decimal(10,6) DEFAULT NULL,
	  `emailAlerta` varchar(150) DEFAULT NULL,
	  `telefonoAlert` varchar(15) DEFAULT NULL,
	  `videoAnalisis` int(1) DEFAULT '0' COMMENT '0 -> No tiene videoanalisis a nivel de empresa\n1-> Si tiene videoanalisis  a nivel de empresa',
	  PRIMARY KEY (`idEmpresa`),
	    KEY `index_info_cliente` (`idEmpresa`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8";

	$resultado_info_cliente = mysqli_query($mysqli2, $tabla_info_cliente);	
	# Comprobamos el proceso de creacion de la tabla
	validar_tabla($resultado_info_cliente, 'historial_posiciones', 'create');
	
	#Insertamos los datos 
	if($resultado_info_cliente){ 
		# echo "<br> vamos hacer el insert info cliente ...";
		$stmt = $mysqli2->prepare("INSERT INTO  ".$nombreBD.".info_cliente (idEmpresa, subdomini, email, telefono1,  prefixstream, server, streamapp, domini, conn, aplicacion, licencias, latEmpresa, lngEmpresa) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)");		
		 $stmt->bind_param('ississsssiiss', $idUsuario, $subdomini,$email, $telefono, $prefixstream,$newServer,$streamapp,$domini,$connapp,$aplicacion,$licencias,$lat,$lng);
		$stmt->execute();
		
		# Comprobamos si se han insertado los datos.
		# printf("<br> Affected rows (INSERT): %d\n", $mysqli2->affected_rows);
		
		$stmt->close();
		 
		
	}
	
	/********************************************************************************
	* Creamos la tabla info_server
	********************************************************************************/	
	$tabla_infoServers="CREATE TABLE IF NOT EXISTS ".$nombreBD.".`info_server` (
								  `id` int(11) NOT NULL AUTO_INCREMENT,
								  `server` varchar(250) NOT NULL,
								  `company` text NOT NULL,
								  `prefixStream` text,
								  `idempresa` int(11) NOT NULL,
								  `ipserver` varchar(25) NOT NULL,
								  `streamapp` text NOT NULL,
								  `recordapp` text NOT NULL,
								   `ip_wz_internal` varchar(15) DEFAULT NULL,
								  PRIMARY KEY (`id`)
						) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

	$resultado_info_server = mysqli_query($mysqli2, $tabla_infoServers);	
	validar_tabla($resultado_info_server, 'info_server', 'create'); #Comprobamos el proceso de creacion de la tabla	
	# echo "<br> hola";

	#Insertamos los datos 
	$server = "rtmp://".$ipServer.":".$puertoServer."/vod";
	$id_infoServer = 1; 
	if($resultado_info_server){ 
	# echo "<br> ahora insertamos datos en la tabla info_server";
	# echo "<br> $id_infoServer, $server, $usuario , $prefixstream , $newId , $ipServer , $streamapp , $recordapp";
		$stmt = $mysqli2->prepare("INSERT INTO  ".$nombreBD.".info_server (id,server,company,prefixStream,idempresa,ipserver,streamapp,recordapp) VALUES(?,?,?,?,?,?,?,?)");		
		$stmt->bind_param('isssisss', $id_infoServer, $server, $usuario , $prefixstream , $idUsuario , $ipServer , $streamapp , $recordapp);
		$stmt->execute();
		
		# Comprobamos si se han insertado los datos.
		# printf("<br> Affected rows (INSERT): %d\n", $mysqli2->affected_rows);
		
		$stmt->close();
	}
	
	/********************************************************************************
	* Creamos la tabla log_consumo_producto
	********************************************************************************/
	$tabla_logConsumoProducto ="CREATE TABLE IF NOT EXISTS  ".$nombreBD.".log_consumo_producto (
													`id` int(11) NOT NULL AUTO_INCREMENT,
													`id_productos_contratados` int(11) DEFAULT NULL,
													`segundos_consumidos` int(21) DEFAULT '0',
													`bytes_consumidos` bigint(20) unsigned DEFAULT '0',
													`fecha_facturacion_sim` timestamp NULL DEFAULT NULL COMMENT 'Limite para facturar ',
													PRIMARY KEY (`id`),
													KEY `index_log_consumo_producto` (`id_productos_contratados`,`fecha_facturacion_sim`)
												) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";
												
	$resultado_logConsumoProducto = mysqli_query($mysqli2, $tabla_logConsumoProducto);	
	#Comprobamos el proceso de creacion de la tabla
	validar_tabla($resultado_logConsumoProducto, 'log_consumo_producto', 'create'); 
	
	/********************************************************************************
	* Creamos la tabla log_pages
	********************************************************************************/
	$tabla_logPages ="CREATE TABLE IF NOT EXISTS ".$nombreBD.".`log_pages` (
						  `id` int(11) NOT NULL AUTO_INCREMENT,
						  `idUser` int(11) DEFAULT NULL,
						  `ip` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
						  `pagina` varchar(50) NOT NULL COMMENT 'nombre de la pagina que accede el cliente',
						  `fechaEntrada` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
						  `fechaSalida` timestamp NULL DEFAULT NULL,
						  `tiempo_transcurrido` varchar(45) DEFAULT NULL,
						  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8";
	
	$resultado_log_pages = mysqli_query($mysqli2, $tabla_logPages);	
	validar_tabla($resultado_log_pages, 'log_pages', 'create'); #Comprobamos el proceso de creacion de la tabla	

	/********************************************************************************
	* Creamos la tabla log_sesiones
	********************************************************************************/
	$tabla_logSesiones ="CREATE TABLE IF NOT EXISTS ".$nombreBD.".`log_sesiones` (
										  `id` int(11) NOT NULL AUTO_INCREMENT,
										  `ip` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
										  `activo` int(1) NOT NULL COMMENT 'activo =  1 --> Sesion del usuario activada\nactivo = 0 --> Sesion del usuario desactivada',
										  `fechaEntrada` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
										  `fechaSalida` timestamp NULL DEFAULT NULL,
										  PRIMARY KEY (`id`)
						) ENGINE=InnoDB DEFAULT CHARSET=utf8";
						
	$resultado_log_sesiones = mysqli_query($mysqli2, $tabla_logSesiones);	
	#Comprobamos el proceso de creacion de la tabla
	validar_tabla($resultado_log_sesiones, 'log_sesiones', 'create'); 
	
	/********************************************************************************
	* Creamos la tabla log_ultimaposicion
	********************************************************************************/
	$tabla_logUltimaposicion ="CREATE TABLE IF NOT EXISTS ".$nombreBD.".`log_ultimaposicion` (
											  `id` int(11) NOT NULL AUTO_INCREMENT,
											  `evento` varchar(45) DEFAULT NULL,
											  `fecha_evento` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
											  `latitude` decimal(10,6) DEFAULT NULL,
											  `longitude` decimal(10,6) DEFAULT NULL,
											  `altitude` float DEFAULT NULL,
											  `uploadTime` timestamp NULL DEFAULT NULL,
											  `compass` decimal(10,2) DEFAULT '0.00',
											  `accuracy` int(11) DEFAULT NULL,
											  `dateInitStream` datetime DEFAULT '0000-00-00 00:00:00',
											  `online` tinyint(1) NOT NULL DEFAULT '0',
											  `onlinetime` datetime DEFAULT NULL,
											  `userlogin` int(11) DEFAULT NULL,
											  `acceIX` decimal(10,2) DEFAULT NULL,
											  `acceIY` decimal(10,2) DEFAULT NULL,
											  `acceIZ` decimal(10,2) DEFAULT NULL,
											  `travelModel` varchar(45) DEFAULT NULL COMMENT 'DRIVING --> Carretera\nWALKING --> Peaton (Default) \n\n',
											  PRIMARY KEY (`id`),
											  KEY `uploadTime_index` (`uploadTime`)
							) ENGINE=InnoDB DEFAULT CHARSET=utf8";
	
	$resultado_log_ultimaposicion = mysqli_query($mysqli2, $tabla_logUltimaposicion);	
	#Comprobamos el proceso de creacion de la tabla
	validar_tabla($resultado_log_ultimaposicion, 'log_ultimaposicion', 'create'); 
	
	/********************************************************************************
	* Creamos la tabla log_zones
	********************************************************************************/
	$tabla_logZones ="CREATE TABLE IF NOT EXISTS ".$nombreBD.".`log_zones` (
							  `id` int(11) NOT NULL AUTO_INCREMENT,
							  `idZona` int(11) DEFAULT NULL,
							  `name` varchar(100) NOT NULL,
							  `color` varchar(7) NOT NULL,
							  `lat` decimal(10,6) DEFAULT NULL,
							  `lng` decimal(10,6) DEFAULT NULL,
							  `center` varchar(100) NOT NULL,
							  `zoom` int(11) NOT NULL,
							  `dirZona` varchar(100) NOT NULL,
							  `fecha_alta` timestamp NULL DEFAULT NULL,
							  `eliminado` int(1) NOT NULL DEFAULT '0' COMMENT '1--> eliminada\n0--> no eliminada',
							  `evento` varchar(45) DEFAULT NULL,
							  `fecha_evento` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
							  `idProducto` int(11) DEFAULT NULL,
							  PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8";
	
	$resultado_log_zones = mysqli_query($mysqli2, $tabla_logZones);	
	#Comprobamos el proceso de creacion de la tabla
	validar_tabla($resultado_log_zones, 'log_zones', 'create'); 
	
	
	/********************************************************************************
	* Creamos la tabla log_zones_points
	********************************************************************************/
	$tabla_logZonesPoints ="CREATE TABLE IF NOT EXISTS ".$nombreBD.".`log_zones_points` (
							  `id` int(11) NOT NULL AUTO_INCREMENT,
							  `idZona` int(11) DEFAULT NULL,
							  `lat` decimal(10,6) DEFAULT NULL,
							  `lng` decimal(10,6) DEFAULT NULL,
							  `fecha_alta` timestamp NULL DEFAULT NULL,
							  `evento` varchar(45) DEFAULT NULL,
							  `fecha_evento` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
							  PRIMARY KEY (`id`)
							) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8";
	
	$resultado_log_zones_points = mysqli_query($mysqli2, $tabla_logZonesPoints);	
	#Comprobamos el proceso de creacion de la tabla
	validar_tabla($resultado_log_zones_points, 'log_zones_points', 'create'); 
	
	
	/********************************************************************************
	* Creamos la tabla login_attempts
	********************************************************************************/
	$tabla_login_attempts ="CREATE TABLE IF NOT EXISTS ".$nombreBD.".`login_attempts` (
							    `user_id` int(11) NOT NULL,
								`time` varchar(30) NOT NULL
							) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Controlar los intentos de acceso.'";

	$resultado_login_attempts = mysqli_query($mysqli2, $tabla_login_attempts);	
	validar_tabla($resultado_login_attempts, 'login_attempts', 'create'); #Comprobamos el proceso de creacion de la tabla
	
	
	/********************************************************************************
	* Creamos la tabla login_users
	********************************************************************************/
	$tabla_loginUsers="CREATE TABLE IF NOT EXISTS ".$nombreBD.".`login_users` (
						    `id` int(11) NOT NULL AUTO_INCREMENT,
							`idRol` int(11) NOT NULL,
							`idEmpresa` int(11) NOT NULL,
							`user` varchar(20) NOT NULL,
							`pass` varchar(250) NOT NULL,
							`mail` varchar(120) DEFAULT '0',
							`phone` varchar(45) DEFAULT NULL,
							`salt` varchar(22) NOT NULL COMMENT 'Sirve para des/encriptar el password',
							`session` varchar(45) DEFAULT NULL COMMENT 'id de la session',
							`disabled` tinyint(3) unsigned DEFAULT '0' COMMENT 'disable = 1 --> cuenta desactivada, o bien porque hay muchos intentos de entrar (desactuvamos 2h) o que esta eliminado\n',
							`created_ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
							`modified_ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
							`token` char(32) DEFAULT NULL,
							`token_expire` timestamp NULL DEFAULT '0000-00-00 00:00:00',
							`idProducto` int(11) DEFAULT NULL,
							PRIMARY KEY (`id`)
					) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8";	

	$resultado_login_users = mysqli_query($mysqli2, $tabla_loginUsers);	
	# Comprobamos el proceso de creacion de la tabla
	validar_tabla($resultado_login_users, 'login_users', 'create');
	
	#Insertamos los datos 
	if($resultado_login_users){		
		
		include_once '../conexion/functions.php';
		login_register($usuario,$email,$pass,$idUsuario,"1",$mysqli2, $nombreBD);
		if(loginUser($usuario,$pass,$mysqli2, $nombreBD)){
			# echo "ok login";
		}else{
			# echo "LOGIN FAIL!!!";
		}
	}
	
	/********************************************************************************
	* Creamos la tabla posiciones
	********************************************************************************/		
	$tabla_posiciones = "CREATE TABLE `posiciones` (
								  `id` int(11) NOT NULL AUTO_INCREMENT,
								  `userId` int(11) NOT NULL,
								  `latitude` decimal(10,6) NOT NULL,
								  `longitude` decimal(10,6) NOT NULL,
								  `altitude` float NOT NULL,
								  `uploadTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
								  `compass` decimal(10,2) NOT NULL DEFAULT '0.00',
								  `accuracy` int(11) NOT NULL,
								  `dateInitStream` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
								  `speed` decimal(10,6) DEFAULT NULL,
								  `accelX` decimal(10,2) DEFAULT NULL,
								  `accelY` decimal(10,2) DEFAULT NULL,
								  `accelZ` decimal(10,2) DEFAULT NULL,
								  `travelModel` varchar(45) DEFAULT NULL COMMENT 'DRIVING --> Carretera\nWALKING --> Peaton (Default) \n\n',
								  `tipoPosicion` int(1) DEFAULT '0' COMMENT '0 -> Automatico, caso por defecto\n1 -> Manual, lo pone el usuario.',
								  `zoom` int(11) DEFAULT '6',
								  PRIMARY KEY (`id`,`userId`,`latitude`,`longitude`,`altitude`,`uploadTime`,`compass`,`accuracy`,`dateInitStream`),
								   KEY `index_posiciones` (`userId`,`uploadTime`)
								) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8";
	
	$resultado_posiciones = mysqli_query($mysqli2, $tabla_posiciones);	
	# Comprobamos el proceso de creacion de la tabla
	validar_tabla($resultado_posiciones, 'posiciones', 'create');
	
	# add trigger
	$trigger_actualizarUltimaPosicion ="CREATE  TRIGGER ".$nombreBD.".`actualizarUltimaPosicion`  AFTER INSERT ON	`posiciones` FOR EACH ROW 
								INSERT INTO 
									".$nombreBD.".ultimaposicion 
								SET 
									userId=new.userId,
									latitude=new.latitude,
									longitude=new.longitude,
									altitude=new.altitude,
									uploadTime=new.uploadTime,
									compass=new.compass,
									accuracy=new.accuracy,
									acceIX=new.accelX,
									acceIY=new.accelY,
									acceIZ=new.accelZ,
									zoom=new.zoom
								ON DUPLICATE KEY 
									update latitude = new.latitude,
									longitude = new.longitude,
									altitude = new.altitude,
									uploadTime = new.uploadTime,
									compass= new.compass,
									accuracy = new.accuracy,
									acceIX=new.accelX,
									acceIY=new.accelY,
									acceIZ=new.accelZ,
									zoom=new.zoom";

	$resultado_actualizarUltimaPosicion = mysqli_query($mysqli2, $trigger_actualizarUltimaPosicion);	
	# Comprobamos el proceso de creacion de la tabla
	validar_tabla($resultado_actualizarUltimaPosicion, 'actualizarUltimaPosicion', 'trigger'); 

	
	/********************************************************************************
	* Creamos la tabla productos_contratados 
	********************************************************************************/		
	$tabla_productosContratados = "CREATE TABLE IF NOT EXISTS ".$nombreBD.".`productos_contratados` (
										`idProductoContratado` int(11) NOT NULL AUTO_INCREMENT,
										`idProducto` int(11) NOT NULL,
										`nameProducto` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
										`idTipoProducto` int(11) DEFAULT NULL,
										`idZona` int(11) DEFAULT NULL,
										`user` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
										`pass` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
										`estado` int(11) DEFAULT NULL COMMENT 'Producto activo = 1\nProducto dado de baja = 0',
										`online` int(11) DEFAULT NULL COMMENT 'Producto conectado (online) = 1\nProducto desconectado (offline) = 0',
										`fechaAlta` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
										`active` bit(1) DEFAULT b'0',
										`notifications` bit(1) DEFAULT b'0',
										`traceability` bit(1) DEFAULT b'0',
										`fps` int(11) DEFAULT '25',
										`orden` int(11) DEFAULT NULL,
										`fechaModificacion` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
										`videoAnalisis` int(1) DEFAULT '0' COMMENT '0 -> No tiene videoanalisis a nivel de producto \n1-> Si tiene videoanalisis  a nivel de producto',
										`intensidadAlerta` int(10) DEFAULT '0',
										`observacion` varchar(255) DEFAULT NULL,
										`diaFacturacionSim` int(11) DEFAULT '1',
										`emailAlerta` varchar(255) DEFAULT NULL,
										`telfAlerta` varchar(10) DEFAULT NULL,
										`serialnum` varchar(45) DEFAULT NULL,
										`dyndns` varchar(45) DEFAULT NULL,
										`isOnlyRecAlarm` int(1) DEFAULT '0',		
										`matrizAlarma` varchar(120) DEFAULT '0',										
										 PRIMARY KEY (`idProducto`),
										 UNIQUE KEY `idProductoContratado_UNIQUE` (`idProductoContratado`),
										 KEY `index_productos_contratados` (`idProducto`,`idZona`,`estado`)
									) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8";
									
	$resultado_productos_contratados = mysqli_query($mysqli2, $tabla_productosContratados);	
	#Comprobamos el proceso de creacion de la tabla
	validar_tabla($resultado_productos_contratados, 'productos_contratados', 'create'); 
	
	
	/********************************************************************************
	* Creamos la tabla ultimaposicion
	********************************************************************************/
	$tabla_ultimaposicion = "CREATE TABLE IF NOT EXISTS ".$nombreBD.".`ultimaposicion` (
							  `userId` int(11) NOT NULL DEFAULT '0',
							  `latitude` decimal(10,6) DEFAULT NULL,
							  `longitude` decimal(10,6) DEFAULT NULL,
							  `altitude` float DEFAULT NULL,
							  `uploadTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
							  `compass` decimal(10,2) DEFAULT '0.00',
							  `accuracy` int(11) DEFAULT NULL,
							  `dateInitStream` datetime DEFAULT '0000-00-00 00:00:00',
							  `online` tinyint(1) NOT NULL DEFAULT '0',
							  `onlinetime` datetime DEFAULT NULL,
							  `userlogin` int(11) DEFAULT NULL,
							  `acceIX` decimal(10,2) DEFAULT NULL,
							  `acceIY` decimal(10,2) DEFAULT NULL,
							  `acceIZ` decimal(10,2) DEFAULT NULL,
							  `travelModel` varchar(45) DEFAULT NULL COMMENT 'DRIVING --> Carretera\nWALKING --> Peaton (Default) \n\n',
							  `zoom` int(11) DEFAULT NULL,
								PRIMARY KEY (`userId`),
							KEY `uploadTime_index` (`uploadTime`)
						) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
				
	$resultado_ultimaposicion = mysqli_query($mysqli2, $tabla_ultimaposicion);	
	#Comprobamos el proceso de creacion de la tabla
	validar_tabla($resultado_ultimaposicion, 'ultimaposicion', 'create'); 
	
	/********************************************************************************
	* Creamos la tabla ultimaposicion
	********************************************************************************/
	$tabla_userProducto = "CREATE TABLE IF NOT EXISTS ".$nombreBD.".`user_producto` (
									  `id` int(11) NOT NULL AUTO_INCREMENT,
									  `idUser` int(11) NOT NULL,
									  `idProducto` int(11) NOT NULL,
									  `permiso` int(11) NOT NULL COMMENT '0 --> No tiene permiso\n1 --> Si tiene permiso',
									  PRIMARY KEY (`id`)
									) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;";
		
	$resultado_user_producto = mysqli_query($mysqli2, $tabla_userProducto);	
	#Comprobamos el proceso de creacion de la tabla
	validar_tabla($resultado_user_producto, 'userProducto', 'create'); 
	
	/********************************************************************************
	* Creamos la tabla zones
	********************************************************************************/
	$tabla_zones = "CREATE TABLE IF NOT EXISTS ".$nombreBD.".`zones` (
						`id` int(11) NOT NULL AUTO_INCREMENT,
						`name` varchar(100) NOT NULL,
						`color` varchar(7) NOT NULL,
						`lat` decimal(10,6) DEFAULT NULL,
						`lng` decimal(10,6) DEFAULT NULL,
						`center` varchar(100) NOT NULL,
						`zoom` int(11) NOT NULL,
						`dirZona` varchar(100) NOT NULL,
						`fecha_alta` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
						`eliminado` int(1) NOT NULL DEFAULT '0' COMMENT '1--> eliminada\n0--> no eliminada',
						PRIMARY KEY (`id`),
						KEY `index_zones` (`id`,`name`,`eliminado`)
					) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8";
	
	$resultado_zones = mysqli_query($mysqli2, $tabla_zones);	
	# Comprobamos el proceso de creacion de la tabla
	validar_tabla($resultado_zones, 'zones', 'create'); 
	
	# Insertamos los datos 
	if($resultado_login_users){	
		$num0 = 0; 
		$num13 = 0; 
		$zonaNoDefinida = "No definida";
		$colorBlanco = '#ffffff';
		$stmt = $mysqli2->prepare("INSERT INTO  ".$nombreBD.".zones (id, name, color, lat, lng, zoom) VALUES(?, ?, ?, ?, ?, ?)");		
		$stmt->bind_param('issssi', $num0,$zonaNoDefinida, $colorBlanco ,$num0, $num0, $num13 );
		$stmt->execute();
		# Comprobamos si se han insertado los datos.
		# printf("<br> Affected rows (INSERT): %d\n", $mysqli2->affected_rows);
		$stmt->close();
				
		#Modificamos id de zones para obligarle que sea el 0 y no el 1
		 $stmt = $mysqli2->prepare("UPDATE  ".$nombreBD.".zones SET id=0 WHERE id=1");		
		 $stmt->execute();
		 
		# Comprobamos si se han insertado los datos.
		# printf("<br> Affected rows (UPDATE): %d\n", $mysqli2->affected_rows);
		$stmt->close();
	}
	
	
	/********************************************************************************
	* Creamos la tabla zones_points
	********************************************************************************/
	$tabla_zones_points = "CREATE TABLE IF NOT EXISTS ".$nombreBD.".`zones_points` (
						  `id` int(11) NOT NULL AUTO_INCREMENT,
						  `idZona` int(11) DEFAULT NULL,
						  `lat` decimal(10,6) DEFAULT NULL,
						  `lng` decimal(10,6) DEFAULT NULL,
						  `eliminado` int(11) NOT NULL DEFAULT '0',
						  `fecha_alta` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
						  `fecha_modificacion` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
						  PRIMARY KEY (`id`),
						   KEY `index_zones_points` (`idZona`)
					) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8";

	$resultado_zones_points = mysqli_query($mysqli2, $tabla_zones_points);	
	#Comprobamos el proceso de creacion de la tabla
	validar_tabla($resultado_zones_points, 'zones_points', 'create'); 
	
	
	
	/********************************************************************************
	* Creamos la vista v_productos_lastpos CREATE VIEW 
	*******************************************************************************/
	$vista_productosLastpos="CREATE VIEW ".$nombreBD.".v_productos_lastpos AS SELECT 
		p.idProducto AS idProducto,
        p.nameProducto AS nameProducto,
        p.user AS user,
        p.online AS online,
        p.active AS active,
        p.notifications AS notifications,
        p.traceability AS traceability,
        p.fps AS fps,
        u.latitude AS latitude,
        u.longitude AS longitude,
        u.uploadTime AS uploadTime,
        tp.nombre AS tipoProducto,
        tp.descr AS tipo_info,
        tp.tp_gps AS tp_gps,
        tp.tp_3G AS tp_3G,
        tp.tp_4G AS tp_4G,
        tp.tp_wifi AS tp_wifi,
        info.ipserver AS ipserver
    FROM
        (((
			".$nombreBD.".productos_contratados AS p
			LEFT JOIN ".$nombreBD.".ultimaposicion as u ON (p.idProducto = u.userId)
			)
		LEFT JOIN configstream.tipo_productos AS tp ON (p.idTipoProducto = tp.idTipoProducto)
		)
        JOIN ".$nombreBD.".info_server AS info)";

       
	$resultado_v_productosLastpos= mysqli_query($mysqli2, $vista_productosLastpos);	
	#Comprobamos el proceso de creacion de la vista
	validar_tabla($resultado_v_productosLastpos, 'v_productosLastpos', 'vista'); 
	
	
	/********************************************************************************
	* Creamos la procedure (grabacions_BEFORE_INSERT)
	*******************************************************************************/
	$procedure_debug_grabacions_trigger=("CREATE  PROCEDURE ".$nombreBD.".debug_grabacions_trigger (IN idP INT(11),IN dur INT(11),IN bytes INT(11))
BEGIN
		DECLARE id_exists Boolean;
		-- Check log_consumo_cajas table exist row for actual billing period.
		DECLARE diaFactSim Integer;
		SET @enabled = TRUE;
		SELECT 1
		INTO @id_exists
		FROM log_consumo_producto
		where 
			fecha_facturacion_sim>curdate() AND id_productos_contratados=idP;
		SELECT diaFacturacionSim INTO @diaFactSim from productos_contratados where idProducto=idP;
		call debug_msg(@enabled,@id_exists);
		call debug_msg(@enabled,@diaFactSim);
		SELECT STR_TO_DATE(CONCAT(@diaFactSim,',',month(curdate()),',',year(curdate())),'%d,%m,%Y') INTO @strdate;
		call debug_msg(@enabled,@strdate);
		IF STR_TO_DATE(CONCAT(@diaFactSim,',',month(curdate()),',',year(curdate())),'%d,%m,%Y')>curdate()
			THEN
				SET @fechaFact=STR_TO_DATE(CONCAT(@diaFactSim,',',month(curdate()),',',year(curdate())),'%d,%m,%Y');
			else
				SET @fechaFact=STR_TO_DATE(CONCAT(@diaFactSim,',',month(curdate()),',',year(curdate())),'%d,%m,%Y')+INTERVAL 1 MONTH;
			END IF;
		call debug_msg(@enabled,@fechaFact);
		IF @id_exists = 1
		THEN
			UPDATE log_consumo_producto
			SET segundos_consumidos = segundos_consumidos+dur,
			   bytes_consumidos = bytes_consumidos+bytes
			WHERE id_productos_contratados = idP AND fecha_facturacion_sim=@fechaFact;
		else
			INSERT INTO log_consumo_producto VALUES  ( default,idP,dur,bytes,@fechaFact);
        END IF;
END");	
	
	$resultado_debug_grabacions_trigger = mysqli_query($mysqli2, $procedure_debug_grabacions_trigger);	
	#Comprobamos el proceso de creacion
	validar_tabla($resultado_debug_grabacions_trigger, 'debug_grabacions_trigger', 'procedure'); 

		/********************************************************************************
	* Creamos la procedure (debug_msg)
	*******************************************************************************/
	$procedure_debug_msg=("CREATE  PROCEDURE ".$nombreBD.".`debug_msg`(enabled INTEGER, msg VARCHAR(255))
	BEGIN
		IF enabled THEN BEGIN
			select concat('**', msg) AS '** DEBUG:';
		END; END IF;
	END");
	$resultado_debug_msg = mysqli_query($mysqli2, $procedure_debug_msg);	
	#Comprobamos el proceso de creacion
	validar_tabla($resultado_debug_msg, 'debug_msg', 'procedure'); 
	
	
	/********************************************************************************
	* Creamos la procedure (productoContratado_insert)
	*******************************************************************************/
	$procedure_productoContratado_insert=("CREATE  PROCEDURE   ".$nombreBD.".productoContratado_insert (_idProducto int(11), _nameProducto varchar(50), _idEmpresa int(11), _idTipoProducto int(11), _idCamara int(11), _idRouter int(11), _idSim int(11), _user varchar(50), _pass varchar(45))
	BEGIN
		INSERT INTO  ".$nombreBD.".productos_contratados(idProducto,nameProducto,idEmpresa,idTipoProducto,idCamara,idRouter,idSim,user,pass, estado) 
		VALUES (_idProducto, _nameProducto, _idEmpresa, _idTipoProducto, _idCamara, _idRouter, _idSim, _user, _pass, 1);		
	END");

	$resultado_productoContratado_insert= mysqli_query($mysqli2, $procedure_productoContratado_insert);	
	#Comprobamos el proceso de creacion 
	validar_tabla($resultado_productoContratado_insert, 'productoContratado_insert', 'procedure'); 
	
	
	/********************************************************************************
	* Creamos la procedure (ultimaPosicion_delete)
	*******************************************************************************/
	$procedure_ultimaPosicion_delete =("CREATE  PROCEDURE  ".$nombreBD.".ultimaPosicion_delete (_idProductoNew int(11),_idProducto int(11))
	BEGIN
	UPDATE ".$nombreBD.".ultimaposicion
	SET
	userId =_idProductoNew,
	estado= 0
	WHERE userId = _idProducto;
	END");

	$resultado_ultimaPosicion_delete= mysqli_query($mysqli2, $procedure_ultimaPosicion_delete);
	#Comprobamos el proceso de creacion 
	validar_tabla($resultado_ultimaPosicion_delete, 'ultimaPosicion_delete', 'procedure'); 
	
	/********************************************************************************
	* Creamos la procedure (ultimaPosicion_insert_default)
	*******************************************************************************/
	$procedure_ultimaPosicion_insert_default=("CREATE  PROCEDURE  ".$nombreBD.".ultimaPosicion_insert_default (_userId int(11), _latitude decimal(10,6), _longitude decimal(10,6), _zoom int(11))
	BEGIN
		INSERT INTO ".$nombreBD.".ultimaposicion (userId, latitude, longitude, zoom, estado) VALUES (_userId, _latitude, _longitude, _zoom, 1);
	END");

	$resultado_ultimaPosicion_insert_default= mysqli_query($mysqli2, $procedure_ultimaPosicion_insert_default);	
	#Comprobamos el proceso de creacion 
	validar_tabla($resultado_ultimaPosicion_insert_default, 'ultimaPosicion_insert_default', 'procedure'); 
	
	$mysqli2->close();
?>