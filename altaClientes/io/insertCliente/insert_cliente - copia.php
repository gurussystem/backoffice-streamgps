<?php
	require_once("../funciones.php");
	conectar_bbdd('eude');
	include_once("../conexion/dbi_connect.php");
	$data = array();
	$txt_createBD = ""; 
	 
	# $mysqli->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);
	 
	# Definimos las variables que pasamos por parametros
	$dni = comprobarParametros('dni');		
	$usuario = limpiarVariable(comprobarParametros('empresa')); //dominio de la empresa
	$email = comprobarParametros('email');
	$passEmpresa = comprobarParametros('pass');
	$direccion = comprobarParametros('direccion');
	$poblacion = comprobarParametros('poblacion');
	$provincia = comprobarParametros('provincia');	
	$idPaisSpain= comprobarParametros('pais');
	$cp = comprobarParametros('cp');
	$tlf = comprobarParametros('tlf_fijo');
	$tlf2 = comprobarParametros('tlf_movil');
	$fechaSolicitudAlta = comprobarParametros('fechaSolicitudAlta');
	
	# Datos Bancarios
	$razonSocial = comprobarParametros('razonSocial'); 
	$titularCuenta = comprobarParametros('titularCuenta');
	$entidad= comprobarParametros('entidad');
	$sucursal= comprobarParametros('sucursal');
	$dc= comprobarParametros('dc');
	$numCuenta= comprobarParametros('numCuenta');
	$iban= comprobarParametros('iban');
	$swift= comprobarParametros('swift');
	
	# Datos de Configuracion	
	$ipServer = comprobarParametros('ipServer');
	$puertoServer = comprobarParametros('puertoServer');
	$newServer = $ipServer.":".$puertoServer;
	$lat= comprobarParametros('lat');
	$lng= comprobarParametros('lng');
	# echo "<br>  coordenadas: (" . $lat ."  , " . $lng .")";
	$videoAnalisis= comprobarParametros('videoAnalisis');
	
	$subdomini = $usuario;
	$domini = "http://".$subdomini.".streamgps.com";
	$streamapp ='live';
	$aplicacion=1;
	$licencias = 0;
	$activo = 1;
	$recordapp = "vod";
	$txtMail_create ="";
	# echo "POST: <pre>";print_r($_POST);echo "</pre> GET <pre>";print_r($_GET);echo "</pre>";
	
	
	# Funcion para validar si esta bien o no la creación de la bd
	function validar_tabla($sqlValuar, $nombreTabla, $opcion){
		
		switch($opcion){
			case 'create':
				$txt_error = '[ERROR],	al crear la tabla '. $nombreTabla .' <br/>';
				$txt_ok = '[OK], la tabla '.$nombreTabla.' se ha creado correctamente. <br/>';
				break;
		
			case 'insert':
				$txt_error = '[ERROR],	al insertar los datos en la tabla '. $nombreTabla .', actualmente estará vacía. <br/>';
				$txt_ok = '[OK], se han insertado correctamente los datosn en la tabla '.$nombreTabla.'. <br/>';
				break;
				
			case 'trigger':
				$txt_error = '[ERROR], al crear el trigger '. $nombreTabla .' <br/>';
				$txt_ok = '[OK],el trigger '. $nombreTabla .' se creo correctamente<br/>' ;
				break;
			
			case 'update':
				$txt_error = '[ERROR], al crear modificar la tabla '. $nombreTabla .' <br/>';
				$txt_ok = '[OK], los datos de la tabla '. $nombreTabla .' se actualizarón correctamente<br/>' ;
				break;
				
			case 'vista':
				$txt_error = '[ERROR], al crear la vista '. $nombreTabla .' <br/>';
				$txt_ok = '[OK], la vista '. $nombreTabla .' se ha creado  correctamente<br/>' ;
				break;
			
			case 'insert_eude':
				$txt_error = '[ERROR], al dar de alta a la nueva empresa en EUDE.<br/> Comprueba que la empresa no exista ya en la Base de datos.';
				$txt_ok = '[OK], Se ha creado la nueva empresa en EUDE correctament<br/>' ;
				break;
				
			case 'cta_empresas':
				$txt_error = '[ERROR], al insertar en la tabla cta.empresas';
				$txt_ok = '[OK], Se ha insertado correctamente<br/>' ;
				break;
				
			case 'procedure':
				$txt_error = '[ERROR], al crear el procedure '. $nombreTabla .' <br/>';
				$txt_ok = '[OK], el procedure '. $nombreTabla .' se ha creado  correctamente<br/>' ;
				break;	
		}
	
		if(!$sqlValuar){
			$bbddok=0;
			$txtMail_create = $txt_error;
		}else{
			$txtMail_create = $txt_ok ;
			
		}
	
		
		if (!$sqlValuar) {
			die('<br> Consulta no válida: ' . $mysqli->error);
		}
		
			# echo "<br> txtMail_create: " . $txtMail_create;
	}		
	
	
	# Consultamos el ultimo id de empresa insertado para añadir el nuevo
	$stmt = $mysqli->prepare("SELECT id FROM eude.empresas order by id desc limit 1");			
	$stmt->execute();
	$stmt->bind_result($id);
	$stmt->fetch();
	$newId = $id + 1;
	# echo "<br> 3 newId: " . $newId;	
	 $stmt->close();
	 
	
	# Consultamos el ultimo id de la tabla eude.cta_empresas	
	$stmt = $mysqli->prepare("SELECT idCta FROM eude.cta_empresas order by idCta desc limit 1");		
	$stmt->execute();
	$stmt->bind_result($idCta);
	$stmt->fetch();
	$newId_cta = $idCta + 1;
	# echo "<br> newId_cta: " . $newId_cta;
	$stmt->close(); 

	# Añadimos el id al nombre de prefix porque asi no nos dara nunca error.
	$prefix = substr($usuario,0,3);
	$prefixstream  = $prefix.$newId;
	# echo "<br> prefixstream: " . $prefixstream;
	
	
	# Componemos el nombre de la nueva bd
	$nombreBD = $usuario.'_'.$newId;
	$bdn = 'sgclients';
	$connapp='jdbc:mysql://'.$bdn.'.cisjdxpoil63.eu-west-1.rds.amazonaws.com:3306/'.$nombreBD;
	# echo "<br> nombreBD: " . $nombreBD;
	
	
	# Guardamos los datos en eude.empresas
	$consulta = "INSERT INTO eude.empresas(
						id,
						nombre,
						pass,
						cif,
						direccion,
						cp,
						poblacion,
						provincia,
						idPais,
						email,
						telefono1,
						telefono2,
						idCta,
						activo,
						bbdd,
						prefixstream,
						server,
						streamapp,
						domini,
						conn,
						aplicacion,
						subdomini,
						licencias,
						fechaSolicitudAlta,
						razonSocial,
						lat,
						lng,
						videoAnalisis
						)	VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	$stmt = $mysqli->prepare($consulta);	
	# $stmt = $mysqli->prepare("call empresa_insert(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");	
	$stmt->bind_param('isssssssisssiissssssisissssi', $newId,$usuario,$passEmpresa, $dni, $direccion, $cp, $poblacion, $provincia, $idPaisSpain,$email,$tlf,$tlf2, $newId_cta, $activo, $nombreBD, $prefixstream, $newServer,$streamapp, $domini, $connapp, $aplicacion,  $subdomini, $licencias,$fechaSolicitudAlta, $razonSocial, $lat, $lng , $videoAnalisis);
	$stmt->execute();
	
	
	# echo "<br> proceso:  " . $proceso ;
	$stmt->close(); 
	

	#Insertamos en la tabla cta_empresas (utilizo el mismo id para las dos tablas.)
	# $stmt = $mysqli->prepare("call ctaEmpresas_insert(?,?,?,?,?,?,?,?,?)");		
	$stmt = $mysqli->prepare("INSERT INTO  eude.cta_empresas (idCta, idEmpresa, titularCuenta, entidad, sucursal, dc, numCuenta, iban, swift) VALUE (?,?,?,?,?,?,?,?,?)");		
	$stmt->bind_param('iisiiiiss', $newId_cta, $newId, $titularCuenta, $entidad, $sucursal, $dc, $numCuenta, $iban, $swift);
	$stmt->execute();
	
	if($stmt->errno){
		$response=0;
		$proceso = 'error';
	}else{
		$response=1;
		$proceso = 'ok';
	}
	# echo "<br> proceso:  " . $proceso ;
	$stmt->close(); 
	
/*
	#Creamos la carpeta para guardar el logo	
	$serv = $_SERVER['DOCUMENT_ROOT'] . '/';
	$ruta = $serv . "backoffice/img/logos/".$subdomini;
	// echo  "<br>".$serv ;
	// echo "<br>". $ruta;
	if(!file_exists($ruta)){
//	mkdir ($ruta);
	mkdir('/opt/bitnami/apps/wordpress/htdocs/backoffice/img/logos/abcd');
	// echo 'Se ha creado el directorio: ' . $ruta;
	} else {
	// echo 'la ruta: ' . $ruta . ' ya existe ';
	}
	*/
	

	#Crear la nueva BD con la clase mysqli
	$resultado = mysqli_query($mysqli,  "CREATE DATABASE $nombreBD");
	
	if ($resultado){
		# echo "Nueva base de datos creada correctamente";
		require("crearNewDB.php");
		#  if($bbddok!=0){	$txt_createBD = 'Base de datos '. $nombreBD . 'se ha creado correctamente <br/>';	}
	}else{
		$bbddok=0;	
		$txt_createBD = mysqli_error($mysqli);
	}

	//Montamos el array 
	$data[] = array(
		"id"=>$newId, 
		"nombreEmpresa"=>$usuario, 
		"passEmpresa"=>$passEmpresa, 
		"dni"=>$dni, 
		"direccion"=>$direccion, 
		"cp"=>$cp, 
		"poblacion"=>$poblacion, 
		"provincia"=>$provincia, 
		"email"=>$email, 
		"email"=>$email, 
		"tlf"=>$tlf, 
		"tlf2"=>$tlf2, 
		"estado"=>$activo, 
		"nombreBD"=>$nombreBD, 
		"prefixstream"=>$prefixstream, 
		"ipServer"=>$newServer, 
		"streamapp"=>$streamapp, 
		"domini"=>$domini, 
		"connapp"=>$connapp, 
		"aplicacion"=>$aplicacion, 
		"subdomini"=>$subdomini, 
		"licencias"=>$licencias, 
		"fechaSolicitudAlta"=>$fechaSolicitudAlta, 
		"razonSocial"=>$razonSocial, 
		"lat"=>$lat, 
		"lng"=>$lng, 
		"idCta"=>$newId_cta, 
		"titularCuenta"=>$titularCuenta, 
		"entidad"=>$entidad, 
		"sucursal"=>$sucursal, 
		"dc"=>$dc, 
		"numCuenta"=>$numCuenta, 
		"iban"=>$iban, 
		"swift"=>$swift,		
		"response" => $response,
		"proceso" => $proceso,
		"accion"=> "insert",
		"tabla"=> "empresa",
		"nomFichero"=> "insert_cliente.php",
		"txtMail_create"=> $txtMail_create,			
		"txt_createBD"=> $txt_createBD,			
	);
	
	// $stmt->close();
	echo json_encode($data); 	
#	$mysqli->commit();
	$mysqli->close();
?> 