<?php 
/***************************************************************
To send email using SMTP, you will need to know the following:
Server Name:  email-smtp.eu-west-1.amazonaws.com
Port:   25, 465 or 587
Use Transport Layer Security (TLS): Yes
SMTP Username: AKIAIH2VQDKIORZ4P3TQ
SMTP Password: AhhiHUfaQ5sbITxBO4FhLXLadlzT2iWuxgtcIYMRqBTA
*************************************************************/

require_once("funciones.php");
/*
conectar_bbdd('eude');
require("conexion/dbi_connect.php");	
*/

$telefonoF_StreamGPS = '+ 34 93 018 52 14';
$telefonoM_StreamGPS = '+34 658 98 75 72';
$email_StreamGPS = 'info@streamgps.com';
$email_StreamGPS_2 = 'soporte@streamgps.com';
# $idEmpresa = comprobarParametros('idEmpresa'); 
$nombreEmpresa = comprobarParametros('nombreEmpresa'); 
$emailClient = comprobarParametros('email'); 
$tipoPlataforma = comprobarParametros('tipoPlataforma'); 
$subdomini = $nombreEmpresa;
$domini = 'https://'.$nombreEmpresa.'.streamgps.com';

# Replace sender@example.com with your "From" address. 
# This address must be verified with Amazon SES.
define('SENDER', $email_StreamGPS);           

# Replace recipient@example.com with a "To" address. If your account 
# is still in the sandbox, this address must be verified.

# Pruebas
# $emailClient = "marisa__Q02@hotmail.com";
# $emailClient = "marisa@streamgps.com";
# $emailClient = "emili@streamgps.com";
define('RECIPIENT',  $emailClient);    

# Replace us-west-2 with the AWS region you're using for Amazon SES.
define('REGION','eu-west-1'); 

# define('SUBJECT','Amazon SES test (AWS SDK for PHP)');
define('SUBJECT','Bienvenido a StreamGPS.com');

# Montamos el cuerpo
// Dominio pruebas
$domini = " https://".$subdomini.".streamgps.com";
$guiaRapida = "http://manual.streamgps.com/guiarapida/";
$manual = "http://manual.streamgps.com/";
$guiaApp= "http://manual.streamgps.com/appEmision";

$appBroadcaster ="https://play.google.com/store/apps/details?id=com.streamgps.position_broadcaster_button_remote&hl=es";
$appLocalizador = "https://play.google.com/store/apps/details?id=com.streamgps.localizador&hl=es";
$appRecepcion = "https://play.google.com/store/apps/details?id=com.streamgps.streamcontrol&hl=es";



// opcion 1 -> Visores y geolocalizacio
if($tipoPlataforma == 1){
	$descargaAPP = "<a href='".$appBroadcaster."' target='_blank'>Descargar StreamGPS Broadcaster: </a> App para emitir en directo en tu plataforma de StreamGPS con tan solo pulsar un botón.";
}


// opcion 2 -> Solo geolocalizacion 
if($tipoPlataforma == 2){
	$descargaAPP = "<a href='".$appLocalizador."' target='_blank'>Descargar StreamGPS Localizador: </a> App que te permite realizar un seguimiento de flotas de dispositivos 24h sin consumo de batería apreciable.  ";
}	


$cuerpo = "
<html>
<head>
 <title> Bienvenido a StreamGPS.com</title>
</head>

<h4>  Bienvenido a StreamGPS.com, </h4> 

<div id='text_01' > 
	<p> 
	Ya dispones de una plataforma StreamGPS.com. 
	<br> 
		A continuación te explicamos los pasos a seguir para acceder a tu nueva plataforma, descargarte la app y también te damos acceso a los manuales.
	</p>
</div> 
<br>		
<br>	

<div id='text_02' > 
	<p> 
	 <strong> Paso 1 - Acceder a la plataforma: </strong>
	<br> 
		Para acceder a tu nueva plataforma de StreamGPS:
		<ul>
			<li><strong>Ruta de acceso:</strong> ".$domini." </li>
			<li><strong>Usuario:</strong> ".$subdomini." </li>
			<li><strong>Contraseña:</strong> La seleccionada por ti. </li>
		</ul>
	</p>
</div> 
<br>		
<br>		

<div id='text_03' > 
	<p> 
	 <strong> Paso 2 - Descargar las apps: </strong>
	<br> 
		Tienes dos tipos de apps:
		<p>1.	".$descargaAPP." 	</p>
		
		<p>2. <a href='".$appRecepcion ."' target='_blank'>Descargar StreamControl: </a> App que te permite visualizar todos los canales en streaming que tengas en tiempo real, actúa como centro de control de la plataforma de StreamGPS:    </p>
			
		<br>		
		<p> Los datos para acceder a dichas apps son: </p>
		<ul>
			<li><strong>Empresa:</strong> ".$nombreEmpresa." </li>
			<li><strong>Usuario:</strong> emisorDemo </li>
			<li><strong>Contraseña:</strong> La seleccionada por ti. </li>
		</ul>
	</p>
</div> 
<br>		
<br>	

<div id='text_04' > 
	<p> 
	 <strong> Paso 3. Manuales de StreamGPS.com </strong>
	<br> 
		Si no entiendes alguna cosa, o quieres saber más sobre el funcionamiento de la plataforma, te dejamos los enlaces de los manuales: 
		<ul>
			<li><strong><a href='".$guiaRapida."' target='_blank'> Guía rápida: </a>:</strong> Manual para que con pocos pasos puedas empezar a utilizar la plataforma de una forma sencilla.</li>
			<li><strong><a href='".$manual."' target='_blank'> Manual completo: </a></strong> Si quieres saber con más detalle cómo funciona la plataforma este es tu manual. Aquí explicamos con detalles todos los apartados de los que está compuesta la plataforma de StreamGPS.  </li>
		</ul>
	</p>
</div> 

<div id='text_05' > 
	<p>
		Durante un mes, a partir de hoy, podrás realizar pruebas de forma gratuita. Pasado ese tiempo, si deseas seguir con nosotros deberás suscribirse como mínimo a un canal.
	</p>
	<br>
	
	<p>
	Cualquier duda que puedas tener no dudes en ponerte en contacto con nosotros a través del correo electrónico soporte@streamgps.com.
	</p>
	<br>
	
	<p> 	
		Esperamos que disfrutes de la experiencia StreamGPS.
	</p>

</div>


</body>
</html>
	";

// echo "<br>".$cuerpo;
define('BODY', $cuerpo);


# require '.././libs/aws/aws-autoloader.php';
require '../../plugins/aws/aws-autoloader.php';

use Aws\Ses\SesClient;

$client = SesClient::factory(array(
    'version'=> 'latest',     
    'region' => REGION,
     'credentials' => array(
        'key'    => 'AKIAJRJRTMRHASC6QLRA',
        'secret' => '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',
    )
));

$request = array();
$request['Source'] = SENDER;
$request['Destination']['ToAddresses'] = array(RECIPIENT);
$request['Message']['Subject']['Data'] = SUBJECT;
#$request['Message']['Body']['Text']['Data'] = BODY; 
$request['Message']['Body']['Html']['Data'] = BODY;

 // echo "<pre>"; print_r($request);echo "</pre>";
 //  echo "<pre>"; print_r(array(RECIPIENT));echo "</pre>";

try {
     $result = $client->sendEmail($request);
     $messageId = $result->get('MessageId');
     echo("Email sent! Message ID: $messageId"."\n");

} catch (Exception $e) {
     echo("The email was not sent. Error message: ");
     echo($e->getMessage()."\n" );
     
     
}

?>